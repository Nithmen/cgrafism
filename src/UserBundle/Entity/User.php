<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User Entity
 * 
 * @ORM\Entity
 * @ORM\Table(name="user__user")
 *
 */
class User extends BaseUser{   
    
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @var string
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @var string
     */
    protected $lastname;
    
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setLastname($lastname) {
        $this->lastname = $lastname;
        return $this;
    }
}
