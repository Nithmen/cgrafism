<?php

namespace ApiBundle\Controller;

use CompetitionBundle\Entity\Draft;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use CompetitionBundle\Entity\FifaPlayer;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

/**
 * FifaController class.
 */
class FifaController extends Controller
{
    /**
     * @Route("/fifa/draft/{id}", name="api_fifa_get_match_draft")
     */
    public function getPlayersByDraft($id)
    {
        /** @var Draft $draft */
        $draft = $this->getDoctrine()->getRepository(Draft::class)->find($id);
        
        $collection = $draft->getFifaPlayers();
        
        $fifaPlayers = array();
        foreach ($collection->getIterator() as $player) {
            $fifaPlayer = $player->getFifaPlayer();
            
            $fifaPlayers[]= [
                'player' => $fifaPlayer->__getData(),
                'order' => $player->getOrder(),
                'card_number' => $player->getCardNumber()
            ]; 
        }
        
        $data = [
            'id' => $draft->getId(),
            'competition_profile' => $draft->getPlayer()->getId(),
            'fifa_players' => $fifaPlayers
        ];
        
        
        return new JsonResponse(['data' => $data]);
    }
    
    /**
     * @Route("/fifa/cards", name="api_fifa_get_cards")
     */
    public function getCards(Request $request)
    {
        $page = $request->query->get('page', 1);
        $size = $request->query->get('size', 20);
        
        $allCards = $this->getDoctrine()->getRepository(FifaPlayer::class)->findBy([], ['rating' => 'DESC'], $size, ($page -1) * $size);
        
        /** @var QueryBuilder $qb */
        $qb = $this->getDoctrine()->getRepository(FifaPlayer::class)->createQueryBuilder('c');
        
        $results = $qb->select('count(c)')->getQuery()->getSingleScalarResult();
        
        $data = array('data' => []);
        foreach($allCards as $card)
        {
            $data['data'][] = $card->__getData();
        }
        
        $data['paginator'] = array(
            'page' => (int)$page,
            'size' => $size,
            'count' => (int)$results
        );
        
        return new JsonResponse($data);
    }
    
    /**
     * @method("POST")
     * @Route("/fifa/search/cards", name="api_fifa_search_cards")
     */
    public function searchCard(Request $request)
    {
        $name = $request->query->get('name', 'Messi');

        /** @var QueryBuilder $qb */
        $qb = $this->getDoctrine()->getRepository(FifaPlayer::class)->createQueryBuilder('c');
       
        $qb->select('c');
        $qb->where($qb->expr()->like('c.name', '?1'));
        $qb->setParameter(1, '%'. $name . '%');
        
        $results = $qb->getQuery()->getResult();
        
        $data = array('data' => []);
        foreach($results as $card)
        {
            $data['data'][] = $card->__getData();
        }
        
        return new JsonResponse($data);
    }
    
    /**
     * @Route("/fifa/card/{id}", name="api_fifa_find_card")
     */
    public function getCard($id)
    {
        $card = $this->getDoctrine()->getRepository(FifaPlayer::class)->find($id);
        
        $data = array('data' => []);
        
        $data['data'] = $card->__getData();

        return new JsonResponse($data);
    }
}

