<?php

namespace ApiBundle\Services;

/**
 * Description of TwitterService
 *
 * @author John
 */
class TwitterService {
    
    private $consumer_key, $consumer_secret;
    private  $api_endpoint;

    public function __construct($consumer_key, $consumer_secret, $api_endpoint)
    {
            $this->consumer_key = $consumer_key;
            $this->consumer_secret = $consumer_secret;
            $this->api_endpoint = $api_endpoint;
    }
    public function getBearerToken()
    {
            $key = "$this->consumer_key:$this->consumer_secret";
            $key = base64_encode($key);

            $path = realpath(__DIR__.'/../../../web/bundles/api/cacert.pem');

            $s = curl_init();

            curl_setopt($s,CURLOPT_URL, "https://api.twitter.com/oauth2/token");
            curl_setopt($s,CURLOPT_HTTPHEADER, array("Authorization: Basic {$key}", "Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
            curl_setopt($s,CURLOPT_POST, TRUE);
            curl_setopt($s,CURLOPT_POSTFIELDS, "grant_type=client_credentials");
            curl_setopt($s,CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($s,CURLOPT_SSL_VERIFYPEER, TRUE);
            curl_setopt($s,CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($s,CURLOPT_CAINFO, $path);

            $json = curl_exec($s);

            if(curl_exec($s) === false){
                    throw new \Exception(curl_error($s)) ;
            }

            curl_close($s);

            return json_decode($json);
    }

    public function searchTweets($hashtag)
    {
            $token = $this->getBearerToken();
            $token = $token->access_token;

            $s = curl_init();

            curl_setopt($s, CURLOPT_URL, "https://api.twitter.com/1.1/search/tweets.json?q=%23{$hashtag}&result_type=recent&count=50");
            curl_setopt($s, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$token}"));
            curl_setopt($s, CURLOPT_ENCODING, "gzip");
            curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($s, CURLOPT_USERAGENT, "Twitter");
            curl_setopt($s,CURLOPT_SSL_VERIFYPEER, FALSE);


            $json = curl_exec($s);

            if(curl_exec($s) === false){
                    throw new \Exception(curl_error($s)) ;
            }

            curl_close($s);


            $json = json_decode($json);

            $tweets = array();

            foreach($json->statuses as $tweet){
                    $tweets[] = array("text" => $tweet->text, "user" => $tweet->user->screen_name);
            }

            return $tweets;
    }

    public function getTweetById($id)
    {
        $token = $this->getBearerToken();
        $token = $token->access_token;
        
        $s = curl_init();
        
        curl_setopt($s, CURLOPT_URL, "https://api.twitter.com/1.1/statuses/show/{$id}.json?tweet_mode=extended");
        curl_setopt($s, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$token}"));
        curl_setopt($s, CURLOPT_ENCODING, "gzip");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_USERAGENT, "Twitter");
        curl_setopt($s,CURLOPT_SSL_VERIFYPEER, FALSE);
        
        
        $json = curl_exec($s);
        
        if(curl_exec($s) === false){
            throw new \Exception(curl_error($s)) ;
        }
        
        curl_close($s);
        
        
        $json = json_decode($json);

        $avatar = $json->user->profile_image_url;
        
        $avatar = str_replace('_normal', '_400x400', $avatar);
        
        return array(
            'text' => $json->full_text,
            'user' => $json->user->screen_name,
            'name' => $json->user->name,
            'avatar' => $avatar
        );
        
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
            return "TwitterConnection[consumer_key=$this->consumer_key,consumer_secret=$this->consumer_secret]";
    }
}
