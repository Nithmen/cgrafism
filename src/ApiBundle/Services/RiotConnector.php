<?php

namespace ApiBundle\Services;

use Symfony\Component\HttpFoundation\Response;

/**
 * Description of RiotConnector
 *
 * @author adria
 */
class RiotConnector 
{
    protected $token = "";
    
    /**
     * URI for regular Riot API.
     * First parameter is the region and second is the match history id.
     * 
     * @var string
     */
    private $apiUri = 'https://%s.api.riotgames.com/lol';

    /**
     *
     * @var type 
     */
    private $connection = null;
    
    public function __construct($region) 
    {
        $this->region = $region;
    }
    
    private function createConnection() 
    {
        $this->connection = new \Curl\Curl();
    }
    
    public function request($endpoint, array $params) 
    {
        if (!$this->connection) {
            $this->createConnection();
        }
        
        $apiUri = sprintf($this->apiUri, $this->region);
        $endpoint = $this->replace($endpoint, $params);
        
        $apiUri = sprintf("%s%s", $apiUri, $endpoint);
        
        $this->connection->setHeader('X-Riot-Token', $this->token);
        $this->connection->setHeader('Accept-Charset', 'application/x-www-form-urlencoded; charset=UTF-8');
        $this->connection->setHeader('Accept-Language', 'es,es-ES,en;q=0.8');
        $this->connection->setDefaultJsonDecoder(true);

        $response = $this->connection->get($apiUri);

        if(isset($response['status']) && $response['status']['status_code'] !== Response::HTTP_OK) {
                $this->throwErrors($response['status']['status_code']);
        }

        $this->connection->close();
        $this->connection = null;

        return $response;
    } 
    
    private function replace($endpoint, array $params) 
    {
        foreach($params as $key => $param) {
            $endpoint = str_replace("{{$key}}", $param, $endpoint);
        }
        
        return $endpoint;
    }
    
    /**
    * Checks the status code and throws the correct error.
    * 
    * @param integer $code
    */
   private function throwErrors($code) 
   {
       switch($code)
       {
               case Response::HTTP_NOT_FOUND:
                       throw new \Exception('Data not found');
                       break;
               case Response::HTTP_FORBIDDEN:
                       throw new \Exception('Access not allowed');
                   break;
               Case Response::HTTP_TOO_MANY_REQUESTS:
                   throw new \Exception('Too many requests.');
                   break;
       }
   }
}
