<?php
namespace ApiBundle\Services;

use Doctrine\ORM\EntityManager;
use CompetitionBundle\Entity\Team;
use CompetitionBundle\Entity\CompetitionProfile;

/**
 * 
 * @author john
 *
 */
class SuperligaService
{
    protected $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getVotesByGameAndCompetitionAndMatch($game, $competition, $match)
    {
        $s = curl_init();
        
        curl_setopt($s, CURLOPT_URL, "https://www.lvp.es/api/superliga/{$game}/{$competition}/match/{$match}/twitter-share");
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_POST, true);
        
        $json = curl_exec($s);
        
        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }

        curl_close($s);
                
        $json = json_decode($json);
        
        $totalVotes = $json->votes_a + $json->votes_b;
        
        $votesOne = round($json->votes_a / $totalVotes * 100);
        $votesTwo = round($json->votes_b / $totalVotes * 100);
        
        $teamA = $this->em->getRepository('CompetitionBundle:Team')->findOneBy(array('superligaId' => $json->team_a->id));
        $teamB = $this->em->getRepository('CompetitionBundle:Team')->findOneBy(array('superligaId' => $json->team_b->id));
        
        $teamOne = array(
            'name' => $teamA->getName(),
            'image' => $json->team_a->image_url,
            'votes' => $votesOne,
            'r' => $teamA->getR(),
            'g' => $teamA->getG(),
            'b' => $teamA->getB()
        );
        
        $teamTwo = array(
            'name' => $teamB->getName(),
            'image' => $json->team_b->image_url,
            'votes' => $votesTwo,
            'r' => $teamB->getR(),
            'g' => $teamB->getG(),
            'b' => $teamB->getB()
        );

        return array('teamOne' => $teamOne, 'teamTwo' => $teamTwo);
    }

    /**
     * @param string $uuid
     *
     * @throws \Exception
     *
     * @return array
     */
    public function getVotesByUuid($uuid)
    {
        $s = curl_init();

        curl_setopt($s, CURLOPT_URL, sprintf('https://www.lvp.es/api/superliga/twitter-share/%s', $uuid));
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_POST, true);

        $json = curl_exec($s);

        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }

        curl_close($s);

        $json = json_decode($json);

        $totalVotes = $json->votes_a + $json->votes_b;

        return [
            'votes_a' => $json->votes_a,
            'votes_b' => $json->votes_b,
            'total_votes' => $totalVotes
        ];
    }
    
    public function getLadderByGameAndCompetition($game, $competition)
    {
        $s = curl_init();
        
        curl_setopt($s, CURLOPT_URL, "https://www.lvp.es/api/superliga/{$game}/{$competition}/ladder");
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_POST, true);
        
        $json = curl_exec($s);
        
        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }

        curl_close($s);
                
        $json = json_decode($json);

        $teams = array();
        
        foreach($json as $team)
        {
            $t = $this->em->getRepository('CompetitionBundle:Team')->findOneBy(array('superligaId' => $team->id));
            $teams[] = array(
                'team' => $t,
                'rank' => $team->rank,
                'win' => $team->win,
                'defeat' => $team->loss,
                'points' => $team->points,
                'draw' => $team->draw
            );
        }

        return $teams;
    }
    
    public function updateCompetitionProfilesClash($competitionId)
    {
        $game = 'clash';
        $competition = 'temporada';
        
        $s = curl_init();
        
        curl_setopt($s, CURLOPT_URL, "https://www.lvp.es/api/superliga/{$game}/{$competition}/ladder");
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_POST, true);
        
        $json = curl_exec($s);
        
        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }
        
        curl_close($s);
        
        $json = json_decode($json);
        
        $competition = $this->em->getRepository('CompetitionBundle:Competition')->find($competitionId);
        
        
        foreach($json as $team)
        {
            $t1= $this->em->getRepository('CompetitionBundle:Team')->findOneBy(array('superligaId' => $team->id));
            /** @var CompetitionProfile $t */
            $t = $this->em->getRepository('CompetitionBundle:CompetitionProfile')->findOneBy(array('competition' => $competition, 'team' => $t1));
            
            $t->setRank($team->rank);
            $t->setWins($team->win);
            $t->setLoses($team->loss);
        }
    }
    
    /**
     * Get match data.
     * 
     * @param integer $matchId
     */
    public function getMatchInfo($matchId)
    {
        $game = 'lol';
        $competition = 'temporada';
        
        $s = curl_init();
        
        curl_setopt($s, CURLOPT_URL, "https://www.lvp.es/api/superliga/{$game}/{$competition}/match/{$matchId}");
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_POST, true);
        
        $json = curl_exec($s);
        
        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }
        
        curl_close($s);
        
        $json = json_decode($json);

        return array(
            'local' => array(
                'id' => $json->team_a->stats_id,
                'score' => $json->result_a,
                'image' => $json->team_a->image_url
            ),
            'visitor' => array(
                'id' => $json->team_b->stats_id,
                'score' => $json->result_b,
                'image' => $json->team_b->image_url
            )
        );
    }
    
    /**
     * 
     * @param integer $teamId
     * @param string $competition
     * 
     * @throws \Exception
     */
    public function getTeamInto($teamId, $competition = 'temporada')
    {
        $game = 'lol';
        
        $s = curl_init();
        
        curl_setopt($s, CURLOPT_URL, "https://www.lvp.es/api/superliga/{$game}/{$competition}/team/{$teamId}");
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_POST, true);
        
        $json = curl_exec($s);
        
        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }
        
        curl_close($s);
        
        $json = json_decode($json);

        $team = $this->em->getRepository('CompetitionBundle:Team')->findOneBy(array('superligaId' => $json->id));
        
        return array(
            'id' => $json->id,
            'name' => $json->name,
            'image' => $json->image_url,
            'colors' => [
                'r' => $team->getR(),
                'g' => $team->getG(),
                'b' => $team->getB()
            ]
        );
    }
}

