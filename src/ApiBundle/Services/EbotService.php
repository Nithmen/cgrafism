<?php

namespace ApiBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;

/**
 * Description of EbotService
 *
 * @author John
 */
class EbotService
{    
    use ContainerAwareTrait;
    
    const WEAPON_KNIFE = "knife%";
    const WEAPON_GRANADE = "hegrenade";        
    
    //Rifles
    const WEAPON_M4A1 = "m4a1";
    const WEAPON_M4A1_SILENCER = "m4a1_silencer";
    const WEAPON_AUG = "aug";
    const WEAPON_FAMAS = "famas";
    const WEAPON_GALIL = "galilar";
    const WEAPON_AK47 = "ak47";
    const WEAPON_SG = "sg556";
    
    //Snipers
    const WEAPON_AWP = "awp";
    const WEAPON_SSG = "ssg08";
    const WEAPON_G3S = "g3sg1";
    const WEAPON_SCAR = "scar20";
    
    //Pistols
    const WEAPON_GLOCK = "glock";
    const WEAPON_ELITE = "elite";
    const WEAPON_P250 = "p250";
    const WEAPON_TEC9 = "tec9";
    const WEAPON_CZ = "cz75a";
    const WEAPON_DEAGLE = "deagle";
    const WEAPON_REVOLVER = "revolver";
    const WEAPON_USP = "usp_silencer";
    const WEAPON_P2000 = "hkp2000";
    const WEAPON_FIVESEVEN = "fiveseven";
    
    public static $knife = array(
        'knife' => self::WEAPON_KNIFE
    );
    
    public static $granade = array(
        'hegrenade' => self::WEAPON_GRANADE
    );
    
    public static $weapons = array(
        'Todas' => 'TODAS',
        'snipers' => 'snipers',
        'pistolas' => 'pistolas',
        'rifles' => 'rifles',
        'knife' => self::WEAPON_KNIFE,
        'AWP' => self::WEAPON_AWP,
        'SSG08' => self::WEAPON_SSG,
        'G3SG1' => self::WEAPON_G3S,
        'SCAR20' => self::WEAPON_SCAR,
        'GLOCK' => self::WEAPON_GLOCK,
        'ELITE' => self::WEAPON_ELITE,
        'P250' => self::WEAPON_P250,
        'TEC9' => self::WEAPON_TEC9,
        'CZ75A' => self::WEAPON_CZ,
        'DEAGLE' => self::WEAPON_DEAGLE,
        'REVOLVER' => self::WEAPON_REVOLVER,
        'USP' => self::WEAPON_USP,
        'P2000' => self::WEAPON_P2000,
        'FIVESEVEN' => self::WEAPON_FIVESEVEN,
        'M4A1_SILENCER' => self::WEAPON_M4A1_SILENCER,
        'M4A1' => self::WEAPON_M4A1,
        'AUG' => self::WEAPON_AUG,
        'FAMAS' => self::WEAPON_FAMAS,
        'GALIL' => self::WEAPON_GALIL,
        'AK47' => self::WEAPON_AK47,
        'SG556' => self::WEAPON_SG,
        'HEGRENADE' => SELF::WEAPON_GRANADE
    );
    
    /**
     * Returns all the sniper weapons of the game.
     * 
     * @var array
     */
    public static $sniperMap = array(
        self::WEAPON_AWP,
        self::WEAPON_SSG,
        self::WEAPON_G3S,
        self::WEAPON_SCAR
    );

    /**
     * Returns all the rifles weapons of the game.
     * 
     * @var array
     */
    public static $rifleMap = array(
        self::WEAPON_M4A1,
        self::WEAPON_M4A1_SILENCER,
        self::WEAPON_AUG,
        self::WEAPON_FAMAS,
        self::WEAPON_GALIL,
        self::WEAPON_AK47,
        self::WEAPON_SG
    );
    
    /**
     * Returns all the pistols of the game.
     * 
     * @var array
     */
    public static $pistolMap = array(
        self::WEAPON_GLOCK,
        self::WEAPON_ELITE,
        self::WEAPON_P250,
        self::WEAPON_TEC9,
        self::WEAPON_CZ,
        self::WEAPON_DEAGLE,
        self::WEAPON_REVOLVER,
        self::WEAPON_USP,
        self::WEAPON_P2000,
        self::WEAPON_FIVESEVEN
    );
    
    /**
     * 
     * @var EntityManager
     */
    protected $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * 
     * @return Connection
     */
    protected function connect()
    {
        return $this->container->get('doctrine.dbal.ebot_connection');
    }
    
    public function getMatchById($id)
    {
        $conn = $this->connect();

        $builder = $conn->createQueryBuilder();
        
        $stm = $builder
                    ->select('*')
                    ->from('matchs')
                    ->where("id = {$id}")
                    ->getSQL();
        
        $results = $conn->fetchAssoc($stm);
                
        $match = array(
            'teamA' => $results['team_a_name'],
            'teamB' => $results['team_b_name'],
            'scoreA' => $results['score_a'],
            'scoreB' => $results['score_b']
        );
        
        return array(
            'match' => $match,
            'adr' => $this->getAdrByMatch($id),
            'rounds' => $this->getRondasById($id)
        );
    }
    
    public function getRondasById($id)
    {
        $conn = $this->connect();
        
        $builder = $conn->createQueryBuilder();
        
        $stm = $builder->select("*")->from('round_summary')->where("match_id = {$id}")->orderBy('round_id', 'ASC')->getSQL();
        
        $results = $conn->fetchAll($stm);
        
        $rounds = array();
        
        foreach($results as $key => $round)
        {
            $rounds[$key+1] = array(
                'matchId' => $round['match_id'],
                'roundId' => $round['round_id'],
                'winType' => $round['win_type'],
                'teamWin' => $round['team_win'],
                'ctWin' => $round['ct_win'],
                'tWin' => $round['t_win']
            );
        }
                
        return $rounds;
    }
    
    
    public function getAdrByMatch($id)
    {
        $conn = $this->connect();
        
        $builder = $conn->createQueryBuilder();
        
        $stm = $builder->select("player_id, SUM(damage)/MAX(round_id) as ADR")->from('players_damage')->where("match_id = {$id}")->groupBy("player_id")->orderBy('ADR', 'DESC')->getSQL();
        
        $results = $conn->fetchAll($stm);
        
        $match = $this->em->getRepository('CompetitionBundle:Match')->findOneBy(array('ebotId' => $id));

        $players['teamA'] = array();
        $players['teamB'] = array();
        
        foreach($results as $player)
        {
            $id = $this->em->getRepository('CompetitionBundle:Player')->findOneBy(array('steamId' => $player['player_id']));
                                    
            if($match->getTeamOne()->getTeam() == $id->getTeam())
            {
                $players['teamA'][] = array(
                    'player' => $id,
                    'adr' => $player['ADR']
                );
            }
            else
            {
                $players['teamB'][] = array(
                    'player' => $id,
                    'adr' => $player['ADR']
                );
            }
            
        }
                
        return array(
            'teamA' => $players['teamA'], 
            'teamB' => $players['teamB'],
            'maxAdr' => $results[0]['ADR']
        );
    }
    
    /**
     * TO DO
     * @param number $id
     * @param number $playerId
     */
    public function getFavouriteWeaponByMatchAndPlayer($id, $playerId)
    {
        $conn = $this->connect();
        
        $builder = $conn->createQueryBuilder();
        
        $stm = $builder->select('COUNT(*) as nb, weapon')->from('player_kill')->where("match_id = {$id}")->andWhere("player_id = {$playerId}")->groupBy('weapon')->orderBy('nb', 'DESC')->getSQL();
        
        $results = $conn->fetchAll($stm);
        
        return $results;
    }
    
    /**
     * 
     * @param String $player    Player's Steamid
     * @param array $weapons    Array of weapons
     */
    public function getPlayerKillsByWeapon($player, array $weapons, array $matches = null, $season = 2)
    {
        $conn = $this->connect();
        
        $builder = $conn->createQueryBuilder();
        
        $guns = "";
        
        foreach($weapons as $weap) {
            $guns .= "'" . $weap . "',";
        }
        
        $guns = substr($guns, 0, -1);
        
        $stm = $builder->select("count(*) as kills FROM player_kill pk LEFT JOIN players p ON p.id = pk.killer_id LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE weapon in ({$guns}) AND steamid = '{$player}' AND season_id={$season}");
        
        if($matches) {
            
            $m = "";
            
            foreach($matches as $match) {
               $m .= "'" . $match . "',";
            }
            
            $m = substr($m, 0, -1);
            
            $stm = $builder->select("count(*) as kills FROM player_kill pk LEFT JOIN players p ON p.id = pk.killer_id LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE weapon in ({$guns}) AND steamid = '{$player}' AND season_id={$season} AND pk.match_id IN ({$m})");
        }
        
        $stm = $stm->getSQL();
        
        $results = $conn->fetchAssoc($stm);
        
        return $results['kills'];
    }
    
    /**
     * 
     * @param string $player
     * @param array $matches
     * @param integer $season
     * 
     * @return integer
     */
    public function getPlayerKills($player, array $matches = null, $season = 2)
    {
        $conn = $this->connect();
        
        $builder = $conn->createQueryBuilder();
        
        $stm = $builder->select("count(*) as kills FROM player_kill pk LEFT JOIN players p ON p.id = pk.killer_id LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE steamid = '{$player}' AND season_id={$season}");
        
        if($matches) {
            
            $m = "";
            
            foreach($matches as $match) {
               $m .= "'" . $match . "',";
            }
            
            $m = substr($m, 0, -1);
            
            $stm = $builder->select("count(*) as kills FROM player_kill pk LEFT JOIN players p ON p.id = pk.killer_id LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE steamid = '{$player}' AND season_id={$season} AND pk.match_id IN ({$m})");
        }
        
        $stm = $stm->getSQL();
        
        $results = $conn->fetchAssoc($stm);
        
        return $results['kills'];
    }
    
    /**
     * 
     * @param integer $player
     * @param integer $matches
     * @param integer $season
     * 
     * return array
     */
    public function getPlayerADR($player, $matches = null, $season = 2)
    {
        $conn = $this->connect();
        
        $builder = $conn->createQueryBuilder();

        $stm = $builder->select("sum(damage) as damage FROM players_damage pk LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE player_id= '{$player}' AND season_id={$season}");

        if($matches) {
             $m = "";
            
            foreach($matches as $match) {
               $m .= "'" . $match . "',";
            }
            
            $m = substr($m, 0, -1);
            
            $stm = $builder->select("sum(damage) as damage FROM players_damage pk LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE player_id= '{$player}' AND match_id IN ({$m})  AND season_id={$season}");
            
        }
        
        $stm = $stm->getSQL();
        
        $damage = $conn->fetchAssoc($stm);
        
        $builder = $conn->createQueryBuilder();
        
        $stm = $builder->select("SUM( score_a + score_b ) AS rounds FROM players pk LEFT JOIN matchs mt ON mt.id = pk.match_id WHERE steamid = '{$player}' AND season_id ={$season}");
        
         if($matches) {
             $stm = $builder->select("SUM( score_a + score_b ) AS rounds FROM players pk LEFT JOIN matchs mt ON mt.id = pk.match_id WHERE steamid = '{$player}' AND season_id ={$season} AND match_id IN ({$m})");
         }
        
         $stm = $stm->getSQL();
         
        $rounds = $conn->fetchAssoc($stm);
        
        $adr = number_format($damage['damage'] / $rounds['rounds'],2 );
        
        return $adr;
    }
    
    public function getKDAByPlayer($player, array $matches = null, $season = 2)
    {
        $conn = $this->connect();
        
        $builder = $conn->createQueryBuilder();
        
        $stm = $builder->select("sum(nb_kill) as kills, sum(assist) as assists , sum(death) as deaths FROM players pk LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE steamid= '{$player}' AND season_id={$season}");
        
        if($matches) {
            
            $m = "";
            
            foreach($matches as $match) {
               $m .= "'" . $match . "',";
            }
            
            $m = substr($m, 0, -1);
            
            $stm = $builder->select("sum(nb_kill) as kills, sum(assist) as assists , sum(death) as deaths FROM players pk LEFT JOIN matchs mt ON mt.id=pk.match_id WHERE match_id IN ({$m}) AND steamid= '{$player}' AND season_id={$season}");
        }
        
        $stm = $stm->getSQL();
        
        $results = $conn->fetchAssoc($stm);
        
        if($results['deaths'] == 0) {
            $kda = $results['kills'] + $results['assists'];
        }
        else {
            $kda = number_format(($results['kills'] + $results['assists']) / $results['deaths'], 2);
        }
        
        return $kda;
    }
    
    public function getPlayerData($player, array $matches = null, array $weapons = null, $season = 2)
    {
        $data = array();
        
        $data['totalKills'] = $this->getPlayerKills($player, $matches, $season);
        $data['adr'] = $this->getPlayerADR($player, $matches, $season);
        $data['kda'] = $this->getKDAByPlayer($player, $matches, $season);
        
        if($weapons) {
            $data['killsWithWeapon'] = $this->getPlayerKillsByWeapon($player, $weapons, $matches, $season);
            $data['percent'] = number_format($data['killsWithWeapon'] / $data['totalKills'] * 100, 2);
        }
        
        return $data;
    }
}
