<?php

namespace ApiBundle\Services;

use ApiBundle\Services\RiotService;

/**
 * 
 */
class RiotService 
{
    
    private $connector;
    
    public function __construct($region = 'euw1') 
    {
        $this->connector = new RiotConnector($region);
    }
    
    private function getSummonerId($summonerName)
    {
        $response = $this->connector->request('/summoner/v3/summoners/by-name/{summonerName}', array(
            'summonerName' => $this->normalizeSummonername($summonerName)
        ));
        
        return $response['id'];
    }
    
    /**
     * Get champion image.
     * 
     * @param type $championId
     * @return type
     */
    private function getChampionImage(array $champions)
    {                
        $file = file_get_contents(__DIR__.'./../Resources/public/champions.json');
        
        $json = json_decode($file, true);

        $data = array();
        
        foreach($champions as $champion)
        {
            $data[$champion] = $json['data'][$champion]['image']['full'];
        }
        
        return $data;
    }
    
    private function normalizeSummonername($summonerName)
    {
        return str_replace(' ', '', strtolower($summonerName));
    }
    
    /**
     * Get picks and bans of a ingame summoner.
     * 
     * @param mixed | $summonerName
     * 
     * @return array
     */
    public function getSpectatorInfo($summonerName)
    {
        $summonerId = $this->getSummonerId($summonerName);
        
        $response = $this->connector->request("/spectator/v3/active-games/by-summoner/{summonerId}", array(
            'summonerId' => $summonerId
        ));
        
        if(!$response)
        {
            throw new \Exception ("No se ha podido completar la petición.");
        }
        
        $data = array();
        foreach($response['participants'] as $participant)
        {
            $side = $participant['teamId'];
            
            $data[$side]['picks'][] = $participant['championId'];
        }
        
        $champions = array();
        
        foreach($response['bannedChampions'] as $champion)
        {
            $side = $champion['teamId'];
            $data[$side]['bans'][] = array(
                'championId' => $champion['championId'],
                'pickTurn' => $champion['pickTurn']
            );
            
            $champions[$champion['championId']] = $champion['championId'];
        }
        
        $static = $this->getChampionImage($champions);
       
        $data['images'] = $static;
        
        
        return $data;
    }
    
    
}
