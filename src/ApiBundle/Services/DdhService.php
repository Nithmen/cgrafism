<?php

namespace ApiBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;

/**
 * Description of EbotService
 *
 * @author John
 */
class DdhService
{

    use ContainerAwareTrait;

    /**
     * 
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * 
     * @return Connection
     */
    protected function connect()
    {
        return $this->container->get('doctrine.dbal.ddh_connection');
    }

    public function getMatches()
    {
        $conn = $this->connect();

        $builder = $conn->createQueryBuilder();

        $stmt = $builder->select('id')
                ->from('stats__lol_matches')
                ->where('id LIKE \'%T2018V-%\' ')
                ->orderBy('id', 'DESC')
                ->getSQL();

        $results = $conn->fetchAll($stmt);

        $matches = array();
        foreach ($results as $match)
        {
            $matches[] = $match['id'];
        }

        return $matches;
    }

    public function getMatchStats($matchId, $superligaId)
    {
        $conn = $this->connect();

        $sql = "SELECT t.dim as team, t.id as team_id, p.name as player, s.champion_id as champion, s.kills as kills, s.deaths as deaths, s.assists as assists, s.minions as creeps, s.total_gold as gold,total_damage_dealt_champions as damage FROM stats__lol_matches m, stats__lol_players p, stats__lol_player_stats s, stats__lol_teams t where  m.id = '{$matchId}' and m.id = s.match_id and s.player_id = p.id and t.id = s.team_id";

        $results = $conn->fetchAll($sql);

        $stats = array();
        foreach ($results as $key => $player)
        {
            if ($key < 5) {
                $index = 'blue';
            } else {
                $index = 'red';
            }

            $player['champion'] = $this->getChampionImage($player['champion']);
            $stats[$index][] = $player;
        }

        $match = $this->container->get('api.superliga')->getMatchInfo($superligaId);

        $data = array_merge($stats, $match);

        $sql = "SELECT team_id as team, champion_id as champion FROM stats__lol_bans where match_id LIKE '{$matchId}'";
        $results = $conn->fetchAll($sql);

        $bans = array();
        foreach ($results as $champ)
        {
            $champ['champion'] = $this->getChampionImage($champ['champion']);
            $bans['bans'][] = $champ;
        }

        $data = array_merge($data, $bans);

        $sql = "SELECT match_time as time FROM stats__lol_matches where id = '{$matchId}'";
        $time = $conn->fetchAssoc($sql);

        $n = $time['time'];
        $whole = floor($n);      // 1
        $fraction = $n - $whole; // .25

        $minutes = (int) $time['time'] % 60;
        $seconds = (int) round($fraction * 60);

        $time = $minutes . ':' . $seconds;

        $data = array_merge($data, array('time' => $time));

        $sql = "SELECT team_id as team, winner, drakes, nashors, towers, inhibitors, gold_match as gold FROM `stats__lol_results` WHERE match_id = '{$matchId}'";
        $results = $conn->fetchAll($sql);

        $base = array();
        foreach ($results as $r)
        {
            $base['base'][$r['team']] = $r;
        }

        $sql = "SELECT team_id as team, sum(kills) as kills, sum(deaths) as deaths, sum(assists) as assists FROM `stats__lol_player_stats` WHERE match_id = '{$matchId}' group by team_id";
        $results = $conn->fetchAll($sql);

        $kda = array();
        foreach ($results as $r)
        {
            $base['base'][$r['team']] = array_merge($base['base'][$r['team']], $r);
        }

        $data = array_merge($data, $base);

        $sql = "SELECT champion_id as champion, team_id as team FROM `stats__lol_player_stats` WHERE match_id = '{$matchId}'  and first_blood_kill = 1";
        $result = $conn->fetchAssoc($sql);

        $firstBlood = $this->getChampionImage($result['champion']);

        $data = array_merge($data, array('firstBlood' => array('champion' => $firstBlood, 'team' => $result['team'])));

        $sql = "SELECT champion_id as champion, team_id as team FROM `stats__lol_player_stats` WHERE match_id = '{$matchId}'  and first_blood_kill = 1";
        $result = $conn->fetchAssoc($sql);

        $firstTower = $this->getChampionImage($result['champion']);

        $data = array_merge($data, array('firstTower' => array('champion' => $firstTower, 'team' => $result['team'])));

        $sql = "SELECT champion_id as champion, team_id as team, largest_killing_spree as spree FROM `stats__lol_player_stats` WHERE match_id = '{$matchId}' and largest_killing_spree = (SELECT max(largest_killing_spree) FROM `stats__lol_player_stats` WHERE match_id = '{$matchId}' )";
        $result = $conn->fetchAssoc($sql);

        $killingSpree = $this->getChampionImage($result['champion']);

        $data = array_merge($data, array('killingSpree' => array('champion' => $killingSpree, 'team' => $result['team'], 'spree' => $result['spree'])));

        return $data;
    }

    /**
     * Get champion image.
     * 
     * @param integer $championId
     * @return string
     */
    private function getChampionImage($championId)
    {
        $file = file_get_contents(__DIR__ . './../Resources/public/champions.json');

        $json = json_decode($file, true);

        return $json['data'][$championId]['image']['full'];
    }

    /**
     * Return all the players who played in a competition
     * 
     * @param String $competition
     * 
     * @return array
     */
    public function getPlayers($competition = 'T2018V-L')
    {
        $conn = $this->connect();

        $sql = "SELECT p.id, p.name FROM `stats__lol_player_stats` s, stats__lol_players p WHERE s.match_id LIKE '%{$competition}%' and p.id = s.player_id group by s.player_id order by p.name";

        $results = $conn->fetchAll($sql);

        $players = array();
        foreach ($results as $player)
        {
            $id = $player['id'];
            $players[$id] = $player['name'];
        }

        return $players;
    }

    /**
     * Return all the champions which has been played in a competition
     * 
     * @param string $competition
     * 
     * @return array
     */
    public function getChampions($competition = 'T2018V-L')
    {
        $conn = $this->connect();

        $sql = "SELECT id, name FROM stats__lol_champions order by name";

        $results = $conn->fetchAll($sql);

        $champions = array();
        foreach ($results as $champ)
        {
            $id = $champ['id'];
            $champions[$id] = $champ['name'];
        }

        return $champions;
    }
    
    /**
     * 
     * @param string $competition
     */
    public function getGamesPlayedByChampOncompetition($championId, $competition = 'T2018V-L')
    {
        $conn = $this->connect();
        
        $sql = "SELECT count(match_id) as games FROM `stats__lol_player_stats` WHERE match_id LIKE '%{$competition}%' and champion_id = {$championId}";
        
        $results = $conn->fetchAssoc($sql);
           
        return $results['games'];
    }
    
    /**
     * 
     * @param string $competition
     */
    public function getGamesPlayedByChampOncompetitionWin($championId, $competition = 'T2018V-L')
    {
        $conn = $this->connect();
        
        $sql = "SELECT count(r.match_id) as games FROM `stats__lol_results` r JOIN stats__lol_player_stats s ON r.match_id = s.match_id and r.team_id = s.team_id WHERE r.match_id LIKE '%{$competition}%' and s.champion_id = {$championId} and r.winner = 1";
        
        $results = $conn->fetchAssoc($sql);
           
        return $results['games'];
    }
    
    /**
     * 
     * @param integer $championId
     * @param string $competition
     */
    public function getWinratioByChampOnCompetition($championId, $competition = 'T2018V-L')
    {
        $games = $this->getGamesPlayedByChampOncompetitionWin($championId, "T2018V-L");

        $totalGamesPlayed = $this->getGamesPlayedByChampOncompetition($championId, $competition);

        if($totalGamesPlayed == 0)
        {
            $total = "0.0";
        }
        else {
            $total = round($games / $totalGamesPlayed * 100, 1);
        }

        return $total;

    }
    
    /**
     * 
     * @param string $competition
     * 
     * @return int
     */
    public function countMatchesOnCompetition($competition = 'T2018V-L')
    {
        $conn = $this->connect();
        
        $sql = "SELECT count(*) as games FROM stats__lol_matches where id LIKE '%{$competition}%'";
        
        $results = $conn->fetchAssoc($sql);
        
        $games = $results['games'];
        
        return $games;
    }
    
    /**
     * 
     * @param int $championId
     * @param string $competition
     */
    public function getBanratioByChampionOnCompetition($championId, $competition = 'T2018V-L')
    {
        $conn = $this->connect();
        
        $sql = "SELECT count(*) as bans FROM `stats__lol_bans` WHERE match_id LIKE '%{$competition}%' and champion_id = {$championId}";
        
        $results = $conn->fetchAssoc($sql);
        
        $bans = $results['bans'];
        
        $games = $this->countMatchesOnCompetition($competition);
        
        $banratio = round($bans / $games * 100, 1);
        
        if($banratio == 0)
        {
            $banratio = "00.0";
        }
        
        return $banratio;
    }

    /**
     * Get KDA of a player with a champion on a competition.
     * 
     * @param integer $championId
     * @param string $competition
     * @param number $playerId
     * 
     * return string
     */
    public function getKDAByPlayerOnCompetition($championId = 51, $competition = 'T2018V-L', $playerId = 30)
    {
        $conn = $this->connect();
        
        $sql = "SELECT sum(kills) as kills, sum(deaths) as deaths, sum(assists) as assists FROM stats__lol_player_stats WHERE match_id LIKE '%{$competition}%' and champion_id = {$championId} and player_id = {$playerId}";        
        
        $results = $conn->fetchAssoc($sql);
        
        $kills = $results['kills'];
        $deaths = $results['deaths'];
        $assists = $results['assists'];
        
        if($deaths == 0) {
            $kda = number_format($kills + $assists, 2);
        }
        else {
            $kda = number_format(($kills + $assists) / $deaths, 2);
        }
        
        if($kills == 0 && $deaths == 0 && $assists == 0) {
            $kda = '0.00';
        }
        
        return $kda;
    }
    
    /**
     * Get total data from a player on a given competition / match.
     * 
     * @param integer $playerId
     * @param string $matchId
     * @return array
     */
    public function getStatsByPlayer($playerId, $matchId = 'T2018V-L')
    {
        $conn = $this->connect();
        
        $sql = "SELECT sum(s.kills) as kills, sum(s.deaths) as deaths, sum(s.assists) as assists, (sum(s.total_gold) / sum(m.match_time)) as gpm, (sum(s.total_damage_dealt_champions) / sum(m.match_time))  as  dmg_min from stats__lol_player_stats s join stats__lol_matches m on m.id = s.match_id where s.match_id LIKE '%{$matchId}%' and s.player_id = {$playerId}";
        
        $results = $conn->fetchAssoc($sql);
        
        $kills = $results['kills'];
        $deaths = $results['deaths'];
        $assists = $results['assists'];
        
        if($deaths == 0) {
            $kda = number_format($kills + $assists, 2);
        }
        else {
            $kda = number_format(($kills + $assists) / $deaths, 2);
        }
        
        if($kills == 0 && $deaths == 0 && $assists == 0) {
            $kda = '0.00';
        }
        
        $data = [
            [
                'key' => 'KDA',
                'value' => $kda 
            ],
            [
                'key' => 'GPM',
                'value' =>  $results['gpm']
            ],
            [
                'key' => 'DMG/MIN',
                'value' => $results['dmg_min']
            ]
        ];
        
        return $data;
    }
    
    /**
     * Get total data from a player on a given competition / match by a given champion.
     *
     * @param integer $playerId
     * @param string $matchId
     * @param integer $championId
     * 
     * @return array
     */
    public function getStatsByPlayerAndChampion($playerId, $matchId = 'T2018V-L', $championId)
    {
        $conn = $this->connect();
        
        $sql = "SELECT sum(s.kills) as kills, sum(s.deaths) as deaths, sum(s.assists) as assists, (sum(s.total_gold) / sum(m.match_time)) as gpm, (sum(s.total_damage_dealt_champions) / sum(m.match_time))  as  dmg_min from stats__lol_player_stats s join stats__lol_matches m on m.id = s.match_id where s.match_id LIKE '%{$matchId}%' and s.player_id = {$playerId} and s.champion_id = {$championId}";
        
        $results = $conn->fetchAssoc($sql);
        
        $kills = $results['kills'];
        $deaths = $results['deaths'];
        $assists = $results['assists'];
        
        if($deaths == 0) {
            $kda = number_format($kills + $assists, 2);
        }
        else {
            $kda = number_format(($kills + $assists) / $deaths, 2);
        }
        
        if($kills == 0 && $deaths == 0 && $assists == 0) {
            $kda = '0.00';
        }
        
        $data = [
            [
                'key' => 'KDA',
                'value' => $kda
            ],
            [
                'key' => 'GPM',
                'value' =>  $results['gpm']
            ],
            [
                'key' => 'DMG/MIN',
                'value' => $results['dmg_min']
            ]
        ];
        
        return $data;
    }
    
    /**
     * Get total data from a player on a given competition / match.
    *
    * @param integer $playerId
    * @param string $matchId
    * @return array
    */
    public function getPlainStatsByPlayer($playerId, $matchId = 'T2018V-L')
    {
        $conn = $this->connect();
        
        $sql = "SELECT sum(s.kills) as kills, sum(s.deaths) as deaths, sum(s.assists) as assists, (sum(s.total_gold) / sum(m.match_time)) as gpm, (sum(s.total_damage_dealt_champions) / sum(m.match_time))  as  dmg_min, s.champion_id as champion from stats__lol_player_stats s join stats__lol_matches m on m.id = s.match_id where s.match_id LIKE '%{$matchId}%' and s.player_id = {$playerId}";
        
        $results = $conn->fetchAssoc($sql);
        
        $kills = $results['kills'];
        $deaths = $results['deaths'];
        $assists = $results['assists'];
        
        if($deaths == 0) {
            $kda = number_format($kills + $assists, 2);
        }
        else {
            $kda = number_format(($kills + $assists) / $deaths, 2);
        }
        
        if($kills == 0 && $deaths == 0 && $assists == 0) {
            $kda = '0.00';
        }
        
        $data = [
            'kda' => $kda,
            'kills' => $kills,
            'deaths' => $deaths,
            'assists' => $assists,
            'champion' => $results['champion']
        ];
        
        return $data;
    }
}
