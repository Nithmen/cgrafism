<?php
namespace ApiBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use CompetitionBundle\Entity\FifaPlayer;

class FUTService
{
    use ContainerAwareTrait;
    
    /**
     *
     * @var EntityManager
     */
    protected $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getPlayers($url, $pages)
    {
        for($i = 1; $i <= $pages; $i++ )
        {
            $json = file_get_contents(str_replace("22:1",sprintf('22:%s', $i), $url));
        
            $json = json_decode($json, true);
            
            foreach($json['items'] as $item)
            {
                /** @var FifaPlayer $fifaPlayer */
                $fifaPlayer = new FifaPlayer();
                $fifaPlayer->setName($item['name']);
                $fifaPlayer->setFirstName($item['firstName']);
                $fifaPlayer->setLastName($item['lastName']);
                $fifaPlayer->setLeague($item['league']['name']);
                $fifaPlayer->setNationality($item['nation']['name']);
                $fifaPlayer->setFlag($item['nation']['imageUrls']['large']);
                $fifaPlayer->setClub($item['club']['name']);
                $fifaPlayer->setClubImage($item['club']['imageUrls']['normal']['large']);
                
                if($item['specialImages']['largeTOTWImgUrl'] != null) {
                    $image = $item['specialImages']['largeTOTWImgUrl'];
                }
                else {
                    $image = $item['headshot']['largeImgUrl'];
                }
                
                $fifaPlayer->setImage($image);
                $fifaPlayer->setQuality($item['quality']);
                $fifaPlayer->setColor($item['color']);
                $fifaPlayer->setIsGk($item['isGK']);
                $fifaPlayer->setPosition($item['positionFull']);
                $fifaPlayer->setSpecialType($item['isSpecialType']);
                $fifaPlayer->setStats($item['attributes']);
                $fifaPlayer->setRating($item['rating']);
                
                $this->em->persist($fifaPlayer);
            }
        }
        
        $this->em->flush();
    }
    
    /**
     * Get player data based on his ID.
     * 
     * @param integer $playerId
     */
    public function getPlayer($playerId)
    {
        $json = file_get_contents(sprintf('https://www.easports.com/uk/fifa/ultimate-team/api/fut/item?id=%s', $playerId));
        
        $json = json_decode($json, true);
        
        foreach($json['items'] as $item)
        {
            /** @var FifaPlayer $fifaPlayer */
            $fifaPlayer = new FifaPlayer();
            $fifaPlayer->setName($item['name']);
            $fifaPlayer->setFirstName($item['firstName']);
            $fifaPlayer->setLastName($item['lastName']);
            $fifaPlayer->setLeague($item['league']['name']);
            $fifaPlayer->setNationality($item['nation']['name']);
            $fifaPlayer->setFlag($item['nation']['imageUrls']['large']);
            $fifaPlayer->setClub($item['club']['name']);
            $fifaPlayer->setClubImage($item['club']['imageUrls']['normal']['large']);
            
            if($item['specialImages']['largeTOTWImgUrl'] != null) {
                $image = $item['specialImages']['largeTOTWImgUrl'];
            }
            else {
                $image = $item['headshot']['largeImgUrl'];
            }
            
            $fifaPlayer->setImage($image);
            $fifaPlayer->setQuality($item['quality']);
            $fifaPlayer->setColor($item['color']);
            $fifaPlayer->setIsGk($item['isGK']);
            $fifaPlayer->setPosition($item['positionFull']);
            $fifaPlayer->setSpecialType($item['isSpecialType']);
            $fifaPlayer->setStats($item['attributes']);
            $fifaPlayer->setRating($item['rating']);

            $this->em->persist($fifaPlayer);
        } 
        
        $this->em->flush();
    }
}

