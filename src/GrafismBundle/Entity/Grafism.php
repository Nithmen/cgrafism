<?php

namespace GrafismBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class GrafismCompetition
 *
 * @ORM\Entity()
 * @ORM\Table(name="app__grafism")
 */
class Grafism
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="`key`")
     */
    protected $key;

    /**
     * @var GrafismCompetition
     *
     * @ORM\ManyToOne(targetEntity="GrafismCompetition", inversedBy="grafisms")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id", nullable=true)
     */
    protected $competition;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $url;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $formPath;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return GrafismCompetition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param GrafismCompetition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

    /**
     * @return string
     */
    public function getFormPath()
    {
        return $this->formPath;
    }

    /**
     * @param string $formPath
     */
    public function setFormPath($formPath)
    {
        $this->formPath = $formPath;
    }
}