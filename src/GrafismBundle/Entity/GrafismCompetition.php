<?php

namespace GrafismBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class GrafismCompetition
 *
 * @ORM\Entity()
 * @ORM\Table(name="grafism__competition")
 */
class GrafismCompetition
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;


    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Grafism", mappedBy="competition", fetch="EAGER", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $grafisms;

    /**
     * GrafismCompetition constructor.
     */
    public function __construct()
    {
        $this->grafisms = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getGrafisms()
    {
        return $this->grafisms;
    }

    /**
     * @param ArrayCollection $grafisms
     */
    public function setGrafisms($grafisms)
    {
        $this->grafisms = $grafisms;
    }
}