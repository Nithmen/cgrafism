<?php

namespace GrafismBundle\Utils;

/**
 * Class GroupUtils
 */
class GroupUtils
{
    /**
     * Gets the letter for given number.
     *
     * @param int $n
     *
     * @return string
     */
    public static function getLetter($n)
    {
        return chr(64 + $n);
    }

    /**
     * Gets the number for given letter.
     *
     * @param char $letter
     *
     * @return number
     */
    public static function getNumber($letter)
    {
        return ord(strtolower($letter)) - 96;
    }
}