<?php
namespace GrafismBundle\Service;

class LolStatsAPI extends AbstractStatsAPI
{
    /*
     * ENDPOINTS
     */

    public function getAllCompetitionMatches($competitionId, $sort = 'round')
    {
        $url = sprintf('%s/%s/%s/%s?sort=%s', $this->getBaseUrl(), 'competitions', $competitionId, 'matches', $sort);
        $response = $this->doRequest($url);

        return $response['data'];
    }

    public function getAllMatchGames($matchId)
    {
        $url = sprintf('%s/%s/%s/%s?cache=false', $this->getBaseUrl(), 'matches', $matchId, 'games');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllCompetitionPlayers($competitionId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'players');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getMatch($matchId)
    {
        $url = sprintf('%s/%s/%s?cache=false', $this->getBaseUrl(), 'matches', $matchId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getGame($gameId)
    {
        $url = sprintf('%s/%s/%s?cache=false', $this->getBaseUrl(), 'games', $gameId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getTeam($teamId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'teams', $teamId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getCompetition($competitionId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId);
        $response = $this->doRequest($url);
        return $response['data'];
    }
    
    public function getCompetitionLadder($competitionId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'ladders');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getCompetitionLadderByGroup($competitionId, $group)
    {
        $url = sprintf('%s/%s/%s/%s?filter[group]=%s', $this->getBaseUrl(), 'competitions', $competitionId, 'ladders', $group);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /*
     * UTIL
     */

    public function listAllCompetitionMatches($competitionId, $sort = 'round')
    {
        $data = $this->getAllCompetitionMatches($competitionId, $sort);
        $list = ['' => ''];
        foreach ($data as $item) {
            $attr = $item['attributes'];
            $text = sprintf('%s - %s vs %s', (isset($attr['label']) ?: '#'), $attr['team_local']['name'], $attr['team_visitor']['name']);
            $list[$item['id']] = $text;
        }
        return $list;
    }

    public function listAllMatchGames($matchId)
    {
        $data = $this->getAllMatchGames($matchId);
        $list = ['' => ''];
        foreach ($data as $item) {
            $attr = $item['attributes'];
            $text = sprintf('GAME %s', $attr['order']);
            $list[$item['id']] = $text;
        }
        return $list;
    }

    public function listAllMatchTeams($matchId)
    {
        $match = $this->getMatch($matchId);
        $local = $match['attributes']['team_local'];
        $visitor = $match['attributes']['team_visitor'];
        $list = ['' => ''];
        $list[$local['id']] = $local['name'];
        $list[$visitor['id']] = $visitor['name'];
        return $list;
    }

    public function listAllTeamPlayers($competitionId, $teamId)
    {
        $players = $this->getAllCompetitionPlayers($competitionId);
        $list = ['' => ''];
        foreach ($players as $player) {
            $attr = $player['attributes'];
            if ($attr['team']['id'] == $teamId) {
                $list[$player['id']] = $attr['nickname'];
            }
        }
        return $list;
    }
}
