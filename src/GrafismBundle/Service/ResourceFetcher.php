<?php

namespace GrafismBundle\Service;

/**
 * Class ResourceFetcher
 */
class ResourceFetcher
{
    /**
     * @var string
     */
    private $endpoint;

    /**
     * ResourceFetcher constructor.
     *
     * @param string $endpoint
     */
    public function __construct($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @param string $context
     * @param string $resource
     * @param string $resourceId
     *
     * @return string
     */
    public function getResourceUrl($context, $resource, $resourceId = null)
    {
        $url = sprintf('%s/%s/v1/%s', $this->endpoint, $context, $resource);

        if ($resourceId) {
            $url = sprintf('%s/%s', $url, $resourceId);
        }

        return $url;
    }
}