<?php
namespace GrafismBundle\Service;

abstract class AbstractStatsAPI
{

    protected $endpoint;
    protected $context;

    public function __construct($endpoint, $context)
    {
        $this->endpoint = $endpoint;
        $this->context = $context;
    }

    protected function doRequest($url)
    {
        $s = curl_init();

        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);

        $json = curl_exec($s);

        if ($json === false) {
            throw new \Exception(curl_error($s));
        }

        curl_close($s);

        $json = json_decode($json, true);

        return $json;
    }
    
    protected function getBaseUrl()
    {
        return sprintf('%s/%s/v1', $this->endpoint, $this->context);
    }
}
