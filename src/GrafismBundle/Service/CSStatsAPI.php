<?php

namespace GrafismBundle\Service;

/**
 * Class CSStatsAPI
 */
class CSStatsAPI extends AbstractStatsAPI
{
    /*
     * ENDPOINTS
     */

    /**
     * Get Competition data.
     *
     * @param $competitionId
     * @return mixed
     * @throws \Exception
     */
    public function getCompetition($competitionId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * Get all competition matches.
     *
     * @param $competitionId
     * @param string $sort
     * @return mixed
     * @throws \Exception
     */
    public function getAllCompetitionMatches($competitionId, $sort = 'round')
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'matches');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * Get players b game.
     *
     * @param $gameId
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getPlayersByGame($gameId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'games', $gameId, 'players');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllMatchGames($matchId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'matches', $matchId, 'games');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * @param $competitionId
     * @param $playerId
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getPlayerInACompetition($competitionId, $playerId)
    {
        $url = sprintf('%s/%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'players', $playerId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * Get all competition matches filtered by round.
     *
     * @param $competitionId
     * @param $round
     * @return mixed
     * @throws \Exception
     */
    public function getAllCompetitionMatchesByRound($competitionId, $round)
    {
        $url = sprintf('%s/%s/%s/%s?filter[round]=%s', $this->getBaseUrl(), 'competitions', $competitionId, 'matches', $round);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getMatch($matchId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'matches', $matchId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllCompetitionTeams($competitionId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'teams');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllCompetitionPlayers($competitionId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'players');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /*
     * UTIL
     */

    public function listAllCompetitionMatches($competitionId, $sort = 'round')
    {
        $data = $this->getAllCompetitionMatches($competitionId, $sort);
        $list = ['' => ''];
        foreach ($data as $item) {
            if(empty($item['teams'])) {
                continue;
            }
            $text = sprintf('Jornada %s - %s vs %s', $item['round'], $item['teams']['local']['team']['name'], $item['teams']['visitor']['team']['name']);
            $list[$item['id']] = $text;
        }
        return $list;
    }

    public function listAllMatchGames($matchId)
    {
        $data = $this->getAllMatchGames($matchId);
        $list = ['' => ''];
        foreach ($data as $item) {
            $text = sprintf('GAME %s', $item['order']);
            $list[$item['id']] = $text;
        }
        return $list;
    }

    public function listAllMatchTeams($matchId)
    {
        $match = $this->getMatch($matchId);

        $local = $match['match']['teams']['local']['team'];
        $visitor = $match['match']['teams']['visitor']['team'];
        $list = ['' => ''];
        $list[$local['id']] = $local['name'];
        $list[$visitor['id']] = $visitor['name'];
        return $list;
    }

    public function listAllTeamPlayers($competitionId, $teamId)
    {
        $players = $this->getAllCompetitionPlayers($competitionId);
        $list = ['' => ''];
        foreach ($players as $player) {
            if ($player['team']['id'] == $teamId) {
                $list[$player['player']['id']] = $player['player']['nickname'];
            }
        }
        return $list;
    }
}
