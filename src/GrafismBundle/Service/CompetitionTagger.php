<?php

namespace GrafismBundle\Service;


/**
 * Class CompetitionTagger.
 */
class CompetitionTagger
{
    /** @var string */
    private $leagueLiteral = 'Ronda';

    /** @var array */
    private $competitionLiterals = array(
            'Final',
            'Semifinal',
            'Cuartos de final',
            'Octavos de final',
            'Ronda',
    );

    /**
     * Returns the literal for round or match week.
     *
     * @param string $competitionType
     * @param integer      $currentRound
     * @param integer      $maxRounds
     *
     * @return string
     */
    public function getLiteral($competitionType, $currentRound, $maxRounds = 5)
    {
        switch ($competitionType) {
            case 'LEAGUE':
                return $this->toMatchWeekTag($currentRound);

            case 'TOURNAMENT':
                return $this->toRoundTag($currentRound, $maxRounds);
        }
    }

    /**
     * Returns the tag for given round.
     *
     * @param number $round
     * @param number $maxRounds
     *
     * @return string
     */
    public function toRoundTag($round, $maxRounds)
    {
        $diff = $maxRounds - $round;

        if ($diff < 0) {
            $diff = 0;
        }

        if ($diff >= 4) {
            return sprintf('%s %s',$this->competitionLiterals[4], $round);
        }

        return $this->competitionLiterals[$diff];
    }

    /**
     * Returns the tag for given match day.
     *
     * @param number $number
     *
     * @return string
     */
    public function toMatchWeekTag($number)
    {
        return sprintf('%s %s',$this->leagueLiteral, $number);
    }
}
