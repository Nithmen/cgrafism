<?php
namespace GrafismBundle\Service;

class FifaStatsAPI extends AbstractStatsAPI
{
    /*
     * ENDPOINTS
     */

    /**
     * Get Competition data.
     *
     * @param $competitionId
     * @return mixed
     * @throws \Exception
     */
    public function getCompetition($competitionId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * Get all competition matches.
     *
     * @param $competitionId
     * @param string $sort
     * @return mixed
     * @throws \Exception
     */
    public function getAllCompetitionMatches($competitionId, $sort = 'round')
    {
        $url = sprintf('%s/%s/%s/%s?sort=%s', $this->getBaseUrl(), 'competitions', $competitionId, 'matches', $sort);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * Get MatchGame.
     *
     * @param $gameId
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getGame($gameId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'games', $gameId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllMatchPlayers($matchId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'matches', $matchId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * Get players b game.
     *
     * @param $gameId
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getPlayersByGame($gameId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'games', $gameId, 'players');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllMatchGames($matchId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'matches', $matchId, 'games');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * @param $competitionId
     * @param $playerId
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getPlayerInACompetition($competitionId, $playerId)
    {
        $url = sprintf('%s/%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'players', $playerId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    /**
     * Get all competition matches filtered by round.
     *
     * @param $competitionId
     * @param $round
     * @return mixed
     * @throws \Exception
     */
    public function getAllCompetitionMatchesByRound($competitionId, $round)
    {
        $url = sprintf('%s/%s/%s/%s?filter[round]=%s', $this->getBaseUrl(), 'competitions', $competitionId, 'matches', $round);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getMatch($matchId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'matches', $matchId);
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllCompetitionPlayers($competitionId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'players');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getMatchDrafts($matchId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'matches', $matchId, 'drafts');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getAllCompetitionCards($competitionId)
    {
        $url = sprintf('%s/%s/%s/%s', $this->getBaseUrl(), 'competitions', $competitionId, 'cards');
        $response = $this->doRequest($url);
        return $response['data'];
    }

    public function getCard($cardId)
    {
        $url = sprintf('%s/%s/%s', $this->getBaseUrl(), 'cards', $cardId);
        $response = $this->doRequest($url);
        return $response['data'];
    }
    
    /*
     * UTIL
     */

    public function listAllCompetitionMatches($competitionId, $sort = 'round')
    {
        $data = $this->getAllCompetitionMatches($competitionId, $sort);
        $list = ['' => ''];

        foreach ($data as $item) {
            $text = sprintf('Jornada %s - %s vs %s', $item['round'], $item['players']['local']['player']['nickname'], $item['players']['visitor']['player']['nickname']);
            $list[$item['id']] = $text;
        }
        return $list;
    }

    public function listAllMatchGames($matchId)
    {
        $data = $this->getAllMatchGames($matchId);
        $list = ['' => ''];
        foreach ($data as $item) {
            $text = sprintf('SET %s - GAME %s', $item['set'], $item['order']);
            $list[$item['id']] = $text;
        }
        return $list;
    }

    public function listAllMatchPlayers($matchId)
    {
        $data = $this->getAllMatchPlayers($matchId);
        $list = ['' => ''];
        foreach ($data['match']['players'] as $side => $player) {
            $list[$player['player']['id']] = $player['player']['nickname'];
        }
        return $list;
    }

    public function listAllCompetitionCards($competitionId)
    {
        $data = $this->getAllCompetitionCards($competitionId);
        $list = ['' => ''];
        foreach ($data['cards'] as $card) {
            $list[$card['id']] = sprintf('%s[%s] - [%s]', $card['name'], $card['rating'], $card['type']);
        }
        return $list;
    }
}
