<?php

namespace GrafismBundle\Form\Iberian;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TimerForm
 */
class TimerForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $choices = [
            'elder' => 'Elder',
            'baron' => 'Baron'
        ];

        $sides = [
            'local' => 'Blue',
            'visitor' => 'Red'
        ];

        $builder->add('type_id', ChoiceType::class, [
            'label' => 'Type',
            'choices' => array_flip($choices),
            'required' => true
        ]);

        $builder->add('side_id', ChoiceType::class, [
            'label' => 'Side',
            'choices' => array_flip($sides),
            'required' => true
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'layer' => 25
        ]);
    }
}