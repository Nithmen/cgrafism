<?php

namespace GrafismBundle\Form\Iberian;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InGameDrakesForm
 */
class InGameDrakesForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $drakes = [
            'infernal' => 'Infernal',
            'cloud' => 'Cloud',
            'water' => 'Water',
            'mountain' => 'Mountain',
            'elder' => 'Elder'
        ];

        $builder->add('drake_id', ChoiceType::class, [
            'label' => 'Drake',
            'choices' => array_flip($drakes),
            'required' => true
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('layer', 21);
    }
}