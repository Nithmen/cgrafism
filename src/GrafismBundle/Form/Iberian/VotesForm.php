<?php

namespace GrafismBundle\Form\Iberian;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VotesForm
 */
class VotesForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $casters = [
            'ibai' => 'Ibai',
            'kuentin' => 'Kuentin',
            'barbe' => 'Barbe',
            'future' => 'Future',
            'wolk' => 'Wolk',
            'ulises' => 'Ulises'
        ];

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => array_flip($options['matches']),
            'required' => true
        ]);

        foreach($casters as $key => $caster){
            $name = sprintf('caster_%s', $key);
            $builder->add($name, ChoiceType::class, [
                'label' => $caster,
                'required' => true,
                'choices' => array_flip($options['team_choices'])
            ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'team_choices' => []
        ]);
    }
}