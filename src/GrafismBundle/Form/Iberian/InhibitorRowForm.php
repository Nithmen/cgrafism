<?php

namespace GrafismBundle\Form\Iberian;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InhibitorRowForm
 */
class InhibitorRowForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $sides = [
            'local' => 'Blue',
            'visitor' => 'Red'
        ];

        $positions = [
          'top' => 'Top',
          'mid' => 'Mid',
          'bot' => 'Bot'
        ];

        $builder->add('side_id', ChoiceType::class, [
            'label' => 'Side',
            'choices' => array_flip($sides),
            'required' => true
        ]);

        $builder->add('inhibitor_id', ChoiceType::class, [
            'label' => 'Inhibitor',
            'choices' => array_flip($positions),
            'required' => true
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'layer' => 35
        ]);
    }
}