<?php

namespace GrafismBundle\Form\SLO\ClashRoyale;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LineUpArenaForm
 */
class LineUpArenaForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => array_flip($options['matches'])
        ]);

        $sides = [
            '100' => 'local',
            '200' => 'visitor'
        ];

        foreach($sides as $side)
        {
            $team = sprintf('team_%s', $side);

            $builder->add(sprintf('%s_id', $team), ChoiceType::class, [
                'label' => 'Team ' .$side,
                'choices' => $options['teams']
            ]);

            for ($i = 1; $i <= 3; $i++) {
                $builder->add(sprintf('%s_player%s_id', $team, $i), ChoiceType::class, [
                    'label' => "Player" . $i,
                    'choices' => $options['players']
                ]);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'teams' => [],
            'players' => []
        ]);
    }


}