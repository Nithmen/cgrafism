<?php

namespace GrafismBundle\Form\SLO\ClashRoyale;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PredictionsForm
 */
class PredictionsForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $casters = [
            'publico' => 'Público',
            'shiki' => 'Shiki',
            'siko' => 'Siiko',
            'heisen' => 'Heisen',
            'kraniel' => 'Kraniel'
        ];

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'required' => true,
            'choices' => array_flip($options['matches'])
        ]);

        foreach($casters as $key => $caster)
        {
            $name = sprintf('caster_%s', $key);
            $builder->add($name, ChoiceType::class, [
                'label' => $caster,
                'required' => true,
                'choices' => array_flip($options['team_choices'])
            ]);
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'team_choices' => []
        ]);
    }


}