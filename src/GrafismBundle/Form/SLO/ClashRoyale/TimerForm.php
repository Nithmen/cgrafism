<?php

namespace GrafismBundle\Form\SLO\ClashRoyale;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TimerForm
 */
class TimerForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $choices = [
            'lineup' => 'Alineaciones',
            'bans' => 'Bans'
        ];

        $builder->add('type', ChoiceType::class, [
            'label' => 'Tipo',
            'required' => true,
            'choices' => array_flip($choices)
        ]);

        $builder->add('minutes', IntegerType::class, [
            'required' => true,
            'data' => 1,
            'attr' => [
                'min' => 0
            ]
        ]);

        $builder->add('seconds', IntegerType::class, [
            'required' => true,
            'data' => 1,
            'attr' => [
                'min' => 0
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'layer' => 21
        ]);
    }


}