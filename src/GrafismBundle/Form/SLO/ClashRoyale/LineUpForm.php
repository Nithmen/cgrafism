<?php

namespace GrafismBundle\Form\SLO\ClashRoyale;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LineUpForm
 */
class LineUpForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $setChoices = [];
        for($i = 1; $i <= $options['setNumber']; $i++)
        {
            $setChoices[$i] = sprintf('Set %s', $i);
        }

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'required' => true,
            'choices' => array_flip($options['matches'])
        ]);

        $builder->add('set_id', ChoiceType::class, [
            'label' => 'Set',
            'choices' => array_flip($setChoices),
            'required' => true
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'setNumber' => 2
        ]);
    }


}