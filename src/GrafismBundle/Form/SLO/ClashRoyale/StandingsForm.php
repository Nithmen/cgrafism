<?php

namespace GrafismBundle\Form\SLO\ClashRoyale;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class StandingsForm
 */
class StandingsForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

    }

}