<?php

namespace GrafismBundle\Form\SLO\CounterStrike;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ScheduledMatchesForm
 */
class ScheduledMatchesForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $roundsChoices = [];
        for($i=1; $i<=$options['rounds']; $i++)
        {
            $roundsChoices[$i] = sprintf('Jornada %s', $i);
        }

        $builder->add('round_id', ChoiceType::class, [
            'label' => 'Jornada',
            'required' => true,
            'choices' => array_flip($roundsChoices)
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
           'rounds' => 24
        ]);
    }

}