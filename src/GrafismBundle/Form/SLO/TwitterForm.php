<?php

namespace GrafismBundle\Form\SLO;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TwitterForm
 */
class TwitterForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('tweet_id', TextType::class, [
            'label' => 'Tweet ID',
            'required' => true
        ]);

        $builder->add('hashtag', TextType::class, [
            'label' => 'Hashtag',
            'required' => true
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([

        ]);
    }
}