<?php

namespace GrafismBundle\Form\CRL;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LowerPlayerVersusForm
 */
class LowerPlayerVersusForm extends GraphismType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => array_flip($options['matches'])
        ]);

        $sides = [
            '100' => 'local',
            '200' => 'visitor'
        ];

        foreach($sides as $side)
        {
            $team = sprintf('team_%s', $side);

            $builder->add(sprintf('%s_id', $team), ChoiceType::class, [
                'label' => 'Team ' .$side,
                'choices' => $options['teams']
            ]);

            $builder->add(sprintf('%s_player1_id', $team), ChoiceType::class, [
                'label' => "Player",
                'choices' => $options['players']
            ]);
        }

        $setChoices = [
            '2' => 'Set 2',
            '3' => 'Set 3',
            '4' => 'Ser 4',
        ];

        $builder->add('set_id', ChoiceType::class, [
           'label' => 'Set',
            'choices' => array_flip($setChoices)
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'teams' => [],
            'players' => []
        ]);
    }
}
