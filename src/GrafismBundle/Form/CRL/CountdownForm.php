<?php

namespace GrafismBundle\Form\CRL;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CountdownForm
 */
class CountdownForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        for($i = 1; $i <= 2; $i++)
        {
            $builder->add(sprintf('match%s_id', $i), ChoiceType::class, [
                'label' => sprintf('Match %s', $i),
                'choices' => array_flip($options['matches'])
            ]);
        }

        $builder->add('minutes', IntegerType::class, [
           'label' => 'Minutes',
           'required' => true
        ]);

        $builder->add('seconds', IntegerType::class, [
            'label' => 'Seconds',
            'required' => true
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
        ]);
    }
}