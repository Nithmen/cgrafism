<?php

namespace GrafismBundle\Form\CRL;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LowerTeamsWinrateForm
 */
class LowerTeamsWinrateForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => array_flip($options['matches'])
        ]);
        $builder->add('set_id', ChoiceType::class, [
            'label' => 'Set',
            'choices' => [
                'Competition' => 'competition',
                '2 vs 2' => '1',
                '1 vs 1' => '2',
                'KoH' => '3'
            ]
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
           'matches' => []
        ]);
    }
}