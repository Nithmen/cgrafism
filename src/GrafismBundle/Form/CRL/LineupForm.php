<?php
namespace GrafismBundle\Form\CRL;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LineupForm
 */
class LineupForm extends GraphismType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => array_flip($options['matches'])
        ]);

        $builder->add('team_id', ChoiceType::class, [
            'label' => 'Team',
            'choices' => $options['teams']
        ]);

        for ($i = 1; $i <= 4; $i++) {
            $builder->add("player" . $i . "_id", ChoiceType::class, [
                'label' => "Player" . $i,
                'choices' => $options['players']
            ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'teams' => [],
            'players' => []
        ]);
    }
}
