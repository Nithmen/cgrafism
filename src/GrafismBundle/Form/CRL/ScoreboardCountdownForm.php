<?php

namespace GrafismBundle\Form\CRL;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ScoreboardCountdownForm
 */
class ScoreboardCountdownForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => $options['matches']
        ]);
        $builder->add('minutes', IntegerType::class, [
            'label' => 'Minutes'
        ]);
        $builder->add('seconds', IntegerType::class, [
            'label' => 'Seconds'
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
           'matches' => []
        ]);
    }
}