<?php

namespace GrafismBundle\Form\CRL;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LowerTeamBansForm
 */
class LowerTeamBansForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $choices = [];
        for($i = 1; $i <= $options['sets']; $i++)
        {
            $choices[$i] = sprintf('SET %s', $i);
        }

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => array_flip($options['matches'])
        ]);
        $builder->add('set_id', ChoiceType::class, [
           'label' => 'Set',
           'choices' => array_flip($choices)
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
           'matches' => [],
            'sets' => 3
        ]);
    }
}