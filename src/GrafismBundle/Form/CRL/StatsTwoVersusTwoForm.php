<?php

namespace GrafismBundle\Form\CRL;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StatsTwoVersusTwoForm
 */
class StatsTwoVersusTwoForm extends GraphismType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'choices' => array_flip($options['matches'])
        ]);

        $sides = [
          '100' => 'local',
          '200' => 'visitor'
        ];

        foreach($sides as $side)
        {
            $team = sprintf('team_%s', $side);

            $builder->add(sprintf('%s_id', $team), ChoiceType::class, [
                'label' => 'Team ' .$side,
                'choices' => $options['teams']
            ]);

            for($i = 1; $i <= 2; $i++)
            {
                $builder->add(sprintf('%s_player%s_id', $team, $i), ChoiceType::class, [
                    'label' => "Player" . $i,
                    'choices' => $options['players']
                ]);
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'teams' => [],
            'players' => []
        ]);
    }
}
