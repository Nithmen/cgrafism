<?php

namespace GrafismBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GrafismType
 */
class GraphismType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('play', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-success caspar-play'
                )
            ))
            ->add('stop', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-danger caspar-stop'
                )
            ))
            ->add('layer', IntegerType::class, array(
                'data' => $options['layer'],
                'attr' => [
                    'class' => 'form-control caspar-layer'
                ]
            ))
            ->add('preview', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-warning caspar-preview'
                )
            ))
            ->add('url', HiddenType::class, [])
            ->add('competitionId', HiddenType::class, [])
            ->add('context', HiddenType::class, [])
            ->add('locale', ChoiceType::class, [
                'choices' => $options['locales']
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => 'Name',
            'locales' => [
                'Spanish' => 'es'
            ],
            'layer' => 20
        ]);
    }
}