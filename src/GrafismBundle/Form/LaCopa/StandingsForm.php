<?php

namespace GrafismBundle\Form\LaCopa;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StandingsForm
 */
class StandingsForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('group_id', ChoiceType::class, [
            'label' => 'Group',
            'choices' => [
                'A' => 'A',
                'B' => 'B',
                'C' => 'C',
                'D' => 'D'
            ],
            'required' => false
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([

        ]);
    }
}