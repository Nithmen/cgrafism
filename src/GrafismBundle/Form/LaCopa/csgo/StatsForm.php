<?php

namespace GrafismBundle\Form\LaCopa\csgo;

use GrafismBundle\Form\GraphismType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class StatsForm
 */
class StatsForm extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);


        $builder->add('side', ChoiceType::class, [
            'label' => 'Side',
            'choices' => array_flip($this->getChoiceOptions())
        ]);

        $builder->add('text', TextareaType::class, [
            'label' => 'Text'
        ]);
    }

    /**
     * @return array
     */
    private function getChoiceOptions()
    {
        return [
            'local' => 'Local',
            'visitor' => 'Visitor',
            'mid' => 'Middle'
        ];
    }
}