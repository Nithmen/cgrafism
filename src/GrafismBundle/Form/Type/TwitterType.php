<?php

namespace GrafismBundle\Form\Type;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TwitterType
 */
class TwitterType extends GraphismType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('hashtag', TextType::class, [
           'label' => 'Hashtag',
            'required' => true
        ]);

        $builder->add('tweet_id', TextType::class, [
            'label' => 'Tweet ID',
            'required' => true
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'name' => "Twitter"
        ]);
    }
}