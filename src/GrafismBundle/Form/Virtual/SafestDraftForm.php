<?php

namespace GrafismBundle\Form\Virtual;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SafestDraftForm
 */
class SafestDraftForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        for($i = 1; $i <= 11; $i++)
        {
            $name = sprintf('player_%s', $i);
            $builder->add($name, ChoiceType::class, [
                'choices' => array_flip($options['cards'])
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('cards', []);
    }


}