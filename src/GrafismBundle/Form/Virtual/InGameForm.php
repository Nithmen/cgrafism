<?php

namespace GrafismBundle\Form\Virtual;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InGameForm
 */
class InGameForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $gameChoices = [
            1 => 'Ida',
            2 => 'Vuelta',
            3 => 'Desempate'
        ];

        $teamChoices = [
            'alcorcon-1' => 'Alcorcón (local)',
            'alcorcon-2' => 'Alcorcón (visitante)',
            'madrid-1' => 'Real Madrid  (local)',
            'madrid-2' => 'Real Madrid (visitante)',
            'oviedo-1' => 'Oviedo (local)',
            'oviedo-2' => 'Oviedo (visitante)',
            'sevilla-1' => 'Sevilla (local)',
            'sevilla-2' => 'Sevilla (visitante)',
        ];

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'required' => true,
            'choices' => array_flip($options['matches'])
        ]);

        $builder->add('game_id', ChoiceType::class, [
            'label' => 'Game',
            'choices' => array_flip($gameChoices),
            'required' => true
        ]);

        $builder->add('half_id', ChoiceType::class, [
           'label' => 'Half',
           'choices' => [
               'Primera parte' => '1',
               'Segunda parte' => '2'
           ]
        ]);

        $builder->add('local_flag', ChoiceType::class, [
            'required' => true,
            'choices' => array_flip($teamChoices)
        ]);

        $builder->add('visitor_flag', ChoiceType::class, [
            'required' => true,
            'choices' => array_flip($teamChoices)
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => []
        ]);
    }


}