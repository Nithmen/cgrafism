<?php

namespace GrafismBundle\Form\Virtual;

use GrafismBundle\Form\GraphismType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DraftForm
 */
class DraftForm extends GraphismType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('match_id', ChoiceType::class, [
            'label' => 'Match',
            'required' => true,
            'choices' => array_flip($options['matches'])
        ]);

        $builder->add('player_id', ChoiceType::class, [
           'label' => 'Player',
           'choices' => array_flip($options['players']),
            'required' => true
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'matches' => [],
            'players' => []
        ]);
    }
}