<?php

namespace GrafismBundle\Twig\Extension;


use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class TranslatorExtension
 */
class TranslatorExtension extends \Twig_Extension
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var string
     */
    private $translationsFile = 'messages';

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('localize', array($this, 'getLocalizer'), ['is_safe' => ['html']]),
        );
    }

    public function getLocalizer($transKey, $isUpper = true)
    {
        $esKey = $this->translator->trans($transKey, [], $this->translationsFile, 'es');
        $ptKey = $this->translator->trans($transKey, [], $this->translationsFile, 'pt');

        if($isUpper) {
            $esKey = strtoupper($esKey);
            $ptKey = strtoupper($ptKey);
        }

        if($esKey == $ptKey) {
            return $esKey;
        }

        return '<span class="es">'.$esKey.'</span><span class="pt" style="display:none;">'.$ptKey.'</span>';
    }

    /**
     *
     * {@inheritDoc}
     * @see Twig_Extension::getName()
     */
    public function getName()
    {
        return 'app_twig_translator';
    }
}