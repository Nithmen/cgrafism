<?php

namespace GrafismBundle\Twig\Extension;

use Twig\TwigFilter;

/**
 * Class PlayerPositionExtension
 */
class PlayerPositionExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new TwigFilter('abbr', array($this, 'toAbbreviation')),
            new TwigFilter('stat', array($this, 'toStatAbbreviation'))
        ];
    }

    public function toAbbreviation($position)
    {
        $abbr = '';

        switch($position)
        {
            case 'Carrilero derecho':
                $abbr = 'CAD';break;
            case 'Carrilero izquierdo':
                $abbr = 'CAI';break;
            case 'Defensa central':
                $abbr = 'DFC';break;
            case 'Delantero centro':
                $abbr = 'DC';break;
            case 'Extremo derecho':
                $abbr = 'ED';break;
            case 'Extremo izquierdo':
                $abbr = 'EI';break;
            case 'Lateral derecho':
                $abbr = 'LD';break;
            case 'Lateral izquierdo':
                $abbr = 'LI';break;
            case 'Medio derecho':
                $abbr = 'MD';break;
            case 'Medio izquierdo':
                $abbr = 'MI';break;
            case 'Mediocentro':
                $abbr = 'MC';break;
            case 'Mediocentro defensivo':
                $abbr = 'MCD';break;
            case 'Mediocentro ofensivo':
                $abbr = 'MCO';break;
            case 'Portero':
                $abbr = 'POR';break;
            case 'Segundo delantero':
                $abbr = 'SD';break;
        }

        return $abbr;
    }

    public function toStatAbbreviation($statName)
    {
        switch($statName)
        {
            case 'fut.attribute.PAC':
                $s = 'RIT';
                break;
            case 'fut.attribute.SHO':
                $s = 'TIR';
                break;
            case 'fut.attribute.PAS':
                $s = 'PAS';
                break;
            case 'fut.attribute.DRI':
                $s = 'REG';
                break;
            case 'fut.attribute.DEF':
                $s = 'DEF';
                break;
            case 'fut.attribute.PHY':
                $s = 'FIS';
                break;
            case 'fut.attribute.DIV':
                $s = 'SAL';
                break;
            case 'fut.attribute.HAN':
                $s = 'EST';
                break;
            case 'fut.attribute.KIC':
                $s = 'SAQ';
                break;
            case 'fut.attribute.REF':
                $s = 'REF';
                break;
            case 'fut.attribute.SPD':
                $s = 'VEL';
                break;
            case 'fut.attribute.POS':
                $s = 'POS';
                break;
            default:
                $s = 'A';
        }

        return $s;
    }
}