<?php

namespace GrafismBundle\Twig\Extension;

use GrafismBundle\Service\CompetitionTagger;
use GrafismBundle\Utils\GroupUtils;

/**
 * Class CompetitionExtension
 */
class CompetitionExtension extends \Twig_Extension
{
    /**
     * @var CompetitionTagger
     */
    private $tagger;

    public function __construct(CompetitionTagger $tagger)
    {
        $this->tagger = $tagger;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_Filter('toLetter', array($this, 'toLetter')),
        );
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('toLiteral', array($this, 'getRoundTagger')),
        );
    }

    /**
     * @param string $competitionType
     * @param integer $currentRound
     * @param integer $maxRounds
     *
     * @return string
     */
    public function getRoundTagger($competitionType, $currentRound, $maxRounds = 5)
    {
        return $this->tagger->getLiteral($competitionType, $currentRound, $maxRounds);
    }

    /**
     * @param $number
     *
     * @return string
     */
    public function toLetter($number)
    {
        return GroupUtils::getLetter($number);
    }

    /**
     *
     * {@inheritDoc}
     * @see Twig_Extension::getName()
     */
    public function getName()
    {
        return 'app_competition';
    }
}