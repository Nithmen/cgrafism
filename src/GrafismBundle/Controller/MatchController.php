<?php

namespace GrafismBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MatchController
 */
class MatchController extends Controller
{
    private $resource = 'matches';

    /**
     * @Route("/matches/{id}", name="grafism_get_one_matches", requirements={"context": "lol|cs|cod|cr"})
     *
     * @param $id
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function getMatchData($id)
    {
        $match = $this->getResource($this->getContext(), $this->resource, $id);

        return new JsonResponse($match, Response::HTTP_OK);
    }
}