<?php

namespace GrafismBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PlayerController
 */
class PlayerController extends Controller
{
    private $resource = 'players';

    /**
     * @Route("/competitions/{id}/players", name="grafism_get_competitions_players", requirements={"context": "lol|cs|cod|cr"})
     *
     * @param $id
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function getCompetitionPlayers($id)
    {
        $fetcher = $this->getResourceFetcher();

        $competitionUrl = $fetcher->getResourceUrl($this->getContext(), 'competitions', $id);

        $data = $this->getChildResource($competitionUrl, $this->resource);

        return new JsonResponse($data, Response::HTTP_OK);
    }
}