<?php

namespace GrafismBundle\Controller;

use GrafismBundle\Service\ResourceFetcher;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Controller
 *
 * @package GrafismBundle\Controller
 */
class Controller extends BaseController
{
    /** @var ResourceFetcher */
    private $resourceFetcher;

    /** @var RequestStack */
    private $request;

    /**
     * Controller constructor.
     *
     * @param ResourceFetcher $resourceFetcher
     * @param RequestStack $request
     */
    public function __construct(ResourceFetcher $resourceFetcher, RequestStack $request)
    {
        $this->resourceFetcher = $resourceFetcher;
        $this->request = $request;
    }

    /**
     * @return ResourceFetcher
     */
    public function getResourceFetcher()
    {
        return $this->resourceFetcher;
    }

    /**
     * @param string $context
     * @param string $resource
     * @param string $resourceId
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getResource($context, $resource, $resourceId = null)
    {
        $url = $this->getResourceFetcher()->getResourceUrl($context, $resource, $resourceId);

        return $this->getData($url);
    }

    /**
     * @param $resource
     * @param $child
     * @param $sort
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function getChildResource($resource, $child, $sort = null)
    {
        $url = sprintf('%s/%s', $resource, $child);

        if($sort) {
            $url = sprintf('%s?sort=%s', $url, $sort);
        }

        return $this->getData($url);
    }

    /**
     * @param $url
     *
     * @return mixed
     *
     * @throws \Exception
     */
    private function getData($url)
    {
        $s = curl_init();

        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);

        $json = curl_exec($s);

        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }

        curl_close($s);

        $json = json_decode($json, true);

        return $json;
    }

    /**
     * @return string
     */
    protected function getContext()
    {
        return $this->request->getCurrentRequest()->get('context');
    }
}