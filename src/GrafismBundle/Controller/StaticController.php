<?php

namespace GrafismBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StaticController
 */
class StaticController extends Controller
{
    /**
     * @Route("/games", name="grafism_get_games", requirements={"context": "static"})
     *
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function getGames()
    {
        $games = $this->getResource($this->getContext(), 'games');

        return new JsonResponse($games, Response::HTTP_OK);
    }
}