<?php

namespace GrafismBundle\Controller;

use GrafismBundle\Entity\Grafism;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CompetitionController
 */
class CompetitionController extends Controller
{
    private $resource = 'competitions';

    /**
     * @Route("/competitions", name="grafism_get_competitions", requirements={"context": "lol|cs|cod|cr|fifa"})
     */
    public function getCompetitions()
    {
        $competitions =  $this->getResource($this->getContext(), $this->resource);

        return new JsonResponse($competitions, Response::HTTP_OK);
    }

    /**
     * @Route("/competitions/{id}", name="grafism_get_one_competitions", requirements={"context": "lol|cs|cod|cr|fifa"})
     *
     *
     * @param $id
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function getCompetition($id)
    {
        $competition =  $this->getResource($this->getContext(), $this->resource, $id);

        return new JsonResponse($competition, Response::HTTP_OK);
    }

    /**
     * @Route("/competitions/{id}/commands", name="grafism_get_competitions_commands", requirements={"context": "lol|cs|cod|cr|fifa"})
     *
     * @param $id
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function getCompetitionCommands($id)
    {
        $competition = $this->getResource($this->getContext(), $this->resource, $id);

        $graphisms = $this->getGrafismsByCompetition($id);

        $data = [
            'competition' => $competition['data'],
            'graphisms' => $graphisms
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    private function getGrafismsByCompetition($competitionId)
    {
        /** @var EntityRepository $grafismRepository */
        $grafismRepository = $this->getDoctrine()->getRepository(Grafism::class);

        $qb = $grafismRepository->createQueryBuilder('g');
        $qb->select(array('g.key', 'g.url', 'g.formPath'));

        $qb->leftJoin('g.competition', 'competition');
        $qb->where($qb->expr()->eq('competition', '?1'));
        $qb->setParameter(1, $competitionId);

        $qb->orderBy('g.key', 'ASC');

        $results = $qb->getQuery()->getArrayResult();

        return $results;
    }
}