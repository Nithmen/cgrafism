<?php
namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\DecksForm;
use GrafismBundle\Form\CRL\LineupForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LineupController
 */
class LineupController extends OverlayController
{
    /**
     * @Route("/lineup", name="crl_lineup")
     */
    public function renderLineup(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');
        $teamId = $request->get('team_id');
        $player1Id = $request->get('player1_id');
        $player2Id = $request->get('player2_id');
        $player3Id = $request->get('player3_id');
        $player4Id = $request->get('player4_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $team = $this->getCRStatsApi()->getTeam($teamId);
        $allPlayers = $this->getCRStatsApi()->getAllCompetitionPlayers($competitionId);
        
        $data = $this->getLineupViewData($match, $team, $allPlayers, $player1Id, $player2Id, $player3Id, $player4Id);

        return $this->render('GrafismBundle:CRL:lineup.html.twig', $data);
    }

    protected function getLineupViewData($match, $team, $allPlayers, $player1Id, $player2Id, $player3Id, $player4Id)
    {
        $players = [];
        foreach ($allPlayers as $player) {
            $position = null;
            switch ($player['id']) {
                case $player1Id:
                    $position = 1;
                    break;
                case $player2Id:
                    $position = 2;
                    break;
                case $player3Id:
                    $position = 3;
                    break;
                case $player4Id:
                    $position = 4;
                    break;
            }
            if ($position) {
                $players[$position] = [
                    'name' => $player['attributes']['nickname'],
                    'photo' => $player['attributes']['photo']['original']
                ];
            }
        }

        $data = [
            'team' => [
                'name' => $team['attributes']['name'],
                'logo' => $team['attributes']['logo']['original'],
                'side' => $this->getTeamMatchSide($match, $team)
            ],
            'players' => $players
        ];

        return $data;
    }

    /**
     * @Route("/lineup-form", name="crl_lineup_form")
     * @Template("GrafismBundle:CRL:form/lineup_form.html.twig")
     */
    public function renderLineupForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LineupForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_lineup'),
            'competitionId' => $competitionId,
            'context' => $context
            ], [
            'name' => 'Lineups',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $match
     * @param $team
     *
     * @return int
     */
    protected function getTeamMatchSide($match, $team)
    {
        $attributes = $match['attributes'];
        return ($attributes['team_local']['id'] == $team['id']) ? 100 : 200;
    }
}
