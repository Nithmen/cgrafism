<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\CountdownForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CountdownController
 */
class CountdownController extends OverlayController
{

    /**
     * @Route("/countdown", name="crl_countdown")
     * @Template("GrafismBundle:CRL:countdown.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderCountdown(Request $request)
    {
        $match1Id = $request->get('match1_id');
        $match2Id = $request->get('match2_id');

        $minutes = $request->get('minutes');
        $seconds = $request->get('seconds');

        $match1 = $this->getCRStatsApi()->getMatch($match1Id);
        $match2 = $this->getCRStatsApi()->getMatch($match2Id);

        $data = $this->getCountdownViewData($match1, $match2, $minutes, $seconds);

        return $data;
    }

    /**
     * @param $match1
     * @param $match2
     * @param $minutes
     * @param $seconds
     *
     * @return array
     */
    public function getCountdownViewData($match1, $match2, $minutes, $seconds)
    {
        $data = [];

        $data['match1'] = [
            'local' => [
                'name' => $match1['attributes']['team_local']['name'],
                'shortname' => $match1['attributes']['team_local']['shortname'],
                'logo' => $match1['attributes']['team_local']['logo']['original'],
                'score' => $match1['attributes']['results']['score_local']
            ],
            'visitor' => [
                'name' => $match1['attributes']['team_visitor']['name'],
                'shortname' => $match1['attributes']['team_visitor']['shortname'],
                'logo' => $match1['attributes']['team_visitor']['logo']['original'],
                'score' => $match1['attributes']['results']['score_visitor']
            ],
            'status' => $match1['attributes']['status']
        ];

        $data['match2'] = [
            'local' => [
                'name' => $match2['attributes']['team_local']['name'],
                'shortname' => $match2['attributes']['team_local']['shortname'],
                'logo' => $match2['attributes']['team_local']['logo']['original'],
                'score' => $match2['attributes']['results']['score_local']
            ],
            'visitor' => [
                'name' => $match2['attributes']['team_visitor']['name'],
                'shortname' => $match2['attributes']['team_visitor']['shortname'],
                'logo' => $match2['attributes']['team_visitor']['logo']['original'],
                'score' => $match2['attributes']['results']['score_local']
            ],
            'status' => $match2['attributes']['status']
        ];

        $data['countdown'] = [
            'minutes' => $minutes,
            'seconds' => $seconds
        ];

        return $data;
    }

    /**
     * @Route("/countdown-form", name="crl_countdown_form")
     * @Template("GrafismBundle:CRL/form:countdown_form.html.twig")
     *
     * @return array
     */
    public function renderCountdownForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(CountdownForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_countdown'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Next matches',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}