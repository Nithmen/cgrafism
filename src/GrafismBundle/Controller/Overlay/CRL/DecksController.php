<?php
namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\DecksForm;
use GrafismBundle\Service\CRStatsAPI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DecksController
 */
class DecksController extends OverlayController
{
    /**
     * @Route("/decks", name="crl_decks")     
     */
    public function renderDecks(Request $request)
    {
        $matchId = $request->get('match_id');
        $gameId = $request->get('game_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $game = $this->getCRStatsApi()->getGame($gameId);

        $modality = '';
        $template = '';
        switch ($game['attributes']['set']) {
            case 1: // 2v2
                $modality = '2v2';
                $template = 'GrafismBundle:CRL:decks_2vs2.html.twig';
                break;
            case 2: // 1v1
            case 3:
            case 4:
                $modality = '1v1';
                $template = 'GrafismBundle:CRL:decks_1vs1.html.twig';
                break;
            case 5: // KoH
                $modality = 'Koh';
                $template = 'GrafismBundle:CRL:decks_1vs1.html.twig';
                break;
        }
        $data = $this->getDecksViewData($modality, $match, $game);
        return $this->render($template, $data);
    }       

    /**
     * @Route("/decks-form", name="crl_decks_form")
     * @Template("GrafismBundle:CRL:form/decks_form.html.twig")
     */
    public function renderDecksForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();
        
        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);
        
        $form = $this->createForm(DecksForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_decks'),
            'competitionId' => $competitionId,
            'context' => $context
            ], [
            'name' => 'FF cartas jugadas',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }

    protected function getDecksViewData($modality, $match, $game)
    {
        $attributes = $game['attributes'];
        $set = $attributes['set'];
        $local = $attributes['team_100'];
        $visitor = $attributes['team_200'];
        $decks = $attributes['decks'];
        $results = $match['attributes']['results'];
        $data = [
            'modality' => $modality,
            'set' => $set,
            'local' => $this->getTeamDTO($local, $decks),
            'visitor' => $this->getTeamDTO($visitor, $decks),
            'results' => [
                'local' => $results['score_local'],
                'visitor' => $results['score_visitor']
            ],
            'order' => $attributes['order']
        ];
        return $data;
    }

    protected function getTeamDTO($team, $decks)
    {
        $players = [];
        foreach ($team['players'] as $item) {
            $cards = [];
            $elixir = 0;
            $deck = (isset($decks[$item['id']])) ? $decks[$item['id']] : [];
            foreach ($deck as $card) {
                $cards[] = [
                    'key' => $card['card']['cardKey'],
                    'rarity' => $card['card']['rarity']
                ];
                $elixir += $card['card']['elixir'];
            }

            $player = [];
            $player['name'] = $item['nickname'];
            $player['photo'] = $item['photo']['original'];
            $player['cards'] = $cards;
            $player['elixir'] = (count($deck) > 0) ? $elixir / count($deck) : 0;
            $players[] = $player;
        }
        $data = [];
        $data['team'] = [
            'name' => $team['name'],
            'shortname' => $team['shortname'],
            'logo' => $team['logo']['original'],
        ];
        $data['players'] = $players;
        return $data;
    }
}
