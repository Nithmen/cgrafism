<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\LowerPlayerForm;
use GrafismBundle\Form\CRL\LowerTeamBansForm;
use GrafismBundle\Form\CRL\LowerTeamsWinrateForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LowerTeamController
 */
class LowerTeamController extends OverlayController
{
    /**
     * @Route("/lower-team", name="crl_lower_team")
     * @Template("GrafismBundle:CRL:lower_team.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderLowerTeam(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');
        $teamId = $request->get('team_id');
        $setNumber = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $teamMetrics = $this->getCRStatsApi()->getCompetitionTeamMetrics($competitionId,$teamId);

        $data = $this->getLowerTeamViewData($match, $teamMetrics, $setNumber);

        return $data;
    }

    /**
     * @param $match
     * @param $team
     *
     * @return array
     */
    protected function getLowerTeamViewData($match, $teamMetrics, $setNumber)
    {
        $team = $teamMetrics['attributes'];

        $stats = $teamMetrics['attributes']['stats'][0];

        $key = $this->getSetKey($setNumber);

        $data = [
            'team' => [
                'name' => $team['name'],
                'logo' => $team['logo']['original'],
                'side' => $this->getTeamMatchSide($match, $teamMetrics),
                'ban' => $match['attributes']['banned_cards'][$setNumber][$teamMetrics['id']]['card']['key'] ?: null,
                'stats' => [
                    'win_ratio' => $stats['win_ratio'][$key],
                    'crown_average' => $stats['crown_average'][$key]
                ]
            ],
            'match' => [
                'label' => sprintf('SET %s - %s', $setNumber, $key)
            ]
        ];

        return $data;
    }

    /**
     * @Route("/lower-team-form", name="crl_lower_team_form")
     * @Template("GrafismBundle:CRL/form:lower_team_form.html.twig")
     *
     *
     * @return
     */
    public function renderLowerTeamForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LowerPlayerForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_lower_team'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower Team',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $match
     * @param $team
     *
     * @return int
     */
    protected function getTeamMatchSide($match, $team)
    {
        $attributes = $match['attributes'];
        return ($attributes['team_local']['id'] == $team['id']) ? 100 : 200;
    }

    /**
     * @Route("/teams-winrate-lower", name="crl_teams_winrate_lower")
     * @Template("GrafismBundle:CRL:teams_winrate_lower.html.twig")
     *
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function renderTeamsWinrateLower(Request $request)
    {
        $competitionId = $this->getCompetition();

        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);
        $setNumber = $request->get('set_id');

        $data = $this->getTeamsWinrateLowerViewData($match, $competitionId, $setNumber);

        return $data;
    }

    /**
     * @param $match
     * @param $competitionId
     * @param $setNumber
     *
     * @return array
     */
    protected function getTeamsWinrateLowerViewData($match, $competitionId, $setNumber)
    {
        $data =  [];

        $key = $this->getSetKey($setNumber);

        $local = $match['attributes']['team_local'];
        $localMetrics = $this->getCRStatsApi()->getCompetitionTeamMetrics($competitionId, $local['id']);

        $visitor = $match['attributes']['team_visitor'];
        $visitorMetrics = $this->getCRStatsApi()->getCompetitionTeamMetrics($competitionId, $visitor['id']);

        $data['local'] = [
            'name' => $local['name'],
            'logo' => $local['logo']['original'],
            'ban' => isset($match['attributes']['banned_cards'][$setNumber]) ? $match['attributes']['banned_cards'][$setNumber][$local['id']]['card']['key'] : end($match['attributes']['banned_cards'])[$local['id']]['card']['key'],
            'ban_name' => isset($match['attributes']['banned_cards'][$setNumber]) ? $match['attributes']['banned_cards'][$setNumber][$local['id']]['card']['name'] : end($match['attributes']['banned_cards'])[$local['id']]['card']['name'],
            'win_ratio' => $localMetrics['attributes']['stats'][0]['win_ratio'][$key],
        ];

        $data['visitor'] = [
            'name' => $visitor['name'],
            'logo' => $visitor['logo']['original'],
            'ban' => isset($match['attributes']['banned_cards'][$setNumber]) ? $match['attributes']['banned_cards'][$setNumber][$visitor['id']]['card']['key'] : end($match['attributes']['banned_cards'])[$visitor['id']]['card']['key'],
            'ban_name' => isset($match['attributes']['banned_cards'][$setNumber]) ? $match['attributes']['banned_cards'][$setNumber][$visitor['id']]['card']['name'] : end($match['attributes']['banned_cards'])[$visitor['id']]['card']['name'],
            'win_ratio' => $visitorMetrics['attributes']['stats'][0]['win_ratio'][$key]
        ];

        return $data;
    }

    /**
     * @Route("/teams-winrate-lower-form", name="crl_teams_winrate_lower_form")
     * @Template("GrafismBundle:CRL:form/teams_winrate_lower_form.html.twig")
     */
    public function renderTeamsWinrateLowerForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LowerTeamsWinrateForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_teams_winrate_lower'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower Teams Winrate',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @Route("/teams-lower-bans", name="crl_teams_lower_bans")
     * @Template("GrafismBundle:CRL:lower_team_bans.html.twig")
     *
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function renderTeamsLowerBans(Request $request)
    {
        $competitionId = $this->getCompetition();

        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);
        $setNumber = $request->get('set_id');

        $data = $this->getTeamsWinrateLowerViewData($match, $competitionId, $setNumber);

        return $data;
    }

    /**
     * @Route("/teams-lower-bans-form", name="crl_teams_lower_bans_form")
     * @Template("GrafismBundle:CRL/form:overlay_form.html.twig")
     */
    public function renderTeamsLowerBansForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LowerTeamBansForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_teams_lower_bans'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower Teams Bans',
            'matches' => $matchesList,
            'sets' => 3,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $setNumber
     *
     * @return string
     */
    protected function getSetKey($setNumber)
    {
        switch($setNumber)
        {
            case 1:
                $key = '2v2';
                break;
            case 2:
                $key = '1v1';
                break;
            case 3:
                $key = 'koh';
                break;
            default:
                $key = 'competition';
        }

        return $key;
    }
}