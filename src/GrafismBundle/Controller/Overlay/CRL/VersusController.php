<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\StatsOneVersusOneForm;
use GrafismBundle\Form\CRL\StatsTwoVersusTwoForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VersusController
 */
class VersusController extends OverlayController
{
    /**
     * @Route("/stats-1vs1", name="crl_stats_1vs1")
     * @Template("GrafismBundle:CRL:stats_1vs1.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderStatsOneVersusOne(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');

        $teamLocalId = $request->get('team_local_id');
        $localPlayerId = $request->get('team_local_player_id');

        $teamVisitorId = $request->get('team_visitor_id');
        $visitorPlayerId = $request->get('team_visitor_player_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $teamLocal = $this->getCRStatsApi()->getTeam($teamLocalId);
        $teamVisitor = $this->getCRStatsApi()->getTeam($teamVisitorId);

        $data = $this->getStatsViewData([
            'local' => [
                'team' => $teamLocal,
                'players' => [$this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId,$localPlayerId)]
            ],
            'visitor' => [
                'team' => $teamVisitor,
                'players' => [$this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $visitorPlayerId)]
            ]
        ], $match, 'STATS 1VS1', 2);

        return $data;
    }

    /**
     * @Route("/stats-2vs2", name="crl_stats_2vs2")
     * @Template("GrafismBundle:CRL:stats_1vs1.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderStatsTwoVersusTwo(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');

        $teamLocalId = $request->get('team_local_id');
        $localPlayer1Id = $request->get('team_local_player1_id');
        $localPlayer2Id = $request->get('team_local_player2_id');

        $teamVisitorId = $request->get('team_visitor_id');
        $visitorPlayer1Id = $request->get('team_visitor_player1_id');
        $visitorPlayer2Id = $request->get('team_visitor_player2_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $teamLocal = $this->getCRStatsApi()->getTeam($teamLocalId);
        $teamVisitor = $this->getCRStatsApi()->getTeam($teamVisitorId);

        $data = $this->getStatsViewData([
            'local' => [
                'team' => $teamLocal,
                'players' => [
                    $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId,$localPlayer1Id),
                    $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId,$localPlayer2Id)
                ]
            ],
            'visitor' => [
                'team' => $teamVisitor,
                'players' => [
                    $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $visitorPlayer1Id),
                    $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $visitorPlayer2Id)
                ]
            ]
        ], $match, 'STATS 2VS2', 1);

        return $data;
    }

    /**
     * @param $teams array
     * @param $match
     * @param $title
     * @param $setNumber
     *
     * @return array
     */
    protected function getStatsViewData($teams, $match, $title, $setNumber)
    {
        $data = [];

        $setKey = $this->getSetKey($setNumber);

        foreach($teams as $key => $team)
        {
            $teamAttr = $team['team']['attributes'];

            $data['teams'][$key] = [
                'name' => $teamAttr['name'],
                'logo' => $teamAttr['logo']['original']
            ];

            foreach($team['players'] as $player)
            {
                $playerAttr = $player['attributes'];

                $playerStats = $player['attributes']['stats'][0];

                $data['teams'][$key]['players'][] =  [
                    'name' => $playerAttr['nickname'],
                    'photo' => $playerAttr['photo']['original'],
                    'win_ratio' => $playerStats['win_ratio'][$setKey],
                    'crown_average' => $playerStats['crown_average'][$setKey],
                    'win_condition' => $playerStats['win_condition']['key']
                ];
            }
        }

        $data['score_local'] = $match['attributes']['results']['score_local'];
        $data['score_visitor'] = $match['attributes']['results']['score_visitor'];

        $data['title'] = $title;

        return $data;
    }

    /**
     * @Route("/stats-1vs1-form", name="crl_stats_1vs1_form")
     * @Template("GrafismBundle:CRL:form/stats_1vs1_form.html.twig")
     */
    public function renderStatsOneVersusOneForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(StatsOneVersusOneForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_stats_1vs1'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Stats 1vs1',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @Route("/stats-2vs2-form", name="crl_stats_2vs2_form")
     * @Template("GrafismBundle:CRL:form/stats_2vs2_form.html.twig")
     */
    public function renderStatsTwoVersusTwoForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(StatsTwoVersusTwoForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_stats_2vs2'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Stats 2vs2',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $setNumber
     *
     * @return string
     */
    protected function getSetKey($setNumber)
    {
        switch($setNumber)
        {
            case 1:
                $key = '2v2';
                break;
            case 2:
                $key = '1v1';
                break;
            case 3:
                $key = 'koh';
                break;
            default:
                $key = 'competition';
        }

        return $key;
    }
}