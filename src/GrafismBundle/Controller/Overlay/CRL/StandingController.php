<?php
namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\FeaturedPlayerForm;
use GrafismBundle\Form\CRL\StandingsForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StandingController
 */
class StandingController extends OverlayController
{

    /**
     * @Route("/standings", name="crl_standings")
     * @Template("GrafismBundle:CRL:standings.html.twig")
     */
    public function renderStandings(Request $request)
    {
        $competitionId = $this->getCompetition();        
        $matchId = $request->get('match_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $ladder = $this->getCRStatsApi()->getCompetitionLadder($competitionId);
        
        return [
            'teams' => $ladder,
            'match' => $match,
        ];
    }

    /**
     * @Route("/standings-form", name="crl_standings_form")
     * @Template("GrafismBundle:CRL:form/standings_form.html.twig")
     */
    public function renderStandingsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();
        
        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);        
        $form = $this->createForm(StandingsForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_standings'),
            'competitionId' => $competitionId,
            'context' => $context
            ], [
            'name' => 'FF Clasificación',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);
        $name = $form->getConfig()->getOption('name');
        $formName = $form->getConfig()->getName();
        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }
}
