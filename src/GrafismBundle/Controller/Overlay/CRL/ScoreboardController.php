<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;

use GrafismBundle\Form\CRL\ScoreboardForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ScoreboardController
 */
class ScoreboardController extends OverlayController
{
    /**
     * @Route("/scoreboard", name="crl_scoreboard")
     */
    public function renderScoreboard(Request $request)
    {
        $matchId = $request->get('match_id');
        $setNumber = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getScoreboardDataView($match, $setNumber);

        $template = 'GrafismBundle:CRL:scoreboard.html.twig';

        if($setNumber == 0) {
            $template = 'GrafismBundle:CRL:scoreboard_general.html.twig';
        }

        return $this->render($template, $data);
    }

    /**
     * @param $match
     * @param $setNumber
     *
     * @return array
     */
    protected function getScoreboardDataView($match, $setNumber)
    {
        $attr = $match['attributes'];

        $localTeam = $attr['team_local'];
        $visitorTeam = $attr['team_visitor'];

        $data = [
            'local' => [
                'name' => $localTeam['name'],
                'shortname' => $localTeam['shortname'],
                'logo' => $localTeam['logo']['original'],
                'score' => $attr['results']['score_local']
            ],
            'visitor' => [
                'name' => $visitorTeam['name'],
                'shortname' => $visitorTeam['shortname'],
                'logo' => $visitorTeam['logo']['original'],
                'score' => $attr['results']['score_visitor']
            ]
        ];

        if($setNumber != 0) {
            $gamesData = $this->getCRStatsApi()->getAllMatchGames($match['id']);

            $games = [];
            foreach ($gamesData as $game) {
                $set = $game['attributes']['set'];
                $order = $game['attributes']['order'];

                if ($set == 5) {
                    continue;
                }

                $score = '-';

                $localScore = $game['attributes']['results']['score_100'];
                $visitorScore = $game['attributes']['results']['score_200'];

                if ($localScore > $visitorScore) {
                    $score = 'local';
                } else if ($visitorScore > $localScore) {
                    $score = 'visitor';
                }

                $games[$set][$order] = [
                    'score' => $score,
                    'label' => sprintf('PARTIDA %s', $order)
                ];
            }

            $games = $games[$setNumber];
        } else {
            $setResults = $attr['detailed_results'];

            $games = [];
            foreach($setResults as $key => $result)
            {
                $score = '-';

                $localScore = $result['team_local'];
                $visitorScore = $result['team_visitor'];

                if ($localScore > $visitorScore) {
                    $score = 'local';
                } else if ($visitorScore > $localScore) {
                    $score = 'visitor';
                }

                $games[$key] = [
                    'score' => $score,
                    'label' => sprintf('%s', $this->getSetLabel($key))
                ];
            }
        }

        $data['games'] = $games;
        $data['label'] = $this->getSetLabel($setNumber);
        $data['set'] = $setNumber;

        return $data;
    }

    /**
     * @Route("/scoreboard-form", name="crl_scoreboard_form")
     * @Template("GrafismBundle:CRL:form/scoreboard_form.html.twig")
     */
    public function renderScoreboardForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $choices = [
            '0' => 'Match',
            '1' => '2 vs 2',
            '2' => '1 vs 1 (Set 2)',
            '3' => '1 vs 1 (Set 3)',
            '4' => '1 vs 1 (Set 4)',
        ];

        $form = $this->createForm(ScoreboardForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_scoreboard'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'FF marcador 2v2 1v1 partido',
            'matches' => $matchesList,
            'sets' => $choices,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }

    /**
     * @param $setNumber
     *
     * @return string
     */
    private function getSetLabel($setNumber)
    {
        $label = '';

        switch($setNumber)
        {
            case 0:
                $label = 'Global Scoreboard';
                break;
            case 1:
                $label = '2v2';
                break;
            case 2:
            case 3:
            case 4:
                $label = '1v1';
                break;
            case 5:
                $label = 'Koh';
                break;
        }

        return $label;
    }
}