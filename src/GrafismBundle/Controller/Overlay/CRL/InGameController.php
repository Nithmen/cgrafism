<?php
namespace GrafismBundle\Controller\Overlay\CRL;

use Exception;
use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\FeaturedPlayerForm;
use GrafismBundle\Form\CRL\OverlayForm;
use GrafismBundle\Form\CRL\OverlayKohForm;
use GrafismBundle\Form\CRL\StandingsForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InGameController
 */
class InGameController extends OverlayController
{

    /**
     * @Route("/overlay", name="crl_overlay")
     * @Template("GrafismBundle:CRL:overlay.html.twig")
     *
     * @param $request
     *
     * @return array
     * @throws Exception
     */
    public function renderOverlay(Request $request)
    {
        $matchId = $request->get('match_id');
        $set = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getOverlayViewData($match, $set);

        return $data;
    }

    /**
     * @param $match
     * @param $currentSet
     *
     * @return array
     */
    protected function getOverlayViewData($match, $currentSet)
    {
        $data = [];

        $data['currentSet'] = $currentSet;

        $attr = $match['attributes'];

        $games = $this->getCRStatsApi()->getAllMatchGames($match['id']);

        $data['teams'] = [
            'local' => [
                'logo' => $attr['team_local']['logo']['original'],
                'shortname' => $attr['team_local']['shortname'],
                'score' => $attr['results']['score_local'],
                'ban' => $attr['banned_cards'][$currentSet][$attr['team_local']['id']]['card']['key']
            ],
            'visitor' => [
                'logo' => $attr['team_visitor']['logo']['original'],
                'shortname' => $attr['team_visitor']['shortname'],
                'score' => $attr['results']['score_visitor'],
                'ban' => $attr['banned_cards'][$currentSet][$attr['team_visitor']['id']]['card']['key']
            ]
        ];

        $data['currentGame'] = 1;
        foreach($games as $game)
        {
            $gameAttr = $game['attributes'];

            if($gameAttr['set'] == $currentSet) {

                $localPlayers = [];
                foreach($gameAttr['team_100']['players'] as $player)
                {
                    $localPlayers[] = [
                        'name' => $player['nickname'],
                        'photo' => $player['avatar']['original']
                    ];
                }

                $visitorPlayers = [];
                foreach($gameAttr['team_200']['players'] as $player)
                {
                    $visitorPlayers[] = [
                        'name' => $player['nickname'],
                        'photo' => $player['avatar']['original']
                    ];
                }

                $scoreLocal = $scoreVisitor = '-';

                if($gameAttr['results']['score_100'] > $gameAttr['results']['score_200']) {
                    $scoreLocal = 'win';
                    $scoreVisitor = 'loss';
                    $data['currentGame'] += 1;
                } else if($gameAttr['results']['score_100'] < $gameAttr['results']['score_200']) {
                    $scoreLocal = 'loss';
                    $scoreVisitor = 'win';
                    $data['currentGame'] += 1;
                }

                $data['games'][$gameAttr['order']] = [
                    'players' => [
                        'local' => $localPlayers,
                        'visitor' => $visitorPlayers
                    ],
                    'scores' => [
                        'local' => $scoreLocal,
                        'visitor' => $scoreVisitor
                    ]
                ];

            } else {
                $score = '-';
                if($gameAttr['results']['score_100'] > $gameAttr['results']['score_200']) {
                    $score = 'local';
                } else if ($gameAttr['results']['score_100'] < $gameAttr['results']['score_200']) {
                    $score = 'visitor';
                }
                $data['results'][$gameAttr['set']][$gameAttr['order']] = $score;

            }
        }

        for($set = 1; $set <= 5; $set++)
        {
            if($set == $currentSet) {
                continue;
            }

            $data['results'][$set] = array_count_values($data['results'][$set]);
        }

        return $data;
    }

    /**
     * @Route("/overlay-form", name="crl_overlay_form")
     * @Template("GrafismBundle:CRL:form/overlay_form.html.twig")
     */
    public function renderOverlayForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();
        
        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);       

        $form = $this->createForm(OverlayForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_overlay'),
            'competitionId' => $competitionId,
            'context' => $context
            ], [
            'name' => 'Ingame 2v2 1v1',
            'matches' => $matchesList,
            'sets' => 4,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);
        
        $name = $form->getConfig()->getOption('name');
        $formName = $form->getConfig()->getName();
        
        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }



    /**
     * @Route("/overlay-koh", name="crl_overlay_koh")
     * @Template("GrafismBundle:CRL:overlay_koh.html.twig")
     *
     * @param $request
     *
     * @return array
     * @throws Exception
     */
    public function renderOverlayKoh(Request $request)
    {
        $matchId = $request->get('match_id');

        $teamLocalId = $request->get('team_local_id');
        $playerLocal1Id = $request->get('team_local_player1_id');
        $playerLocal2Id = $request->get('team_local_player2_id');
        $playerLocal3Id = $request->get('team_local_player3_id');
        $playerLocal1Ch = $request->get('team_local_player1_ch');
        $playerLocal2Ch = $request->get('team_local_player2_ch');
        $playerLocal3Ch = $request->get('team_local_player3_ch');

        $teamVisitorId = $request->get('team_visitor_id');
        $playerVisitor1Id = $request->get('team_visitor_player1_id');
        $playerVisitor2Id = $request->get('team_visitor_player2_id');
        $playerVisitor3Id = $request->get('team_visitor_player3_id');
        $playerVisitor1Ch = $request->get('team_visitor_player1_ch');
        $playerVisitor2Ch = $request->get('team_visitor_player2_ch');
        $playerVisitor3Ch = $request->get('team_visitor_player3_ch');

        $teamLocal = $this->getCRStatsApi()->getTeam($teamLocalId);
        $teamVisitor = $this->getCRStatsApi()->getTeam($teamVisitorId);

        $localData = [
            'team' => $teamLocal,
            'players' => [
                'player_1' => [
                    'id' => $playerLocal1Id,
                    'ch' => $playerLocal1Ch
                ],
                'player_2' => [
                    'id' => $playerLocal2Id,
                    'ch' => $playerLocal2Ch
                ],
                'player_3' => [
                    'id' => $playerLocal3Id,
                    'ch' => $playerLocal3Ch
                ]
            ]
        ];

        $visitorData = [
            'team' => $teamVisitor,
            'players' => [
                'player_1' => [
                    'id' => $playerVisitor1Id,
                    'ch' => $playerVisitor1Ch
                ],
                'player_2' => [
                    'id' => $playerVisitor2Id,
                    'ch' => $playerVisitor2Ch
                ],
                'player_3' => [
                    'id' => $playerVisitor3Id,
                    'ch' => $playerVisitor3Ch
                ]
            ]
        ];

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $teams = [
            'local' => $localData,
            'visitor' => $visitorData
        ];

        $data = $this->getOverlayKohViewData($teams, $match);

        return $data;
    }

    /**
     * @param $teams
     * @param $match
     *
     * @return array
     */
    protected function getOverlayKohViewData($teams, $match)
    {
        $data = [];

        $data['currentSet'] = $currentSet = 5;

        $attr = $match['attributes'];

        $games = $this->getCRStatsApi()->getAllMatchGames($match['id']);

        $data['teams'] = [
            'local' => [
                'logo' => $attr['team_local']['logo']['original'],
                'shortname' => $attr['team_local']['shortname'],
                'score' => $attr['results']['score_local'],
                'ban' => $attr['banned_cards'][$currentSet][$attr['team_local']['id']]['card']['key']
            ],
            'visitor' => [
                'logo' => $attr['team_visitor']['logo']['original'],
                'shortname' => $attr['team_visitor']['shortname'],
                'score' => $attr['results']['score_visitor'],
                'ban' => $attr['banned_cards'][$currentSet][$attr['team_visitor']['id']]['card']['key']
            ]
        ];

        $data['currentGame'] = 1;
        foreach($games as $game)
        {
            $gameAttr = $game['attributes'];

            if($gameAttr['set'] == $currentSet) {

                $localPlayers = [];
                foreach($gameAttr['team_100']['players'] as $player)
                {
                    $localPlayers[] = [
                        'name' => $player['nickname']
                    ];
                }

                $visitorPlayers = [];
                foreach($gameAttr['team_200']['players'] as $player)
                {
                    $visitorPlayers[] = [
                        'name' => $player['nickname']
                    ];
                }

                $data['games'][$gameAttr['order']] = [
                    'players' => [
                        'local' => $localPlayers,
                        'visitor' => $visitorPlayers
                    ]
                ];

            } else {
                $score = '-';

                if($gameAttr['results']['score_100'] > $gameAttr['results']['score_200']) {
                    $score = 'local';
                } else if ($gameAttr['results']['score_100'] < $gameAttr['results']['score_200']) {
                    $score = 'visitor';
                }

                $data['results'][$gameAttr['set']][$gameAttr['order']] = $score;
            }
        }

        // Photos
        foreach($teams as $side => $team)
        {
            foreach($team['players'] as $player)
            {
                $playerData = $this->getCRStatsApi()->getCompetitionPlayerMetrics($attr['competition'], $player['id']);

                if($player['ch'] == 0) {
                    $data['currentGame'] += 1;
                }

                $data['teams'][$side]['players'][] = [
                    'name' => $playerData['attributes']['nickname'],
                    'alive' => $player['ch'],
                    'photo' => $playerData['attributes']['avatar']['original']
                ];
            }
        }

        // Set results
        for($set = 1; $set <= 5; $set++)
        {
            if($set == $currentSet) {
                continue;
            }

            $data['results'][$set] = array_count_values($data['results'][$set]);
        }

        return $data;
    }

    /**
     * @Route("/overlay-koh-form", name="crl_overlay_koh_form")
     * @Template("GrafismBundle:CRL:form/overlay_koh_form.html.twig")
     */
    public function renderOverlayKohForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(OverlayKohForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_overlay_koh'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Ingame koh',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}
