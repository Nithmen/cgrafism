<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;

use GrafismBundle\Form\CRL\LowerTeamsWinrateForm;
use GrafismBundle\Form\CRL\OverlayForm;
use GrafismBundle\Form\CRL\ScoreboardBansForm;
use GrafismBundle\Form\CRL\ScoreboardCountdownForm;
use GrafismBundle\Form\CRL\StandingsForm;
use GrafismBundle\Form\Type\CardsPlayedType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CRLController
 */
class CRLController extends OverlayController
{
    /**
     * @Route("/scoreboard-countdown", name="crl_scoreboard_countdown")
     * @Template("GrafismBundle:CRL:scoreboard_countdown.html.twig")
     *
     * @return array
     * @throws \Exception
     */
    public function renderScoreboardCountdown()
    {
        $matchId = $this->getRequest()->query->get('match_id');

        $match = $this->getResource($this->getContext(), 'matches', $matchId);
        $match = array_shift($match);

        $fetcher = $this->getResourceFetcher();
        $matchUrl = $fetcher->getResourceUrl($this->getContext(), 'matches', $matchId);

        $games = $this->getChildResource($matchUrl, 'games');

        $games = array_shift($games);

        $results = $match['attributes']['results'];

        $currentSet = $results['score_local'] + $results['score_visitor'];

        $gamesBySet = [];
        foreach($games as $game)
        {
            $set = $game['attributes']['set'];

            if(!isset($gamesBySet[$set]['score_local'])) {
                $gamesBySet[$set]['score_local'] = 0;
                $gamesBySet[$set]['score_visitor'] = 0;
            }

            $localScore = $game['attributes']['results']['score_100'];
            $visitorScore = $game['attributes']['results']['score_200'];

            if($localScore > $visitorScore) {
                $gamesBySet[$set]['score_local'] += 1;
            } else if ($localScore < $visitorScore) {
                $gamesBySet[$set]['score_visitor'] += 1;
            }

            $gamesBySet[$set][] = $game;
        }

        $minutes = $this->getRequest()->query->get('minutes', 3);
        $seconds = $this->getRequest()->query->get('seconds', 0);

        return [
            'match' => $match,
            'sets' => $gamesBySet,
            'currentSet' => $currentSet,
            'minutes' => $minutes,
            'seconds' => $seconds
        ];
    }

    /**
     * @Route("/scoreboard-countdown-form", name="crl_scoreboard_countdown_form")
     * @Template("GrafismBundle:CRL:form/scoreboard_countdown_form.html.twig")
     */
    public function renderScoreboardCountdownForm()
    {
        $id = $this->getCompetition();
        $context = $this->getContext();

        $fetcher = $this->getResourceFetcher();
        $competitionUrl = $fetcher->getResourceUrl($context, 'competitions', $id);

        $data = $this->getChildResource($competitionUrl, 'matches', 'round');

        $data = array_shift($data);

        $matches = [];
        foreach($data as $match)
        {
            $attr = $match['attributes'];

            $key = sprintf('%s - %s vs %s', $attr['label'], $attr['team_local']['name'], $attr['team_visitor']['name']);
            $matches[$key] = $match['id'];
        }

        $form = $this->createForm(ScoreboardCountdownForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_scoreboard_countdown'),
            'competitionId' => $id,
            'context' => $context
        ], [
            'name' => 'Scoreboard Countdown',
            'matches' => $matches,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }

    /**
     * @Route("/scoreboard-bans", name="crl_scoreboard_bans")
     * @Template("GrafismBundle:CRL:scoreboard_bans.html.twig")
     *
     * @return array
     * @throws \Exception
     */
    public function renderScoreboardBans()
    {
        $matchId = $this->getRequest()->query->get('match_id');

        $match = $this->getResource($this->getContext(), 'matches', $matchId);
        $match = array_shift($match);

        $fetcher = $this->getResourceFetcher();
        $matchUrl = $fetcher->getResourceUrl($this->getContext(), 'matches', $matchId);

        $games = $this->getChildResource($matchUrl, 'games');

        $games = array_shift($games);

        $results = $match['attributes']['results'];

        $currentSet = $results['score_local'] + $results['score_visitor'];

        $localCrowns = 0;
        $visitorCrowns = 0;

        $gamesBySet = [];
        foreach($games as $game)
        {
            $set = $game['attributes']['set'];

            if(!isset($gamesBySet[$set]['score_local'])) {
                $gamesBySet[$set]['score_local'] = 0;
                $gamesBySet[$set]['score_visitor'] = 0;
            }

            $localScore = $game['attributes']['results']['score_100'];
            $visitorScore = $game['attributes']['results']['score_200'];

            if($localScore > $visitorScore) {
                $gamesBySet[$set]['score_local'] += 1;
            } else if ($localScore < $visitorScore) {
                $gamesBySet[$set]['score_visitor'] += 1;
            }

            $gamesBySet[$set][] = $game;

            $localCrowns += $localScore;
            $visitorCrowns += $visitorScore;
        }

        $crowns = [
            'team_local' => $localCrowns,
            'team_visitor' => $visitorCrowns
        ];

        return [
            'match' => $match,
            'sets' => $gamesBySet,
            'currentSet' => $currentSet,
            'crowns' => $crowns
        ];
    }

    /**
     * @Route("/scoreboard-bans-form", name="crl_scoreboard_bans_form")
     * @Template("GrafismBundle:CRL:form/scoreboard_bans_form.html.twig")
     */
    public function renderScoreboardBansForm()
    {
        $id = $this->getCompetition();
        $context = $this->getContext();

        $fetcher = $this->getResourceFetcher();
        $competitionUrl = $fetcher->getResourceUrl($context, 'competitions', $id);

        $data = $this->getChildResource($competitionUrl, 'matches', 'round');

        $data = array_shift($data);

        $matches = [];
        foreach($data as $match)
        {
            $attr = $match['attributes'];

            $key = sprintf('%s - %s vs %s', $attr['label'], $attr['team_local']['name'], $attr['team_visitor']['name']);
            $matches[$key] = $match['id'];
        }

        $form = $this->createForm(ScoreboardBansForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_scoreboard_bans'),
            'competitionId' => $id,
            'context' => $context
        ], [
            'name' => 'Scoreboard Bans',
            'matches' => $matches,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }
}