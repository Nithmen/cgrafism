<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Type\CardsPlayedType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LowerCardsController
 */
class LowerCardsController extends OverlayController
{
    /**
     * @Route("/cards-played", name="crl_cards_played")
     * @Template("GrafismBundle:CRL:cards_played.html.twig")
     *
     * @return array
     * @throws \Exception
     */
    public function renderCardsPlayed(Request $request)
    {
        $competitionId = $this->getCompetition();
        $playerId = $request->get('player_id');
        $matchId = $request->get('match_id');
        $teamId = $request->get('team_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $playerMetrics = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $playerId);



        $data = $this->getCardsPlayedViewData($playerMetrics, $match, $teamId);

        return [
            'data' => $data
        ];
    }

    /**
     * @param $playerMetrics
     * @param $match
     * @param $teamId
     *
     * @return array
     */
    private function getCardsPlayedViewData($playerMetrics, $match, $teamId)
    {
        $stats = $playerMetrics['attributes']['stats'][0];
        $player = $playerMetrics['attributes'];

        unset($player['stats']);

        $localTeam = $match['attributes']['team_local']['id'];

        ($localTeam == $teamId) ? $side = 100 : $side = 200;

        $data = [
            'player' => $player,
            'stats' => $stats,
            'side' => $side
        ];

        return $data;
    }

    /**
     * @Route("/cards-played-form", name="crl_cards_played_form")
     * @Template("GrafismBundle:CRL:form/cards_played_form.html.twig")
     */
    public function renderCardsPlayedForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(CardsPlayedType::class, [
            'url' => $this->generateAbsoluteUrl('crl_cards_played'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Played Cards',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}