<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\ElixirForm;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ElixirController
 */
class ElixirController extends OverlayController
{
    /**
     * @Route("/elixir", name="crl_elixir")
     * @Template("GrafismBundle:CRL:elixir.html.twig")
     *
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function renderElixir(Request $request)
    {
        $matchId = $request->get('match_id');
        $gameId = $request->get('game_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $game = $this->getCRStatsApi()->getGame($gameId);

        $data = $this->getElixirDataView($match,$game);

        return $data;
    }

    /**
     * @param $match
     * @param $game
     *
     * @return array
     */
    protected function getElixirDataView($match, $game)
    {
        $results = $match['attributes']['results'];
        $localTeamId = $match['attributes']['team_local']['id'];
        $visitorTeamId = $match['attributes']['team_visitor']['id'];

        $set = $game['attributes']['set'];

        $attr = $match['attributes'];

        $data['teams'] = [
            'local' => [
                'score' => $results['score_local'],
                'ban' => $attr['banned_cards'][$set][$localTeamId]['card']['key'],
                'name' => $attr['team_local']['name'],
                'logo' => $attr['team_local']['logo']['original']
            ],
            'visitor' => [
                'score' => $results['score_local'],
                'ban' => $attr['banned_cards'][$set][$visitorTeamId]['card']['key'],
                'name' => $attr['team_visitor']['name'],
                'logo' => $attr['team_visitor']['logo']['original']
            ]
        ];

        $data['set'] = $set;
        $data['game'] = $game['attributes']['order'];

        $localPlayers = $visitorPlayers = [];

        $playersSize = count($game['attributes']['team_100']['players']);
        for($i = 0; $i < $playersSize; $i++)
        {
            $localPlayers[] = [
              'name' =>   $game['attributes']['team_100']['players'][$i]['nickname']
            ];

            $visitorPlayers[] = [
                'name' =>   $game['attributes']['team_200']['players'][$i]['nickname']
            ];
        }

        $data['teams']['local']['players'] = $localPlayers;
        $data['teams']['visitor']['players'] = $visitorPlayers;

        return $data;
    }

    /**
     * @Route("/elixir-form", name="crl_elixir_form")
     * @Template("GrafismBundle:CRL/form:elixir_form.html.twig")
     */
    public function renderElixirForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(ElixirForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_elixir'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Elixir',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }
}