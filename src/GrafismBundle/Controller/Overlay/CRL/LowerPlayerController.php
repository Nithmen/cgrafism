<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\LowerPlayerForm;
use GrafismBundle\Form\CRL\LowerPlayerVersus2v2Form;
use GrafismBundle\Form\CRL\LowerPlayerVersusForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LowerPlayerController
 */
class LowerPlayerController extends OverlayController
{
    /**
     * @Route("/lower-player", name="crl_lower_player")
     * @Template("GrafismBundle:CRL:lower_player.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderLowerPlayer(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');
        $teamId = $request->get('team_id');
        $playerId = $request->get('player_id');
        $setId = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $player = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $playerId);
        $team = $this->getCRStatsApi()->getTeam($teamId);

        $data = $this->getLowerPlayerViewData($match, $player, $team, $setId);

        return $data;
    }

    /**
     * @param $match
     * @param $player
     * @param $team
     * @param $set
     *
     * @return array
     */
    protected function getLowerPlayerViewData($match, $player, $team, $set)
    {
        $playerStats = $player['attributes']['stats'][0];

        $key = $this->getSetKey($set);

        $teamAttr = $team['attributes'];

        $data = [
          'player' => [
              'name' => $player['attributes']['nickname'],
              'photo' => $player['attributes']['photo']['original'],
              'win_ratio' => $playerStats['win_ratio'][$key],
              'crown_average' => $playerStats['crown_average'][$key],
              'ban' => $match['attributes']['banned_cards'][$set][$team['id']]['card']['key']
          ],
            'team' => [
                'name' => $teamAttr['name'],
                'logo' => $teamAttr['logo']['original'],
                'side' => $this->getTeamMatchSide($match, $team)
            ],
            'match' => [
                'label' => sprintf('SET %s - %s', $set, $key)
            ]
        ];

        return $data;
    }

    /**
     * @Route("/lower-player-form", name="crl_lower_player_form")
     * @Template("GrafismBundle:CRL/form:lower_player_form.html.twig")
     *
     *
     * @return LowerPlayerForm
     */
    public function renderLowerPlayerForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LowerPlayerForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_lower_player'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower Player',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    // VERSUS 1V1

    /**
     * @Route("/lower-player-versus", name="crl_lower_player_versus")
     * @Template("GrafismBundle:CRL:lower_player_versus.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderPlayerVersus(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');

        $localPlayerId = $request->get('team_local_player1_id');
        $visitorPlayerId = $request->get('team_visitor_player1_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $set = $request->get('set_id');

        $data = $this->getPlayerVersusViewData($match, $localPlayerId, $visitorPlayerId, $competitionId, $set);

        return $data;
    }

    /**
     * @param $match
     * @param $localPlayerId
     * @param $visitorPlayerId
     * @param $competitionId
     * @param $set
     *
     * @return array
     */
    protected function getPlayerVersusViewData($match, $localPlayerId, $visitorPlayerId, $competitionId, $set)
    {
        $localTeam = $match['attributes']['team_local'];
        $visitorTeam = $match['attributes']['team_visitor'];

        $localPlayer = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $localPlayerId);
        $visitorPlayer = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $visitorPlayerId);

        $data = [
            'local' => [
                'name' => $localTeam['name'],
                'player' => [
                    'name' => $localPlayer['attributes']['nickname'],
                    'photo' => $localPlayer['attributes']['photo']['original']
                ],
                'ban' => $match['attributes']['banned_cards'][$set][$localTeam['id']]['card']['key']
            ],
            'visitor' => [
                'name' => $visitorTeam['name'],
                'player' => [
                    'name' => $visitorPlayer['attributes']['nickname'],
                    'photo' => $visitorPlayer['attributes']['photo']['original']
                ],
                'ban' => $match['attributes']['banned_cards'][$set][$visitorTeam['id']]['card']['key']
            ]
        ];

        return $data;
    }

    /**
     * @Route("/lower-player-versus-form", name="crl_lower_player_versus_form")
     * @Template("GrafismBundle:CRL/form:lower_player_versus_form.html.twig")
     *
     *
     * @return LowerPlayerVersusForm
     */
    public function renderLowerPlayerVersusForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LowerPlayerVersusForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_lower_player_versus'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower Bans 1v1',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    // VERSUS 2V2

    /**
     * @Route("/lower-player-versus-2v2", name="crl_lower_player_versus_2v2")
     * @Template("GrafismBundle:CRL:lower_player_versus_2v2.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderPlayerVersus2v2(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');

        $localPlayer1Id = $request->get('team_local_player1_id');
        $localPlayer2Id = $request->get('team_local_player2_id');

        $visitorPlayer1Id = $request->get('team_visitor_player1_id');
        $visitorPlayer2Id = $request->get('team_visitor_player2_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $set = 1;

        $data = $this->getPlayerVersus2v2ViewData($match, $localPlayer1Id, $visitorPlayer1Id, $localPlayer2Id, $visitorPlayer2Id, $competitionId, $set);

        return $data;
    }

    /**
     * @param $match
     * @param $localPlayer1Id
     * @param $visitorPlayer1Id
     * @param $localPlayer2Id
     * @param $visitorPlayer2Id
     * @param $competitionId
     * @param $set
     *
     * @return array
     */
    protected function getPlayerVersus2v2ViewData($match, $localPlayer1Id, $visitorPlayer1Id,$localPlayer2Id, $visitorPlayer2Id, $competitionId, $set)
    {
        $localTeam = $match['attributes']['team_local'];
        $visitorTeam = $match['attributes']['team_visitor'];

        $localPlayer = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $localPlayer1Id);
        $visitorPlayer = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $visitorPlayer1Id);

        $localPlayer2 = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $localPlayer2Id);
        $visitorPlayer2 = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $visitorPlayer2Id);

        $data = [
            'local' => [
                'name' => $localTeam['name'],
                'player' => [
                    'name' => $localPlayer['attributes']['nickname'],
                    'photo' => $localPlayer['attributes']['photo']['original']
                ],
                'player_2' => [
                    'name' => $localPlayer2['attributes']['nickname'],
                    'photo' => $localPlayer2['attributes']['photo']['original']
                ],
                'ban' => $match['attributes']['banned_cards'][$set][$localTeam['id']]['card']['key']
            ],
            'visitor' => [
                'name' => $visitorTeam['name'],
                'player' => [
                    'name' => $visitorPlayer['attributes']['nickname'],
                    'photo' => $visitorPlayer['attributes']['photo']['original']
                ],
                'player_2' => [
                    'name' => $visitorPlayer2['attributes']['nickname'],
                    'photo' => $visitorPlayer2['attributes']['photo']['original']
                ],
                'ban' => $match['attributes']['banned_cards'][$set][$visitorTeam['id']]['card']['key']
            ]
        ];

        return $data;
    }

    /**
     * @Route("/lower-player-versus-2v2-form", name="crl_lower_player_versus_2v2_form")
     * @Template("GrafismBundle:CRL/form:lower_player_versus_2v2_form.html.twig")
     *
     *
     * @return array
     */
    public function renderLowerPlayerVersus2v2Form()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LowerPlayerVersus2v2Form::class, [
            'url' => $this->generateAbsoluteUrl('crl_lower_player_versus_2v2'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower bans 2v2',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    // KOH

    /**
     * @Route("/lower-player-versus-koh", name="crl_lower_player_versus_koh")
     * @Template("GrafismBundle:CRL:lower_player_versus_koh.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderPlayerVersusKoh(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');

        $localPlayerId = $request->get('team_local_player1_id');
        $visitorPlayerId = $request->get('team_visitor_player1_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $set = 5;

        $data = $this->getPlayerVersusKohViewData($match, $localPlayerId, $visitorPlayerId, $competitionId, $set);

        return $data;
    }

    /**
     * @param $match
     * @param $localPlayerId
     * @param $visitorPlayerId
     * @param $competitionId
     * @param $set
     *
     * @return array
     */
    protected function getPlayerVersusKohViewData($match, $localPlayerId, $visitorPlayerId, $competitionId, $set)
    {
        $localTeam = $match['attributes']['team_local'];
        $visitorTeam = $match['attributes']['team_visitor'];

        $localPlayer = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $localPlayerId);
        $visitorPlayer = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $visitorPlayerId);

        $data = [
            'local' => [
                'name' => $localTeam['name'],
                'shortname' => $localTeam['shortname'],
                'player' => [
                    'name' => $localPlayer['attributes']['nickname'],
                    'photo' => $localPlayer['attributes']['photo']['original']
                ],
                'ban' => $match['attributes']['banned_cards'][$set][$localTeam['id']]['card']['key'],
                'logo' => $match['attributes']['team_local']['logo']['original']
            ],
            'visitor' => [
                'name' => $visitorTeam['name'],
                'shortname' => $visitorTeam['shortname'],
                'player' => [
                    'name' => $visitorPlayer['attributes']['nickname'],
                    'photo' => $visitorPlayer['attributes']['photo']['original']
                ],
                'ban' => $match['attributes']['banned_cards'][$set][$visitorTeam['id']]['card']['key'],
                'logo' => $match['attributes']['team_visitor']['logo']['original']
            ]
        ];

        return $data;
    }

    /**
     * @Route("/lower-player-versus-koh-form", name="crl_lower_player_versus_koh_form")
     * @Template("GrafismBundle:CRL/form:lower_player_versus_form.html.twig")
     *
     *
     * @return array
     */
    public function renderLowerPlayerVersusKohForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LowerPlayerVersusForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_lower_player_versus_koh'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower Bans Koh',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $match
     * @param $team
     *
     * @return int
     */
    protected function getTeamMatchSide($match, $team)
    {
        $attributes = $match['attributes'];
        return ($attributes['team_local']['id'] == $team['id']) ? 100 : 200;
    }

    /**
     * @param $setNumber
     *
     * @return string
     */
    protected function getSetKey($setNumber)
    {
        switch($setNumber)
        {
            case 1:
                $key = '2v2';
                break;
            case 2:
            case 3:
            case 4:
                $key = '1v1';
                break;
            case 5:
                $key = 'koh';
                break;
            default:
                $key = 'competition';
        }

        return $key;
    }
}