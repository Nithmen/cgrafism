<?php
namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\FeaturedPlayerForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FeaturedController
 */
class FeaturedController extends OverlayController
{

    /**
     * @Route("/featured-player", name="crl_featured_player")
     * @Template("GrafismBundle:CRL:featured_player.html.twig")     
     */
    public function renderFeaturedPlayer(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');
        $teamId = $request->get('team_id');
        $playerId = $request->get('player_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);
        $team = $this->getCRStatsApi()->getTeam($teamId);
        $player = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $playerId);

        return $this->getFeaturedPlayerViewData($match, $team, $player);
    }

    protected function getFeaturedPlayerViewData($match,$team, $player)
    {    
        $data = [];        
        $data['side'] = $this->getTeamMatchSide($match, $team);        
        $attributes = $team['attributes'];
        $data['team'] = [
            'name' => $attributes['name'],
            'logo' => $attributes['logo']['original']
        ];
        $attributes = $player['attributes']; 
        $data['player'] = [
            'name' => $attributes['nickname'],
            'photo' => $attributes['photo']['original'],
            'stats' => $this->getPlayerMetricsDTO($attributes['stats'][0])
        ];

        return $data;
    }
    
    protected function getPlayerMetricsDTO($stats)
    {
        return [
            'win_ratio' => $stats['win_ratio']['competition'],
            'crown_average' => $stats['crown_average']['competition'],
            'win_condition'=> isset($stats['win_condition']['key']) ?: null
        ];
    }
    
    protected function getTeamMatchSide($match, $team)
    {
        $attributes = $match['attributes'];
        return ($attributes['team_local']['id'] == $team['id']) ? 100 : 200;
    }

    /**
     * @Route("/featured-player-form", name="crl_featured_player_form")
     * @Template("GrafismBundle:CRL:form/featured_player_form.html.twig")
     */
    public function renderFeaturedPlayerForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(FeaturedPlayerForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_featured_player'),
            'competitionId' => $competitionId,
            'context' => $context
            ], [
            'name' => 'Featured player',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');
        $formName = $form->getConfig()->getName();
        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}
