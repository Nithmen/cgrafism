<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\PlayerCardForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PlayerCardController
 */
class PlayerCardController extends OverlayController
{
    /**
     * @Route("/player-card", name="crl_player_card")
     * @Template("GrafismBundle:CRL:player_card.html.twig")
     *
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function renderPlayerCard(Request $request)
    {
        $competitionId = $this->getCompetition();
        $teamId = $request->get('team_id');
        $playerId = $request->get('player_id');
        $matchId = $request->get('match_id');

        $playerMetrics = $this->getCRStatsApi()->getCompetitionPlayerMetrics($competitionId, $playerId);
        $team = $this->getCRStatsApi()->getTeam($teamId);
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getPlayerCardViewData($team, $playerMetrics, $match);

        return $data;
    }

    /**
     * @param $team
     * @param $playerMetrics
     * @param $match
     *
     * @return array|mixed
     */
    protected function getPlayerCardViewData($team, $playerMetrics, $match)
    {
        $data = [];

        $data['team']['logo'] = $team['attributes']['logo']['original'];

        $metricsAttributes = $playerMetrics['attributes']['stats'][0]['overall'];


        $cardsUsed = [];
        for($i = 0; $i < 3; $i++)
        {
            $cardsUsed[] = $playerMetrics['attributes']['stats'][0]['cards_used'][$i];
        }

        $data['player'] = [
            'name' => $playerMetrics['attributes']['nickname'],
            'stats' => [
                'koh' => $metricsAttributes['koh'],
                '1v1' => $metricsAttributes['1v1'],
                '2v2' => $metricsAttributes['2v2']
            ],
            'cards_used' => $cardsUsed
        ];

        $data['side'] = $this->getTeamMatchSide($match, $team);

        return $data;
    }

    /**
     * @Route("/player-card-form", name="crl_player_card_form")
     * @Template("GrafismBundle:CRL:form/player_card_form.html.twig")
     *
     * @return array
     * @throws \Exception
     */
    public function renderPlayerCardForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);
        $form = $this->createForm(PlayerCardForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_player_card'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lower cartas más jugadas',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');
        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $match
     * @param $team
     *
     * @return int
     */
    protected function getTeamMatchSide($match, $team)
    {
        $attributes = $match['attributes'];
        return ($attributes['team_local']['id'] == $team['id']) ? 100 : 200;
    }
}