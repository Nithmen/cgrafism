<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\KoHLineupForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KohController
 */
class KohController extends OverlayController
{
    /**
     * @Route("/koh-lineup", name="crl_koh_lineup")
     * @Template("GrafismBundle:CRL:koh_lineup.html.twig")
     *
     * @return array
     * @throws \Exception
     */
    public function renderKoHLineup(Request $request)
    {
        $competitionId = $this->getCompetition();

        $matchId = $request->get('match_id');

        $teamLocalId = $request->get('team_local_id');
        $playerLocal1Id = $request->get('team_local_player1_id');
        $playerLocal2Id = $request->get('team_local_player2_id');
        $playerLocal3Id = $request->get('team_local_player3_id');
        $playerLocal1Ch = $request->get('team_local_player1_ch');
        $playerLocal2Ch = $request->get('team_local_player2_ch');
        $playerLocal3Ch = $request->get('team_local_player3_ch');

        $teamVisitorId = $request->get('team_visitor_id');
        $playerVisitor1Id = $request->get('team_visitor_player1_id');
        $playerVisitor2Id = $request->get('team_visitor_player2_id');
        $playerVisitor3Id = $request->get('team_visitor_player3_id');
        $playerVisitor1Ch = $request->get('team_visitor_player1_ch');
        $playerVisitor2Ch = $request->get('team_visitor_player2_ch');
        $playerVisitor3Ch = $request->get('team_visitor_player3_ch');

        $teamLocal = $this->getCRStatsApi()->getTeam($teamLocalId);
        $teamVisitor = $this->getCRStatsApi()->getTeam($teamVisitorId);

        $localData = [
            'team' => $teamLocal,
            'players' => [
                'player_1' => [
                    'id' => $playerLocal1Id,
                    'ch' => $playerLocal1Ch
                ],
                'player_2' => [
                    'id' => $playerLocal2Id,
                    'ch' => $playerLocal2Ch
                ],
                'player_3' => [
                    'id' => $playerLocal3Id,
                    'ch' => $playerLocal3Ch
                ]
            ]
        ];

        $visitorData = [
            'team' => $teamVisitor,
            'players' => [
                'player_1' => [
                    'id' => $playerVisitor1Id,
                    'ch' => $playerVisitor1Ch
                ],
                'player_2' => [
                    'id' => $playerVisitor2Id,
                    'ch' => $playerVisitor2Ch
                ],
                'player_3' => [
                    'id' => $playerVisitor3Id,
                    'ch' => $playerVisitor3Ch
                ]
            ]
        ];

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $teams = [
            'local' => $localData,
            'visitor' => $visitorData
        ];

        $data = $this->getKohLineupViewData($teams, $match);

        return [
            'data' => $data
        ];
    }

    /**
     * @param $teams
     * @return array
     */
    protected function getKohLineupViewData($teams, $match)
    {
        $data = [];
        foreach($teams as $key => $team)
        {
            $data[$key] = $team['team']['attributes'];

            foreach($team['players'] as $player)
            {
                $playerData = $this->getCRStatsApi()->getCompetitionPlayerMetrics($match['attributes']['competition'], $player['id']);

                $data[$key]['players'][] = [
                  'alive' => (int)$player['ch'],
                  'data' => $playerData['attributes']
                ];
            }
        }

        $results = $match['attributes']['results'];

        $data['match'] = [
            'score_local' => $results['score_local'],
            'score_visitor' => $results['score_visitor'],
            'round' => $match['attributes']['round']
        ];

        return $data;
    }

    /**
     * @Route("/koh-lineup-form", name="crl_koh_lineup_form")
     * @Template("GrafismBundle:CRL:form/koh_lineup_form.html.twig")
     */
    public function renderKohLineupForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(KoHLineupForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_koh_lineup'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'KoH marcador / lineup',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}