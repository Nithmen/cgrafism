<?php

namespace GrafismBundle\Controller\Overlay\CRL;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\ReplaysForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReplaysController
 */
class ReplaysController extends OverlayController
{

    /**
     * @Route("/replays", name="crl_replays")
     * @Template("GrafismBundle:CRL:replays.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderReplays(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getReplayViewData($match);

        return $data;
    }

    /**
     * @param $match
     *
     * @return array
     */
    public function getReplayViewData($match)
    {
        $localTeam = $match['attributes']['team_local'];
        $visitorTeam = $match['attributes']['team_visitor'];

        $data = [
            'local' =>  [
                'name' => $localTeam['name'],
                'shortname' => $localTeam['shortname'],
                'logo' => $localTeam['logo']['original']
            ],
            'visitor' => [
                'name' => $visitorTeam['name'],
                'shortname' => $visitorTeam['shortname'],
                'logo' => $visitorTeam['logo']['original']
            ]
        ];


        return $data;
    }

    /**
     * @Route("/replays-form", name="crl_replays_form")
     * @Template("GrafismBundle:CRL/form:replays_form.html.twig")
     *
     * @return array
     */
    public function renderReplaysForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(ReplaysForm::class, [
            'url' => $this->generateAbsoluteUrl('crl_replays'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'PiP Repeticiones',
            'matches' => $matchesList,
            'locales' => [
                'Spanish' => 'es',
                'English' => 'en',
                'Portuguese' => 'pt'
            ]
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}