<?php

namespace GrafismBundle\Controller\Overlay\SLO\CounterStrike;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\CounterStrike\PredictionsForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PredictionsController
 */
class PredictionsController extends OverlayController
{
    /**
     * @Route("/predictions", name="slo_csgo_predictions")
     * @Template("@Grafism/SLO/csgo/predictions.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderPredictions(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCSStatsApi()->getMatch($matchId);

        $casterVotes = [];
        foreach($request->query->all() as $key => $param)
        {
            if(preg_match('~^caster_~', $key)) {
                $casterVotes[$key] = $param;
            }
        }

        $data = $this->getPredictionsViewData($match, $casterVotes);

        return $data;
    }

    private function getPredictionsViewData($match, $casterVotes)
    {
        $attr = $match['match'];

        $localId = $attr['teams']['local']['team']['id'];
        $visitorId = $attr['teams']['visitor']['team']['id'];

        $data = [];

        $data['local']['team'] = $this->getTeamDto($attr['teams']['local']['team']);
        $data['visitor']['team'] = $this->getTeamDto($attr['teams']['visitor']['team']);

        foreach($casterVotes as $key => $vote)
        {
            $casterVote = 'none';

            if($vote == $visitorId) {
                $casterVote = 'visitor';
            } else if($vote == $localId) {
                $casterVote = 'local';
            }

            $data['votes'][$key] = $casterVote;
        }

        return $data;
    }

    private function getTeamDto($team)
    {
        return [
            'id' => $team['id'],
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'logo' => $team['logo']['original'],
            'color' => $team['color']
        ];
    }

    /**
     * @Route("/predictions-form", name="slo_csgo_predictions_form")
     * @Template("@Grafism/SLO/csgo/form/predictions_form.html.twig")
     *
     * @return array
     */
    public function renderPredictionsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCSStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(PredictionsForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_csgo_predictions'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Predictions',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}