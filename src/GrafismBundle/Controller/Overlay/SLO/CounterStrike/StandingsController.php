<?php

namespace GrafismBundle\Controller\Overlay\SLO\CounterStrike;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\CounterStrike\StandingsForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StandingsController
 */
class StandingsController extends OverlayController
{
    /**
     * @Route("/standings", name="slo_csgo_standings")
     * @Template("@Grafism/SLO/csgo/standings.html.twig")
     *
     * @param Request $request
     */
    public function renderStandings(Request $request)
    {
        $competitionId = $request->get('competitionId');

        $competition = $this->getCSStatsApi()->getCompetition($competitionId);
        $teams = $this->getCSStatsApi()->getAllCompetitionTeams($competitionId);

        $data = $this->getStandingsViewData($competition, $teams);

        return $data;
    }

    /**
     * @param $competition
     * @param $teams
     */
    private function getStandingsViewData($competition, $teams)
    {
        $data = [
            'currentRound' => $competition['competition']['currentRound']
        ];

        foreach($teams as $team)
        {
            $data['teams'][] = [
                'team' => $this->getTeamDTO($team['team']),
                'victories' => $team['victories'],
                'defeats' => $team['defeats']
            ];
        }

        return $data;
    }

    private function getTeamDTO($team)
    {
        return [
            'name' => $team['name'],
            'color' => $team['color'],
            'logo' => $team['logo']['original']
        ];
    }

    /**
     * @Route("/standings-form", name="slo_csgo_standings_form")
     * @Template("@Grafism/SLO/csgo/form/standings_form.html.twig")
     *
     */
    public function renderScheduledMatchesForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(StandingsForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_csgo_standings'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Standings'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}