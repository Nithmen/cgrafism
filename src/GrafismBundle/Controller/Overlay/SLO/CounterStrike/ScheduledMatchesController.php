<?php

namespace GrafismBundle\Controller\Overlay\SLO\CounterStrike;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\CounterStrike\ScheduledMatchesForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ScheduledMatchesController
 */
class ScheduledMatchesController extends OverlayController
{
    /**
     * @Route("/scheduled-matches", name="slo_csgo_scheduled_matches")
     * @Template("@Grafism/SLO/csgo/scheduled_matches.html.twig")
     *
     * @param Request $request
     */
    public function renderScheduledMatches(Request $request)
    {
        $competitionId = $request->get('competitionId');
        $round = $request->get('round_id');

        $matches = $this->getCSStatsApi()->getAllCompetitionMatchesByRound($competitionId, $round);

        $data = $this->getScheduledMatchesViewData($matches, $round);

        return $data;
    }

    /**
     * @param $matches
     * @param $round
     */
    private function getScheduledMatchesViewData($matches, $round)
    {
        $date = new \DateTime();
        $date->setTimestamp($matches[0]['scheduledAt']);

        $date2 = new \DateTime();
        $date2->setTimestamp($matches[3]['scheduledAt']);

        $data = [
            'round' => $round,
            'dayName' => $date->format('l'),
            'dayNumber' => $date->format('j'),
            'month' => $date->format('M'),
            'dayName2' => $date2->format('l'),
            'dayNumber2' => $date2->format('j'),
            'month2' => $date2->format('M'),
        ];
        foreach($matches as $match)
        {
            $data['matches'][] = [
                'status' => $match['status'],
                'scheduledAt' => (new \DateTime())->setTimestamp($match['scheduledAt'])->format('H:i'),
                'local' => $this->getTeamDTO($match['teams']['local']['team']),
                'visitor' => $this->getTeamDTO($match['teams']['visitor']['team']),
                'scores' => [
                    'local' => $match['scores']['local'],
                    'visitor' => $match['scores']['visitor']
                ]
            ];
        }

        return $data;
    }

    private function getTeamDTO($team)
    {
        return [
            'name' => $team['name'],
            'color' => $team['color'],
            'logo' => $team['logo']['original']
        ];
    }

    /**
     * @Route("/scheduled-matches-form", name="slo_csgo_scheduled_matches_form")
     * @Template("@Grafism/SLO/csgo/form/scheduled_matches_form.html.twig")
     */
    public function renderScheduledMatchesForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $competition = $this->getCSStatsApi()->getCompetition($competitionId);

        $form = $this->createForm(ScheduledMatchesForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_csgo_scheduled_matches'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Scheduled Matches',
            'rounds' => $competition['competition']['maxRounds']
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}