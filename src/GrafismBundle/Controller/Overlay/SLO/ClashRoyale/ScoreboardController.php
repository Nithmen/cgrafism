<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\ClashRoyale\ScoreboardForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ScoreboardController
 */
class ScoreboardController extends OverlayController
{
    /**
     * @Route("/scoreboard", name="slo_clash_scoreboard")
     * @Template("@Grafism/SLO/clash/scoreboard.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderScoreboard(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getScoreboardViewData($match);

        return $data;
    }

    private function getScoreboardViewData($match)
    {
        $attr = $match['match'];

        return [
            'local' => $this->getTeamDto($attr['teams']['local']['team']),
            'visitor' => $this->getTeamDto($attr['teams']['visitor']['team']),
            'bans' => $match['bannedCards'],
            'scores' => $attr['scores'],
            'setScores' => $attr['setScores']
        ];

    }

    private function getTeamDto($team)
    {
        return [
            'id' => $team['id'],
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'logo' => $team['logo']['original'],
            'color' => $team['color']
        ];
    }

    /**
     * @Route("/scoreboard-form", name="slo_clash_scoreboard_form")
     * @Template("@Grafism/SLO/clash/form/scoreboard_form.html.twig")
     *
     * @return array
     */
    public function renderPredictionsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(ScoreboardForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_scoreboard'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Scoreboard',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}