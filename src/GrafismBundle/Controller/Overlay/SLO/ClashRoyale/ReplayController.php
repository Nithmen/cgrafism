<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\ClashRoyale\ReplayForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReplayController
 */
class ReplayController extends OverlayController
{
    /**
     * @Route("/replay", name="slo_clash_replay")
     * @Template("@Grafism/SLO/clash/replay.html.twig")
     *
     * @param Request $request
     */
    public function renderReplay(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $gameId = $request->get('game_id');

        $data = $this->getReplayViewData($match, $gameId);

        return $data;
    }

    private function getReplayViewData($match, $gameId)
    {
        $attr = $match['match'];

        $players = $this->getCRStatsApi()->getPlayersByGame($gameId);

        $data = [];
        foreach($players as $side => $playerData)
        {
            foreach($playerData as $player)
            {
                $player = array_shift($player);
                $data['players'][$side] = [
                    'photo' => $player['photo']['original']
                ];
            }
        }

        $data['teams']['local'] = $this->getTeamDTO($attr['teams']['local']['team']);
        $data['teams']['visitor'] = $this->getTeamDTO($attr['teams']['visitor']['team']);

        return $data;
    }

    private function getTeamDTO($team)
    {
        return [
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'color' => $team['color'],
            'logo' => $team['logo']['original']
        ];
    }

    /**
     * @Route("/replay-form", name="slo_clash_replay_form")
     * @Template("@Grafism/SLO/clash/form/replay_form.html.twig")
     *
     * @return array
     */
    public function renderReplayForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(ReplayForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_replay'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Replay',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}