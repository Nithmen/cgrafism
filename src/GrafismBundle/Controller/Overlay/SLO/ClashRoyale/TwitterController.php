<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\TwitterForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TwitterController
 */
class TwitterController extends OverlayController
{
    /**
     * @Route("/twitter", name="slo_clash_twitter")
     * @Template("GrafismBundle:SLO:default/twitter.html.twig")
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    public function renderTwitter(Request $request)
    {
        $tweetId = $request->get('tweet_id');
        $hashtag = $request->get('hashtag');

        $tweet = $this->get('api.twitter_connection')->getTweetById($tweetId);

        $data = $this->getTwitterViewData($tweet, $hashtag);

        return $data;
    }

    private function getTwitterViewData($tweet, $hashtag)
    {
        return array_merge($tweet, ['hashtag' => $hashtag]);
    }

    /**
     * @Route("/twitter-form", name="slo_twitter_form")
     * @Template("GrafismBundle:SLO:default/form/twitter_form.html.twig")
     */
    public function renderTwitterForm()
    {
        $competitionId = $this->getCompetition();

        $context = $this->getContext();

        $form = $this->createForm(TwitterForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_twitter'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Twitter'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }
}