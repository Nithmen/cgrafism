<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\SLO\SLOController;
use GrafismBundle\Form\SLO\ClashRoyale\InGameArenaForm;
use GrafismBundle\Form\SLO\ClashRoyale\InGameForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InGameController
 */
class InGameController extends SLOController
{
    /**
     * @Route("/in-game", name="slo_clash_in_game")
     * @Template("@Grafism/SLO/clash/in_game.html.twig")
     */
    public function renderInGame(Request $request)
    {
        $matchId = $request->get('match_id');
        $setNumber = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $data = $this->getInGameViewData($match, $setNumber);

        return $data;
    }

    /**
     * @Route("/in-game-arena", name="slo_clash_in_game_arena")
     * @Template("@Grafism/SLO/clash/in_game_arena.html.twig")
     */
    public function renderInGameArena(Request $request)
    {
        $competitionId = $this->getCompetition();

        $matchId = $request->get('match_id');

        $teamLocalId = $request->get('team_local_id');
        $playerLocal1Id = $request->get('team_local_player1_id');
        $playerLocal2Id = $request->get('team_local_player2_id');
        $playerLocal3Id = $request->get('team_local_player3_id');
        $playerLocal1Ch = $request->get('team_local_player1_ch');
        $playerLocal2Ch = $request->get('team_local_player2_ch');
        $playerLocal3Ch = $request->get('team_local_player3_ch');

        $teamVisitorId = $request->get('team_visitor_id');
        $playerVisitor1Id = $request->get('team_visitor_player1_id');
        $playerVisitor2Id = $request->get('team_visitor_player2_id');
        $playerVisitor3Id = $request->get('team_visitor_player3_id');
        $playerVisitor1Ch = $request->get('team_visitor_player1_ch');
        $playerVisitor2Ch = $request->get('team_visitor_player2_ch');
        $playerVisitor3Ch = $request->get('team_visitor_player3_ch');

        $localData = [
            'team' => $teamLocalId,
            'players' => [
                'player_1' => [
                    'id' => $playerLocal1Id,
                    'ch' => $playerLocal1Ch
                ],
                'player_2' => [
                    'id' => $playerLocal2Id,
                    'ch' => $playerLocal2Ch
                ],
                'player_3' => [
                    'id' => $playerLocal3Id,
                    'ch' => $playerLocal3Ch
                ]
            ]
        ];

        $visitorData = [
            'team' => $teamVisitorId,
            'players' => [
                'player_1' => [
                    'id' => $playerVisitor1Id,
                    'ch' => $playerVisitor1Ch
                ],
                'player_2' => [
                    'id' => $playerVisitor2Id,
                    'ch' => $playerVisitor2Ch
                ],
                'player_3' => [
                    'id' => $playerVisitor3Id,
                    'ch' => $playerVisitor3Ch
                ]
            ]
        ];

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $teams = [
            'local' => $localData,
            'visitor' => $visitorData
        ];

        $data = $this->getInGameArenaViewData($teams, $match, $competitionId);

        return $data;
    }

    private function getInGameArenaViewData($teams, $match, $competitionId)
    {
        $attr = $match['match'];

        $data = [
            'match' => [
                'round' => $attr['round'],
                'set' => 3
            ],
            'teams' => [
                'local' => $this->getTeamDTO($attr['teams']['local']['team']),
                'visitor' => $this->getTeamDTO($attr['teams']['visitor']['team']),
            ],
            'bannedCards' => $match['bannedCards'],
            'setScores' => $attr['setScores'],
            'scores' => [
                'local' => $attr['scores']['local'],
                'visitor' => $attr['scores']['visitor']
            ]
        ];

        $games = $this->getCRStatsApi()->getGamesBySet($attr['id'], 3);

        $data['currentGame'] = 1;
        foreach($games as $game)
        {
            $localScore = $game['scores']['local'];
            $visitorScore = $game['scores']['visitor'];

            $score = '';

            if($localScore > $visitorScore || $localScore < $visitorScore) {
                $data['currentGame']  += 1;
            }

            if($localScore > $visitorScore) {
                $score = 'local';
            } else if($visitorScore > $localScore)  {
                $score = 'visitor';
            }

            $data['games'][$game['order']]['winner'] = $score;
        }

        foreach($teams as $side => $teamData)
        {
            foreach($teamData['players'] as $player)
            {
                $playerData = $this->getCRStatsApi()->getPlayerInACompetition($competitionId, $player['id']);

                $data['players'][$side][] = [
                    'alive' => $player['ch'],
                    'name' => $playerData['player']['nickname'],
                    'photo' => $playerData['player']['photo']['original']
                ];
            }
        }

        return $data;
    }

    private function getInGameViewData($match, $setNumber)
    {
        $attr = $match['match'];

        $data = [
            'match' => [
                'round' => $attr['round'],
                'set' => $setNumber
            ],
            'teams' => [
                'local' => $this->getTeamDTO($attr['teams']['local']['team']),
                'visitor' => $this->getTeamDTO($attr['teams']['visitor']['team']),
            ],
            'bannedCards' => $match['bannedCards'],
            'setScores' => $attr['setScores'],
            'scores' => [
                'local' => $attr['scores']['local'],
                'visitor' => $attr['scores']['visitor']
            ]
        ];

        $games = $this->getCRStatsApi()->getGamesBySet($attr['id'], $setNumber);

        $data['currentGame'] = 1;
        foreach($games as $game)
        {
            $localScore = $game['scores']['local'];
            $visitorScore = $game['scores']['visitor'];

            $score = '';

            if($localScore > $visitorScore || $localScore < $visitorScore) {
                $data['currentGame']  += 1;
            }

            if($localScore > $visitorScore) {
                $score = 'local';
            } else if($visitorScore > $localScore)  {
                $score = 'visitor';
            }

            $data['games'][$game['order']]['winner'] = $score;

            $gamePlayers = $this->getCRStatsApi()->getPlayersByGame($game['id']);

            foreach($gamePlayers as $side => $playersBySide)
            {
                foreach($playersBySide as $player)
                {
                    $playerData = $player['player'];

                    $data['games'][$game['order']]['players'][$side] = [
                        'name' => $playerData['nickname'],
                        'photo' => $playerData['photo']['original']
                    ];
                }
            }

        }

        return $data;
    }

    /**
     * @Route("/in-game-form", name="slo_clash_in_game_form")
     * @Template("@Grafism/SLO/clash/form/in_game_form.html.twig")
     *
     * @return array
     */
    public function renderInGameForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(InGameForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_in_game'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'In Game',
            'matches' => $matchesList,
            'setNumber' => 2
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @Route("/in-game-arena-form", name="slo_clash_in_game_arena_form")
     * @Template("@Grafism/SLO/clash/form/in_game_arena_form.html.twig")
     *
     * @return array
     */
    public function renderInGameArenaForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(InGameArenaForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_in_game_arena'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'In Game Arena',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    private function getTeamDTO($team)
    {
        return [
            'id' => $team['id'],
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'color' => $team['color'],
            'logo' => $team['logo']['original']
        ];
    }
}