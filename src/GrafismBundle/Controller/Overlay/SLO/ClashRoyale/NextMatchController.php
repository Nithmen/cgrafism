<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\NextMatchForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NextMatchController
 */
class NextMatchController extends OverlayController
{
    /**
     * @Route("/next-match", name="slo_clash_next_match")
     * @Template("@Grafism/SLO/default/next_match.html.twig")
     *
     * @param Request $request
     */
    public function renderNextMatch(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getNextMatchViewData($match);

        return $data;
    }

    private function getNextMatchViewData($match)
    {
        $match = $match['match'];

        $localTeam = $match['teams']['local']['team'];
        $visitorTeam = $match['teams']['visitor']['team'];

        $date = new \DateTime();
        $date->setTimestamp($match['scheduledAt']);

        return [
            'scheduledAt' => $date->format('H:i'),
            'local' => [
                'logo' => $localTeam['logo']['original'],
                'color' => $localTeam['color']
            ],
            'visitor' => [
                'logo' => $visitorTeam['logo']['original'],
                'color' => $visitorTeam['color']
            ]
        ];
    }

    /**
     * @Route("/next-match-form", name="slo_clash_next_match_form")
     * @Template("@Grafism/SLO/default/form/next_match_form.html.twig")
     *
     */
    public function renderNextMatchForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(NextMatchForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_next_match'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Next Match',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}