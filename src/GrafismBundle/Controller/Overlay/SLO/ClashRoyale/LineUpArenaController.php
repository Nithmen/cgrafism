<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\ClashRoyale\LineUpArenaForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LineUpArenaController
 */
class LineUpArenaController extends OverlayController
{
    /**
     * @Route("/line-up-arena", name="slo_clash_line_up_arena")
     * @Template("@Grafism/SLO/clash/line_up_arena.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderLineUpArena(Request $request)
    {
        $competitionId = $this->getCompetition();

        $matchId = $request->get('match_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $teamLocalId = $request->get('team_local_id');
        $playerLocal1Id = $request->get('team_local_player1_id');
        $playerLocal2Id = $request->get('team_local_player2_id');
        $playerLocal3Id = $request->get('team_local_player3_id');

        $teamVisitorId = $request->get('team_visitor_id');
        $playerVisitor1Id = $request->get('team_visitor_player1_id');
        $playerVisitor2Id = $request->get('team_visitor_player2_id');
        $playerVisitor3Id = $request->get('team_visitor_player3_id');

        $localData = [
            'team' => $teamLocalId,
            'players' => [
                'player_1' => $playerLocal1Id,
                'player_2' =>  $playerLocal2Id,
                'player_3' => $playerLocal3Id
            ]
        ];

        $visitorData = [
            'team' => $teamVisitorId,
            'players' => [
                'player_1' => $playerVisitor1Id,
                'player_2' =>  $playerVisitor2Id,
                'player_3' => $playerVisitor3Id
            ]
        ];

        $teams = [
            'local' => $localData,
            'visitor' => $visitorData
        ];

        $data = $this->getLineUpArenaViewData($match, $teams, $competitionId);

        return $data;
    }

    private function getLineUpArenaViewData($match, $teams, $competitionId)
    {
        $attr = $match['match'];
        $bannedCards = $match['bannedCards'];

        $data = [];
        foreach($teams as $side => $teamData)
        {
            foreach($teamData['players'] as $player)
            {
                $playerData = $this->getCRStatsApi()->getPlayerInACompetition($competitionId, $player);

                $data[$side]['players'][] = [
                    'name' => $playerData['player']['nickname'],
                    'photo' => $playerData['player']['photo']['original']
                ];
            }
        }

        $data['local']['team'] = $this->getTeamDto($attr['teams']['local']['team']);
        $data['visitor']['team'] = $this->getTeamDto($attr['teams']['visitor']['team']);
        $data['bans'] = $bannedCards;

        return $data;
    }

    private function getTeamDto($team)
    {
        return [
            'id' => $team['id'],
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'logo' => $team['logo']['original'],
            'color' => $team['color']
        ];
    }

    /**
     * @Route("/line-up-arena-form", name="slo_clash_line_up_arena_form")
     * @Template("@Grafism/SLO/clash/form/line_up_arena_form.html.twig")
     *
     * @return array
     */
    public function renderInGameForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LineUpArenaForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_line_up_arena'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Line Up Arena',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}