<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\ClashRoyale\SetSubsForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SetSubsController
 */
class SetSubsController extends OverlayController
{
    /**
     * @Route("/set-subs", name="slo_clash_set_subs")
     * @Template("@Grafism/SLO/clash/set_subs.html.twig")
     *
     * @param Request $request
     */
    public function renderSetSubs(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getSetSubsViewData($match);

        return $data;
    }

    private function getSetSubsViewData($match)
    {
        $attr = $match['match'];

        $playersPerGame = [];
        for($set = 1; $set <= 2; $set++)
        {
            $games = $this->getCRStatsApi()->getGamesBySet($attr['id'], $set);

            foreach($games as $game)
            {
                $players = $this->getCRStatsApi()->getPlayersByGame($game['id']);
                foreach($players as $side => $player)
                {
                    $player = array_shift($player);
                    $playersPerGame[$set][$side][] = $this->getPlayerDto($player['player']);
                }
            }
        }

        $changes = [
            'local' => [
                'gone' => array_values(array_diff($playersPerGame[1]['local'],$playersPerGame[2]['local'])),
                'new' => array_values(array_diff($playersPerGame[2]['local'],$playersPerGame[1]['local'])),
                'team' => $this->getTeamDto($attr['teams']['local']['team'])
            ],
            'visitor' => [
                'gone' => array_values(array_diff($playersPerGame[1]['visitor'],$playersPerGame[2]['visitor'])),
                'new' => array_values(array_diff($playersPerGame[2]['visitor'],$playersPerGame[1]['visitor'])),
                'team' => $this->getTeamDto($attr['teams']['visitor']['team'])
            ]
        ];

        return $changes;
    }

    private function getPlayerDTO($player)
    {
        return $player['nickname'];
    }

    private function getTeamDto($team)
    {
        return [
            'id' => $team['id'],
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'logo' => $team['logo']['original'],
            'color' => $team['color']
        ];
    }

    /**
     * @Route("/set-subs-form", name="slo_clash_set_subs_form")
     * @Template("@Grafism/SLO/clash/form/set_subs_form.html.twig")
     *
     * @return array
     */
    public function renderSetSubsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(SetSubsForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_set_subs'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Set subs',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}