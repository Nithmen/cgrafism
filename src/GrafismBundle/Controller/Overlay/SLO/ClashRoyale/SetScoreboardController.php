<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\ClashRoyale\ScoreboardForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SetScoreboardController
 */
class SetScoreboardController extends OverlayController
{
    /**
     * @Route("/set-scoreboard", name="slo_clash_set_scoreboard")
     * @Template("@Grafism/SLO/clash/set_scoreboard.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderSetScoreboard(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getSetScoreboardViewData($match);

        return $data;
    }

    private function getSetScoreboardViewData($match)
    {
        $attr = $match['match'];

        $date = new \DateTime();
        $date->setTimestamp($attr['scheduledAt']);

        $data = [
            'teams' => [
                'local' => $this->getTeamDto($attr['teams']['local']['team']),
                'visitor' => $this->getTeamDto($attr['teams']['visitor']['team'])
            ],
            'scheduledAt' => $date->format('H:i'),
            'dayName' => $date->format('D'),
            'day' => $date->format('j'),
            'scores' => [
                'local' => $attr['scores']['local'],
                'visitor' => $attr['scores']['visitor']
            ],
            'round' => $attr['round']
        ];

        $games = $this->getCRStatsApi()->getAllMatchGames($attr['id']);

        foreach($games as $game)
        {
            if($game['set'] == 3 && $game['scores']['local'] == 0 && $game['scores']['visitor'] == 0) {
                break;
            }

            $players = $this->getCRStatsApi()->getPlayersByGame($game['id']);

            $data['sets'][$game['set']]['games'][$game['order']]['scores'] = [
                'local' => $game['scores']['local'],
                'visitor' => $game['scores']['visitor']
            ];

            foreach($players as $side => $playerData)
            {
                foreach($playerData as $player)
                {
                    $data['sets'][$game['set']]['games'][$game['order']][$side]['player'] = $this->getPlayerDTO($player['player']);
                }
            }
        }

        return $data;
    }

    private function getPlayerDTO($player)
    {
        return [
            'name' => $player['nickname'],
            'photo' => $player['photo']['original']
        ];
    }

    private function getTeamDto($team)
    {
        return [
            'id' => $team['id'],
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'logo' => $team['logo']['original'],
            'color' => $team['color']
        ];
    }

    /**
     * @Route("/set-scoreboard-form", name="slo_clash_set_scoreboard_form")
     * @Template("@Grafism/SLO/clash/form/set_scoreboard_form.html.twig")
     *
     * @return array
     */
    public function renderSetScoreboardForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(ScoreboardForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_set_scoreboard'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Set Scoreboard',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}