<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\ClashRoyale\LineUpArenaForm;
use GrafismBundle\Form\SLO\ClashRoyale\LineUpForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LineUpController
 */
class LineUpController extends OverlayController
{
    /**
     * @Route("/line-up", name="slo_clash_line_up")
     * @Template("@Grafism/SLO/clash/line_up.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderLineUp(Request $request)
    {
        $matchId = $request->get('match_id');
        $set = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getLineUpViewData($match, $set);

        return $data;
    }

    private function getLineUpViewData($match, $setNumber)
    {
        $attr = $match['match'];
        $bannedCards = $match['bannedCards'];

        $data = [];

        $data['local']['team'] = $this->getTeamDto($attr['teams']['local']['team']);
        $data['visitor']['team'] = $this->getTeamDto($attr['teams']['visitor']['team']);
        $data['bans'] = $bannedCards;

        $games = $this->getCRStatsApi()->getGamesBySet($attr['id'], $setNumber);

        foreach($games as $game)
        {
            $data['set'] = $game['set'];
            $players = $this->getCRStatsApi()->getPlayersByGame($game['id']);

            foreach($players as $side => $player)
            {
                $player = array_shift($player);
                $data['games'][$game['order']][$side] = $this->getPlayerDTO($player['player']);
            }
        }

        return $data;
    }

    private function getPlayerDTO($player)
    {
        return [
            'name' => $player['nickname'],
            'photo' => $player['photo']['original']
        ];
    }

    private function getTeamDto($team)
    {
        return [
            'id' => $team['id'],
            'name' => $team['name'],
            'shortName' => $team['shortName'],
            'logo' => $team['logo']['original'],
            'color' => $team['color']
        ];
    }

    /**
     * @Route("/line-up-form", name="slo_clash_line_up_form")
     * @Template("@Grafism/SLO/clash/form/line_up_form.html.twig")
     *
     * @return array
     */
    public function renderLineUpForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LineUpForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_line_up'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Line Up',
            'matches' => $matchesList,
            'setNumber' => 2
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}