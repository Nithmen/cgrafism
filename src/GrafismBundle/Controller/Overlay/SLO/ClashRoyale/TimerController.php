<?php

namespace GrafismBundle\Controller\Overlay\SLO\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\ClashRoyale\TimerForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TimerController
 */
class TimerController extends OverlayController
{
    /**
     * @Route("/timer", name="slo_clash_timer")
     * @Template("@Grafism/SLO/clash/timer.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderScoreboard(Request $request)
    {
        $type = $request->get('type');

        $minutes = $request->get('minutes');
        $seconds= $request->get('seconds');

        $data = $this->getTimerViewData($type, $minutes, $seconds);

        return $data;
    }

    private function getTimerViewData($type, $minutes, $seconds)
    {
        $label = '';
        if($type == 'lineup') {
            $label = 'Alineaciones';
        } else if ($type == 'bans') {
            $label = 'Baneos';
        }

        return [
            'label' => $label,
            'minutes' => $minutes,
            'seconds' => $seconds
        ];

    }

    /**
     * @Route("/timer-form", name="slo_clash_timer_form")
     * @Template("@Grafism/SLO/clash/form/timer_form.html.twig")
     *
     * @return array
     */
    public function renderTimerForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(TimerForm::class, [
            'url' => $this->generateAbsoluteUrl('slo_clash_timer'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Timer'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}