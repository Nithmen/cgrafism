<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;

use GrafismBundle\Form\Iberian\TwitterForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TwitterController
 */
class TwitterController extends OverlayController
{
    /**
     * @Route("/twitter", name="iberian_twitter")
     * @Template("GrafismBundle:Iberian:twitter.html.twig")
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    public function renderTwitter(Request $request)
    {
        $context = $this->getContext();

        $tweetId = $request->get('tweet_id');
        $hashtag = $request->get('hashtag');

        $data = $this->getTwitterDataView($context, $tweetId, $hashtag);

        return $data;
    }

    /**
     * @param $context
     * @param $tweetId
     * @param $hashtag
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getTwitterDataView($context, $tweetId, $hashtag)
    {
        $twitter = $this->get('api.twitter_connection');
        $tweet = $twitter->getTweetById($tweetId);

        $data = [
            'tweet' => $tweet['text'],
            'user' => $tweet['user'],
            'username' => $tweet['name'],
            'avatar' => $tweet['avatar'],
            'hashtag' => $hashtag
        ];

        return $data;
    }

    /**
     * @Route("/twitter-form", name="iberian_twitter_form")
     * @Template("GrafismBundle:Iberian/form:twitter_form.html.twig")
     */
    public function renderTwitterForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(TwitterForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_twitter'),
            'competitionId' => $competitionId,
            'context' => $context
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}