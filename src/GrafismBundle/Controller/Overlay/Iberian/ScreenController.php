<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ScreenController
 */
class ScreenController extends OverlayController
{
    /**
     * @Route("/screens", name="iberian_screens")
     * @Template("GrafismBundle:Iberian:screens.html.twig")
     *
     * @return array
     */
    public function renderScreens()
    {
        $ddhService = $this->get('api.ddh');
        $champions = $ddhService->getChampions();

        $teams = [
            'kiyf' => 'KIYF',
            'vgia' => 'Vodafone Giants',
            'mad' => 'Mad Lions Falco',
            'mad2' => 'Mad Lions Kanani',
            'army' => 'ASUS'
        ];

        return array(
            'champions' => $champions,
            'teams' => $teams
        );
    }
}