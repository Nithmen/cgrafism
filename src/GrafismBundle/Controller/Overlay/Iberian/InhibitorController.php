<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Iberian\InhibitorForm;
use GrafismBundle\Form\Iberian\InhibitorRowForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InhibitorController
 */
class InhibitorController extends OverlayController
{
    /**
     * @Route("/inhibitor", name="iberian_inhibitor")
     * @Template("GrafismBundle:Iberian:inhibitor.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function renderInhibitor(Request $request)
    {
        $side = $request->get('side_id');

        $data = [];
        switch($side)
        {
            case 'local':
                $data['img'] = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/iberian/ingame/Inhibidor-Azul.png';
                break;
            case 'visitor':
                $data['img'] = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/iberian/ingame/Inhibidor-Rojo.png';
                break;
        }

        $data['side'] = $side;

        return $data;
    }

    /**
     * @Route("/inhibitor-form", name="iberian_inhibitor_form")
     * @Template("GrafismBundle:Iberian/form:inhibitor_form.html.twig")
     */
    public function renderInhibitorForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(InhibitorForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_inhibitor'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Inhibitor pastilla'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @Route("/inhibitor-row", name="iberian_inhibitor_row")
     * @Template("GrafismBundle:Iberian:inhibitor_row.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function renderInhibitorRow(Request $request)
    {
        $side = $request->get('side_id');
        $role = $request->get('inhibitor_id');

        $data = [];
        switch($side)
        {
            case 'local':
                $data['img'] = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/iberian/ingame/Inhibidor-Azul.png';
                break;
            case 'visitor':
                $data['img'] = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/iberian/ingame/Inhibidor-Rojo.png';
                break;
        }

        $data['side'] = $side;
        $data['rol'] = $role;

        return $data;
    }

    /**
     * @Route("/inhibitor-row-form", name="iberian_inhibitor_row_form")
     * @Template("GrafismBundle:Iberian/form:inhibitor_row_form.html.twig")
     */
    public function renderInhibitorRowForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(InhibitorRowForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_inhibitor_row'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Inhibitor timer'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}