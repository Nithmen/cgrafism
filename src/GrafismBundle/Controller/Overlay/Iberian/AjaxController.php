<?php
namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\CRL\FeaturedPlayerForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 */
class AjaxController extends OverlayController
{
    /**
     * AJAX teams
     *
     * @Route("/form-select-teams", name="iberian_form_select_teams")
     */
    public function getJsonTeams()
    {
        $matchId = $this->getRequest()->get('id');
        $teams = $this->getLolStatsApi()->listAllMatchTeams($matchId);
        return new JsonResponse($teams, Response::HTTP_OK);
    }

    /**
     * AJAX players
     *
     * @Route("/form-select-players", name="iberian_form_select_players")
     */
    public function getJsonPlayers()
    {
        $competitionId = $this->getRequest()->get('competitionId');
        $teamId = $this->getRequest()->get('teamId');
        $list = $this->getLolStatsApi()->listAllTeamPlayers($competitionId, $teamId);
        return new JsonResponse($list, Response::HTTP_OK);
    }    
    
    
    /**
     * AJAX games
     * 
     * @Route("/form-select-games", name="iberian_form_select_games")
     */
    public function getJsonGames()
    {
        $matchId = $this->getRequest()->get('id');        
        $games = $this->getLolStatsApi()->listAllMatchGames($matchId);
        return new JsonResponse($games, Response::HTTP_OK);
    }
}
