<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Iberian\StandingsForm;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StandingController extends OverlayController
{
    /**
     * @Route("/standings", name="iberian_standings")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function renderStandings(Request $request)
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $group = $request->get('group_id', null);

        $api = $this->getLolStatsApi();

        $competition = $api->getCompetition($competitionId);

        if(!$group) {
            $ladder = $api->getCompetitionLadder($competitionId);

            $data = $this->getStandingsViewData($ladder, $competition, $context);
            $template = 'GrafismBundle:Iberian:standings.html.twig';
        } else {
            $ladder = $api->getCompetitionLadderByGroup($competitionId, $group);

            $data = $this->getStandingsViewData($ladder, $competition, $context);
            $template = 'GrafismBundle:Iberian:standing.html.twig';
        }

        return $this->render($template, $data);
    }

    /**
     * @param $ladder
     * @param $competition
     * @param $context
     *
     * @return array
     */
    protected function getStandingsViewData($ladder, $competition, $context)
    {
        $data = [];

        foreach($ladder as $team)
        {
            $attributes = $team['attributes'];

            $data['groups'][$attributes['group']][] = [
                'name' => $attributes['name'],
                'logo' => $attributes['logo']['original'],
                'victories' => $attributes['victories'],
                'defeats' => $attributes['defeats'],
                'group' => $attributes['group'],
                'shortname' => $attributes['shortname']
            ];
        }

        $data['context'] = $context;

        $data['ladder']['finished'] = $competition['attributes']['is_finished'];

        return $data;
    }

    /**
     * @Route("/standings-form", name="iberian_standings_form")
     * @Template("GrafismBundle:Iberian/form:standings_form.html.twig")
     */
    public function renderStandingsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(StandingsForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_standings'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Standings'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}