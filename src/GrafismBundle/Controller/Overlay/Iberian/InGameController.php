<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Iberian\InGameDrakesForm;
use GrafismBundle\Form\Iberian\InGameForm;
use GrafismBundle\Utils\GroupUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InGameController extends OverlayController
{
    /**
     * @Route("/in-game", name="iberian_in_game")
     * @Template("GrafismBundle:Iberian:in_game.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function renderInGame(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getLolStatsApi()->getMatch($matchId);

        $gameId = $request->get('game_id');
        $patch = $request->get('patch_id');

        $game = $this->getLolStatsApi()->getGame($gameId);
        $data = $this->getInGameViewData($game, $patch, $match);

        return $data;
    }

    /**
     * @param array $game
     * @param string $patch
     * @param int $group
     *
     * @return array
     */
    private function getInGameViewData($game, $patch, $match)
    {
        $attr = $game['attributes'];

        $matchAttr = $match['attributes'];

        return [
            'local' => [
                'name' => $attr['team_100']['name'],
                'shortname' => $attr['team_100']['shortname'],
                'score' => $matchAttr['results']['score_local']
            ],
            'visitor' => [
                'name' => $attr['team_200']['name'],
                'shortname' => $attr['team_200']['shortname'],
                'score' => $matchAttr['results']['score_visitor']
            ],
            'patch' => $patch,
            'phase' => $this->get('grafism.competition_tagger')->toRoundTag($matchAttr['round'], 3)
        ];
    }

    /**
     * @Route("/in-game-form", name="iberian_in_game_form")
     * @Template("GrafismBundle:Iberian/form:in_game_form.html.twig")
     */
    public function renderInGameForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getLolStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(InGameForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_in_game'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'In Game',
            'matches' => $matchesList,
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @Route("/in-game-drakes", name="iberian_in_game_drakes")
     * @Template("GrafismBundle:Iberian:in_game_drakes.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderInGameDrakes(Request $request)
    {
        $drake = $request->get('drake_id');

        return [
            'drake' => $drake
        ];
    }

    /**
     * @Route("/in-game-drakes-form", name="iberian_in_game_drakes_form")
     * @Template("GrafismBundle:Iberian/form:in_game_drakes_form.html.twig")
     */
    public function renderInGameDrakesForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(InGameDrakesForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_in_game_drakes'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Drakes',
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}