<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Iberian\TimerForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TimerController extends OverlayController
{
    /**
     * @Route("/timer", name="iberian_timer")
     * @Template("GrafismBundle:Iberian:timer.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function renderTimer(Request $request)
    {
        $type = $request->get('type_id');
        $side = $request->get('side_id');

        $data = $this->getTimerViewData($type, $side);

        return $data;
    }

    /**
     * @param string $type
     * @param string $side
     *
     * @return array
     */
    private function getTimerViewData($type, $side)
    {
        $data = [];
        switch($type)
        {
            case 'baron':
                $data['img'] = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/iberian/ingame/Baron-Nashor.png';
                break;
            case 'elder':
                $data['img'] = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/iberian/ingame/Elder-Izquierda.png';
                break;
            default:
                $data['img'] = '';
        }

        switch ($side) {
            case 'local':
                $data['side'] = sprintf('%s <span class="blue">LADO AZUL</span>', $type);
                break;
            case 'visitor':
                $data['side'] = sprintf('%s <span class="red">LADO ROJO</span>', $type);
                break;
        }


        $data['type'] = $type;

        return $data;
    }

    /**
     * @Route("/timer-form", name="iberian_timer_form")
     * @Template("GrafismBundle:Iberian/form:timer_form.html.twig")
     */
    public function renderInGameForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(TimerForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_timer'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Timer'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}