<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Iberian\VotesForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class VoteController extends OverlayController
{
    /**
     * @Route("/votes", name="iberian_votes")
     * @Template("GrafismBundle:Iberian:votes.html.twig"))
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderVotes(Request $request)
    {
        $matchId = $request->get('match_id');

        $match = $this->getLolStatsApi()->getMatch($matchId);

        $casterVotes = [];
        foreach($request->query->all() as $key => $param)
        {
            if(preg_match('~^caster_~', $key)) {
                $casterVotes[$key] = $param;
            }
        }

        $data = $this->getVotesViewData($match, $casterVotes);

        return $data;
    }

    private function getVotesViewData($match, $casterVotes)
    {
        $votes = $this->getDdhApi()->getVotesByUuid($match['id']);

        $attr = $match['attributes'];

        $data = [
            'local' => [
                'shortname' => $attr['team_local']['shortname'],
                'votes' => $votes['votes_a']
            ],
            'visitor' => [
                'shortname' => $attr['team_visitor']['shortname'],
                'votes' => $votes['votes_b']
            ],
            'total_votes' => $votes['total_votes']
        ];

        $localId = $attr['team_local']['id'];
        $visitorId = $attr['team_visitor']['id'];

        foreach($casterVotes as $key => $vote)
        {
            $casterVote = 'none';

            if($vote == $visitorId) {
                $casterVote = 'visitor';
            } else if($vote == $localId) {
                $casterVote = 'local';
            }

            $data['votes'][$key] = $casterVote;
        }

        return $data;
    }

    /**
     * @Route("/votes-form", name="iberian_votes_form")
     * @Template("GrafismBundle:Iberian/form:votes_form.html.twig")
     */
    public function renderStandingsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getLolStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(VotesForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_votes'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Votes',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}