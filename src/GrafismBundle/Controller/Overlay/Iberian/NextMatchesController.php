<?php

namespace GrafismBundle\Controller\Overlay\Iberian;

use GrafismBundle\Controller\Overlay\OverlayController;

use GrafismBundle\Form\Iberian\NextMatchesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class NextMatchesController extends OverlayController
{
    /**
     * @Route("/next-matches", name="iberian_next_matches")
     * @Template("GrafismBundle:Iberian:next_matches.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function renderNextMatches(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getLolStatsApi()->getMatch($matchId);

        $data = $this->getNextMatchesViewData($match);

        return $data;
    }

    private function getNextMatchesViewData($match)
    {
        $attr = $match['attributes'];

        $date = new \DateTime();
        $date->setTimestamp($attr['scheduled_at']);

        return [
            'local' => [
                'shortname' => $attr['team_local']['shortname']
            ],
            'visitor' => [
                'shortname' => $attr['team_visitor']['shortname']
            ],
            'hour' => $date->format('H:i')
        ];
    }

    /**
     * @Route("/next-matches-form", name="iberian_twitter_share_form")
     * @Template("GrafismBundle:Iberian/form:next_matches_form.html.twig")
     */
    public function renderStandingsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getLolStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(NextMatchesForm::class, [
            'url' => $this->generateAbsoluteUrl('iberian_next_matches'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Next Matches',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

}