<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\CounterStrike;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\IngameForm;
use GrafismBundle\Form\LaCopa\csgo\StatsForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatsController
 */
class StatsController extends OverlayController
{
    /**
     * @Route("/csgo/stats", name="copa_csgo_stats")
     * @Template("GrafismBundle:LaCopa/csgo:stats.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderStats(Request $request)
    {
        $side = $request->get('side');
        $text = $request->get('text');

        return [
            'side' => $side,
            'text' => $text
        ];
    }

    /**
     * @Route("/csgo/stats-form", name="copa_csgo_stats_form")
     * @Template("GrafismBundle:LaCopa/csgo/form:stats_form.html.twig")
     *
     * @return array
     */
    public function renderStatsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(StatsForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_csgo_stats'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Stats in game'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}