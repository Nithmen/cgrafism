<?php

namespace GrafismBundle\Controller\Overlay\LaCopa;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\StandingsForm;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StandingsController
 */
class StandingsController extends OverlayController
{
    /**
     * @Route("/standings", name="copa_standings")
     */
    public function renderStandings(Request $request)
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $group = $request->get('group_id', null);

        $apiName = sprintf('grafism.%s_stats_api', $context);

        $api = $this->get($apiName);

        $competition = $api->getCompetition($competitionId);

        if(!$group) {
            $ladder = $api->getCompetitionLadder($competitionId);

            $data = $this->getStandingsViewData($ladder, $competition, $context);
            $template = 'GrafismBundle:LaCopa:standings.html.twig';
        } else {
            $ladder = $api->getCompetitionLadderByGroup($competitionId, $group);

            $data = $this->getStandingsViewData($ladder, $competition, $context);
            $template = 'GrafismBundle:LaCopa:standing.html.twig';
        }

        return $this->render($template, $data);
    }

    /**
     * @param $ladder
     * @param $competition
     * @param $context
     *
     * @return array
     */
    protected function getStandingsViewData($ladder, $competition, $context)
    {
        $data = [];

        foreach($ladder as $team)
        {
            $attributes = $team['attributes'];

            isset($attributes['round_diff']) ? $roundDiff = $attributes['round_diff'] : $roundDiff = 0;
            isset($attributes['set_diff']) ? $setDiff = $attributes['set_diff'] : $setDiff = 0;

            $data['groups'][$attributes['group']][] = [
                'name' => $attributes['name'],
                'logo' => $attributes['logo']['original'],
                'victories' => $attributes['victories'],
                'defeats' => $attributes['defeats'],
                'group' => $attributes['group'],
                'shortname' => $attributes['shortname'],
                'round_diff' => $roundDiff['round_diff'],
                'set_diff' => $setDiff['round_diff']
            ];
        }

        $data['context'] = $context;

        $data['ladder']['finished'] = $competition['attributes']['is_finished'];

        return $data;
    }

    /**
     * @Route("/standings-form", name="copa_standings_form")
     * @Template("GrafismBundle:LaCopa/form:standings_form.html.twig")
     */
    public function renderStandingsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(StandingsForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_standings'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Standings'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}