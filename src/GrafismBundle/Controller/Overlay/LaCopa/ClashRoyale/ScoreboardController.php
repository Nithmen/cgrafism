<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\ScoreboardForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ScoreboardController
 */
class ScoreboardController extends OverlayController
{
    /**
     * @Route("/clash/scoreboard", name="copa_clash_scoreboard")
     * @Template("GrafismBundle:LaCopa/clash:scoreboard.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderScoreboard(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getScoreboardDataView($match);

        return $data;
    }

    /**
     * @param $match
     *
     * @return array
     */
    protected function getScoreboardDataView($match)
    {
        $attr = $match['attributes'];

        $data['local'] = $this->getTeamsDTO($attr['team_local']);
        $data['visitor'] = $this->getTeamsDTO($attr['team_visitor']);

        $setResults = $attr['detailed_results'];
        foreach($setResults as $order => $setResult) {
            (isset($attr['banned_cards'][$order])) ? $ban = array_shift($attr['banned_cards'][$order])['card']['key'] : $ban = '';

            $data['games'][$order] = [
                'score_local' => ($setResult['team_local'] == 0 && $setResult['team_visitor'] == 0) ? '-' : $setResult['team_local'],
                'score_visitor' => ($setResult['team_local'] == 0 && $setResult['team_visitor'] == 0) ? '-' : $setResult['team_visitor'],
                'ban' => $ban
            ];
        }

        $data['score_local'] = $attr['results']['score_local'];
        $data['score_visitor'] = $attr['results']['score_visitor'];

        return $data;
    }

    /**
     * @param $team
     *
     * @return array
     */
    private function getTeamsDTO($team)
    {
        return [
            'name' => $team['name'],
            'shortname' => $team['shortname'],
            'logo' => $team['logo']['original']
        ];
    }

    /**
     * @Route("/clash/scoreboard-form", name="copa_clash_scoreboard_form")
     * @Template("GrafismBundle:LaCopa/clash/form:scoreboard_form.html.twig")
     *
     * @return array
     */
    public function renderScoreboardForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(ScoreboardForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_scoreboard'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Scoreboard',
            'matches' => $matchesList,
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}