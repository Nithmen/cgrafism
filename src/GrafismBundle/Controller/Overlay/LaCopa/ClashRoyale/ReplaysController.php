<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\ReplaysForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReplaysController
 */
class ReplaysController extends OverlayController
{
    /**
     * @Route("/clash/replays", name="copa_clash_replays")
     * @Template("GrafismBundle:LaCopa/clash:replays.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderReplays(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $currentSet = $request->get('set_id');

        $data = $this->getReplaysDataView($match, $currentSet);

        return $data;
    }

    /**
     * @param $match
     * @param $currentSet
     *
     * @return array
     */
    protected function getReplaysDataView($match, $currentSet)
    {
        $attr = $match['attributes'];

        $data['local'] = $this->getTeamsDTO($attr['team_local']);
        $data['visitor'] = $this->getTeamsDTO($attr['team_visitor']);

        $setResults = $attr['detailed_results'];
        foreach($setResults as $order => $setResult) {
            (isset($attr['banned_cards'][$order])) ? $ban = array_shift($attr['banned_cards'][$order])['card']['key'] : $ban = '';

            $data['sets'][$order] = [
                'score_local' => ($setResult['team_local'] == 0 && $setResult['team_visitor'] == 0) ? '' : $setResult['team_local'],
                'score_visitor' => ($setResult['team_local'] == 0 && $setResult['team_visitor'] == 0) ? '' : $setResult['team_visitor'],
                'ban' => $ban
            ];
        }

        $games = $this->getCRStatsApi()->getAllMatchGames($match['id']);

        foreach($games as $game)
        {
            $gameAttr = $game['attributes'];

            if($gameAttr['set'] != $currentSet) {
                continue;
            }

            $data['games'][$gameAttr['order']] = [
                'score_local' => $gameAttr['results']['score_100'],
                'player_local' => $gameAttr['team_100']['players'][0]['nickname'],
                'score_visitor' => $gameAttr['results']['score_200'],
                'player_visitor' => $gameAttr['team_200']['players'][0]['nickname']
            ];
        }

        $data['score_local'] = $attr['results']['score_local'];
        $data['score_visitor'] = $attr['results']['score_visitor'];

        $data['set'] = $currentSet;

        return $data;
    }

    /**
     * @param $team
     *
     * @return array
     */
    private function getTeamsDTO($team)
    {
        return [
            'name' => $team['name'],
            'shortname' => $team['shortname'],
            'logo' => $team['logo']['original']
        ];
    }

    /**
     * @Route("/clash/replays-form", name="copa_clash_replays_form")
     * @Template("GrafismBundle:LaCopa/clash/form:replays_form.html.twig")
     *
     * @return array
     */
    public function renderScoreboardForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(ReplaysForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_replays'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Scoreboard',
            'matches' => $matchesList,
            'set_number' => 5
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}