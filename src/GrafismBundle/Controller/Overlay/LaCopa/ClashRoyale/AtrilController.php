<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;

use GrafismBundle\Form\LaCopa\clash\AtrilForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AtrilController extends OverlayController
{
    /**
     * @Route("/clash/atril", name="copa_clash_atril")
     * @Template("GrafismBundle:LaCopa/clash:atril.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderAtril(Request $request)
    {
        $matchId = $request->get('match_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $games = $this->getCRStatsApi()->getAllMatchGames($matchId);
        $data = $this->getAtrilViewData($match, $games);

        return $data;
    }

    protected function getAtrilViewData($match, $games)
    {
        $attr = $match['attributes'];

        $currentSet = ($attr['results']['score_local'] + $attr['results']['score_visitor']) + 1;

        if($currentSet > 5) {
            $currentSet = 5;
        }

        $data = [];

        $data['currentSet'] = $currentSet;

        $currentGame = 1;
        foreach($games as $game)
        {
            if($game['attributes']['set'] == $currentSet) {
                $localScore = '-';
                $visitorScore = '-';

                if($game['attributes']['results']['score_100'] > $game['attributes']['results']['score_200']) {
                    $localScore = 'win';
                    $visitorScore = 'loss';
                    $currentGame += 1;

                } else if($game['attributes']['results']['score_100'] < $game['attributes']['results']['score_200']) {
                    $localScore = 'loss';
                    $visitorScore = 'win';
                    $currentGame += 1;
                }

                $data['games'][$game['attributes']['order']] = [
                    'local' => [
                        'score' => $localScore,
                        'player' => $game['attributes']['team_100']['players'][0]['nickname'],
                        'logo' => $game['attributes']['team_100']['shortname']
                    ],
                    'visitor' => [
                        'score' => $visitorScore,
                        'player' => $game['attributes']['team_200']['players'][0]['nickname'],
                        'logo' => $game['attributes']['team_200']['shortname']
                    ]
                ];
            }

            $data['currentGame'] = ($currentGame > 3) ? 3 : $currentGame;
        }

        return $data;
    }

    /**
     * @Route("/clash/atril-form", name="copa_clash_atril_form")
     * @Template("GrafismBundle:LaCopa/clash/form:atril_form.html.twig")
     *
     * @return array
     */
    public function renderAtrilForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(AtrilForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_atril'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Atril',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}