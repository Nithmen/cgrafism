<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\LineupForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LineupController
 */
class LineupController extends OverlayController
{
    /**
     * @Route("/clash/lineup", name="copa_clash_lineup")
     * @Template("GrafismBundle:LaCopa/clash:lineup.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderLineup(Request $request)
    {
        $matchId = $request->get('match_id');
        $setNumber = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $games = $this->getCRStatsApi()->getAllMatchGames($matchId);

        $data = $this->getLineupViewData($match, $games,  $setNumber);

        return $data;
    }

    /**
     * @param $match
     * @param $games
     * @param $setNumber
     *
     * @return array
     */
    protected function getLineupViewData($match, $games, $setNumber)
    {
        $data = [];

        foreach($games as $game)
        {
            $set = $game['attributes']['set'];

            if($set != $setNumber) {
                continue;
            }

            $order = $game['attributes']['order'];

            $attr = $game['attributes'];

            $data['games'][$order] = [
                'local' => $attr['team_100']['players'][0]['nickname'],
                'visitor' => $attr['team_200']['players'][0]['nickname']
            ];
        }

        $local = $match['attributes']['team_local'];
        $visitor = $match['attributes']['team_visitor'];

        $data['local'] = [
            'name' => $local['name'],
            'shortname' => $local['shortname'],
            'logo' => $local['logo']['original']
        ];

        $data['visitor'] = [
            'name' => $visitor['name'],
            'shortname' => $visitor['shortname'],
            'logo' => $visitor['logo']['original']
        ];

        return $data;
    }

    /**
     * @Route("/clash/lineup-form", name="copa_clash_lineup_form")
     * @Template("GrafismBundle:LaCopa/clash/form:lineup_form.html.twig")
     *
     * @return array
     */
    public function renderLineupForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(LineupForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_lineup'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Lineup',
            'matches' => $matchesList,
            'set_number' => 5
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}