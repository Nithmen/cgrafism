<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\RecapSetlForm;
use GrafismBundle\Form\LaCopa\clash\SetScoreForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SetScoreController extends OverlayController
{
    /**
     * @Route("/clash/set-score", name="copa_clash_set_score")
     * @Template("GrafismBundle:LaCopa/clash:set_score.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderSetScore(Request $request)
    {
        $matchId = $request->get('match_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getSetScoreData($match);

        return $data;
    }

    protected function getSetScoreData($match)
    {
        $attr = $match['attributes'];

        $data = [
            'local' => [
                'name' => $attr['team_local']['name'],
                'shortname' => $attr['team_local']['shortname'],
                'score' => $attr['results']['score_local']
            ],
            'visitor' => [
                'name' => $attr['team_visitor']['name'],
                'shortname' => $attr['team_visitor']['shortname'],
                'score' => $attr['results']['score_visitor']
            ]
        ];

        return $data;
    }

    /**
     * @Route("/clash/set-score-form", name="copa_clash_set_score_form")
     * @Template("GrafismBundle:LaCopa/clash/form:set_score_form.html.twig")
     *
     * @return array
     */
    public function renderRecapSetForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(SetScoreForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_set_score'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Set Score',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}