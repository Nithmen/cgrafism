<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\MatchResultsForm;
use GrafismBundle\Form\LaCopa\clash\ScoreboardForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MatchResultsController
 */
class MatchResultsController extends OverlayController
{
    /**
     * @Route("/clash/match-results", name="copa_clash_match_results")
     * @Template("GrafismBundle:LaCopa/clash:match_results.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderMatchResults(Request $request)
    {
        $matchId = $request->get('match_id');
        $match = $this->getCRStatsApi()->getMatch($matchId);

        $data = $this->getMatchResultsDataView($match);

        return $data;
    }

    /**
     * @param $match
     *
     * @return array
     */
    protected function getMatchResultsDataView($match)
    {
        $data = [];

        $attr = $match['attributes'];

        $games = $this->getCRStatsApi()->getAllMatchGames($match['id']);

        foreach($games as $game)
        {
            $gameAttr  = $game['attributes'];

            $scoreLocal = $gameAttr['results']['score_100'];
            $scoreVisitor = $gameAttr['results']['score_200'];

            $score = '-';

            if($scoreLocal > $scoreVisitor) {
                $score = 'local';
            } elseif ($scoreVisitor > $scoreLocal) {
                $score = 'visitor';
            }

            $data['sets'][$gameAttr['set']][$gameAttr['order']] = [
                'player_local' => $gameAttr['team_100']['players'][0]['nickname'],
                'player_visitor' => $gameAttr['team_200']['players'][0]['nickname'],
                'score' => $score,
            ];
        }

        $data['local'] = [
            'name' => $attr['team_local']['name'],
            'shortname' => $attr['team_local']['shortname'],
            'logo' => $attr['team_local']['logo']['original'],
            'score' => $attr['results']['score_local']
        ];

        $data['visitor'] = [
            'name' => $attr['team_visitor']['name'],
            'shortname' => $attr['team_visitor']['shortname'],
            'logo' => $attr['team_visitor']['logo']['original'],
            'score' => $attr['results']['score_visitor']
        ];

        $currentSet = (($attr['results']['score_local'] + $attr['results']['score_visitor']) == 0) ? 1 : ($attr['results']['score_local'] + $attr['results']['score_visitor']);


        foreach($attr['detailed_results'] as $setNumber => $setResult) {
            $score = '-';

            if($setResult['team_local'] > $setResult['team_visitor']) {
                $score = 'local';
            } elseif($setResult['team_visitor'] > $setResult['team_local']) {
                $score = 'visitor';
            }

            $data['set_results'][$setNumber] = $score;
        }

        $data['currentSet'] = $currentSet;

        return $data;
    }
    /**
     * @Route("/clash/match-results-form", name="copa_clash_match_results_form")
     * @Template("GrafismBundle:LaCopa/clash/form:match_results_form.html.twig")
     *
     * @return array
     */
    public function renderMatchResultsForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(MatchResultsForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_match_results'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Scoreboard',
            'matches' => $matchesList,
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}