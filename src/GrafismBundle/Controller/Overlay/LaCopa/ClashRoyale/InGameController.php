<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\IngameForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InGameController
 */
class InGameController extends OverlayController
{
    /**
     * @Route("/clash/ingame", name="copa_clash_ingame")
     * @Template("GrafismBundle:LaCopa/clash:ingame.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderIngame(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');
        $set = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $competition = $this->getCRStatsApi()->getCompetition($competitionId);

        $data = $this->getIngameViewData($match, $set, $competition);

        return $data;
    }

    /**
     * @param $match
     * @param $set
     * @param $competition
     *
     * @return array
     */
    protected function getIngameViewData($match, $set, $competition)
    {
        $data = [];

        $attr = $match['attributes'];

        $competitionAttr = $competition['attributes'];
        $data['competition'] = [
            'current_round' => $attr['round'],
            'max_rounds' => $competitionAttr['max_rounds'],
            'type' => $competitionAttr['competition_type'],
            'group' => $attr['group']
        ];

        $data['teams'] = [
          'local' => [
              'name' => $attr['team_local']['name'],
              'shortname' => $attr['team_local']['shortname'],
              'logo' => $attr['team_local']['logo']['original'],
              'score' => $attr['results']['score_local']
          ],
            'visitor' => [
                'name' => $attr['team_visitor']['name'],
                'shortname' => $attr['team_visitor']['shortname'],
                'logo' => $attr['team_visitor']['logo']['original'],
                'score' => $attr['results']['score_visitor']
            ]
        ];

        foreach($attr['banned_cards'] as $ban)
        {
            $teamId = key($ban);
            $ban = array_shift($ban);

            $data['bans'][] = [
                'side' => $this->getTeamMatchSide($match, $teamId),
                'card' => $ban['card']['key']
            ];
        }

        $games = $this->getCRStatsApi()->getAllMatchGames($match['id']);
        $currentGame = 1;
        foreach($games as $game)
        {
            $gameAttr = $game['attributes'];

            $localScore = '-';
            $visitorScore = '-';

            if($gameAttr['results']['score_100'] > $gameAttr['results']['score_200']) {
                $localScore = 'win';
                $visitorScore = 'loss';
            } else if($gameAttr['results']['score_100'] < $gameAttr['results']['score_200']) {
                $localScore = 'loss';
                $visitorScore = 'win';
            }

            if($gameAttr['set'] == $set) {
                $data['set'] = $gameAttr['set'];

                $data['games'][] = [
                    'local' => [
                        'player' => $gameAttr['team_100']['players'][0]['nickname'],
                        'score' => $localScore
                    ],
                    'visitor' => [
                        'player' => $gameAttr['team_200']['players'][0]['nickname'],
                        'score' => $visitorScore
                    ]
                ];

                if($gameAttr['results']['score_100'] < $gameAttr['results']['score_200'] || $gameAttr['results']['score_100'] > $gameAttr['results']['score_200']) {
                    $currentGame += 1;
                }
            }

            $data['currentGame'] = ($currentGame <= 3) ? $currentGame: 3;
        }

        $data['set'] = $set;

        return $data;
    }

    /**
     * @Route("/clash/ingame-form", name="copa_clash_ingame_form")
     * @Template("GrafismBundle:LaCopa/clash/form:ingame_form.html.twig")
     *
     * @return array
     */
    public function renderIngameForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(IngameForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_ingame'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Ingame',
            'matches' => $matchesList,
            'set_number' => 5
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @Route("/clash/ingame-event", name="copa_clash_ingame_event")
     * @Template("GrafismBundle:LaCopa/clash:ingame_event.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderIngameEvent(Request $request)
    {
        $competitionId = $this->getCompetition();
        $matchId = $request->get('match_id');
        $set = $request->get('set_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);
        $competition = $this->getCRStatsApi()->getCompetition($competitionId);

        $data = $this->getIngameViewData($match, $set, $competition);

        return $data;
    }

    /**
     * @Route("/clash/ingame-event-form", name="copa_clash_ingame_event_form")
     * @Template("GrafismBundle:LaCopa/clash/form:ingame_form.html.twig")
     *
     * @return array
     */
    public function renderIngameEventForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(IngameForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_ingame_event'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Ingame',
            'matches' => $matchesList,
            'set_number' => 5
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $match
     * @param $teamId
     *
     * @return int
     */
    protected function getTeamMatchSide($match, $teamId)
    {
        $attributes = $match['attributes'];
        return ($attributes['team_local']['id'] == $teamId) ? 100 : 200;
    }
}