<?php

namespace GrafismBundle\Controller\Overlay\LaCopa\ClashRoyale;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\LaCopa\clash\RecapSetlForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecapSetController extends OverlayController
{
    /**
     * @Route("/clash/recap-set", name="copa_clash_recap_set")
     * @Template("GrafismBundle:LaCopa/clash:recap_set.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderRecapSet(Request $request)
    {
        $matchId = $request->get('match_id');

        $match = $this->getCRStatsApi()->getMatch($matchId);

        $games = $this->getCRStatsApi()->getAllMatchGames($matchId);
        $data = $this->getRecapSetlViewData($match, $games);

        return $data;
    }

    protected function getRecapSetlViewData($match, $games)
    {
        $attr = $match['attributes'];

        $currentSet = ($attr['results']['score_local'] + $attr['results']['score_visitor']) + 1;

        if($currentSet > 5) {
            $currentSet = 5;
        }

        $data = [];

        $data['currentSet'] = $currentSet;

        $currentGame = 1;
        foreach($games as $game)
        {
            if($game['attributes']['set'] == $currentSet) {
                $localScore = '-';
                $visitorScore = '-';

                if($game['attributes']['results']['score_100'] > $game['attributes']['results']['score_200']) {
                    $localScore = 'win';
                    $visitorScore = 'loss';
                    $currentGame += 1;

                } else if($game['attributes']['results']['score_100'] < $game['attributes']['results']['score_200']) {
                    $localScore = 'loss';
                    $visitorScore = 'win';
                    $currentGame += 1;
                }

                $data['games'][$game['attributes']['order']] = [
                    'local' => [
                        'score' => $localScore,
                        'player' => $game['attributes']['team_100']['players'][0]['nickname'],
                        'logo' => $game['attributes']['team_100']['shortname']
                    ],
                    'visitor' => [
                        'score' => $visitorScore,
                        'player' => $game['attributes']['team_200']['players'][0]['nickname'],
                        'logo' => $game['attributes']['team_200']['shortname']
                    ]
                ];
            }

            $data['currentGame'] = ($currentGame > 3) ? 3 : $currentGame;
        }

        return $data;
    }

    /**
     * @Route("/clash/recap-set-form", name="copa_clash_recap_set_form")
     * @Template("GrafismBundle:LaCopa/clash/form:recap_set_form.html.twig")
     *
     * @return array
     */
    public function renderRecapSetForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getCRStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(RecapSetlForm::class, [
            'url' => $this->generateAbsoluteUrl('copa_clash_recap_set'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Recap Set',
            'matches' => $matchesList
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}