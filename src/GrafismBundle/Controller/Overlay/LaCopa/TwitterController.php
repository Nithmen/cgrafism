<?php

namespace GrafismBundle\Controller\Overlay\LaCopa;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Type\TwitterType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TwitterController
 */
class TwitterController extends OverlayController
{
    /**
     * @Route("/twitter", name="copa_twitter")
     * @Template("GrafismBundle:LaCopa:twitter.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderTwitter(Request $request)
    {
        $context = $this->getContext();

        $tweetId = $request->get('tweet_id');
        $hashtag = $request->get('hashtag');

        $data = $this->getTwitterDataView($context, $tweetId, $hashtag);

        return $data;
    }

    /**
     * @param $context
     * @param $tweetId
     * @param $hashtag
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getTwitterDataView($context, $tweetId, $hashtag)
    {
        $twitter = $this->get('api.twitter_connection');
        $tweet = $twitter->getTweetById($tweetId);

        $logo = '';
        switch ($context)
        {
            case 'cr':
                $logo = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/la-copa/logos/copa-clash-700x700.png'; break;
            case 'cs':
                $logo = 'https://s3-eu-west-1.amazonaws.com/lvp-graphism/la-copa/logos/copa-csgo-700x700.png'; break;
        }

        $data = [
            'tweet' => $tweet['text'],
            'user' => $tweet['user'],
            'username' => $tweet['name'],
            'avatar' => $tweet['avatar'],
            'logo' => $logo,
            'hashtag' => $hashtag
        ];

        return $data;
    }

    /**
     * @Route("/twitter-form", name="copa_twitter_form")
     * @Template("GrafismBundle:form:twitter_form.html.twig")
     */
    public function renderTwitterForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $form = $this->createForm(TwitterType::class, [
            'url' => $this->generateAbsoluteUrl('copa_twitter'),
            'competitionId' => $competitionId,
            'context' => $context
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}