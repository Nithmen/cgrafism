<?php

namespace GrafismBundle\Controller\Overlay;

use ApiBundle\Services\SuperligaService;
use Exception;
use GrafismBundle\Form\GraphismType;
use GrafismBundle\Service\CRStatsAPI;
use GrafismBundle\Service\CSStatsAPI;
use GrafismBundle\Service\FifaStatsAPI;
use GrafismBundle\Service\LolStatsAPI;
use GrafismBundle\Service\ResourceFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class OverlayController
 */
class OverlayController extends BaseController
{
    /** @var RequestStack */
    private $requestStack;

    /** @var ResourceFetcher */
    private $resourceFetcher;

    /**
     * OverlayController constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, ResourceFetcher $resourceFetcher)
    {
        $this->requestStack = $requestStack;
        $this->resourceFetcher = $resourceFetcher;
    }

    /**
     * @return null|Request
     */
    public function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * @return ResourceFetcher
     */
    public function getResourceFetcher()
    {
        return $this->resourceFetcher;
    }

    /**
     * @param $routeName
     *
     * @return string
     */
    protected function generateAbsoluteUrl($routeName)
    {
        return $this->generateUrl($routeName, [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @param $renderName
     * @param $formType
     * @param $graphismName
     *
     * @return array
     */
    protected function renderGenericFormView($renderName, $formType,  $graphismName)
    {
        $form = $this->createForm($formType, [
            'url' => $this->generateAbsoluteUrl($renderName),
            'competitionId' => $this->getCompetition(),
            'context' => $this->getContext()
        ], [
            'name' => $graphismName
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return new Response($this->renderView('GrafismBundle::form/base_form.html.twig', [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ]));
    }

    /**
     * @return string
     */
    protected function getContext()
    {
        return $this->getRequest()->query->get('context');
    }

    /**
     * @return string
     */
    protected function getCompetition()
    {
        return $this->getRequest()->query->get('competitionId');
    }

    /**
     * @param string $context
     * @param string $resource
     * @param string $resourceId
     *
     * @return array
     *
     * @throws Exception
     */
    protected function getResource($context, $resource, $resourceId = null)
    {
        $url = $this->getResourceFetcher()->getResourceUrl($context, $resource, $resourceId);

        return $this->getData($url);
    }

    /**
     * @param $resource
     * @param $child
     * @param $sort
     *
     * @return mixed
     *
     * @throws Exception
     */
    protected function getChildResource($resource, $child, $sort = null)
    {
        $url = sprintf('%s/%s', $resource, $child);

        if($sort) {
            $url = sprintf('%s?sort=%s', $url, $sort);
        }

        return $this->getData($url);
    }

    /**
     * @param $url
     *
     * @return mixed
     *
     * @throws Exception
     */
    private function getData($url)
    {
        $s = curl_init();

        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);

        $json = curl_exec($s);

        if($json === false){
            throw new Exception(curl_error($s)) ;
        }

        curl_close($s);

        $json = json_decode($json, true);

        return $json;
    }
    
    /**
     * @return CRStatsAPI
     */
    protected function getCRStatsApi()
    {
        return $this->get('grafism.cr_stats_api');
    }

    /**
     * @return FifaStatsAPI
     */
    protected function getFifaStatsApi()
    {
        return $this->get('grafism.fifa_stats_api');
    }

    /**
     * @return CSStatsAPI
     */
    protected function getCSStatsApi()
    {
        return $this->get('grafism.cs_stats_api');
    }

    /**
     * @return LolStatsAPI
     */
    protected function getLolStatsApi()
    {
        return $this->get('grafism.lol_stats_api');
    }

    /**
     * @return SuperligaService
     */
    protected function getDdhApi()
    {
        return $this->get('api.superliga');
    }
}