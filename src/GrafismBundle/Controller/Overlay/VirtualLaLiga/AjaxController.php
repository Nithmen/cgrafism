<?php

namespace GrafismBundle\Controller\Overlay\VirtualLaLiga;

use GrafismBundle\Controller\Overlay\OverlayController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController
 */
class AjaxController extends OverlayController
{
    /**
     * AJAX players
     *
     * @Route("/form-select-players", name="fifa_form_select_players")
     */
    public function getJsonPlayers()
    {
        $matchId = $this->getRequest()->get('matchId');
        $list = $this->getFifaStatsApi()->listAllMatchPlayers($matchId);
        return new JsonResponse($list, Response::HTTP_OK);
    }
    
    
    /**
     * AJAX games
     * 
     * @Route("/form-select-games", name="virtual_form_game")
     */
    public function getJsonGame()
    {
        $gameId = $this->getRequest()->get('id');
        $game = $this->getFifaStatsApi()->getGame($gameId);
        return new JsonResponse($game, Response::HTTP_OK);
    }
}
