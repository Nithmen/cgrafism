<?php

namespace GrafismBundle\Controller\Overlay\VirtualLaLiga;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\SLO\TwitterForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TwitterController
 */
class TwitterController extends OverlayController
{
    /**
     * @Route("/twitter", name="virtual_twitter")
     * @Template("GrafismBundle:Virtual:twitter.html.twig")
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    public function renderTwitter(Request $request)
    {
        $tweetId = $request->get('tweet_id');

        $tweet = $this->get('api.twitter_connection')->getTweetById($tweetId);

        return $tweet;
    }

    /**
     * @Route("/twitter-form", name="virtual_twitter_form")
     * @Template("GrafismBundle:Virtual:form/twitter_form.html.twig")
     */
    public function renderTwitterForm()
    {
        $competitionId = $this->getCompetition();

        $context = $this->getContext();

        $form = $this->createForm(TwitterForm::class, [
            'url' => $this->generateAbsoluteUrl('virtual_twitter'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Twitter'
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName
        ];
    }
}