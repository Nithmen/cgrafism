<?php

namespace GrafismBundle\Controller\Overlay\VirtualLaLiga;

use GrafismBundle\Controller\Overlay\OverlayController;

use GrafismBundle\Form\Virtual\SafestDraftForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SafestDraftController
 */
class SafestDraftController extends OverlayController
{
    /**
     * @Route("/safest-draft", name="virtual_safest_draft")
     * @Template("@Grafism/Virtual/safest_draft.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderSafestDraft(Request $request)
    {
        $params = $request->query->all();

        $players = [];
        foreach($params as $key => $value)
        {
            if(preg_match('~^player~', $key)) {
                $players[$key] = $value;
            }
        }

        $data = $this->getSafestDraftViewData($players);

        return $data;
    }

    private function getSafestDraftViewData($players)
    {
        $data = [];

        foreach($players as $key => $cardPick)
        {
            $data['cards'][$key] = $this->getFifaStatsApi()->getCard($cardPick);
        }

        return $data;
    }

    /**
     * @Route("/safest-draft-form", name="virtual_safest_draft_form")
     * @Template("@Grafism/Virtual/form/safest_draft_form.html.twig")
     *
     * @return array
     */
    public function renderSafestDraftForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $cardList = $this->getFifaStatsApi()->listAllCompetitionCards($competitionId);

        $form = $this->createForm(SafestDraftForm::class, [
            'url' => $this->generateAbsoluteUrl('virtual_safest_draft'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Safest Draft',
            'cards' => $cardList,
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}