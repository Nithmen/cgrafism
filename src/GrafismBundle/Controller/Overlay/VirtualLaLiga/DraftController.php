<?php

namespace GrafismBundle\Controller\Overlay\VirtualLaLiga;

use GrafismBundle\Controller\Overlay\OverlayController;
use GrafismBundle\Form\Virtual\DraftForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DraftController
 */
class DraftController extends OverlayController
{
    /**
     * @Route("/draft", name="virtual_draft")
     * @Template("@Grafism/Virtual/draft.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderDraft(Request $request)
    {
        $matchId = $request->get('match_id');
        $playerId = $request->get('player_id');

        $drafts = $this->getFifaStatsApi()->getMatchDrafts($matchId);

        $data = $this->getDraftViewData($playerId, $drafts);

        return $data;
    }

    private function getDraftViewData($playerId, $drafts)
    {
        $data =  [];

        $side = ($drafts['match']['players']['local']['player']['id'] == $playerId) ? 'local' : 'visitor';

        foreach($drafts['draft'][$side] as $key => $card)
        {
            $data['cards'][$key] = $card['card'];
        }

        $data['side'] = $side;
        $data['player'] = $drafts['match']['players'][$side]['player'];
        // Sort by pick order
        ksort($data['cards']);

        return $data;
    }

    /**
     * @Route("/draft-form", name="virtual_draft_form")
     * @Template("@Grafism/Virtual/form/draft_form.html.twig")
     *
     * @return array
     */
    public function renderInGameForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getFifaStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(DraftForm::class, [
            'url' => $this->generateAbsoluteUrl('virtual_draft'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'Draft',
            'matches' => $matchesList,
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }
}