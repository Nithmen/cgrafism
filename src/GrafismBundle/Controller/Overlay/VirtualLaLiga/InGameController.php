<?php

namespace GrafismBundle\Controller\Overlay\VirtualLaLiga;

use GrafismBundle\Controller\Overlay\OverlayController;

use GrafismBundle\Form\Virtual\InGameForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InGameController
 */
class InGameController extends OverlayController
{
    /**
     * @Route("/in-game", name="virtual_in_game")
     * @Template("@Grafism/Virtual/in_game.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function renderInGame(Request $request)
    {
        $matchId = $request->get('match_id');
        $currentGame = $request->get('game_id', 1);
        $half = $request->get('half_id', 1);
        $localFlag = $request->get('local_flag');
        $visitorFlag = $request->get('visitor_flag');

        $match = $this->getFifaStatsApi()->getMatch($matchId);

        $games = $this->getFifaStatsApi()->getAllMatchGames($matchId);

        $data = $this->getInGameViewData($match, $games, $currentGame, $half, $localFlag, $visitorFlag);

        return $data;
    }

    private function getInGameViewData($match, $games, $currentGame, $half, $localFlag, $visitorFlag)
    {
        $data = [
            'currentGame' => $currentGame,
            'scores' => [
                'local' => 0,
                'visitor' => 0
            ],
            'half' => $half,
            'flags' => [
                'local' => $localFlag,
                'visitor' => $visitorFlag
            ]
        ];

        $localPlayer = $match['match']['players']['local']['player']['id'];

        foreach($games as $game)
        {
            $referenceSide = ($localPlayer == $game['players']['local']['player']['id']) ? 'local' : 'visitor';
            $data['games'][$game['order']]['id'] = $game['id'];

            foreach($game['players'] as $side => $player)
            {
                if($side == $referenceSide) {
                    $data['scores']['local'] += $game['scores'][$side];
                } else {
                    $data['scores']['visitor'] += $game['scores'][$side];
                }

                $data['games'][$game['order']][$side] = [
                    'nickname' => $player['player']['nickname'],
                    'tag' => $player['tag'],
                    'score' => $game['scores'][$side]
                ];
            }
        }

        $data['label'] = $this->getLabel($currentGame);
        $data['round'] = $this->getRound($match['match']['round']);

        return $data;
    }

    /**
     * @Route("/in-game-form", name="virtual_in_game_form")
     * @Template("@Grafism/Virtual/form/in_game_form.html.twig")
     *
     * @return array
     */
    public function renderInGameForm()
    {
        $competitionId = $this->getCompetition();
        $context = $this->getContext();

        $matchesList = $this->getFifaStatsApi()->listAllCompetitionMatches($competitionId);

        $form = $this->createForm(InGameForm::class, [
            'url' => $this->generateAbsoluteUrl('virtual_in_game'),
            'competitionId' => $competitionId,
            'context' => $context
        ], [
            'name' => 'In Game',
            'matches' => $matchesList,
        ]);

        $name = $form->getConfig()->getOption('name');

        $formName = $form->getConfig()->getName();

        return [
            'form' => $form->createView(),
            'name' => $name,
            'formName' => $formName,
            'competitionId' => $competitionId
        ];
    }

    /**
     * @param $game
     *
     * @return string
     */
    private function getLabel($game)
    {
        $label = '';
        switch($game)
        {
            case 1:
                $label = 'Partido de ida';
                break;
            case 2:
                $label = 'Partido de vuelta';
                break;
            case 3:
                $label = 'Partido de desempate';
                break;
        }

        return $label;
    }

    private function getRound($round)
    {
        $label = '';

        switch($round)
        {
            case 1:
                $label = 'SEMIFINALES';
                break;
            case 2:
                $label = 'FINALES';
                break;
            case 3:
                $label = 'KING OF THE HILL';
                break;
        }

        return $label;
    }
}