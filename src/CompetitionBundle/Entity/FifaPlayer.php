<?php

namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="game__fifa_players")
 */
class FifaPlayer
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=45)
     * 
     * @var string
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $firstName;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $lastName;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $league;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $nationality;
    
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    protected $flag;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $club;
    
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    protected $clubImage;
    
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    protected $image;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $quality;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $color;
    
    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    protected $isGk = false;
    
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var string
     */
    protected $position;
    
    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    protected $specialType = false;
    
    /**
     * @ORM\Column(type="json")
     *
     * @var boolean
     */
    protected $stats;
    
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    protected $rating;
    
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @param string $league
     */
    public function setLeague($league)
    {
        $this->league = $league;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * @param string $flag
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;
    }

    /**
     * @return string
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param string $club
     */
    public function setClub($club)
    {
        $this->club = $club;
    }

    /**
     * @return string
     */
    public function getClubImage()
    {
        return $this->clubImage;
    }

    /**
     * @param string $clubImage
     */
    public function setClubImage($clubImage)
    {
        $this->clubImage = $clubImage;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @param string $quality
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return boolean
     */
    public function isIsGk()
    {
        return $this->isGk;
    }

    /**
     * @param boolean $isGk
     */
    public function setIsGk($isGk)
    {
        $this->isGk = $isGk;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return boolean
     */
    public function isSpecialType()
    {
        return $this->specialType;
    }

    /**
     * @param boolean $specialType
     */
    public function setSpecialType($specialType)
    {
        $this->specialType = $specialType;
    }

    /**
     * @return boolean
     */
    public function isStats()
    {
        return $this->stats;
    }

    /**
     * @param boolean $stats
     */
    public function setStats($stats)
    {
        $this->stats = $stats;
    }

    /**
     * @return number
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param number $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    public function __getData() 
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'league' => $this->league,
            'nationality' => $this->nationality,
            'flag' => $this->flag,
            'club' => $this->club,
            'club_img' => $this->clubImage,
            'img' => $this->image,
            'quality' => $this->quality,
            'color' => $this->color,
            'is_gk' => $this->isGk,
            'position' => $this->position,
            'special_type' => $this->specialType,
            'stats' => $this->stats,
            'rating' => $this->rating
        );
    }
    
    public function __toString()
    {
        return sprintf("%s - %s", $this->name, $this->club);
    }
}