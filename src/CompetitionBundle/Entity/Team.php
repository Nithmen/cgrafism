<?php

namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="CompetitionBundle\Entity\Repository\TeamRepository")
 * @ORM\Table(name="game__team")
 * 
 * @author John
 */
class Team {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\Game", fetch="EAGER")
     * @JoinColumn(name="game", referencedColumnName="id", nullable=true)
     * 
     */
    protected $game;
    
    /**
     * @ORM\Column(type="string", length=50)
     * @var string
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @var string
     */
    protected $shortname;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $r;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    protected $g;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    protected $b;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * 
     * @var string
     */
    protected $hex = '#';
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $superligaId;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $image;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_win_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $imageWin;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_set_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $imageSet;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_scoreboard_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $imageScoreboard;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_lineup_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $imageLineup;
    
    public function getId() {
        return $this->id;
    }

    public function getGame() {
        return $this->game;
    }

    public function getName() {
        return $this->name;
    }

    public function setGame($game) {
        $this->game = $game;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    public function getShortname()
    {
        return $this->shortname;
    }

    public function setShortname($shortname)
    {
        $this->shortname = $shortname;
        return $this;
    }

        
    /**
     * @return the $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \CompetitionBundle\Entity\Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return the $r
     */
    public function getR()
    {
        return $this->r;
    }

    /**
     * @return the $g
     */
    public function getG()
    {
        return $this->g;
    }

    /**
     * @return the $a
     */
    public function getB()
    {
        return $this->b;
    }

    /**
     * @param number $r
     */
    public function setR($r)
    {
        $this->r = $r;
        return $this;
    }

    /**
     * @param number $g
     */
    public function setG($g)
    {
        $this->g = $g;
        return $this;
    }

    /**
     * @param number $a
     */
    public function setB($b)
    {
        $this->b = $b;
        return $this;
    }
    

    /**
     * @return the $hex
     */
    public function getHex()
    {
        return $this->hex;
    }

    /**
     * @param string $hex
     */
    public function setHex($hex)
    {
        $this->hex = $hex;
        return $this;
    }
    
    /**
     * @return the $imageWin
     */
    public function getImageWin()
    {
        return $this->imageWin;
    }

    /**
     * @return the $imageSet
     */
    public function getImageSet()
    {
        return $this->imageSet;
    }

    /**
     * @param field_type $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $imageWin
     */
    public function setImageWin($imageWin)
    {
        $this->imageWin = $imageWin;
        return $this;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $imageSet
     */
    public function setImageSet($imageSet)
    {
        $this->imageSet = $imageSet;
        return $this;
    }
    
    /**
     * @return the $imageScoreboard
     */
    public function getImageScoreboard()
    {
        return $this->imageScoreboard;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $imageScoreboard
     */
    public function setImageScoreboard($imageScoreboard)
    {
        $this->imageScoreboard = $imageScoreboard;
        return $this;
    }
    
    /**
     * @return the $superligaId
     */
    public function getSuperligaId()
    {
        return $this->superligaId;
    }

    /**
     * @param field_type $superligaId
     */
    public function setSuperligaId($superligaId)
    {
        $this->superligaId = $superligaId;
    }
    
    /**
     * @return the $imageLineup
     */
    public function getImageLineup()
    {
        return $this->imageLineup;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $imageLineup
     */
    public function setImageLineup($imageLineup)
    {
        $this->imageLineup = $imageLineup;
        return $this;
    }

    public function __toString()
    {
        return sprintf("%s - %s",$this->getName(), $this->getGame());
    }

}
