<?php
namespace CompetitionBundle\Entity;

use GameBundle\Entity\Game;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity
 * @ORM\Table(name="competition__competition")
 */
class Competition
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     * 
     */
    protected $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\Game", fetch="EAGER")
     * @JoinColumn(name="game", referencedColumnName="id", nullable=false)
     * 
     * @var Game
     */
    protected $game;
        
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return the $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param field_type $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return $game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param \GameBundle\Entity\Game $game
     */
    public function setGame($game)
    {
        $this->game = $game;
    }

    public function __toString()
    {       
        return sprintf("%s - %s", $this->getName(), $this->getGame());
    }
    
}

