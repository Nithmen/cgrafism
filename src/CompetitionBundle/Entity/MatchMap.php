<?php
namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="CompetitionBundle\Entity\Repository\MatchMapRepository")
 * @ORM\Table(name="competition__match_map")
 *
 */
class MatchMap
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Match", inversedBy="matchMaps")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id", nullable=true)
     *
     * @var Match
     */
    protected $match;
    
    /**
     * @ORM\Column(type="integer", nullable=false, name="`set`")
     * 
     * @var integer
     */
    protected $set;
    
    /**
     * @ORM\ManyToOne(targetEntity="Player", fetch="EAGER")
     * @JoinColumn(name="player_one", referencedColumnName="id", nullable=false)
     * 
     */
    protected $playerOne;
    
    /**
     * @ORM\ManyToOne(targetEntity="Player", fetch="EAGER")
     * @JoinColumn(name="player_two", referencedColumnName="id", nullable=false)
     *
     */
    protected $playerTwo;
    
    /**
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    protected $crownsOne = 0;
    
    /**
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    protected $crownsTwo = 0;
    
    /**
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Player $playerOne
     */
    public function getPlayerOne()
    {
        return $this->playerOne;
    }

    /**
     * @return Player $playerTwo
     */
    public function getPlayerTwo()
    {
        return $this->playerTwo;
    }

    /**
     * @return integer $crownsOne
     */
    public function getCrownsOne()
    {
        return $this->crownsOne;
    }

    /**
     * @return integer $crownsTwo
     */
    public function getCrownsTwo()
    {
        return $this->crownsTwo;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param Player $playerOne
     */
    public function setPlayerOne($playerOne)
    {
        $this->playerOne = $playerOne;
        return $this;
    }

    /**
     * @param Player $playerTwo
     */
    public function setPlayerTwo($playerTwo)
    {
        $this->playerTwo = $playerTwo;
        return $this;
    }


    /**
     * @param string $crownsOne
     */
    public function setCrownsOne($crownsOne)
    {
        $this->crownsOne = $crownsOne;
        return $this;
    }

    /**
     * @param string $crownsTwo
     */
    public function setCrownsTwo($crownsTwo)
    {
        $this->crownsTwo = $crownsTwo;
        return $this;
    }
    
    /**
     * @return the $matchSet
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param \CompetitionBundle\Entity\MatchSet $matchSet
     */
    public function setMatch($match)
    {
        $this->match = $match;
        return $this;
    }

    /**
     * @return the $set
     */
    public function getSet()
    {
        return $this->set;
    }

    /**
     * @param number $set
     */
    public function setSet($set)
    {
        $this->set = $set;
        return $this;
    }

    public function __toString()
    {
        return sprintf("Set %s -  %s vs %s", $this->getSet(), $this->getPlayerOne(), $this->getPlayerTwo());    
    }
}

