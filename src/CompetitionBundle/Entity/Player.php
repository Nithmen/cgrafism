<?php

namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Application\Sonata\MediaBundle\Entity\Media;

/**
 * @ORM\Entity
 * @ORM\Table(name="game__player")
 *
 * @author John
 */
class Player {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
        
    /**
     * @ORM\Column(type="string", length=30)
     * 
     * @var string
     */
    protected $nickname;
    
    /**
     * @ORM\Column(type="string", length=100)
     * 
     * @var string
     */
    protected $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Team", fetch="EAGER")
     * @JoinColumn(name="team", referencedColumnName="id", nullable=true)
     */
    protected $team;
        
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $steamId;
    
    /**
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\Game", fetch="EAGER")
     * @JoinColumn(name="game", referencedColumnName="id", nullable=true)
     * 
     */
    protected $game;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $image;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_lineup_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $imageLineup;
    
    /**
     * @return the $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;   
    }

    public function getId() {
        return $this->id;
    }

    public function getNickname() {
        return $this->nickname;
    }

    public function getName() {
        return $this->name;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setNickname($nickname) {
        $this->nickname = $nickname;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    
    public function __toString() 
    {
        return sprintf("%s", $this->getNickname());
    }
    
    public function getTeam() {
        return $this->team;
    }

    public function getGame()
    {
        return $this->game;
    }

    public function setGame($game)
    {
        $this->game = $game;
        return $this;
    }

    public function setTeam($team) {
        $this->team = $team;
        return $this;
    }
    
    
    
    /**
     * @return the $steamId
     */
    public function getSteamId()
    {
        return $this->steamId;
    }

    /**
     * @param string $steamId
     */
    public function setSteamId($steamId)
    {
        $this->steamId = $steamId;
        
        return $this;
    }

    /**
     * @return the $imageLineup
     */
    public function getImageLineup()
    {
        return $this->imageLineup;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $imageLineup
     */
    public function setImageLineup($imageLineup)
    {
        $this->imageLineup = $imageLineup;
        return $this;
    }
}
