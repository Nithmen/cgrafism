<?php

namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use GameBundle\Entity\Card;

/**
 * @ORM\Entity
 * @ORM\Table(name="competition__ban")
 *
 */
class MatchBan
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Match", inversedBy="bans")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id", nullable=true)
     * 
     * @var Match
     */
    protected $match;
    
    /**
     * @ORM\Column(type="integer", name="`set`", nullable=true)
     * 
     */
    protected $set;
    
    /**
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\Card", cascade={"persist"})
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id", nullable=true)
     * 
     * @var Card
     */
    protected $card;
    
    /**
     * @ORM\ManyToOne(targetEntity="CompetitionProfile", cascade={"persist"})
     * @ORM\JoinColumn(name="competition_profile_id", referencedColumnName="id", nullable=true)
     * 
     * @var CompetitionProfile
     */
    protected $team;
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return the $match
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @return the $set
     */
    public function getSet()
    {
        return $this->set;
    }

    /**
     * @return the $card
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @return the $team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param field_type $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param \CompetitionBundle\Entity\Match $match
     */
    public function setMatch($match)
    {
        $this->match = $match;
        return $this;
    }

    /**
     * @param field_type $set
     */
    public function setSet($set)
    {
        $this->set = $set;
        return $this;
    }

    /**
     * @param \GameBundle\Entity\Card $card
     */
    public function setCard($card)
    {
        $this->card = $card;
        return $this;
    }

    /**
     * @param \CompetitionBundle\Entity\CompetitionProfile $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
        return $this;
    }

    public function __toString()
    {
        return sprintf("%s", $this->getCard()->getName());
    }
}

