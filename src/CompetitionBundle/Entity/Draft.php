<?php
namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="competition__draft")
 */
class Draft
{
    const SIDE_LOCAL = 100;
    const SIDE_VISITOR = 200;
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Match")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id", nullable=true)
     *
     * @var Match
     */
    protected $match;
    
    /**
     * @ORM\ManyToOne(targetEntity="MatchDraft", inversedBy="drafts")
     * @ORM\JoinColumn(name="matchdaft_id", referencedColumnName="id", nullable=true)
     *
     * @var Match
     */
    protected $matchDraft;
    
    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="FifaPlayerChoice", indexBy="cardNumber", orphanRemoval=true, cascade={"persist", "remove"})
     * @ORM\JoinTable(name="competition__draft_choices",
     * joinColumns={@ORM\JoinColumn(name="draft_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="fifa_player_id", referencedColumnName="id", unique=false)})
     *
     */
    protected $fifaPlayers;
    
    /**
     * @ORM\ManyToOne(targetEntity="CompetitionProfile", cascade={"persist"})
     * @ORM\JoinColumn(name="competition_profile_id", referencedColumnName="id", nullable=true)
     *
     * @var CompetitionProfile
     */
    protected $player;
    
    /**
     * @ORM\Column(type="integer")
     * 
     * @var string
     */
    protected $side = self::SIDE_LOCAL;
    
    public function __construct()
    {
        $this->fifaPlayers = new ArrayCollection();
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \CompetitionBundle\Entity\Match
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param \CompetitionBundle\Entity\Match $match
     */
    public function setMatch($match)
    {
        $this->match = $match;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFifaPlayers()
    {
        return $this->fifaPlayers;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $fifaPlayers
     */
    public function setFifaPlayers($fifaPlayers)
    {
        $this->fifaPlayers = $fifaPlayers;
    }
    
    /**
     * 
     * @param FifaPlayerChoice $player
     * @throws \Exception
     * @return boolean
     */
    public function addFifaPlayer(FifaPlayerChoice $player)
    {
        if($this->fifaPlayers->contains($player)) {
            throw new \Exception('This player already is in the draft.');
        }
        
        $this->fifaPlayers->add($player);
        
        return true;
    }
    
    /**
     *
     * @param FifaPlayerChoice $player
     * @throws \Exception
     * @return boolean
     */
    public function removeFifaPlayer(FifaPlayerChoice $player)
    {
        if(!$this->fifaPlayers->contains($player)) {
            throw new \Exception('This player is not in the draft.');
        }
        
        $this->fifaPlayers->removeElement($player);
        
        return true;
    }
    
    /**
     * Removes all the fifaplayers from the collection.
     */
    public function removeFifaPlayers()
    {
        $this->fifaPlayers->clear();
    }
    
    /**
     * @return \CompetitionBundle\Entity\CompetitionProfile
     */
    public function getPlayer()
    {
        return $this->player;
    }
    
    /**
     * @param CompetitionProfile $player
     * 
     * @return \CompetitionBundle\Entity\Draft
     */
    public function setPlayer(CompetitionProfile $player)
    {
        $this->player = $player;
        return $this;
    }
    
    /**
     * @return \CompetitionBundle\Entity\Match
     */
    public function getMatchDraft()
    {
        return $this->matchDraft;
    }
    
    /**
     * @param MatchDraft $matchDraft
     * 
     * @return \CompetitionBundle\Entity\Draft
     */
    public function setMatchDraft(MatchDraft $matchDraft)
    {
        $this->matchDraft = $matchDraft;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getSide()
    {
        return $this->side;
    }
    
    /**
     * @param integer $side
     * 
     * @return \CompetitionBundle\Entity\Draft
     */
    public function setSide($side)
    {
        $this->side = $side;
        return $this;
    }
}

