<?php

namespace CompetitionBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use GameBundle\Entity\Game;

class TeamRepository extends EntityRepository{
   public function getTeamsByGame(Game $game)
   {       
       return $this->createQueryBuilder('t')
            ->where("t.game = :game")
            ->setParameter('game', $game)
            ->orderBy('t.name');
   }
}
