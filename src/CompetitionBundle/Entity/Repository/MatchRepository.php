<?php

namespace CompetitionBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use CompetitionBundle\Entity\Match;
use CompetitionBundle\Entity\CompetitionProfile;
use CompetitionBundle\Entity\MatchMap;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMappingBuilder;


class MatchRepository extends EntityRepository
{       
    public function getMatchScoreByMatch(Match $match)
    {        
        $sets = $this->_em->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        $results['scoreOne'] = 0;
        $results['scoreTwo'] = 0;

        for($i = 1; $i <= $sets; $i++)
        {
            $qb = $this->_em->getRepository('CompetitionBundle:MatchMap')->createQueryBuilder('m2'); 
            $qb->select('count(m2)');            
            $qb->where('m2.crownsOne > m2.crownsTwo');
            
            $qb->andWhere('m2.set = :setNumber');
            $qb->setParameter('setNumber', $i);
            
            $qb->andWhere('m2.match = :match');
            $qb->setParameter('match', $match);
            
            $scoresOne[$i] = $qb->getQuery()->getSingleScalarResult();
             
            $qb = $this->_em->getRepository('CompetitionBundle:MatchMap')->createQueryBuilder('m2');
            $qb->select('count(m2)');
            $qb->where('m2.crownsOne < m2.crownsTwo');
            
            $qb->andWhere('m2.set = :setNumber');
            $qb->setParameter('setNumber', $i);
            
            $qb->andWhere('m2.match = :match');
            $qb->setParameter('match', $match);
            
            $scoresTwo[$i] = $qb->getQuery()->getSingleScalarResult();
            
            if($scoresOne[$i] == 2 || $scoresTwo[$i] == 2)
            {
                if($scoresOne[$i] > $scoresTwo[$i])
                {
                    $results['scoreOne'] += 1;
                }
                else if($scoresOne[$i] < $scoresTwo[$i])
                {
                    $results['scoreTwo'] += 1;
                }
            }
        }
        
        $results['teamOne'] = $scoresOne;
        $results['teamTwo'] = $scoresTwo;
                
        return $results;
    }
    
    public function getActiveSetByMatch(Match $match)
    {
        $scores = $this->getMatchScoreByMatch($match);
        
        $activeSets = count($scores['teamOne']);
              
        return $activeSets;
    }
    
    public function getPlayerSetChanges(Match $match)
    {
        $sets = $this->_em->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        $results = array();
        
        //Team 1
        $rsm = new ResultSetMappingBuilder($this->_em);
        
        $rsm->addRootEntityFromClassMetadata('CompetitionBundle\Entity\MatchMap', 'mm');
        
        //new
        $sql = 'SELECT *
            FROM competition__match_map mm
            WHERE mm.match_id = :match
                and mm.set = :nowSet
                and mm.player_one NOT IN (
                    SELECT mm2.player_one
                    FROM competition__match_map mm2
                    WHERE mm2.match_id = :match
                        and mm2.set = :lastSet )';
        
        
        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter('match', $match->getId());
        $query->setParameter('lastSet',$sets-1);
        $query->setParameter('nowSet', $sets);
        
        $r = $query->getOneOrNullResult();
        if($r)
        {
            $results[] = $r->getPlayerOne();
        }
        
        //last
        $sql = 'SELECT *
            FROM competition__match_map mm
            WHERE mm.match_id = :match
                and mm.set = :lastSet
                and mm.player_one NOT IN (
                    SELECT mm2.player_one
                    FROM competition__match_map mm2
                    WHERE mm2.match_id = :match
                        and mm2.set = :nowSet )';
        
        
        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter('match', $match->getId());
        $query->setParameter('lastSet',$sets-1);
        $query->setParameter('nowSet', $sets);
        
        $r = $query->getOneOrNullResult();
        if($r)
        {
            $results[] = $r->getPlayerOne();
        }
        
        //Team 2
        //new
        $sql = 'SELECT * 
            FROM competition__match_map mm
            WHERE mm.match_id = :match 
                and mm.set = :nowSet
                and mm.player_two NOT IN (
                    SELECT mm2.player_two 
                    FROM competition__match_map mm2 
                    WHERE mm2.match_id = :match 
                        and mm2.set = :lastSet )';
        
        
        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter('match', $match->getId());
        $query->setParameter('lastSet',$sets-1);
        $query->setParameter('nowSet', $sets);
        
        $r = $query->getOneOrNullResult();
        if($r)
        {
            $results[] = $r->getPlayerTwo();
        }
        
        //last
        $sql = 'SELECT *
            FROM competition__match_map mm
            WHERE mm.match_id = :match
                and mm.set = :lastSet
                and mm.player_two NOT IN (
                    SELECT mm2.player_two
                    FROM competition__match_map mm2
                    WHERE mm2.match_id = :match
                        and mm2.set = :nowSet )';
        
        
        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter('match', $match->getId());
        $query->setParameter('lastSet',$sets-1);
        $query->setParameter('nowSet', $sets);
        
        $r = $query->getOneOrNullResult();
        if($r)
        {
            $results[] = $r->getPlayerTwo();
        }
        
        return $results;
    }
        
    
    public function getLastJornadaByCompetition($competition)
    {
        $qb = $this->createQueryBuilder('c');
        
        $qb->select('MAX(c.jornada)');
        $qb->where('c.competition = :competition');
        $qb->setParameter('competition', $competition);
        
        return $qb->getQuery()->getOneOrNullResult();
    }
}
