<?php

namespace CompetitionBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use CompetitionBundle\Entity\Match;
use CompetitionBundle\Entity\CompetitionProfile;
use CompetitionBundle\Entity\Competition;
use CompetitionBundle\Entity\Player;


class MatchMapRepository extends EntityRepository
{
    public function countSetsByMatch(Match $match)
    {
        $qb = $this->createQueryBuilder("ms");
        
        $qb->select("count(distinct ms.set)");
        $qb->where("ms.match = :match");
        $qb->setParameter("match", $match);
                
        return $qb->getQuery()->getSingleScalarResult();
    }
    
    public function getActiveMapByMatch(Match $match)
    {
        $nowSet = $this->_em->getRepository('CompetitionBundle:Match')->getActiveSetByMatch($match);
        
        $matchMaps = $this->getMapsByMatchAndSetA($match, $nowSet);
        
        if(($matchMaps[0]['crownsOne'] == 0) && ($matchMaps[0]['crownsTwo'] == 0))
        {
            $activeMap = 1;
        }
        else if((($matchMaps[0]['crownsOne'] > $matchMaps[0]['crownsTwo']) && ($matchMaps[1]['crownsOne'] < $matchMaps[1]['crownsTwo'])) || (($matchMaps[0]['crownsOne'] < $matchMaps[0]['crownsTwo']) && ($matchMaps[1]['crownsOne'] > $matchMaps[1]['crownsTwo'])) )
        {
            $activeMap = 3;
        }
        else
        {
            $activeMap = 2;
        }
        
        return $activeMap;
    }
    
    public function getMapsByMatchAndSet(Match $match, $set)
    {
        $nowSet = $this->_em->getRepository('CompetitionBundle:Match')->getActiveSetByMatch($match);
        
        $qb = $this->createQueryBuilder('m2');
        $qb->where('m2.match = :match');
        $qb->setParameter('match', $match);
        $qb->andWhere('m2.set = :set');
        $qb->setParameter('set', $nowSet);
        
        return $qb->getQuery()->getResult();
    }
    
    public function getMapsByMatchAndSetA(Match $match, $set)
    {
        $nowSet = $this->_em->getRepository('CompetitionBundle:Match')->getActiveSetByMatch($match);
        
        $qb = $this->createQueryBuilder('m2');
        $qb->where('m2.match = :match');
        $qb->setParameter('match', $match);
        $qb->andWhere('m2.set = :set');
        $qb->setParameter('set', $nowSet);
        
        return $qb->getQuery()->getArrayResult();
    }
    
    public function getCrownAvgByCompetition(Competition $competition)
    {
        $qb = $this->createQueryBuilder('mm')
        ->select('mm')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'mm.match = m')
        ->innerJoin('CompetitionBundle:Player', 'p1', 'WITH', 'mm.playerOne = p1')
        ->innerJoin('CompetitionBundle:Player', 'p2', 'WITH', 'mm.playerTwo = p2')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->andWhere('mm.crownsOne > 0 or mm.crownsTwo > 0');
        
        $result = $qb->getQuery()->getResult();
        
        $players = array();
        $defaults = array('player' => null, 'victories' => 0, 'defeats' => 0, 'crownsIn' => 0, 'crownsOut' => 0, 'timesPlayed' => 0);
        
        /** @var MatchMap $map */
        foreach($result as $map)
        {
            $playerOne = $map->getPlayerOne()->getNickname();
            if(!isset($players[$playerOne]))
            {
                $players[$playerOne] = $defaults;
                $players[$playerOne]['player'] = $map->getPlayerOne();
            }
            
            $playerTwo = $map->getPlayerTwo()->getNickname();
            if(!isset($players[$playerTwo]))
            {
                $players[$playerTwo] = $defaults;
                $players[$playerTwo]['player'] = $map->getPlayerTwo();
            }
            
            if($map->getCrownsOne() > $map->getCrownsTwo())
            {
                $players[$playerOne]['victories'] += 1;
                $players[$playerTwo]['defeats'] += 1;
            }
            else if($map->getCrownsOne() < $map->getCrownsTwo())
            {
                $players[$playerTwo]['victories'] += 1;
                $players[$playerOne]['defeats'] += 1;
            }
            
            //Crowns
            $players[$playerOne]['crownsIn'] += $map->getCrownsOne();
            $players[$playerOne]['crownsOut'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsIn'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsOut'] += $map->getCrownsOne();
            
            //Times played
            $players[$playerOne]['timesPlayed'] += 1;
            $players[$playerTwo]['timesPlayed'] += 1;
        }
        
        $data = array();
        
        /** @var Player $player */
        foreach($players as $player)
        {                    
            if($player['player']->getTeam() != null)
            {
                //MATCH NUMBER PLAYED
                if($player['timesPlayed'] >= 15)
                {
                    $data[] = array(
                        'player' => $player['player'],
                        'victories' => $player['victories'],
                        'defeats' => $player['defeats'],
                        'winrate' => round($player['victories'] / ($player['victories'] +  $player['defeats']) * 100, 2),
                        'crownAvg' => round(sprintf("%s",($player['crownsIn'] - $player['crownsOut']) / $player['timesPlayed']), 2),
                    );
                }
            
            }
        }        
        
        $players = array();
        
        foreach($data as $key => $player)
        {
            $players[$key] = array('winrate' => $player['winrate'],'crownAvg' => $player['crownAvg'], 'victories' => $player['victories'], 'player' => $player['player']);
        }
        
        array_multisort($players, SORT_DESC, $data);
        
         foreach($players as $key => $p)
        {
            if($p['victories'] <= 1)
            {
                unset($players[$key]);
            }
        }
        
        $players = array_values($players);
        
       return $players;
    }
    
    public function getWinrateByPlayer(Competition $competition, $id)
    {
        $qb = $this->createQueryBuilder('mm')
        ->select('mm')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'mm.match = m')
        ->innerJoin('CompetitionBundle:Player', 'p1', 'WITH', 'mm.playerOne = p1')
        ->innerJoin('CompetitionBundle:Player', 'p2', 'WITH', 'mm.playerTwo = p2')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->andWhere('mm.crownsOne > 0 or mm.crownsTwo > 0');
        
        $result = $qb->getQuery()->getResult();
        
        $players = array();
        $defaults = array('player' => null, 'victories' => 0, 'defeats' => 0, 'crownsIn' => 0, 'crownsOut' => 0, 'timesPlayed' => 0);
        
        /** @var MatchMap $map */
        foreach($result as $map)
        {
            $playerOne = $map->getPlayerOne()->getNickname();
            if(!isset($players[$playerOne]))
            {
                $players[$playerOne] = $defaults;
                $players[$playerOne]['player'] = $map->getPlayerOne();
            }
            
            $playerTwo = $map->getPlayerTwo()->getNickname();
            if(!isset($players[$playerTwo]))
            {
                $players[$playerTwo] = $defaults;
                $players[$playerTwo]['player'] = $map->getPlayerTwo();
            }
            
            if($map->getCrownsOne() > $map->getCrownsTwo())
            {
                $players[$playerOne]['victories'] += 1;
                $players[$playerTwo]['defeats'] += 1;
            }
            else if($map->getCrownsOne() < $map->getCrownsTwo())
            {
                $players[$playerTwo]['victories'] += 1;
                $players[$playerOne]['defeats'] += 1;
            }
            
            //Crowns
            $players[$playerOne]['crownsIn'] += $map->getCrownsOne();
            $players[$playerOne]['crownsOut'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsIn'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsOut'] += $map->getCrownsOne();
            
            //Times played
            $players[$playerOne]['timesPlayed'] += 1;
            $players[$playerTwo]['timesPlayed'] += 1;
        }
        
        $p = $this->getEntityManager()->getRepository('CompetitionBundle:Player')->find($id);
        
        return $players[$p->getNickname()];
    }
    
    public function getWinConditionByCompetitionAndPlayer(Competition $competition, Player $player)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('GameBundle:Deck')->createQueryBuilder('d');
        $qb->select('count(c) as times, c')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'd.match = m')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->innerJoin('GameBundle:Card', 'c', 'WITH', 'd.winCondition = c')
        ->innerJoin('CompetitionBundle:Player', 'p', 'WITH', 'd.player = p')
        ->andWhere('p = :player')
        ->setParameter('player', $player)
        ->groupBy('c, p')
        ->orderBy('times', 'DESC');
        //         ->setMaxResults(1);
        
        $result = $qb->getQuery()->getResult();
        
        return $result;
        
    }
}

