<?php

namespace CompetitionBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use GameBundle\Entity\Game;
use CompetitionBundle\Entity\Match;

class MatchBanRepository extends EntityRepository{
   public function getBansByMatch(Match $match)
   {       
       $bans = array();
       
       for($i = 2; $i < 5; $i++)
       {
           $bans['teamOne'][$i] = null;
           $bans['teamTwo'][$i] = null;
           
           $qb = $this->createQueryBuilder("qb");
           $qb->select('c.media_id');
           $qb->from('GameBundle\Entity\Card', 'c');
           $qb->from('CompetitionBundle\Entity\MatchBan', 'b');
           $qb->where("b.set = :set");
           $qb->setParameter('set', $i);
           $qb->andWhere('b.competition_profile_id = :team');
           $qb->setParameter('team', $match->getTeamOne());
           $qb->andWhere("b.card_id = c.id");
           
           dump($qb->getQuery()->getResult());
           die();
       }
       
       
   }
}
