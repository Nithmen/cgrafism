<?php
namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;

use CompetitionBundle\Entity\Competition;

/**
 * @ORM\Entity
 * @ORM\Table(name="competition__competition_profile")
 *
 */
class CompetitionProfile
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Competition", fetch="EAGER")
     * @JoinColumn(name="competition", referencedColumnName="id", nullable=false)
     * 
     * @var Competition
     */
    protected $competition;
    
    /**
     * @ORM\ManyToOne(targetEntity="Team",fetch="EAGER" )
     * @JoinColumn(name="player", referencedColumnName="id", nullable=true)
     * 
     */
    protected $team;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $rank;
    
    /**
     * 
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $wins = 0;
    
    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $loses = 0;
    
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return the $competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @return the $team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @return the $rank
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @return the $wins
     */
    public function getWins()
    {
        return $this->wins;
    }

    /**
     * @return the $loses
     */
    public function getLoses()
    {
        return $this->loses;
    }

    /**
     * @param field_type $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @param \CompetitionBundle\Entity\Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
        return $this;
    }

    /**
     * @param field_type $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
        return $this;
    }

    /**
     * @param field_type $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     * @param field_type $wins
     */
    public function setWins($wins)
    {
        $this->wins = $wins;
        return $this;
    }

    /**
     * @param field_type $loses
     */
    public function setLoses($loses)
    {
        $this->loses = $loses;
        return $this;
    }
    
    public function __toString() 
    {
        return sprintf("%s",$this->getTeam());
    }
}

