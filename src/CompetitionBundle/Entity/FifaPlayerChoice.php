<?php
namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="competition__fifa_players_choices")
 */
class FifaPlayerChoice
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="FifaPlayer", fetch="EAGER")
     * @ORM\JoinColumn(name="fifa_player_id", referencedColumnName="id", nullable=false)
     *
     * @var FifaPlayer
     */
    protected $fifaPlayer;
    
    /**
     * @ORM\ManyToOne(targetEntity="CompetitionProfile", cascade={"persist"})
     * @ORM\JoinColumn(name="competition_profile_id", referencedColumnName="id", nullable=false)
     * 
     * @var CompetitionProfile
     */
    protected $player;
    
    /**
     * @ORM\ManyToOne(targetEntity="Draft")
     * @ORM\JoinColumn(name="draft_id", referencedColumnName="id", nullable=false)
     *
     * @var Draft
     */
    protected $draft;
    
    /**
     * @ORM\Column(type="integer", name="`order`", nullable=false)
     *
     * @var integer
     */
    protected $order = 0;
    
    /**
     * @ORM\Column(type="integer", name="card_number", nullable=true)
     * 
     * @var integer
     */
    protected $cardNumber = 0;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \CompetitionBundle\Entity\FifaPlayer
     */
    public function getFifaPlayer()
    {
        return $this->fifaPlayer;
    }

    /**
     * @param \CompetitionBundle\Entity\FifaPlayer $fifaPlayer
     */
    public function setFifaPlayer($fifaPlayer)
    {
        $this->fifaPlayer = $fifaPlayer;
    }

    /**
     * @return \CompetitionBundle\Entity\CompetitionProfile
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param \CompetitionBundle\Entity\CompetitionProfile $player
     */
    public function setPlayer($player)
    {
        $this->player = $player;
    }

    /**
     * @return \CompetitionBundle\Entity\Match
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param \CompetitionBundle\Entity\Match $match
     */
    public function setMatch($match)
    {
        $this->match = $match;
    }

    /**
     * @return number
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param number $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
    
    /**
     * @return integer
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }
    
    /**
     * @param integer $cardNumber
     * 
     * @return \CompetitionBundle\Entity\FifaPlayerChoice
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }
    /**
     * @return \CompetitionBundle\Entity\Draft
     */
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * @param \CompetitionBundle\Entity\Draft $draft
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;
        return $this;
    }

    
    
}

