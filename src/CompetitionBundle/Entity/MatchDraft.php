<?php
namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="competition__match_draft")
 */
class MatchDraft
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Match")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id", nullable=true)
     *
     * @var Match
     */
    protected $match;
    
    /**
     * @ORM\OneToMany(targetEntity="Draft", mappedBy="matchDraft", fetch="EAGER", indexBy="side", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $drafts;
    
    public function __construct(){
        $this->drafts = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \CompetitionBundle\Entity\Match
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param \CompetitionBundle\Entity\Match $match
     */
    public function setMatch($match)
    {
        $this->match = $match;
    }

    /**
     * @param mixed $draft
     */
    public function setDrafts($drafts)
    {
        $this->drafts = $drafts;
    }
    
    /**
     * Returns if the MatchDraft has Drafts or not.
     * 
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->drafts->isEmpty();
    }
    
    /**
     * 
     * @param CompetitionProfile $team
     * 
     * @throws \Exception
     * 
     * @return NULL| Draft
     */
    public function getDraft(CompetitionProfile $team = null)
    {
        if($this->drafts->isEmpty()) {
            throw new \Exception("There are no drafts");
        }
        
        if($team) {
            /** @var Draft $draft */
            foreach($this->drafts as $d) {
                if($d->getPlayer() == $team) {
                    $draft = $d;
                }
            }
            
            if(!$draft) {
                return null;
            }
            
            return $draft;
        }
        
        return $this->draft;
    }
    
    public function getDrafts()
    {
        return $this->drafts;
    }
    
    /**
     * @param Draft $draft
     * @return boolean
     */
    public function addDraft(Draft $draft) 
    {
        $this->drafts->add($draft);
        
        return true;
    }
}

