<?php
namespace CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="CompetitionBundle\Entity\Repository\MatchRepository")
 * @ORM\Table(name="competition__match")
 *
 */
class Match
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Competition", fetch="EAGER")
     * @ORM\JoinColumn(name="competition", referencedColumnName="id", nullable=true)
     * 
     * @var Competition
     */
    protected $competition;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     * 
     * @var integer
     */
    protected $jornada;
    
    /**
     * @ORM\ManyToOne(targetEntity="CompetitionProfile", fetch="EAGER")
     * @ORM\JoinColumn(name="team_one", referencedColumnName="id", nullable=true)
     * 
     * @var Team
     */
    protected $teamOne;
    
    /**
     * @ORM\ManyToOne(targetEntity="CompetitionProfile", fetch="EAGER")
     * @ORM\JoinColumn(name="team_two", referencedColumnName="id", nullable=true)
     *
     * @var Team
     */
    protected $teamTwo;
    
    /**
     * @ORM\Column(type="integer")
     * 
     * @var integer
     */
    protected $scoreOne = 0;
    
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    protected $scoreTwo = 0;
        
    /**
     * @ORM\OneToMany(targetEntity="MatchMap", mappedBy="match", fetch="EAGER", cascade={"persist", "remove"}, orphanRemoval=true)
     * 
     * @var Collection
     */
    protected $matchMaps;
    
    /**
     * @ORM\OneToMany(targetEntity="MatchBan", mappedBy="match", fetch="EAGER", cascade={"persist", "remove"}, orphanRemoval=true)
     * 
     * @var Collection
     */
    protected $bans;
    
    /**
     * @ORM\OneToMany(targetEntity="GameBundle\Entity\Deck", mappedBy="match", fetch="EAGER", cascade={"persist", "remove"}, orphanRemoval=true)
     * 
     * @var Collection
     */
    protected $decks;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $superligaId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    protected $ebotId;
    
    /**
     * @ORM\ManyToOne(targetEntity="MatchDraft", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="matchdraft_id", referencedColumnName="id", nullable=true)
     * 
     * @var MatchDraft
     */
    protected $matchDraft;
    
    public function __construct()
    {
        $this->matchMaps = new ArrayCollection(); 
        $this->bans = new ArrayCollection();
        $this->decks = new ArrayCollection();
    }
    
    /**
     * @return the $bans
     */
    public function getBans()
    {
        return $this->bans;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $bans
     */
    public function setBans($bans)
    {
        $this->bans = $bans;
        return $this;
    }

    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return the $competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @return the $teamOne
     */
    public function getTeamOne()
    {
        return $this->teamOne;
    }

    /**
     * @return the $teamTwo
     */
    public function getTeamTwo()
    {
        return $this->teamTwo;
    }

    /**
     * @return the $scoreOne
     */
    public function getScoreOne()
    {
        return $this->scoreOne;
    }

    /**
     * @return the $scoreTwo
     */
    public function getScoreTwo()
    {
        return $this->scoreTwo;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param \CompetitionBundle\Entity\Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

    /**
     * @param \CompetitionBundle\Entity\CompetitionProfile $teamOne
     */
    public function setTeamOne($teamOne)
    {
        $this->teamOne = $teamOne;
    }

    /**
     * @param \CompetitionBundle\Entity\CompetitionProfile $teamTwo
     */
    public function setTeamTwo($teamTwo)
    {
        $this->teamTwo = $teamTwo;
    }

    /**
     * @param number $scoreOne
     */
    public function setScoreOne($scoreOne)
    {
        $this->scoreOne = $scoreOne;
    }

    /**
     * @param number $scoreTwo
     */
    public function setScoreTwo($scoreTwo)
    {
        $this->scoreTwo = $scoreTwo;
    }
    
    /**
     * @return matchMap $matchMaps
     */
    public function getMatchMaps()
    {
        return $this->matchMaps;
    }

    /**
     * @param \CompetitionBundle\Entity\matchMap $matchMaps
     */
    public function setMatchMaps($matchMaps)
    {
        $this->matchMaps = $matchMaps;
        return $this;
    }
    /**
     * @return $jornada
     */
    public function getJornada()
    {
        return $this->jornada;
    }

    /**
     * @param number $jornada
     */
    public function setJornada($jornada)
    {
        $this->jornada = $jornada;
        return $this;
    }
    
    public function addMatchMap($matchMap) 
    {
        $this->matchMaps->add($matchMap);
        $matchMap->setMatch($this);
    }
    
    public function addBan($ban)
    {
        $this->bans->add($ban);
        $ban->setMatch($this);
    }
    
    public function addDeck($deck)
    {
        $this->decks->add($deck);
        $deck->setMatch($this);
    }
    
    public function getDecks()
    {
        return $this->decks;
    }

    public function setDecks($decks)
    {
        $this->decks = $decks;
        return $this;
    }
        
    /**
     * @return the $superligaId
     */
    public function getSuperligaId()
    {
        return $this->superligaId;
    }

    /**
     * @param field_type $superligaId
     */
    public function setSuperligaId($superligaId)
    {
        $this->superligaId = $superligaId;
        return $this;
    }
    
    

    /**
     * @return the $ebotId
     */
    public function getEbotId()
    {
        return $this->ebotId;
    }

    /**
     * @param field_type $ebotId
     */
    public function setEbotId($ebotId)
    {
        $this->ebotId = $ebotId;
        return $this;
    }
    
    /**
     * @return \CompetitionBundle\Entity\MatchDraft
     */
    public function getMatchDraft()
    {
        return $this->matchDraft;
    }
    
    /**
     * @param MatchDraft $matchDraft
     * 
     * @return \CompetitionBundle\Entity\Match
     */
    public function setMatchDraft(MatchDraft $matchDraft)
    {
        $this->matchDraft = $matchDraft;
        return $this;
    }

    public function __toString()
    {
        return sprintf("Jornada #%s - %s vs %s ", $this->getJornada(), $this->getTeamOne(), $this->getTeamTwo() );
    }
}

