<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * FifaCommand Class.
 */
class FifaCommand extends Command 
{
    /**
     * {@inheritDoc}
     * 
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('fifa:import-players');
        $this->setDescription('Imports FUT18 from API.');
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        $output->write('Importing FROM FUT18 (..) ');
        $io->newLine(2);
        
        $output->write('Importing players...');
        $io->newLine();
        $this->getApplication()->getKernel()->getContainer()->get('api.fut')->getPlayers('https://www.easports.com/uk/fifa/ultimate-team/api/fut/item?jsonParamObject=%7B%22page%22:1,%22club%22:%22240,241,242,243,448,449,450,452,457,461,463,467,472,481,483,573,1853,1860,100888,110062,244,260,459,462,477,479,480,1854,1861,1867,1968,2026,15012,15019,100831,110704,110714,110827,110831,110832,110839,110854%22,%22position%22:%22LF,CF,RF,ST,LW,LM,CAM,CDM,CM,RM,RW,LWB,LB,CB,RB,RWB%22%7D', 53);
        
        $output->write('Importing goalkeepers...');
        $io->newLine();
        $this->getApplication()->getKernel()->getContainer()->get('api.fut')->getPlayers('https://www.easports.com/uk/fifa/ultimate-team/api/fut/item?jsonParamObject=%7B%22page%22:1,%22club%22:%22240,241,242,243,448,449,450,452,457,461,463,467,472,481,483,573,1853,1860,100888,110062,244,260,459,462,477,479,480,1854,1861,1867,1968,2026,15012,15019,100831,110704,110714,110827,110831,110832,110839,110854%22,%22position%22:%22GK%22%7D', 6);
        
        $output->write('Importing Icon Goalkeepers...');
        $io->newLine();
        $this->getApplication()->getKernel()->getContainer()->get('api.fut')->getPlayers('https://www.easports.com/uk/fifa/ultimate-team/api/fut/item?jsonParamObject=%7B%22page%22:1,%22club%22:%22112658%22,%22position%22:%22GK%22%7D', 1);
        
        $output->write('Importinc Icon players...');
        $io->newLine();
        $this->getApplication()->getKernel()->getContainer()->get('api.fut')->getPlayers('https://www.easports.com/uk/fifa/ultimate-team/api/fut/item?jsonParamObject=%7B%22page%22:1,%22club%22:%22112658%22,%22position%22:%22LF,CF,RF,ST,LW,LM,CAM,CDM,CM,RM,RW,LWB,LB,CB,RB,RWB%22%7D', 5);
        
        $output->write('DONE');
    }
}