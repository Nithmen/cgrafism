<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;

/**
 * FifaPlayerCommand Class.
 */
class FifaPlayerCommand extends Command 
{
    /**
     * {@inheritDoc}
     * 
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('fifa:import-player');
        $this->setDescription('Imports FUT18 from API.');
        $this->addArgument('playerId', InputArgument::REQUIRED, 'FUT API Player Id.');
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $playerId = $input->getArgument('playerId');
        
        $io = new SymfonyStyle($input, $output);
        
        $output->write('Importing Player... ');
        $io->newLine(2);
        
        $this->getApplication()->getKernel()->getContainer()->get('api.fut')->getPlayer($playerId);
        
        $output->write('Jugador importado correctamente.');
        $io->newLine();
    }
}