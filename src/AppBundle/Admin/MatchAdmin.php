<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use CompetitionBundle\Entity\Match;
use CompetitionBundle\Entity\MatchMap;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class MatchAdmin extends AbstractAdmin
{    
    protected function configureFormFields(FormMapper $formMapper)
    {                 
        $competition = $this->getEntityManager()->getRepository('CompetitionBundle:Competition')->find($this->getSubject()->getCompetition());
                
        $competitionProfiles = $this->getEntityManager()->createQueryBuilder()
                ->select('cp')
                ->from('CompetitionBundle:CompetitionProfile', 'cp')
                ->where('cp.competition = :competition')
                ->setParameter('competition', $competition);
               
        $formMapper
        ->with('Configuration', array('class' => 'col-md-3'))
        ->add('competition')
        ->add('jornada', NumberType::class)
        
        ->add('teamOne', null, array(
            'query_builder' => $competitionProfiles
        ))
        
        ->add('teamTwo', null, array(
            'query_builder' => $competitionProfiles
        ))->end()
        ->with('Score', array('class' => 'col-md-2'))
        ->add('scoreOne', NumberType::class)
        ->add('scoreTwo', NumberType::class)->end()
        
        ->with('Superliga', array('class' => 'col-md-1'))
        ->add('superligaId', null, array(
            'required' => false
        ))->end();
        
        
        if($this->getSubject()->getCompetition()->getGame()->getName() == "Counter Strike: Global Offensive")
        {
            $formMapper->with('eBot Id', array('class' => 'col-md-1'))
            ->add('ebotId', null, array(
                'required' => false
            ) )->end();
        }
        
        if($this->getSubject()->getCompetition()->getGame()->getName() == "Fifa Ultimate Team" && $this->getSubject()->getId()) {
            $formMapper->add('matchDraft', UrlType::class, array(
                'data' => sprintf('http://45.76.142.122/admin/fut/draft/%s', $this->getSubject()->getId()),
                'disabled' => true
            ))->end();
        }
        
       if($this->getSubject()->getCompetition()->getGame()->getName() == "Clash Royale")
       {
            if ($this->getSubject()->getId())
            {            
                $formMapper->with('Bans', array('class' => 'col-md-7'))
                ->add('bans', 'sonata_type_collection', array(
                    'type_options' => array(
                        'delete' => true
                    )

                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array('match' => $this->getSubject())
                ))->end();

                $formMapper->add('matchMaps', 'sonata_type_collection', array(
                    'type_options' => array(
                        'delete' => true
                    )
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array('match' => $this->getSubject())
                ))->end();

                $formMapper->with('Decks', array('class' => 'col-md-12'))
                ->add('decks', 'sonata_type_collection', array(
                    'type_options' => array(
                        'delete' => true
                    )
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array('match' => $this->getSubject())
                ))->end();
            }
       }
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
        ->add('competition')
        ->add('jornada')
        ->add('teamOne')
        ->add('teamTwo');
        
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('jornada');
    }
    
    protected function getEntityManager()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();
    }
    
    /**
     * crear
     * {@inheritDoc}
     * @see \Sonata\AdminBundle\Admin\AbstractAdmin::prePersist()
     */
    public function prePersist($object)
    {
        foreach($object->getMatchMaps() as $matchMap)
        {
            /** @var MatchMap $matchMap*/
            $matchMap->setMatch($object);
        }
        
        foreach($object->getBans() as $ban)
        {
            $ban->setMatch($object);
        }
        
        foreach($object->getDecks() as $deck)
        {
            $deck->setMatch($object);
        }
    }
    
    /**
     * Editar
     * {@inheritDoc}
     * @see \Sonata\AdminBundle\Admin\AbstractAdmin::preUpdate()
     */
    public function preUpdate($object)
    {
        foreach($object->getMatchMaps() as $matchMap)
        {
            /** @var MatchMap $matchMap*/
            $matchMap->setMatch($object);
        }
        
        foreach($object->getBans() as $ban)
        {
            $ban->setMatch($object);
        }
        
        foreach($object->getDecks() as $deck)
        {
            $deck->setMatch($object);
        }      
        
        if($object->getCompetition()->getGame()->getName() == "Clash Royale")
        {
//             $this->getConfigurationPool()->getContainer()->get('api.superliga')->updateCompetitionProfilesClash($object->getCompetition()->getId());
        }
    }
    
    public function getParentAssociationMapping()
    {
        return 'competition';
    }
}

