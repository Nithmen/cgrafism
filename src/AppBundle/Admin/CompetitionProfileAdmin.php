<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CompetitionProfileAdmin extends AbstractAdmin
{    
    protected function configureFormFields(FormMapper $formMapper)
    {                
        $teams = $this->getEntityManager()->createQueryBuilder()
                ->select('t')
                ->from('CompetitionBundle:Team', 't')
                ->where('t.game = :game')
                ->setParameter('game', $this->getParent()->getSubject()->getGame());
                
        $formMapper->with('Configuration', array('class' => 'col-md-4'))
        ->add('competition',null, array(
            'disabled' => true
        ), array(
            'edit' => 'inline',
            'inline' => 'table',
            'sortable' => 'position'
        ))
        ->add('team', null, array(
            'query_builder' => $teams
        ))->end()
        ->with('Scores', array('class' => 'col-md-2'))
        ->add('rank', NumberType::class, array(
            'required' => false
        ))
        ->add('wins', NumberType::class)
        ->add('loses', NumberType::class);
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
        ->add('competition')
        ->add('team')
        ->add('rank');
        
    }
    
    protected function getEntityManager()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();
    }
    
    public function getParentAssociationMapping()
    {
        return 'competition';
    }
    
}

