<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class GrafismAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('key', TextType::class, array(
                'label' => 'Key'
            ))
            ->add('url', TextType::class, array(
                'label' => 'Url'
            ))
            ->add('formPath', TextType::class, array(
                'label' => 'Form path',
                'required' => false
            ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add('key')
            ->add('url');

    }

    protected function getEntityManager()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();
    }

    public function getParentAssociationMapping()
    {
        return 'competition';
    }
}

