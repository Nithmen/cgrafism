<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
class DeckAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $params = $this->getParameters();
        $match = $params['match'];
        
        $players = $this->getEntityManager()->createQueryBuilder()
                ->select('p')
                ->from('CompetitionBundle:Player' , 'p')
                ->where('p.team = :teamOne')
                ->setParameter('teamOne', $match->getTeamOne()->getTeam())
                ->orwhere('p.team = :teamTwo')
                ->setParameter('teamTwo', $match->getTeamTwo()->getTeam())
                ->orderby('p.nickname', 'ASC');
        
        $formMapper->add('name', TextType::class)
                    ->add('player',null, array(
                        'query_builder' => $players
                    ))
                    ->add('cards', 'sonata_type_model', array(
                        'class' => 'GameBundle\Entity\Card',
                        'multiple' => true,
                        'btn_add' => false
                    ))
                    ->add('winCondition', 'sonata_type_model', array(
                        'class' => 'GameBundle\Entity\Card',
                        'btn_add' => false,
                        'btn_delete' => false,
                        'multiple' => false
                    ))
                    ->add('elixir')
                    ->add('set');
    }
    
    public function getParentAssociationMapping()
    {
        return 'match';
    }
    
    protected function getParameters()
    {
        $params = array();
        if ($this->hasParentFieldDescription())
        {
            $params = $this->getParentFieldDescription()
            ->getOption('link_parameters', array());
        }
        
        return $params;
    }
    
    protected function getEntityManager()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();
    }
}

