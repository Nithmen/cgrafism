<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Knp\Menu\ItemInterface as MenuItemInterface;

class CompetitionAdmin extends AbstractAdmin
{
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class)
                    ->add('game', 'sonata_type_model', array(
                        'class' => 'GameBundle\Entity\Game',
                        'btn_add' => false
                    ));
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
        ->add('name')
        ->add('game');
        
    }
    
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('new','edit', 'show', 'delete'))) {
            return;
        }
        
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        
        
        if ($this->isGranted('LIST')) {
            $menu->addChild('Competition Profiles', array(
                'uri' => $admin->generateUrl('app.admin.competition.competition_profile.list', array('id' => $id))
            ));
            $menu->addChild('Matches', array(
                'uri' => $admin->generateUrl('app.admin.competition.match.list', array('id' => $id))
            )); 
        }
    }
    
}

