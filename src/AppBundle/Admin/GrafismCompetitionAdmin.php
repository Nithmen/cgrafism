<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class GrafismCompetitionAdmin
 */
class GrafismCompetitionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Configuration', array('class' => 'col-md-6'))
            ->add('id', TextType::class, array(
                'label' => 'Competition id'
            ))
            ->add('name', TextType::class, array(
                'label' => 'Name'
            ))->end();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add('name');

    }

    /**
     * @param MenuItemInterface $menu
     *
     * @param $action
     * @param AdminInterface|null $childAdmin
     * @return mixed|void
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('new','edit', 'show', 'delete'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('LIST')) {
            $menu->addChild('Grafisms', array(
                'uri' => $admin->generateUrl('app.admin.grafism.grafism.list', array('id' => $id))
            ));
        }
    }
}