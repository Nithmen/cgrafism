<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Admin\AbstractAdmin;

class CardAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class)
                    ->add('description', TextareaType::class)
                    ->add('elixir', NumberType::class)
                    ->add('rarity', ChoiceType::class, array(
                        'choices' => array(
                            'Common' => 'Common',
                            'Rare' => 'Rare',
                            'Epic' => 'Epic',
                            'Legendary' => 'Legendary'
                        ),
                    ))
                    ->add('image', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'card_media',
                            'catergory' => null
                        )
                    ))
                    ->add('bigImage', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'card_media',
                            'catergory' => null
                        )
                    ))
                    ->add('pipImage', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'card_pip_media',
                            'catergory' => null
                        )
                    ));
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
        ->add('name')
        ->add('elixir')
        ->add('rarity');
        
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
                        ->add('rarity')
                        ->add('elixir');
    }
}

