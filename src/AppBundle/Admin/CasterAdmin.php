<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CasterAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('alias', TextType::class)
                    ->add('name', TextType::class)
                    ->add('lastname', TextType::class)
                    ->add('twitter', TextType::class)
                    ->add('game', 'sonata_type_model', array(
                        'class' => 'GameBundle\Entity\Game',
                        'btn_add' => false
                    ));
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('game');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
                    ->add('alias')
                    ->add('twitter')
                    ->add('game');
    }
}

