<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\Filter\NumberType;

class MatchBanAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $params = $this->getParameters();
        $match = $params['match'];
        
        
        $formMapper->add('set')
        ->add('team', null, array(
            'choices' => array(
                'teamOne' => $match->getTeamOne(),
                'teamTwo' => $match->getTeamTwo()
            )
        ))
        ->add('card', 'sonata_type_model', array(
            'class' => 'GameBundle\Entity\Card',
            'btn_add' => false,
            'btn_delete' => false,
            'multiple' => false
        ));
    }
    
    public function getParentAssociationMapping()
    {
        return 'match';
    }
    
    protected function getParameters()
    {
        $params = array();
        if ($this->hasParentFieldDescription())
        {
            $params = $this->getParentFieldDescription()
            ->getOption('link_parameters', array());
        }
        
        return $params;
    }
    
    protected function getEntityManager()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();
    }
    
}

