<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PlayerAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('nickname', TextType::class)
                    ->add('name', TextType::class)
                    ->add('team', 'sonata_type_model', array(
                        'class' => 'CompetitionBundle\Entity\Team',
                        'btn_add' => false,
                        'required' => false
                    ))
                    ->add('game', 'sonata_type_model', array(
                        'class' => 'GameBundle\Entity\Game',
                        'btn_add' => false
                    ))
                    ->add('steamid', TextType::class, array(
                        'required' => false
                    ))
                    ->add('image', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'player_media',
                            'catergory' => null
                        ))
                    )
                ->add('imageLineup', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'player_lineup_media',
                            'catergory' => null
                        )));
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('game')->add('team');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
                    ->add('nickname')
                    ->add('name')
                    ->add('team')
                    ->add('game');
    }
}

