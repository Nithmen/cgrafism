<?php
namespace AppBundle\Admin\Listener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

class MenuBuilderListener
{
    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();
        
        //////////////////////////////
        //    ESTATISTICS
        //////////////////////////////
        
        $parent = $menu->addChild('Estadísticas')->setExtra('icon', '<i class="fa fa-trophy"></i>');
        
        //Clash Royale
        $clash = $parent->addChild('clash')->setLabel('Clash Royale')->setExtra('icon', '<i class="fa fa-gamepad"></i>');
        $clash->addChild('players', array('route' => 'stats_clash_players'))->setLabel('Players');
        $clash->addChild('teams', array('route' => 'stats_clash_teams'))->setLabel('Teams');
        $clash->addChild('cards', array('route' => 'stats_clash_cards'))->setLabel('Cards');
        
        $fifa = $parent->addChild('fifa')->setLabel('Fifa')->setExtra('icon', '<i class="fa fa-gamepad"></i>');
        $fifa->addChild('Fifa players', array('route' => 'stats_fifa_players'))->setLabel('Cartas');
        
        // FIFA
        $fifa = $menu->addChild('FIFA')->setExtra('icon', '<i class="fa fa-soccer-ball-o"></i>');
        $fifa->addChild('Drafs', ['route' => 'fifa_show_drafts'])->setLabel('Drafts');
        
     
        $streaming = $menu->getChild('app.admin.group.streaming');
        $streaming->addChild('Fifa', array('route' => 'fifa_streaming_drafts'));
    }
}

