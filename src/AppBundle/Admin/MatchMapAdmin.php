<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\QueryBuilder;


class MatchMapAdmin extends AbstractAdmin
{    
    protected function configureFormFields(FormMapper $formMapper)
    {    
        $params = $this->getParameters();
        $match = $params['match'];
                
        $playersOne = $this->getEntityManager()->createQueryBuilder()
                        ->add('select', 'p')
                        ->add('from', 'CompetitionBundle:Player p')
                        ->add('where', 'p.team = :team')
                        ->setParameter('team', $match->getTeamOne()->getTeam());
        
        $playersTwo = $this->getEntityManager()->createQueryBuilder()
                        ->add('select', 'p')
                        ->add('from', 'CompetitionBundle:Player p')
                        ->add('where', 'p.team = :team')
                        ->setParameter('team', $match->getTeamTwo()->getTeam());
        
        $formMapper
        ->add('set')
        ->add('playerOne', null, array(
            'query_builder' => $playersOne
        ))
        ->add('playerTwo', null, array(
            'query_builder' => $playersTwo
        ))
        ->add('crownsOne')
        ->add('crownsTwo');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
        ->add('match')
        ->add('set')
        ->add('playerOne')
        ->add('playerTwo');
        
    }

    protected function getParameters()
    {
        $params = array();
        if ($this->hasParentFieldDescription())
        {
            $params = $this->getParentFieldDescription()
            ->getOption('link_parameters', array());
        }
        
        return $params;
    }
    
    
    protected function getEntityManager()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();
    }
    
    protected function getMatch()
    {
        $parent = $this->getParent();
        
        if ($parent)
        {
            return $parent->getSubject();
        }
        else
        {
            $subject = $this->getSubject();
            return ($subject) ? $subject->getMatch() : null;
        }
    }
    
    
    public function getParentAssociationMapping()
    {
        return 'match';
    }
    
    
}

