<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TeamAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->with('Team configuration', array('class' => 'col-md-4'))
                    ->add('name', TextType::class)
                    ->add('shortname', TextType::class)
                    ->add('game', 'sonata_type_model', array(
                        'class' => 'GameBundle\Entity\Game',
                        'btn_add' => false
                    ))
                    ->add('image', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'team_media',
                            'catergory' => null
                        )
                    ))
                    ->add('imageWin', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'team_win_media',
                            'catergory' => null
                        )
                    ))
                    ->add('imageSet', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'team_set_media',
                            'catergory' => null
                        )
                    ))
                    ->add('imageScoreboard', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'team_scoreboard_media',
                            'catergory' => null
                        )
                    ))
                    ->add('imageLineup', 'sonata_type_model_list', array(), array(
                        'provider' => 'sonata.media.provider.image',
                        'link_parameters' => array(
                            'context' => 'team_lineup_media',
                            'catergory' => null
                        )
                    ))->end()
                    ->with('Styles', array('class' => 'col-md-2'))
                        ->add('r')
                        ->add('g')
                        ->add('b')
                        ->add('hex')->end()
                    ->with('Superliga', array('class' => 'col-md-2'))
                        ->add('superligaId');
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('game');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
                    ->add('name')
                    ->add('game')
                    ->add('r')
                    ->add('g')
                    ->add('b')
                    ->add('hex');
        
    }
}

