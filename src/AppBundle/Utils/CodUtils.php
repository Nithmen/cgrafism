<?php

namespace AppBundle\Utils;

class CodUtils
{
    const hardpointMaps = [
        "Ardennes Forest",
        "Gibraltar",
        "London Docks",
        "Sainte Marie Du Mont",
        "Valkyrie"
    ];
    
    const bydMaps = [
        "Ardennes Forest",
        "London Docks",
        "Sainte Marie Du Mont",
        "USS Texas",
        "Valkyrie"
    ];
    
    const tlbMaps = [
        "Ardennes Forest",
        "Flak Tower",
        "London Docks"
    ];

    /**
     * Get hardpoints name by its Map name.
     * 
     * @param string $map
     * 
     * @return array
     */
    public static function getHardpointsName($map = "Ardennes Forest")
    {
        switch($map)
        {
            case "Ardennes Forest":
                $maps = ['Cueva', 'Ruinas', 'Bunker', 'Carretera Principal'];
                break;
                
            case "Gibraltar":
                $maps = ['Foso (centro)', 'Patio del fuerte', 'Carretera del castillo', 'Torreta'];
                break;
                
            case "London Docks":
                $maps = ['Estatua', 'Carretera Principal', 'Muelle', 'Bodega', 'Grúa'];
                break;
                
            case "Sainte Marie Du Mont":
                $maps = ['Restaurante', 'Solar de la bodega', 'Aparcamiento', 'Torre de vigilancia'];
                break;
                
            case "Valkyrie":
                $maps = ['Ventilación', 'Operaciones', 'Bunker', 'Oficinas', 'Chimeneas'];
                break;
        }
        
        return $maps;
    }
}

