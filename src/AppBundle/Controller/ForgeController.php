<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;

class ForgeController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/forge", name="ui_forge")
     * @Template("AppBundle:Stream:Forge/interface.html.twig")
     */
    public function renderUi()
    {
        
        return array(

        );
    }  
    
    /**
     * @Route("/overlay-forge", name="forge_overlay_ingame")
     * @Template("AppBundle:Stream:Forge/overlay_ingame.html.twig")
     */
    public function renderOverlayIngame(Request $request)
    {
        $queryParams = $request->query->all();
        
        $patch = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_patch'));
        
        return array(
            'data' => $queryParams,
            'patch' => $patch
        );
    }
    
    /**
     * @Route("/overlay-forge-drakes", name="forge_overlay_ingame_drakes")
     * @Template("AppBundle:Stream:Forge/overlay_ingame_drakes.html.twig")
     */
    public function renderOverlayIngameDrakes(Request $request)
    {
        $drake = $request->query->get('drake', "");
        
        switch($drake)
        {
            case "infernal": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/infernal.png";break;
            case "cloud": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/cloud.png";break;
            case "mountain": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/mountain.png";break;
            case "elder": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/elder.png";break;
            case "ocean": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/water.png";break;
            default: $url = " ";
        }
        
        return array(
            'drake' => $url
        );
    }
    
    /**
     * @Route("/overlay-forge-twitter", name="forge_overlay_twitter")
     * @Template("AppBundle:Stream:Forge/twitter.html.twig")
     */
    public function renderChyronTwitter(Request $request)
    {
        $id = $request->get('id');
        $hashtag = $request->get('hashtag', "FORGEOFCHAMPIONS");
        
        /** @var TwitterService $twitter */
        $twitter = $this->get('api.twitter_connection');
        
        $tweet = $twitter->getTweetById($id);
        
        return array(
            'tweet' => $tweet,
            'hashtag' => $hashtag
        );
    }
    
    
     // CASPAR ACTIONS
     
    /**
     * @Route("/caspar-play-lol-forge", name="caspar_play_forge_overlay", options={"expose"=true})
     */
    public function casparOverlayIngame(Request $request)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = "";
        foreach($request->query->all() as $key => $value)
        {
            $route .= "&{$key}={$value}";
        }
        
        $casparConnector->sendHtml(20, "/overlay-forge?{$route}", $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-forge-drakes", name="caspar_play_forge_overlay_drakes", options={"expose"=true})
     */
    public function casparOverlayIngameDrakes(Request $request)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        $drake = $request->request->get('drake', ' ');

        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = '/overlay-forge-drakes?drake='.$drake;
        
        $casparConnector->sendHtml(21, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-tweet-forge", name="caspar_play_tweet_forge", options={"expose"=true})
     */
    public function sendToCasparTweetForge(Request $request)
    {
        $id = $request->get('tweet');
        $hashtag = $request->get('hashtag');
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(23, "/overlay-forge-twitter?id={$id}&hashtag={$hashtag}", $transition->getValue());
        
        return new JsonResponse();
    }
}

