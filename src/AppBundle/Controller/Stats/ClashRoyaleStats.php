<?php
namespace AppBundle\Controller\Stats;

use CompetitionBundle\Entity\Competition;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\DBAL\Query\QueryBuilder;
use GameBundle\Entity\Card;
use CompetitionBundle\Entity\MatchMap;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use CompetitionBundle\Entity\Player;
use CompetitionBundle\Entity\Team;
use CompetitionBundle\Entity\MatchBan;
use Doctrine\Common\Collections\Collection;

class ClashRoyaleStats extends Controller
{
    
    // TEAMS
    
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/admin/cr/stats/teams", name="stats_clash_teams")
     * @Template("AppBundle:Stats:ClashRoyale/cr_teams.html.twig")
     */
    public function renderTeamStatistics()
    {                
        $game = $this->getDoctrine()->getRepository('GameBundle:Game')->findBy(array('name' => 'Clash Royale'));
        
        //$competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('game' => $game), array('id' => 'DESC'));
        
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->find(11);
        
        return array(
            'data' => $this->getTeamsWinRatioByCompetition($competition),
            'teamBans' => $this->getBansByCompetition($competition)
        );
    }
   
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/admin/cr/stats/players", name="stats_clash_players")
     * @Template("AppBundle:Stats:ClashRoyale/cr_players.html.twig")
     */
    public function renderPlayerStatistics()
    {        
        $game = $this->getDoctrine()->getRepository('GameBundle:Game')->findBy(array('name' => 'Clash Royale'));

        $competitions = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findBy(array('game' => $game), array('id' => 'DESC'));

        $players = $this->getPlayersWinRatioByCompetition(array('competition' => $competitions[0]));

        $winC = array();
        
        foreach($players as $player)
        {
            $winCondition = $this->getWinConditionByCompetitionAndPlayer($competitions[0], $player['player']);
            
            $cards = array();
            
            foreach($winCondition as $card)
            {
                $cards[] = array('card' => $card[0], 'times' => $card['times']);
                
            }

            $winC[] = array(
                'player' => $player['player']->getNickname(),
                'team' => (string) $player['player']->getTeam(),
                'cards' => $cards
            );
        }
        
        return array(
            'competitions' => $competitions,
            'winC' => $winC
        );
    }
    
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/admin/cr/stats/cards", name="stats_clash_cards")
     * @Template("AppBundle:Stats:ClashRoyale/cr_cards.html.twig")
     */
    public function renderCardsStatistics()
    {
        $game = $this->getDoctrine()->getRepository('GameBundle:Game')->findBy(array('name' => 'Clash Royale'));
        
        $competitions = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findBy(array('game' => $game), array('id' => 'DESC'));
                       
        $cards = $this->getDoctrine()->getRepository('GameBundle:Card')->findAll();
        
        $count = 0;
        
        foreach ($cards as $card)
        {
            $count += $this->getCountCardUsageByCompetition($competitions[0], $card);            
        }
        
        return array(
            'competitions' => $competitions,
            'count' => $count
        );
    }
    
    /**
     * @Route("/admin/cr/fetch/players", name="fetch_stats_clash_players")
     */
    public function fetchPlayerStatistics(Request $request)
    {
        $query = $request->query->get('query', array());
        
        $players = $this->getPlayersWinRatioByCompetition($query);
        
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => $query['competition']));

        $jornada = $query['jornada'];
        
        if($jornada == 0) {
            $query = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getLastJornadaByCompetition($competition);
            $jornada = $query[1];
        }

        $data = array();
        foreach ($players as $player)
        {
            if($jornada == 0 && $jornada >= $player['timesPlayed']) {
                continue;
            }

            $winCondition = $this->getWinConditionByCompetitionAndPlayer($competition, $player['player']);
                        
            $winratio = 100;
                        
            if($player['defeats'] != 0)
            {
                $winratio = round(($player['victories'] / ($player['victories'] + $player['defeats']) * 100),2);
            }
           
            $data[] = array(
                $player['player']->getNickname(),
                (string) $player['player']->getTeam(),
                $player['victories'],
                $player['defeats'],
                sprintf('%s%%', $winratio),
                round(sprintf("%s",($player['crownsIn'] - $player['crownsOut']) / $player['timesPlayed']), 2),
                empty($winCondition) ? "" : $winCondition[0][0]->getName()
            );
        }
        
        $c1 = array();
        $c2 = array();
        
        foreach($data as $key => $row) {
            $c1[$key] = $row[4];
            $c2[$key] = $row[5];
        }
        
        array_multisort($c1, SORT_DESC, $c2, SORT_DESC, $data);
        
        return new JsonResponse(array(
            'data' => $data
        ), Response::HTTP_OK);
    }
    
    /**
     * @Route("/admin/cr/fetch/playerswin", name="fetch_stats_clash_players_win")
     */
    public function fetchPlayeWinStatistics(Request $request)
    {
        $query = $request->query->get('query', array());
        
        $players = $this->getPlayersWinRatioByCompetition($query);

        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => $query['competition']));
   
        $data = array();
        foreach ($players as $player)
        {
            $winCondition = $this->getWinConditionByCompetitionAndPlayer($competition, $player['player']);

            $cards = array();
             
            foreach($winCondition as $card)
            {
                $cards[] = array('card' => $card[0]->getName(), 'times' => $card['times']);   
            }
            
            $data[] = array(
                $player['player']->getNickname(),
                (string) $player['player']->getTeam(),
                $cards
            );
        }

        return new JsonResponse(array(
            'data' => $data
        ), Response::HTTP_OK);
    }
    
    /**
     * @Route("/admin/cr/fetch/cards", name="fetch_stats_clash_cards")
     */
    public function fetchCardsStatistics(Request $request)
    {
        $query = $request->query->get('query', array());
                                        
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => $query['competition']));
        
        $cards = $this->getDoctrine()->getRepository('GameBundle:Card')->findAll();
        
        $winCondition = 0;
        $usage = 0;
        $ban = 0;
        
        foreach($cards as $card)
        {
            $winCondition += $this->getCountWinConditionByCompetition($competition, $card);
            $usage += $this->getCountCardUsageByCompetition($competition, $card);
            $ban += $this->getBansPerCard($competition, $card);
        }
        
        $data = array();
        foreach($cards as $card)
        {
            
            
            $data[] = array(
                $card->getName(),
                $this->getCountWinConditionByCompetition($competition, $card),
                sprintf("%s%%",round($this->getCountWinConditionByCompetition($competition, $card)/$winCondition * 100,2)),
                $this->getCountCardUsageByCompetition($competition, $card),
                sprintf("%s%%",round($this->getCountCardUsageByCompetition($competition, $card)/$usage * 100,2)),
                $this->getBansPerCard($competition, $card),
                sprintf("%s%%",round($this->getBansPerCard($competition, $card)/$this->getBansPerCompetition($competition) * 100,2)),
            );
            
            
        }
        
        return new JsonResponse(array('data' => $data), Response::HTTP_OK);
    }
    
    
    public function getTeamsWinRatioByCompetition($competition)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $query */
        $query = $em->getRepository('CompetitionBundle:CompetitionProfile')->createQueryBuilder('t');
        
        $result = $query->select('t.wins, t.loses, (t.wins/(t.wins+t.loses) * 100) as wr,t2.name')
            ->where('t.competition = :competition')
            ->setParameter('competition', $competition)
            ->innerJoin('CompetitionBundle:Team', 't2', 'WITH', 't.team = t2')
            ->orderBy('t.rank', 'ASC')
            ->getQuery()->getResult();

            return $result;
    }
    
    public function getPlayersWinRatioByCompetition($query)
    {
        if(!isset($query['competition']))
        {
            return array(); 
        }
        
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('CompetitionBundle:MatchMap')->createQueryBuilder('mm');
        
        $qb->select('mm')
           ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'mm.match = m')
           ->innerJoin('CompetitionBundle:Player', 'p1', 'WITH', 'mm.playerOne = p1')
           ->innerJoin('CompetitionBundle:Player', 'p2', 'WITH', 'mm.playerTwo = p2')
           ->where('m.competition = :competition')
           ->setParameter('competition', $query['competition'])
            ->andWhere('mm.crownsOne > 0 or mm.crownsTwo > 0');
        
       if(isset($query['jornada']) && $query['jornada'])
       {
           $qb->andWhere('m.jornada = :jornada');
           $qb->setParameter('jornada', $query['jornada']);
       }
       
       $result = $qb->getQuery()->getResult();
                   
       $players = array();
       $defaults = array('player' => null, 'victories' => 0, 'defeats' => 0, 'crownsIn' => 0, 'crownsOut' => 0, 'timesPlayed' => 0);
       
       /** @var MatchMap $map */
       foreach($result as $map)
       {                        
            $playerOne = $map->getPlayerOne()->getNickname();
            if(!isset($players[$playerOne]))
            {
                $players[$playerOne] = $defaults;
                $players[$playerOne]['player'] = $map->getPlayerOne();
            }
  
            $playerTwo = $map->getPlayerTwo()->getNickname();
            if(!isset($players[$playerTwo]))
            {
                $players[$playerTwo] = $defaults;
                $players[$playerTwo]['player'] = $map->getPlayerTwo();
            }
            
            if($map->getCrownsOne() > $map->getCrownsTwo())
            {
                $players[$playerOne]['victories'] += 1;
                $players[$playerTwo]['defeats'] += 1;
            }
            else if($map->getCrownsOne() < $map->getCrownsTwo())
            {
                $players[$playerTwo]['victories'] += 1;
                $players[$playerOne]['defeats'] += 1;
            }
            
            //Crowns
            $players[$playerOne]['crownsIn'] += $map->getCrownsOne();
            $players[$playerOne]['crownsOut'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsIn'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsOut'] += $map->getCrownsOne();
            
            //Times played
            $players[$playerOne]['timesPlayed'] += 1;
            $players[$playerTwo]['timesPlayed'] += 1;
        }

        return $players;
    }
    
    private function getWinConditionByCompetition(Competition $competition)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('GameBundle:Deck')->createQueryBuilder('d');
        $qb->select('c')
            ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'd.match = m')
            ->where('m.competition = :competition')
            ->setParameter('competition', $competition)
            ->innerJoin('GameBundle:Card', 'c', 'WITH', 'd.winCondition = c');
        
        $result = $qb->getQuery()->getResult();
        
        return $result;
        
    }
    
    private function getCountWinConditionByCompetition(Competition $competition, Card $card)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('GameBundle:Deck')->createQueryBuilder('d');
        $qb->select('count(c)')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'd.match = m')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->innerJoin('GameBundle:Card', 'c', 'WITH', 'd.winCondition = c')
        ->andWhere('c = :card')
        ->setParameter('card', $card);
        
        $result = $qb->getQuery()->getSingleScalarResult();
                
        return $result;
    }
    
    private function getWinConditionByCompetitionAndPlayer(Competition $competition, Player $player)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('GameBundle:Deck')->createQueryBuilder('d');
        $qb->select('count(c) as times, c')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'd.match = m')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->innerJoin('GameBundle:Card', 'c', 'WITH', 'd.winCondition = c')
        ->innerJoin('CompetitionBundle:Player', 'p', 'WITH', 'd.player = p')
        ->andWhere('p = :player')
        ->setParameter('player', $player)
        ->groupBy('c, p')
        ->orderBy('times', 'DESC');
//         ->setMaxResults(1);
        
        $result = $qb->getQuery()->getResult();
                
        return $result;
        
    }
    
    private function getCountCardUsageByCompetition(Competition $competition, Card $card)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('GameBundle:Deck')->createQueryBuilder('d');
        $qb->select('count(c)')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'd.match = m')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->innerJoin('GameBundle:Card', 'c', 'WITH', 'c MEMBER OF d.cards')
        ->andWhere('c = :card')
        ->setParameter('card', $card);
        
        $result = $qb->getQuery()->getSingleScalarResult();
        
        return $result;
    }
    
    private function getBansByCompetition(Competition $competition)
    {
        $em = $this->getEntityManager();
                
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('CompetitionBundle:MatchBan')->createQueryBuilder('mb');
        $qb->select('mb')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'mb.match = m')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition);
        
        $result = $qb->getQuery()->getResult();
        
        $teams = array();
        $defaults = array('team' => null, 'cards' => array());
        $cardDefaults = array('card' => null, 'times' => 0);
                
        /** @var MatchBan $ban */
        foreach($result as $ban)
        {                        
            $teamName = $ban->getTeam()->getTeam()->getName();
            
            if(!isset($teams[$teamName]))
            {
                $teams[$teamName] = $defaults;
                $teams[$teamName]['team'] = $ban->getTeam()->getTeam();
            }
            
            $cardName = $ban->getCard()->getName();
            
            if(!isset($teams[$teamName]['cards'][$cardName]['c']) || !in_array($ban->getCard(), $teams[$teamName]['cards'][$cardName]))
            {
                $teams[$teamName]['cards'][$cardName]['c'] = $ban->getCard();
                $teams[$teamName]['cards'][$cardName]['times'] = 1;
            }
            else
            {
                $teams[$teamName]['cards'][$cardName]['times'] += 1;
            }
                
        }
        
        return $teams;
    }
    
    private function getBansPerCard(Competition $competition, Card $card)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('CompetitionBundle:MatchBan')->createQueryBuilder('mb');
        $qb->select('count(mb.card)')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'mb.match = m')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->innerJoin('GameBundle:Card', 'c', 'WITH', 'mb.card = c')
        ->andWhere('c = :card')
        ->setParameter('card', $card);
        
        $result = $qb->getQuery()->getSingleScalarResult();
         
         return $result;
    }
    
    /**
     * Get amount of cards banned on a given competition.
     *
     * @param integer $competition
     *
     * @return integer
     */
    private function getBansPerCompetition($competition)
    {
        $em = $this->getEntityManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('CompetitionBundle:MatchBan')->createQueryBuilder('mb');
        $qb->select('count(DISTINCT(mb.match))')
        ->innerJoin('mb.match', 'm')
        ->where($qb->expr()->eq('m.competition', '?1'))
        ->setParameter(1, $competition);
        
        $result = $qb->getQuery()->getSingleScalarResult();
        
        return $result;
    }
    
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
    
}

