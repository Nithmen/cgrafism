<?php

namespace AppBundle\Controller\Stats;

use CompetitionBundle\Entity\Competition;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CompetitionBundle\Entity\FifaPlayerChoice;

class FifaStatsController extends Controller
{
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/admin/fifa/stats/fifa-players", name="stats_fifa_players")
     * @Template("AppBundle:Stats:Fifa/fifa_players.html.twig")
     */
    public function fifaPlayersController()
    {
        $em = $this->getEntityManager();
        $competitionRepository = $em->getRepository(Competition::class);
        
        $ps4 = $competitionRepository->find(14);
        $xbox = $competitionRepository->find(16);
        
        $mostPickedPs4 = $this->getFifaPlayersMorePlayedInCompetition($ps4);
        $mostPickedXbox = $this->getFifaPlayersMorePlayedInCompetition($xbox);
        
//         return array(
//             'fifa' => $competitions
//         );
    }
    
    /**
     * 
     * @param Competition $competition
     * 
     * @return array
     */
    public function getFifaPlayersMorePlayedInCompetition(Competition $competition)
    {
        $em = $this->getEntityManager();
        
        $qb = $em->getRepository(FifaPlayerChoice::class)->createQueryBuilder('fpc');
        $qb->select(array('player.id','player.name','count(player) as times'))
            ->join('fpc.player', 'profile')
            ->join('fpc.fifaPlayer', 'player')
            ->join('profile.competition', 'competition')
            ->where($qb->expr()->eq('competition.id', '?1'))
            ->setParameter(1, $competition->getId())
            ->groupBy('player')
            ->orderBy('times', 'DESC')
            ->addOrderBy('player.rating', 'DESC');
        
        $results = $qb->getQuery()->getArrayResult();
        
        dump($results);
        die();
    }
        
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}

