<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CasparBundle\Services\CasparConnectService;
use CasparBundle\Services\MessageSenderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use CompetitionBundle\Entity\MatchMap;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CompetitionBundle\Entity\Match;
use GameBundle\Entity\Card;
use GameBundle\Entity\Deck;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\AST\SelectStatement;
use CompetitionBundle\Entity\MatchBan;

class ClashController extends Controller
{
    //CLASH ROYALE
    
    /**
     * @Route("/overlay/{id}", name="clash_overlay_ingame")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame.html.twig")
     */
    public function renderOverlay($id)
    {  
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        /** @var MatchMap $sets */
        $sets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        $scores = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getMatchScoreByMatch($match);
                
        $activeMap = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getActiveMapByMatch($match);
        
        $activeMapsBySets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getMapsByMatchAndSet($match, $activeMap);
        
        return array(
            'match' => $match,
            'sets' => $sets,
            'scores' => $scores,
            'activeMap' => $activeMap,
            'activeSet' => $activeMapsBySets
        );
    }
    
    /**
     * @Route("/overlay-/{id}", name="clash_overlay_ingame_")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame_.html.twig")
     */
    public function renderOverlay2($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        /** @var MatchMap $sets */
        $sets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        $scores = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getMatchScoreByMatch($match);
        
        $activeMap = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getActiveMapByMatch($match);
        
        $activeMapsBySets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getMapsByMatchAndSet($match, $activeMap);
        
        return array(
            'match' => $match,
            'sets' => $sets,
            'scores' => $scores,
            'activeMap' => $activeMap,
            'activeSet' => $activeMapsBySets
        );
    }
    
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/clash/{competition}", name="ui_clash")
     * @Template("AppBundle:Stream:ClashRoyale/interface.html.twig")
     */
    public function renderUi($competition = 11)
    {
        $clashRoyale = $this->getDoctrine()->getRepository('GameBundle:Game')->findOneBy(array('name' => 'Clash Royale'));
        
        $matches = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->findBy(array('competition' => $competition), array('id' => 'ASC', 'jornada' => 'ASC'));
        
        $thisCompetition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_competition'));
        
        $players = $this->getDoctrine()->getRepository('CompetitionBundle:Player')->findBy(array('game' => $clashRoyale));
        
        return array(
            'game' => 'clash',
            'matches' => $matches,
            'competition' => $thisCompetition->getValue(),
            'players' => $players
        );
    }
    
    /**
     * @Route("/scoreboard-out/{id}", name="clash_overlay_scoreboard_out")
     * @Template("AppBundle:Stream:ClashRoyale/scoreboard_out.html.twig")
     */
    public function renderScoreboardOut($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        
        $scores = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getMatchScoreByMatch($match);
        
        $sets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
                
        return array(
            'match' => $match,
            'scores' => $scores,
            'sets' => $sets
        );
    }
    
    /**
     * @Route("/scoreboard-out-/{id}", name="clash_overlay_scoreboard_out_")
     * @Template("AppBundle:Stream:ClashRoyale/scoreboard_out_.html.twig")
     */
    public function renderScoreboardOut2($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        
        $scores = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getMatchScoreByMatch($match);
        
        $sets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        return array(
            'match' => $match,
            'scores' => $scores,
            'sets' => $sets
        );
    }
    
    /**
     * @Route("/overlay-bans/{id}", name="clash_overlay_bans")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_bans.html.twig")
     */
    public function renderOverlayBans($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
             
        $cardBans = $this->getDoctrine()->getRepository(MatchBan::class)->findBy(['match' => $id], ['set' => 'ASC']);
        
        $totalBans = $this->getBansPerCompetition(11);
        
        $bans = array();
        foreach ($cardBans as $ban)
        {
            $bans[] = $this->getBansPerCard(11, $ban->getCard());
        }
        
        return array(
            'match' => $match,
            'bans' => $bans,
            'totalBans' => $totalBans
        );
    }
    
    /**
     * @Route("/overlay-lan/{id}", name="clash_overlay_ingame_lan")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame_lan.html.twig")
     */
    public function renderOverlayLan($id)
    {  
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        /** @var MatchMap $sets */
        $sets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        $scores = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getMatchScoreByMatch($match);
                
        $activeMap = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getActiveMapByMatch($match);
        
        $activeMapsBySets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getMapsByMatchAndSet($match, $activeMap);
        
        return array(
            'match' => $match,
            'sets' => $sets,
            'scores' => $scores,
            'activeMap' => $activeMap,
            'activeSet' => $activeMapsBySets
        );
    }
    
    /**
     * @Route("/overlay-static/{id}", name="clash_overlay_ingame_static")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame_static.html.twig")
     */
    public function renderOverlayStatic($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        /** @var MatchMap $sets */
        $sets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        $scores = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getMatchScoreByMatch($match);
        
        $activeMap = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getActiveMapByMatch($match);
        
        $activeMapsBySets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getMapsByMatchAndSet($match, $activeMap);
        
        return array(
            'match' => $match,
            'sets' => $sets,
            'scores' => $scores,
            'activeMap' => $activeMap,
            'activeSet' => $activeMapsBySets
        );
    }
    
    /**
     * @Route("/overlay-sustitution/{id}", name="clash_overlay_sustitution")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_sustitution.html.twig")
     */
    public function renderOverlaySustitution($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        
        $changes = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getPlayerSetChanges($match);

        return array(
            'match' => $match,
            'changes' => $changes
        );
    }
    
    /**
     * @Route("/overlay-lineup/{id}", name="clash_overlay_lineup")
     * @Template("AppBundle:Stream:ClashRoyale/lineup_overlay.html.twig")
     */
    public function renderOverlayLineup($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
                
        $activeMap = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getActiveMapByMatch($match);
        
        $activeMapsBySets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getMapsByMatchAndSet($match, $activeMap);
        
        return array(
            'match' => $match,
            'activeMap' => $activeMapsBySets,
            'set' => $activeMap
        );
    }
    
    /**
     * @Route("/overlay-ladder/{game}/{competition}", name="clash_overlay_ladder")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ladder.html.twig")
     */
    public function renderOverlayLadder($game, $competition)
    {        
        $ladder = $this->get('api.superliga')->getLadderByGameAndCompetition($game, $competition);
        
        //Competition id
        $a = 11;
        
        $jornada = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getLastJornadaByCompetition($a);
        
        return array(
            'ladder' => $ladder,
            'jornada' => $jornada
        );
    }
    
    /**
     * @Route("/overlay-resumen/{id}", name="clash_overlay_resumen")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_resumen.html.twig")
     */
    public function renderOverlayResumen($id)
    {
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        /** @var MatchMap $sets */
        $sets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->countSetsByMatch($match);
        
        $scores = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getMatchScoreByMatch($match);
                
        $activeMap = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getActiveMapByMatch($match);
        
        $activeMapsBySets = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getMapsByMatchAndSet($match, $activeMap);
                
        return array(
            'match' => $match,
            'sets' => $sets,
            'scores' => $scores,
            'activeMap' => $activeMap,
            'activeSet' => $activeMapsBySets
        );
    }
    
    /**
     * @Route("/gm-top10", name="clash_overlay_gm_top10")
     * @Template("AppBundle:Stream:ClashRoyale/gm_top10.html.twig")
     */
    public function renderGMTop10()
    {
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => 11));
        
        $data = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getCrownAvgByCompetition($competition);
        
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/overlay-stats-lower/{id}", name="clash_overlay_stats_lower")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_stats_lower.html.twig")
     */
    public function renderOverlayStatsLower($id)
    {
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => 11));
        
        $data = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinrateByPlayer($competition, $id);        
                    
        $winCondition = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinConditionByCompetitionAndPlayer($competition, $data['player']);
        
        return array(
            'data' => $data,
            'winCondition' => $winCondition,
            'player' => $data['player']
        );
    }
    
    /**
     * @Route("/overlay-stats-pip/{p1}-{p2}", name="clash_overlay_stats_pip")
     * @Template("AppBundle:Stream:ClashRoyale/lower_pip.html.twig")
     */
    public function renderPipLower($p1, $p2)
    {
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => 11));
        
        $p1 = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinrateByPlayer($competition, $p1);
        
        $p2 = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinrateByPlayer($competition, $p2);
        
        $winConditionP1 = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinConditionByCompetitionAndPlayer($competition, $p1['player']);
        
        $winConditionP2 = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinConditionByCompetitionAndPlayer($competition, $p2['player']);
        return array(
            'p1' => $p1['player'],
            'p2' => $p2['player'],
            'winConditionP1' => $winConditionP1,
            'winConditionP2' => $winConditionP2,
            'data1' => $p1,
            'data2' => $p2
        );
    }
    
    /**
     * @Route("/overlay-time/{minutes}/{text}", name="clash_overlay_time")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_time.html.twig")
     */
    public function renderBanTime($minutes = 1, $text = "Tiempo de baneo")
    {
        return array(
            'time' => $minutes,
            'text' => $text
        );
    }
    
    /**
     * @Route("/overlay-winrate/{id}", name="clash_overlay_winrate")
     * @Template("AppBundle:Stream:ClashRoyale/overlay_winrate.html.twig")
     */
    public function renderWinrate($id = 1)
    {
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => 11));
        
        $data = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinrateByPlayer($competition, $id);

        $jornada = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getLastJornadaByCompetition($competition);
        
        $winCondition = $this->getDoctrine()->getRepository('CompetitionBundle:MatchMap')->getWinConditionByCompetitionAndPlayer($competition, $data['player']);
        
        foreach($winCondition as $key => $card)
        {
            $winCondition[$key]['totalWinsAsWinCondition'] = $this->getCountWinConditionWinByCompetition($competition, $card[0], $id);
        }

        return array(
            'data' => $data,
            'winCondition' => $winCondition,
            'player' => $data['player'],
            'jornada' => $jornada[1],
            'totalGames' => $data['victories'] + $data['defeats']
        );
    }
    
    //CASPAR
    
    /**
     * 
     * @Route("/caspar-play/{id}", name="caspar_play", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame.html.twig")
     */
    public function sendToCaspar($id)
    {        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     *
     * @Route("/caspar-play-static/{id}", name="caspar_play_static", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame_static.html.twig")
     */
    public function sendToCasparStatic($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-static/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * 
     * @Route("/caspar-play-lan/{id}", name="caspar_play_lan", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame_lan.html.twig")
     */
    public function sendToCasparLan($id)
    {        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lan/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     *
     * @Route("/caspar-play-desempate/{id}", name="caspar_play_desempate", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ingame_desempate.html.twig")
     */
    public function sendToCasparDesempate($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-stop", name="caspar_stop", options={"expose"=true})
     */
    public function stop()
    {
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->fadeToBlack(20);
        
        return new JsonResponse();
    }
        
    /**
     *
     * @Route("/caspar-play-scoreboard/{id}", name="caspar_play_scoreboard", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/scoreboard_out.html.twig")
     */
    public function sendToCasparScoreboard($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_scoreboard_out_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/scoreboard-out/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     *
     * @Route("/caspar-play-scoreboard-desempate/{id}", name="caspar_play_scoreboard_desempate", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/scoreboard_out.html.twig")
     */
    public function sendToCasparScoreboardDesempate($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_scoreboard_out_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/scoreboard-out-/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     *
     * @Route("/caspar-play-bans/{id}", name="caspar_play_bans", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_bans.html.twig")
     */
    public function sendToCasparBans($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_scoreboard_out_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(22, '/overlay-bans/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-sustitution/{id}", name="caspar_play_sustitution", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_sustitution.html.twig")
     */
    public function sendToCasparSustitution($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_sustitution_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-sustitution/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-ladder/{game}/{competition}", name="caspar_play_ladder", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_ladder.html.twig")
     */
    public function sendToCasparBracket($game, $competition)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'ladder_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-ladder/'.$game.'/'.$competition, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-resumen/{id}", name="caspar_play_resumen", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_resumen.html.twig")
     */
    public function sendToCasparResumen($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_resumen_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-resumen/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lineup/{id}", name="caspar_play_lineup", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/lineup_overlay.html.twig")
     */
    public function sendToCasparLineup($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_resumen_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lineup/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lower-stats/{id}", name="caspar_play_lower_stats", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/lineup_overlay.html.twig")
     */
    public function sendToCasparLowerStats($id)
   
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_resumen_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-stats-lower/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-pip/{p1}-{p2}", name="caspar_play_pip", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/lower_pip.html.twig")
     */
    public function sendToCasparPip($p1, $p2)
    
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_resumen_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-stats-pip/'.$p1 . '-' . $p2, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-top10gm", name="caspar_play_gm_top10", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/gm_top10.html.twig")
     */
    public function sendToCasparTop10GM()
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_resumen_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/gm-top10', $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-timer/{minutes}/{text}", name="caspar_play_timer", options={"expose"=true})
     * @Template("AppBundle:Stream:ClashRoyale/overlay_time.html.twig")
     */
    public function sendToCasparTimer($minutes, $text)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_resumen_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(21, '/overlay-time/'. $minutes .'/'.$text, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-player-winrate/{id}", name="caspar_play_player_winrate", options={"expose"=true})
     */
    public function sendToCasparWinrate($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_resumen_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-winrate/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    // QUERYS
    
    private function getCountWinConditionWinByCompetition($competition, $card, $playerId = null)
    {
        /** @var RegistryInterface $em */
        $em = $this->getDoctrine();
        
        $sql = 'SELECT COUNT(dc.card_id) as times FROM `game__deck_cards` dc 
                JOIN game__deck d ON d.id = dc.deck_id 
                JOIN competition__match m ON m.id = d.match_id 
                JOIN competition__match_map mp ON mp.match_id = d.match_id AND mp.set = d.set
                WHERE m.competition = :competition and d.player = :player AND d.winCondition = :card AND dc.card_id = :card
                AND ((mp.player_one = :player AND (mp.crowns_one > mp.crowns_two)) OR (mp.player_two = :player AND (mp.crowns_two > mp.crowns_one)))';
        
        /** @var SelectStatement $stmt */
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('competition', $competition->getId());
        $stmt->bindValue('player', $playerId);
        $stmt->bindValue('card', $card->getId());
        $stmt->execute();
        
        $results = $stmt->fetchAll();
   
        return $results[0]['times'];
    }
    
    private function getBansPerCard($competition, $card)
    {
        $em = $this->getDoctrine()->getManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('CompetitionBundle:MatchBan')->createQueryBuilder('mb');
        $qb->select('count(mb.card)')
        ->innerJoin('mb.match', 'm')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->innerJoin('mb.card', 'c')
        ->andWhere('c = :card')
        ->setParameter('card', $card);
        
        $result = $qb->getQuery()->getSingleScalarResult();
        
        return $result;
    }
    
    /**
     * Get amount of cards banned on a given competition.
     * 
     * @param integer $competition
     * 
     * @return integer
     */
    private function getBansPerCompetition($competition)
    {
        $em = $this->getDoctrine()->getManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('CompetitionBundle:MatchBan')->createQueryBuilder('mb');
        $qb->select('count(DISTINCT(mb.match))')
        ->innerJoin('mb.match', 'm')
        ->where($qb->expr()->eq('m.competition', '?1'))
        ->setParameter(1, $competition);
        
        $result = $qb->getQuery()->getSingleScalarResult();
        
        return $result;
    }
}
