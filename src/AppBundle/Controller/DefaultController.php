<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CasparBundle\Services\CasparConnectService;
use CasparBundle\Services\MessageSenderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use CompetitionBundle\Entity\MatchMap;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Controller\Stats\ClashRoyaleStats;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    //CLASH ROYALE
    
    /**
     * @Route("/", name="homepage")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
        
    /**
     * @Route("/twitter-votes/{game}/{competition}/{match}", name="overlay_twitter_votes")
     * @Template("AppBundle:Stream:twitter_votes.html.twig")
     */
    public function renderTwitterVotes($game, $competition, $match)
    {                
        $data = $this->get('api.superliga')->getVotesByGameAndCompetitionAndMatch($game, $competition, $match);
        
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/twitter-votes-chyron/{game}/{competition}/{match}", name="overlay_twitter_votes_chyron")
     * @Template("AppBundle:Stream:twitter_votes_chyron.html.twig")
     */
    public function renderTwitterVotesChyron($game, $competition, $match)
    {                
        $data = $this->get('api.superliga')->getVotesByGameAndCompetitionAndMatch($game, $competition, $match);

        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/ladder/{game}/{competition}", name="overlay_ladder")
     * @Template("AppBundle:Stream:overlay_ladder.html.twig")
     */
    public function renderLadder($game, $competition)
    {        
        $ladder = $this->get('api.superliga')->getLadderByGameAndCompetition($game, $competition);

        if($game == 'csgo')
        {
            $a = 12;
        }
        
        if($game == 'lol')
        {
            $a = 18;
        }
        
        if($game == 'cod') {
            $a = 17;
        }
        
        $jornada = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getLastJornadaByCompetition($a);
        
        return array(
            'ladder' => $ladder,
            'jornada' => $jornada,
            'game' => $game
        );
    }
    
    /**
     * @Route("/tweet/{id}/{hashtag}", name="overlay_tweet")
     * @Template("AppBundle:Stream:twitter.html.twig")
     */
    public function renderTweet($id, $hashtag)
    {
        /** @var TwitterService $twitter */
        $twitter = $this->get('api.twitter_connection');
        
        $tweet = $twitter->getTweetById($id);
        
        return array(
            'tweet' => $tweet,
            'hashtag' => $hashtag
        );
        
        
    }
    
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/nexo", name="ui_nexo")
     * @Template("AppBundle:Stream:LeagueOfLegends/Nexo/interface.html.twig")
     */
    public function renderUiNexo(Request $request)
    {
        $competition = $request->get('competition', 18);

        $teams = $this->getDoctrine()->getRepository('CompetitionBundle:CompetitionProfile')->findBy(array('competition' => $competition));

        return array(
            'teams' => $teams
        );
    }
    
    /**
     * @Route("/nexo-tweet/{id}/{hashtag}", name="overlay_nexo_tweet")
     * @Template("AppBundle:Stream:twitter_nexo.html.twig")
     */
    public function renderTweetNexo($id, $hashtag)
    {
        /** @var TwitterService $twitter */
        $twitter = $this->get('api.twitter_connection');
        
        $tweet = $twitter->getTweetById($id);

        return array(
            'tweet' => $tweet,
            'hashtag' => $hashtag
        );
        
        
    }
    
    /**
     * @Route("/nexo-countdown", name="overlay_nexo_countdown")
     * @Template("AppBundle:Stream:LeagueOfLegends/Nexo/countdown.html.twig")
     */
    public function renderCountdownNexo(Request $request)
    {
        $localId = $request->get('local', 1);
        $visitorId = $request->get('visitor', 3);
        
        $minutes = $request->get('minutes', '05');
        $seconds = $request->get('seconds', '00');

        $local = $this->get('api.superliga')->getTeamInto($localId);
        $visitor = $this->get('api.superliga')->getTeamInto($visitorId);

        return array(
            'local' => $local,
            'visitor' => $visitor,
            'minutes' => $minutes,
            'seconds' => $seconds
        );
    }
    
    /**
     * @Route("/crl/{id}/ladder", name="overlay_crl_ladder")
     * @Template("AppBundle:Stream:CRL/overlay_clasificacion.html.twig")
     */
    public function renderCRLLadder($id)
    {
        $data = $this->curl("https://api.lvp.es/cr/v1/competitions/{$id}/ladders");

        return array(
            'data' => $data
        );
    }
    
    /**
     * 
     * @param string $url
     * 
     * @throws \Exception
     * 
     * @return mixed
     */
    private function curl($url, $post = false)
    {
        $s = curl_init();
        
        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($s, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($s, CURLOPT_POST, $post);
        
        $json = curl_exec($s);
        
        if($json === false){
            throw new \Exception(curl_error($s)) ;
        }
        
        curl_close($s);
        
        return json_decode($json, true);
    }
    
    /**
     *
     * @Route("/caspar-play-twitter-share/{game}/{competition}/{id}", name="caspar_play_twitter_share", options={"expose"=true})
     * @Template("AppBundle:Stream:twitter_votes.html.twig")
     */
    public function sendToCasparTwitterShare($game, $competition, $id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_share_transition'));
        
        /** @var Match $match */
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        
        $match_id = $match->getSuperligaId();
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/twitter-votes/'.$game.'/'.$competition.'/'.$match_id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     *
     * @Route("/caspar-play-twitter-chyron/{game}/{competition}/{id}", name="caspar_play_twitter_chyron", options={"expose"=true})
     * @Template("AppBundle:Stream:twitter_votes_chyron.html.twig")
     */
    public function sendToCasparTwitterChyron($game, $competition, $id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_share_transition'));
        
        /** @var Match $match */
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        
        $match_id = $match->getSuperligaId();
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/twitter-votes-chyron/'.$game.'/'.$competition.'/'.$match_id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-ladder-generic/{game}/{competition}", name="caspar_play_ladder_generic", options={"expose"=true})
     * @Template("AppBundle:Stream:overlay_ladder.html.twig")
     */
    public function sendToCasparLadder($game, $competition)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'ladder_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/ladder/'.$game.'/'.$competition, $transition->getValue());
        
        return new JsonResponse();
    }
        
    /**
     * @Route("/caspar-play-tweet/{id}/{hashtag}", name="caspar_play_tweet", options={"expose"=true})
     * @Template("AppBundle:Stream:twitter.html.twig")
     */
    public function sendToCasparTweet($id, $hashtag)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/tweet/'.$id.'/'.$hashtag, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-tweet-nexo", name="caspar_play_tweet_nexo", options={"expose"=true})
     */
    public function sendToCasparTweetNexo(Request $request)
    {
        $id = $request->get('tweet');
        $hashtag = $request->get('hashtag');

        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(23, '/nexo-tweet/'.$id.'/'.$hashtag, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-nexo-countdown", name="caspar_play_nexo_countdown", options={"expose"=true})
     */
    public function sendToCasparCountdown(Request $request)
    {
        $localId = $request->get('local', 1);
        $visitorId = $request->get('visitor', 3);
        
        $minutes = $request->get('minutes', '05');
        $seconds = $request->get('seconds', '00');
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(25, "/nexo-countdown?local={$localId}&visitor={$visitorId}&minutes={$minutes}&seconds={$seconds}", $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/top5/{competition}/{jornada}", name="", options={"expose"=true})
     */
    public function getTop5Clash($competition = 11, $jornada = 0)
    {
        $query = array('competition' => $competition, 'jornada' => $jornada);
        
        $players = $this->getPlayersWinRatioByCompetition($query);
        
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->findOneBy(array('id' => $query['competition']));
        
        if($jornada == 0) {
            $a = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->getLastJornadaByCompetition($competition);
            $jornada = $a[1];
        }
        
        $data = array();
        foreach ($players as $player)
        {
            if($jornada >= $player['timesPlayed']) {
                continue;
            }
            
            $winCondition = $this->getWinConditionByCompetitionAndPlayer($competition, $player['player']);
            
            $winratio = 100;
            
            if($player['defeats'] != 0)
            {
                $winratio = number_format(round(($player['victories'] / ($player['victories'] + $player['defeats']) * 100),2),2);
            }
            
            $data[] = array(
                'name' => $player['player']->getNickname(),
                'team' => (string) $player['player']->getTeam(),
                'victories' => $player['victories'],
                'defeats' => $player['defeats'],
                'winrate' => sprintf('%s%%', $winratio),
                'crown_average' => number_format(round(sprintf("%s",($player['crownsIn'] - $player['crownsOut']) / $player['timesPlayed']), 2),2),
                'win_condition' => empty($winCondition) ? "" : $winCondition[0][0]->getName()
            );
        }
        
        $c1 = array();
        $c2 = array();
        
        foreach($data as $key => $row) {
            $c1[$key] = $row['winrate'];
            $c2[$key] = $row['crown_average'];
        }
        
        array_multisort($c1, SORT_DESC, $c2, SORT_DESC, $data);
        
        return new JsonResponse(array(
            'data' => $data
        ), Response::HTTP_OK);
    }
    
    public function getPlayersWinRatioByCompetition($query)
    {
        if(!isset($query['competition']))
        {
            return array();
        }
        
        $em = $this->getDoctrine()->getManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('CompetitionBundle:MatchMap')->createQueryBuilder('mm');
        
        $qb->select('mm')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'mm.match = m')
        ->innerJoin('CompetitionBundle:Player', 'p1', 'WITH', 'mm.playerOne = p1')
        ->innerJoin('CompetitionBundle:Player', 'p2', 'WITH', 'mm.playerTwo = p2')
        ->where('m.competition = :competition')
        ->setParameter('competition', $query['competition'])
        ->andWhere('mm.crownsOne > 0 or mm.crownsTwo > 0');
        
        if(isset($query['jornada']) && $query['jornada'])
        {
            $qb->andWhere('m.jornada = :jornada');
            $qb->setParameter('jornada', $query['jornada']);
        }
        
        $result = $qb->getQuery()->getResult();
        
        $players = array();
        $defaults = array('player' => null, 'victories' => 0, 'defeats' => 0, 'crownsIn' => 0, 'crownsOut' => 0, 'timesPlayed' => 0);
        
        /** @var MatchMap $map */
        foreach($result as $map)
        {
            $playerOne = $map->getPlayerOne()->getNickname();
            if(!isset($players[$playerOne]))
            {
                $players[$playerOne] = $defaults;
                $players[$playerOne]['player'] = $map->getPlayerOne();
            }
            
            $playerTwo = $map->getPlayerTwo()->getNickname();
            if(!isset($players[$playerTwo]))
            {
                $players[$playerTwo] = $defaults;
                $players[$playerTwo]['player'] = $map->getPlayerTwo();
            }
            
            if($map->getCrownsOne() > $map->getCrownsTwo())
            {
                $players[$playerOne]['victories'] += 1;
                $players[$playerTwo]['defeats'] += 1;
            }
            else if($map->getCrownsOne() < $map->getCrownsTwo())
            {
                $players[$playerTwo]['victories'] += 1;
                $players[$playerOne]['defeats'] += 1;
            }
            
            //Crowns
            $players[$playerOne]['crownsIn'] += $map->getCrownsOne();
            $players[$playerOne]['crownsOut'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsIn'] += $map->getCrownsTwo();
            $players[$playerTwo]['crownsOut'] += $map->getCrownsOne();
            
            //Times played
            $players[$playerOne]['timesPlayed'] += 1;
            $players[$playerTwo]['timesPlayed'] += 1;
        }
        
        return $players;
    }
    
    private function getWinConditionByCompetitionAndPlayer($competition, $player)
    {
        $em = $this->getDoctrine()->getManager();
        
        /** @var QueryBuilder $qb */
        $qb = $em->getRepository('GameBundle:Deck')->createQueryBuilder('d');
        $qb->select('count(c) as times, c')
        ->innerJoin('CompetitionBundle:Match', 'm', 'WITH', 'd.match = m')
        ->where('m.competition = :competition')
        ->setParameter('competition', $competition)
        ->innerJoin('GameBundle:Card', 'c', 'WITH', 'd.winCondition = c')
        ->innerJoin('CompetitionBundle:Player', 'p', 'WITH', 'd.player = p')
        ->andWhere('p = :player')
        ->setParameter('player', $player)
        ->groupBy('c, p')
        ->orderBy('times', 'DESC');
        //         ->setMaxResults(1);
        
        $result = $qb->getQuery()->getResult();
        
        return $result;
        
    }
    
    /**
     * @Route("/caspar-stop-layer", name="caspar_stop_layer", options={"expose"=true})
     */
    public function stop(Request $request)
    {
        $layer = $request->query->get('layer', 22);
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->fadeToBlack($layer);
        
        return new JsonResponse();
    }
}
