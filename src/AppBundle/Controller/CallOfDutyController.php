<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CompetitionBundle\Entity\CompetitionProfile;
use AppBundle\Utils\CodUtils;
use CompetitionBundle\Entity\Match;

/**
 * Description of CallOfDutyController
 *
 * @author John
 */
class CallOfDutyController extends Controller
{     
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/cod/{competitionId}", name="ui_acod")
     * @Template("AppBundle:Stream:CallOfDuty/interface.html.twig")
     */
    public function renderUi($competitionId = 17)
    {
        $competition = $this->getDoctrine()->getRepository('CompetitionBundle:Competition')->find($competitionId);
        
        $matches = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->findBy(array('competition' => $competitionId), array('id' => 'ASC', 'jornada' => 'ASC'));
        
        $thisCompetition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_competition'));

        $teams = $this->getDoctrine()->getRepository(CompetitionProfile::class)->findBy(['competition' => $competition]);

        return array(
            'game' => 'cod',
            'matches' => $matches,
            'competition' => $thisCompetition->getValue(),
            'teams' => $teams
        );
    }    
    
    /**
     * @Route("/overlay-cod/hardpoint", name="cod_overlay_hardpoint")
     * @Template("AppBundle:Stream:CallOfDuty/chyron_hardpoint.html.twig")
     */
    public function renderChyronHardpoint(Request $request)
    {
        $competitionProfileRepository = $this->getDoctrine()->getManager()->getRepository(CompetitionProfile::class);
        
        $query = $request->query->all();
        
        $data = array();
        foreach($query as $key => $value)
        {
            if(strpos($value, ',')) {
                $value = explode(',', $value);
            }
            
            $data[$key] = $value;
        }
        
        $localTeam = $competitionProfileRepository->find($request->query->get('localTeam'));
        $visitorTeam = $competitionProfileRepository->find($request->query->get('visitorTeam'));

        return [
            'localTeam' => $localTeam->getTeam(),
            'visitorTeam' => $visitorTeam->getTeam(),
            'data' => $data
        ];
    }
    
    /**
     * @Route("/overlay-cod/resumen-hardpoint", name="cod_overlay_resumen_hardpoint")
     * @Template("AppBundle:Stream:CallOfDuty/resumen_hardpoint.html.twig")
     */
    public function renderResumenHardpoint(Request $request)
    {
        $competitionProfileRepository = $this->getDoctrine()->getManager()->getRepository(CompetitionProfile::class);
        
        $query = $request->query->all();
        
        $data = array();
        foreach($query as $key => $value)
        {
            if(strpos($value, ',')) {
                $value = explode(',', $value);
            }
            
            $data[$key] = $value;
        }

        $localTeam = $competitionProfileRepository->find($request->query->get('localTeam'));
        $visitorTeam = $competitionProfileRepository->find($request->query->get('visitorTeam'));
        
        return array(
            'localTeam' => $localTeam->getTeam(),
            'visitorTeam' => $visitorTeam->getTeam(),
            'data' => $data,
            'hardpoints' => CodUtils::getHardpointsName($data['map'])
        );
    }
    
    /**
     * @Route("/overlay-cod/resumen-byd", name="cod_overlay_resumen_byd")
     * @Template("AppBundle:Stream:CallOfDuty/resumen_byd.html.twig")
     */
    public function renderResumenByd(Request $request)
    {
        $competitionProfileRepository = $this->getDoctrine()->getManager()->getRepository(CompetitionProfile::class);
        
        $query = $request->query->all();
        
        $data = array();
        foreach($query as $key => $value)
        {
            if(strpos($value, ',')) {
                $value = explode(',', $value);
            }
            
            $data[$key] = $value;
        }
        
        $localTeam = $competitionProfileRepository->find($request->query->get('localTeam'));
        $visitorTeam = $competitionProfileRepository->find($request->query->get('visitorTeam'));
        
        
        return array(
            'localTeam' => $localTeam->getTeam(),
            'visitorTeam' => $visitorTeam->getTeam(),
            'data' => $data
        );
    }
    
    /**
     * @Route("/overlay-cod/resumen-tlb", name="cod_overlay_resumen_tlb")
     * @Template("AppBundle:Stream:CallOfDuty/resumen_tlb.html.twig")
     */
    public function renderResumenTlb(Request $request)
    {
        $competitionProfileRepository = $this->getDoctrine()->getManager()->getRepository(CompetitionProfile::class);
        
        $query = $request->query->all();
        
        $data = array();
        foreach($query as $key => $value)
        {
            if(strpos($value, ',')) {
                $value = explode(',', $value);
            }
            
            $data[$key] = $value;
        }
        
        $localTeam = $competitionProfileRepository->find($request->query->get('localTeam'));
        $visitorTeam = $competitionProfileRepository->find($request->query->get('visitorTeam'));
        
        
        return array(
            'localTeam' => $localTeam->getTeam(),
            'visitorTeam' => $visitorTeam->getTeam(),
            'data' => $data
        );
    }
    
    /**
     * @Route("/overlay-cod/resumen-mapas/{matchId}", name="cod_overlay_resumen_mapas")
     * @Template("AppBundle:Stream:CallOfDuty/resumen_mapas.html.twig")
     */
    public function renderResumenMapas($matchId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $query = $request->query->all();

        $data = array();
        foreach($query as $key => $value)
        {
            if(strpos($value, ',')) {
                $value = explode(',', $value);
                $value = array_filter($value, function($val) {
                    return ($val !== null && $val !== false && $val !== ''); 
                });
            }

            $data[$key] = $value;
        }
        
        $competitionProfileRepository = $em->getRepository(CompetitionProfile::class);
        
        /** @var Match $match */
        $match = $em->getRepository(Match::class)->find($matchId);
        
        $localTeam = $competitionProfileRepository->find($match->getTeamOne());
        $visitorTeam = $competitionProfileRepository->find($match->getTeamTwo());
        
        return array(
            'match' => $match,
            'localTeam' => $localTeam->getTeam(),
            'visitorTeam' => $visitorTeam->getTeam(),
            'data' => $data
        );
    }
    
    /**
     * @Route("/twitter-copa/{id}", name="cod_twitter_copa")
     * @Template("AppBundle:Stream:CallOfDuty/twitter_copa.html.twig")
     */
    public function renderTwitterCopa($id)
    {
        /** @var TwitterService $twitter */
        $twitter = $this->get('api.twitter_connection');
        
        $tweet = $twitter->getTweetById($id);
        
        return array(
            'tweet' => $tweet
        );
    }
    
    /**
     * @Route("/twitter-cwl/{id}", name="cod_twitter_cwl")
     * @Template("AppBundle:Stream:CallOfDuty/twitter_cwl.html.twig")
     */
    public function renderTwitterCWL($id)
    {
        /** @var TwitterService $twitter */
        $twitter = $this->get('api.twitter_connection');
        
        $tweet = $twitter->getTweetById($id);
                
        return array(
            'tweet' => $tweet
        );
    }
    
    /**
     * @Route("/lower-cwl/{text}", name="cod_lower_cwl")
     * @Template("AppBundle:Stream:CallOfDuty/lower_stats.html.twig")
     */
    public function renderLowerStats($text)
    {
        return array(
            'text' => $text
        );
    }
    
    
    /**
     * CASPAR ACTIONS
     */

    /**
     * @Route("/caspar-play-cod-chyron-hardpoint", name="caspar_play_cod_chyron_hardpoint", options={"expose"=true})
     */
    public function casparPlayChyronHardpoint(Request $request)
    {
        $query = $request->query->all();
 
        $route = "";
        
        foreach($query as $key => $value)
        {
            if(is_array($value)) {
                $value = implode(',', $value);
            }
            
            $route .= "&{$key}={$value}";
        }
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-cod/hardpoint?'.$route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-cod-resumen-hardpoint", name="caspar_play_cod_resumen_hardpoint", options={"expose"=true})
     */
    public function casparPlayResumenHardpoint(Request $request)
    {
        /** @var UploadedFile $csv */
        $csv = $request->files->get('file', null);
        
        $data = $this->get('app.csv')->extractData($csv);

        $localTeam = $request->request->get('local-team');
        $visitorTeam = $request->request->get('visitor-team');
        $map = $request->request->get('map');
        
        $route = "";
        foreach($data as $key => $value)
        {
            $value = implode(',', $value);
            $route .= "&{$key}={$value}";
        }
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-cod/resumen-hardpoint?localTeam='. $localTeam .'&visitorTeam='.$visitorTeam . $route . '&map='.$map, $transition->getValue());
        
        return $this->redirect("/cod");
    }
    
    /**
     * @Route("/caspar-play-cod-resumen-byd", name="caspar_play_cod_resumen_byd", options={"expose"=true})
     */
    public function casparPlayResumenByd(Request $request)
    {
        $query = $request->query->all();
        
        $route = "";
        
        foreach($query as $key => $value)
        {
            if(is_array($value)) {
                $value = implode(',', $value);
            }
            
            $route .= "&{$key}={$value}";
        }

        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-cod/resumen-byd?'.$route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-cod-resumen-tlb", name="caspar_play_cod_resumen_tlb", options={"expose"=true})
     */
    public function casparPlayResumenTlb(Request $request)
    {
        $query = $request->query->all();
        
        $route = "";
        
        foreach($query as $key => $value)
        {
            if(is_array($value)) {
                $value = implode(',', $value);
            }
            
            $route .= "&{$key}={$value}";
        }
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-cod/resumen-tlb?'.$route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-cod-resumen-mapas", name="caspar_play_cod_resumen_mapas", options={"expose"=true})
     */
    public function casparPlayResumenMapas(Request $request)
    {
        $query = $request->query->all();
        
        $matchId = $request->query->get('match');
        
        $route = "";
        
        foreach($query as $key => $value)
        {
            if(is_array($value)) {
                $value = implode(',', $value);
            }
            
            $route .= "&{$key}={$value}";
        }
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-cod/resumen-mapas/'. $matchId .'?'.$route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-twitter-copa/{id}", name="caspar_play_cod_twitter_copa", options={"expose"=true})
     */
    public function casparPlayTwitterCopa($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/twitter-copa/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-twitter-cwl/{id}", name="caspar_play_cod_twitter_cwl", options={"expose"=true})
     */
    public function casparPlayTwitterCWL($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/twitter-cwl/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-cwl/{text}", name="caspar_play_cod_cwl", options={"expose"=true})
     */
    public function casparPlayCWL($text)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'cod_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/lower-cwl/'.$text, $transition->getValue());
        
        return new JsonResponse();
    }
            
    /**
     * @Route("/caspar-stop", name="caspar_stop_cod", options={"expose"=true})
     */
    public function stop()
    {
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->fadeToBlack(20);
        
        return new JsonResponse();
    }
}
