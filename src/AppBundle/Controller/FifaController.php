<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CompetitionBundle\Entity\Match;
use CompetitionBundle\Entity\Draft;
use CompetitionBundle\Entity\MatchDraft;
use Aws\Sns\Exception\NotFoundException;
use CompetitionBundle\Entity\FifaPlayer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use CompetitionBundle\Entity\FifaPlayerChoice;
use CompetitionBundle\Entity\CompetitionProfile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\ORM\Query\Expr\Join;
use GameBundle\Entity\FifaPlayerFlag;

class FifaController extends Controller
{
    /**
     * @Route("/admin/fut/draft/{matchId}", name="fut_draft")
     * @Template("AppBundle:Stream:Fifa/draft.html.twig")
     */
    public function draftAction($matchId)
    {
        /** @var Match $match */
        $match = $this->getDoctrine()->getRepository(Match::class)->find($matchId);

        if(!$match) {
            throw new NotFoundException('Match not found.');
        }
         
        $matchDraft = $match->getMatchDraft();
        
        if(!$matchDraft) {
            $matchDraft = new MatchDraft();
            $matchDraft->setMatch($match);
            
            $localDraft = new Draft();
            $localDraft->setMatch($match);
            $localDraft->setMatchDraft($matchDraft);
            $localDraft->setPlayer($match->getTeamOne());
            $localDraft->setSide(Draft::SIDE_LOCAL);
            
            $visitorDraft = new Draft();
            $visitorDraft->setMatch($match);
            $visitorDraft->setMatchDraft($matchDraft);
            $visitorDraft->setPlayer($match->getTeamTwo());
            $visitorDraft->setSide(Draft::SIDE_VISITOR);
            
            $matchDraft->addDraft($localDraft);
            $matchDraft->addDraft($visitorDraft);
            
            $match->setMatchDraft($matchDraft);
            
            $this->getDoctrine()->getManager()->persist($match);
            $this->getDoctrine()->getManager()->flush($match);
        }
        else {
            $localDraft = $matchDraft->getDraft($match->getTeamOne());
            $visitorDraft = $matchDraft->getDraft($match->getTeamTwo());
        }

        $local = $localDraft->getFifaPlayers()->toArray();
        $visitor = $visitorDraft->getFifaPlayers()->toArray();
        
        $localPlayers = array();
        $visitorPlayers = array();
        
        for($i = 1; $i <= 15; $i++)
        {
            if(!isset($local[$i])) {
                $localPlayers[$i] = null;
            }
            else {
                $localPlayers[$i] = $local[$i];
            }
            
            if(!isset($visitor[$i])) {
                $visitorPlayers[$i] = null;
            }
            else {
                $visitorPlayers[$i] = $visitor[$i];
            }
        }

        
        return array(
            'match' => $match,
            'localDraft' => $localDraft,
            'visitorDraft' => $visitorDraft,
            'localPlayers' => $localPlayers,
            'visitorPlayers' => $visitorPlayers
        );
    }
    
    /**
     * @Route("/admin/fut/cards", name="fut_cards")
     * @Template("AppBundle:Stream:Fifa/modal/modal_list.html.twig")
     */
    public function getCards(Request $request)
    {
        $page = $request->query->get('page', 1);
        $size = $request->query->get('size', 20);
        
        $cards = $this->getDoctrine()->getRepository(FifaPlayer::class)->findBy([], ['rating' => 'DESC'], $size, ($page - 1) * $size);
        
        return ['cards' => $cards];
    }
    
    /**
     * @method("POST")
     * @Route("/admin/fut/cards/search", name="fut_print_cards")
     * @Template("AppBundle:Stream:Fifa/modal/modal_list.html.twig")
     */
    public function printCards(Request $request) 
    {
        $data = $request->request->get('data');
        
        $ids = array();
        foreach($data['data'] as $card)
        {
            $ids[] = $card['id'];
        }
        
        $cards = $this->getDoctrine()->getRepository(FifaPlayer::class)->findBy(['id' => $ids], ['rating' => 'DESC']);
        
        return ['cards' => $cards];
    }
    
    /**
     * @method("POST")
     * @Route("/admin/fut/save", name="fut_save")
     */
    public function saveAction(Request $request) 
    {   
        $data = $request->request->get('data', null);
        
        $local = $data[0];
        $visitor = $data[1];
        
        $matchId = $request->request->get('match', null);
        if(!$matchId) {
            throw new NotFoundException('Match not found.');
        }
        
        $em =  $this->getDoctrine()->getManager();
        
        $qb = $em->getRepository(Match::class)->createQueryBuilder('m');
        $qb->select(array('m'));
        $qb->innerJoin('m.matchDraft', 'md');
        $qb->where($qb->expr()->eq('m', "?1"));
        $qb->setParameter(1, $matchId);
        
        /** @var Match $match */
        $match = $qb->getQuery()->getOneOrNullResult();
        
        $localPlayer = $em->getReference(CompetitionProfile::class, $local['id']);
        $visitorPlayer = $em->getReference(CompetitionProfile::class, $visitor['id']);
        
        // Remove players
        try {
            $em->beginTransaction();

            $localDraft = $match->getMatchDraft()->getDraft($localPlayer);
            $visitorDraft = $match->getMatchDraft()->getDraft($visitorPlayer);
            
            /** @var Draft $localDraft */
            $localDraft->removeFifaPlayers();
            $visitorDraft->removeFifaPlayers();
            
            $em->flush($localDraft);
            $em->flush($visitorDraft);
            
            // Create players
            if(isset($local['draft'])) {
                foreach($local['draft'] as $choice) {
                    $player = $choice['id'];
                    
                    /** @var FifaPlayerChoice $fifaPlayerChoice */
                    $fifaPlayerChoice = new FifaPlayerChoice();
                    $fifaPlayer = $em->getReference(FifaPlayer::class, $player);
                    $fifaPlayerChoice->setFifaPlayer($fifaPlayer);
                    $fifaPlayerChoice->setCardNumber($choice['cardNumber']);
                    $fifaPlayerChoice->setMatch($match);
                    $fifaPlayerChoice->setPlayer($localPlayer);
                    $fifaPlayerChoice->setDraft($localDraft);
                    $fifaPlayerChoice->setOrder($choice['order']);
                    
                    $localDraft->addFifaPlayer($fifaPlayerChoice);
                }
                $em->flush();
            }
            
            if(isset($visitor['draft'])) {
                foreach($visitor['draft'] as $choice) {
                    $player = $choice['id'];
                    
                    /** @var FifaPlayerChoice $fifaPlayerChoice */
                    $fifaPlayerChoice = new FifaPlayerChoice();
                    $fifaPlayer = $em->getReference(FifaPlayer::class, $player);
                    $fifaPlayerChoice->setFifaPlayer($fifaPlayer);
                    $fifaPlayerChoice->setCardNumber($choice['cardNumber']);
                    $fifaPlayerChoice->setMatch($match);
                    $fifaPlayerChoice->setPlayer($visitorPlayer);
                    $fifaPlayerChoice->setDraft($visitorDraft);
                    $fifaPlayerChoice->setOrder($choice['order']);
                    
                    $visitorDraft->addFifaPlayer($fifaPlayerChoice);
                }
                
                $em->flush();
            }
            
            $em->commit();
        } catch (\Exception $ex) {
            $em->rollback();
            
            throw new \Exception($ex->getMessage());
        }
        
        return new JsonResponse('done');
    }
    
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/fifa/{competition}", name="ui_fifa")
     * @Template("AppBundle:Stream:Fifa/interface.html.twig")
     */
    public function renderUi($competition = 14)
    {
        $fifa = $this->getDoctrine()->getRepository('GameBundle:Game')->findOneBy(array('name' => 'Fifa Ultimate Team'));
        
        $matches = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->findBy(array('competition' => $competition), array('id' => 'ASC', 'jornada' => 'ASC'));
        
        $thisCompetition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'clash_competition'));
        
        $players = $this->getFifaPlayedByCompetition($competition);
        
        $gk = $this->getFifaPlayedByCompetition($competition, true);
        
        $flags =  $this->getDoctrine()->getRepository(FifaPlayerFlag::class)->findAll();
        
        return array(
            'game' => 'fut',
            'matches' => $matches,
            'fifa' => $fifa,
            'competition' => $thisCompetition->getValue(),
            'players' => $players,
            'gks' => $gk,
            'flags' => $flags
        );
    }
    
    /**
     * @Route("/overlay-fifa/allianz", name="fifa_allianz", options={"expose"=true})
     * @Template("AppBundle:Stream:Fifa/allianz.html.twig")
     */
    public function renderAllianz(Request $request)
    {
        $players = $request->query->get('players', null);
        
        if(!$players) {
            $players = $this->getDoctrine()->getManager()->getRepository(FifaPlayer::class)->findBy([], ['rating' => 'DESC'], 11);
        }
        else {
            $players = explode(',', $players);
            $qb = $this->getDoctrine()->getManager()->getRepository(FifaPlayer::class)->createQueryBuilder('fp');
            $qb->select(array('fp'))
            ->where($qb->expr()->in('fp.id', $players));
            
            $orderBy = sprintf('FIELD(fp.id, \'%s\')', implode('\', \'', $players));
            $qb->addOrderBy($orderBy);
            
            $players = $qb->getQuery()->getResult();
        }

        return array(
            'players' => $players
        );
    }
    
    /**
     * @Route("/overlay-fifa/lineup/{matchId}/{teamId}", name="fifa_render_draft", options={"expose"=true})
     * @Template("AppBundle:Stream:Fifa/lineup.html.twig")
     */
    public function renderLineup($matchId, $teamId)
    {
        /** @var Match $match */
        $match = $this->getDoctrine()->getManager()->getRepository(Match::class)->find($matchId);
        
        if(!$match) {
            throw new NotFoundException("No existe el match");
        }

        $qb = $this->getDoctrine()->getManager()->getRepository(FifaPlayerChoice::class)->createQueryBuilder('fp');
        
        $qb->select(array('d', 'fp', 'f'));
        $qb->innerJoin('fp.fifaPlayer', 'f');
        $qb->innerJoin('fp.draft', 'd');
        $qb->where($qb->expr()->eq('d.match', '?1'));
        $qb->setParameter(1, $matchId);
        $qb->andWhere($qb->expr()->eq('d.player', '?2'));
        $qb->setParameter(2, $teamId);
        $qb->orderBy('fp.cardNumber', 'ASC');
        
        $choices = $qb->getQuery()->getResult();

        return array(
            'choices' => $choices
        );
    }
    
    /**
     * @Route("/overlay-fifa/lineup-full/{matchId}/{teamId}", name="fifa_render_draft_full", options={"expose"=true})
     * @Template("AppBundle:Stream:Fifa/lineup-full.html.twig")
     */
    public function renderLineupFull($matchId, $teamId)
    {
        /** @var Match $match */
        $match = $this->getDoctrine()->getManager()->getRepository(Match::class)->find($matchId);
        
        if(!$match) {
            throw new NotFoundException("No existe el match");
        }
        
        $qb = $this->getDoctrine()->getManager()->getRepository(FifaPlayerChoice::class)->createQueryBuilder('fp');
        
        $qb->select(array('d', 'fp', 'f'));
        $qb->innerJoin('fp.fifaPlayer', 'f');
        $qb->innerJoin('fp.draft', 'd');
        $qb->where($qb->expr()->eq('d.match', '?1'));
        $qb->setParameter(1, $matchId);
        $qb->andWhere($qb->expr()->eq('d.player', '?2'));
        $qb->setParameter(2, $teamId);
        $qb->orderBy('fp.cardNumber', 'ASC');
        
        $choices = $qb->getQuery()->getResult();
        
        return array(
            'choices' => $choices
        );
    }
    
    /**
     * @Route("/overlay-fifa/scoreboard/{matchId}/{game}", name="fifa_render_scoreboard", options={"expose"=true})
     * @Template("AppBundle:Stream:Fifa/scoreboard.html.twig")
     */
    public function renderScoreboard($matchId, $game = 1, Request $request)
    {
        $match = $this->getDoctrine()->getManager()->getRepository(Match::class)->find($matchId);
        
        if(!$match) {
            throw new NotFoundException("No existe el match");
        }
        
        $localFlag = $this->getDoctrine()->getManager()->getRepository(FifaPlayerFlag::class)->find($request->query->get('localFlag', 1));
        $visitorFlag = $this->getDoctrine()->getManager()->getRepository(FifaPlayerFlag::class)->find($request->query->get('visitorFlag', 1));
        
        return array(
            'match' => $match,
            'game' => $game,
            'swap' => $request->query->get('swap', 0),
            'localFlag' => $localFlag,
            'visitorFlag' => $visitorFlag,
            'scores' => explode(",",$request->query->get('scores', array(0, 0))),
            'title' => $request->query->get('title', 'FINAL')
        );
    }
    
    /**
     * @Route("/overlay-fifa/score/{side}/{score}", name="fifa_render_score", options={"expose"=true})
     * @Template("AppBundle:Stream:Fifa/includes/score.html.twig")
     */
    public function renderScore($side = 100, $score = 1)
    {
        return array(
            'side' => $side,
            'score' => $score
        );
    }
    
    
    /**
     * @Route("/overlay-fifa/tweet/{id}/{hashtag}", name="overlay_tweet_fifa")
     * @Template("AppBundle:Stream:Fifa/twitter.html.twig")
     */
    public function renderTweet($id, $hashtag)
    {
        /** @var TwitterService $twitter */
        $twitter = $this->get('api.twitter_connection');
        
        $tweet = $twitter->getTweetById($id);
        
        return array(
            'tweet' => $tweet,
            'hashtag' => $hashtag
        );   
    }
    
    // CASPAR ACTIONS
    
    /**
     * @Route("/caspar-play-allianz", name="caspar_play_allianz", options={"expose"=true})
     */
    public function sendToCasparAllianz(Request $request)
    {
        $players = $request->query->get('players');
        
        $players = implode(',', $players);
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'fifa_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf('/overlay-fifa/allianz?players=%s',$players);
        
        $casparConnector->sendHtml(20, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-fifa-lineup", name="caspar_play_fifa_lineup", options={"expose"=true})
     */
    public function sendToCasparLineup(Request $request)
    {
        $matchId = $request->query->get('matchId');
        $side = $request->query->get('side', 100);
        
        /** @var Match $match */
        $match = $this->getDoctrine()->getManager()->getRepository(Match::class)->find($matchId);
        
        if($side == Draft::SIDE_LOCAL) {
            $draftId = $match->getMatchDraft()->getDraft($match->getTeamOne())->getPlayer()->getId();
        }
        else {
            $draftId = $match->getMatchDraft()->getDraft($match->getTeamTwo())->getPlayer()->getId();
        }

        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'fifa_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf('/overlay-fifa/lineup/%s/%s', $matchId, $draftId);
        
        $casparConnector->sendHtml(20, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-fifa-lineup-full", name="caspar_play_fifa_lineup_full", options={"expose"=true})
     */
    public function sendToCasparLineupFull(Request $request)
    {
        $matchId = $request->query->get('matchId');
        $side = $request->query->get('side', 100);
        
        /** @var Match $match */
        $match = $this->getDoctrine()->getManager()->getRepository(Match::class)->find($matchId);
        
        if($side == Draft::SIDE_LOCAL) {
            $draftId = $match->getMatchDraft()->getDraft($match->getTeamOne())->getPlayer()->getId();
        }
        else {
            $draftId = $match->getMatchDraft()->getDraft($match->getTeamTwo())->getPlayer()->getId();
        }
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'fifa_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf('/overlay-fifa/lineup-full/%s/%s', $matchId, $draftId);
        
        $casparConnector->sendHtml(20, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-fifa-scoreboard", name="caspar_play_fifa_scoreboard", options={"expose"=true})
     */
    public function sendToCasparScoreboard(Request $request)
    {
        $matchId = $request->query->get('matchId');
        $game = $request->query->get('game', 1);
        $swap = $request->query->get('swap', 0);
        $localFlag = $request->query->get('flagL', 1);
        $visitorFlag = $request->query->get('flagV', 1);
        $title = $request->query->get('title', 'FINAL');

        $scores = $request->query->get('scores');
        $scores = implode(',', $scores);
        
        /** @var Match $match */
        $match = $this->getDoctrine()->getManager()->getRepository(Match::class)->find($matchId);
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'fifa_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf('/overlay-fifa/scoreboard/%s/%s?swap=%s&localFlag=%s&visitorFlag=%s&title=%s&scores=%s', $matchId, $game, $swap,$localFlag, $visitorFlag, $title, $scores);

        $casparConnector->sendHtml(22, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-fifa-score", name="caspar_play_fifa_score", options={"expose"=true})
     */
    public function sendToCasparScore(Request $request)
    {
        $layer = $request->query->get('layer');
        
        $side = $request->query->get('side');
        $score = $request->query->get('score');
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'fifa_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf('/overlay-fifa/score/%s/%s', $side, $score);
        
        $casparConnector->sendHtml($layer, $route,$transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-fifa-tweet/{id}/{hashtag}", name="caspar_play_fifa_tweet", options={"expose"=true})
     */
    public function sendToCasparTweet($id, $hashtag)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-fifa/tweet/'.$id.'/'.$hashtag, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * Get FifaPlayers played on a given competition.
     * 
     * @param number $competition
     * @param boolean $gk
     * 
     * @return mixed|array|NULL
     */
    private function getFifaPlayedByCompetition($competition = 14, $gk = false)
    {
        $qb = $this->getDoctrine()->getManager()->getRepository(FifaPlayer::class)->createQueryBuilder('f');
        
        $qb->select(array('f'));
        $qb->innerJoin(FifaPlayerChoice::class, 'fp', Join::WITH, 'fp.fifaPlayer = f');
        $qb->innerJoin('fp.player', 'cp');
        $qb->where($qb->expr()->eq('cp.competition', '?1'));
        $qb->setParameter(1, $competition);

        $qb->andWhere($qb->expr()->eq('f.isGk', '?2'));
        $qb->setParameter(2, $gk);
        
        $qb->groupBy('f');
        
        return $qb->getQuery()->getResult();
    }
    
    // CUSTOM VIEW
    
    /**
     * @Route("/admin/drafts", name="fifa_show_drafts")
     * @Template("AppBundle:Stream:Fifa/drafts.html.twig")
     */
    public function showDraftAction()
    {
        $ps4 = $this->getDraftsByCompetition(14);
        $xbox = $this->getDraftsByCompetition(16);
         
        return array(
            'ps4' => $ps4,
            'xbox' => $xbox
        );
    }
    
    /**
     * @Route("/admin/streaming/fifa", name="fifa_streaming_drafts")
     * @Template("AppBundle:Stream:Fifa/drafts_streaming.html.twig")
     */
    public function showStreamingDraftsAction()
    {
        $ps4 = $this->getDraftsByCompetition(14);
        $xbox = $this->getDraftsByCompetition(16);
        
        return array(
            'ps4' => $ps4,
            'xbox' => $xbox
        );
    }
    
    private function getDraftsByCompetition($competition)
    {
        $matchRepository = $this->getDoctrine()->getManager()->getRepository(Match::class);
        
        $qb = $matchRepository->createQueryBuilder('m');
        $qb->select(array('m'))
        ->where('m.matchDraft IS NOT NULL')
        ->andWhere($qb->expr()->eq('m.competition', '?1'))
        ->setParameter(1, $competition);
        
        return $qb->getQuery()->getResult();
    }
}

