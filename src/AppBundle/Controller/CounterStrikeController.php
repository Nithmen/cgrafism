<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CasparBundle\Services\CasparConnectService;
use CasparBundle\Services\MessageSenderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ApiBundle\Services\TwitterService;
use ApiBundle\Services\EbotService;

/**
 * Description of CounterStrikeController
 *
 * @author John
 */
class CounterStrikeController extends Controller
{
        
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/csgo/{competition}", name="ui_cod")
     * @Template("AppBundle:Stream:CounterStrike/interface.html.twig")
     */
    public function renderUi($competition = 12)
    {
        $matches = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->findBy(array('competition' => $competition), array('id' => 'ASC', 'jornada' => 'ASC'));
        
        $thisCompetition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'csgo_competition'));
 
        $weapons = EbotService::$weapons;
        
        $players = $this->getDoctrine()->getRepository('CompetitionBundle:Player')->findBy(array('game' => 4), array('nickname' => 'ASC'));
        
        return array(
            'game' => 'csgo',
            'competition' => $thisCompetition->getValue(),
            'matches' => $matches,
            'weapons' => $weapons,
            'players' => $players
        );
    }    
            
    /**
     *  @Route("/resumen-csgo/{id}", name="resumen_csgo")
     *  @Template("AppBundle:Stream:CounterStrike/overlay_resumen.html.twig")
     */ 
    public function renderOverlayResumen($id)
    {        
        /** @var EbotService $ebot */
        $ebot = $this->get('api.ebot');
        
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        
        $teamOne = $this->getDoctrine()->getRepository('CompetitionBundle:Team')->find($match->getTeamOne()->getTeam());
        $teamTwo = $this->getDoctrine()->getRepository('CompetitionBundle:Team')->find($match->getTeamTwo()->getTeam());
        
        return array(
            'match' => $ebot->getMatchById($match->getEbotId()),
            'teamA' => $teamOne,
            'teamB' => $teamTwo
        );
    }
    
    /**
     *  @Route("/csgo-comparativa/{playerOne}/{playerTwo}/{weapons}/{matches}", name="csgo_comparativa")
     *  @Template("AppBundle:Stream:CounterStrike/overlay_comparativa.html.twig")
     */ 
    public function renderComparativa($playerOne = "STEAM_1:1:10387", $playerTwo = "STEAM_1:0:7392268", $weapons = null, $matches = null)
    {
        $choosen = $this->weaponChooser($weapons);
           
        $m = $this->parseMatches($matches);
        
        /** @var EbotService $ebot */
        $ebot = $this->get('api.ebot');
        
        $p1 = $ebot->getPlayerData($playerOne, $m, $choosen['weapon']);
        $p2 = $ebot->getPlayerData($playerTwo, $m, $choosen['weapon']);
        
        return array(
            'p1' => $p1,
            'p2' => $p2,
            'steamOne' => $playerOne,
            'steamTwo' => $playerTwo,
            'weapon' => $choosen['title']
        );
    }
    
    // CASPAR ACTIONS
    
    /**
     *  @Route("/caspar-play-resumen-csgo/{id}", name="caspar_play_resumen_csgo", options={"expose"=true})
     *  @Template("AppBundle:Stream:CounterStrike/overlay_resumen.html.twig")
     */ 
    public function SendToCasparResumen($id)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'ladder_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/resumen-csgo/'.$id, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     *  @Route("/caspar-play-comparativa-csgo/{playerOne}/{playerTwo}/{weapons}/{matches}", name="caspar_play_comparativa_csgo", options={"expose"=true})
     *  @Template("AppBundle:Stream:CounterStrike/overlay_comparativa.html.twig")
     */ 
    public function SendToCasparComparativa($playerOne, $playerTwo, $weapons, $matches = null)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'ladder_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/csgo-comparativa/'.$playerOne . '/'. $playerTwo . '/' . $weapons . '/' . $matches, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-stop", name="caspar_stop_csgo", options={"expose"=true})
     */
    public function stop()
    {
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->fadeToBlack(20);
        
        return new JsonResponse();
    }
    
    private function weaponChooser($weapon) 
    {
        switch($weapon)
        {
            case "TODAS":
                $choosen = EbotService::$weapons;
                $title = "ARMAS";
                break;
            case "snipers":
                $choosen = EbotService::$sniperMap;
                $title = "SNIPER";
                break;
            case "pistolas":
                $choosen = EbotService::$pistolMap;
                $title = "PISTOLA";
                break;
            case "rifles":
                $choosen = EbotService::$rifleMap;
                $title = "RIFLE";
                break;
            case "knife%":
                $choosen = EbotService::$knife;
                $title = "CUCHILLO";
                break;
            default:
               $choosen = array($weapon);
                $title = $weapon;
                break;
        }
        
        return array(
            'weapon' => $choosen,
            'title' => $title
        );
    }
    
    private function parseMatches($matches)
    {
        if($matches == null){
            return null;
        }
        
        return explode(',', $matches);
    }
}
