<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CasparBundle\Services\CasparConnectService;
use CasparBundle\Services\MessageSenderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\ORM\Query\AST\Functions\SubstringFunction;

/**
 * Description of LeagueOfLegendsController
 *
 * @author John
 */
class LeagueOfLegendsController extends Controller
{
    /**
     * @Route("/overlay-lol/{id}/{switch}/{drake}", name="lol_overlay_ingame")
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_ingame.html.twig")
     */
    public function renderOverlay($id, $switch=0, $drake=0)
    {  
        $patch = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_patch'));
                
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
                
        switch($drake)
        {
            case "infernal": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/infernal.png";break;
            case "cloud": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/cloud.png";break;
            case "mountain": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/mountain.png";break;
            case "elder": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/elder.png";break;
            case "ocean": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/water.png";break;
            default: $url = " ";
        }
        
        return array(
            'match' => $match,
            'patch' => $patch,
            'switch' => $switch,
            'drake' => $url
        );
    }
    
    /**
     * @Route("/overlay-lol-online/{id}/{switch}/{drake}", name="lol_overlay_ingame_online")
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_ingame_online.html.twig")
     */
    public function renderOverlayOnline($id, $switch=0, $drake=0)
    {
        $patch = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_patch'));
        
        $match = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($id);
        
        switch($drake)
        {
            case "infernal": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/infernal.png";break;
            case "cloud": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/cloud.png";break;
            case "mountain": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/mountain.png";break;
            case "elder": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/elder.png";break;
            case "ocean": $url = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/League+of+Legends/water.png";break;
            default: $url = " ";
        }
        
        return array(
            'match' => $match,
            'patch' => $patch,
            'switch' => $switch,
            'drake' => $url
        );
    }
    
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/lol/{competition}", name="ui_lol")
     * @Template("AppBundle:Stream:LeagueOfLegends/interface.html.twig")
     */
    public function renderUi($competition = 18)
    {
        $matches = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->findBy(array('competition' => $competition), array('id' => 'ASC', 'jornada' => 'ASC'));
        
        $thisCompetition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_competition'));
        
        $teams = $this->getDoctrine()->getRepository('CompetitionBundle:CompetitionProfile')->findBy(array('competition' => $competition));
        
         $ddhService = $this->get('api.ddh');
         
         $slo = $ddhService->getMatches();
        
        $players = $ddhService->getPlayers("T2018V-L-");
         $champions = $ddhService->getChampions("T2018V-L-");
         
        return array(
            'game' => 'lol',
            'matches' => $matches,
            'competition' => $thisCompetition->getValue(),
            'teams' => $teams,
            'slo' => $slo,
            'champions' => $champions,
            'players' => $players
        );
    }    
    
    
    /**
     * @Route("/overlay-lol-picks/{player}/{blueTeam}/{purpleTeam}", name="lol_overlay_picks")
     * @Template("AppBundle:Stream:LeagueOfLegends/picks.html.twig")
     */
    public function renderOverlayPicks($player = "MAD Falco", $blueTeam = 1, $purpleTeam = 1)
    {
        $patch = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_patch'));
        
        $request = new \ApiBundle\Services\RiotService();
        $data = $request->getSpectatorInfo($player);
        
        return array(
            'patch' => $patch,
            'data' => $data,
            'blue' => $blueTeam,
            'purple' => $purpleTeam
        );
    }
    
    /**
     * @Route("/overlay-lol-ticker/{matchId}/{superligaId}", name="lol_overlay_ticker")
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_ticker.html.twig")
     */
    public function renderOverlayTicker($matchId = 'T2018V-L-J1-P1-1', $superligaId = 1281)
    {
        $ddhService = $this->get('api.ddh');
        
        $stats = $ddhService->getMatchStats($matchId, $superligaId);
        $map = substr($matchId, -1);
        
        return array(
            'stats' => $stats,
            'map' => $map
        );
    }
    
    /**
     * @Route("/overlay-lol-postmatch/{matchId}/{superligaId}", name="lol_overlay_postmatch")
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_postmatch.html.twig")
     */
    public function renderOverlayPostmatch($matchId = 'T2018V-L-J1-P1-1', $superligaId = 1281)
    {
        $ddhService = $this->get('api.ddh');
        
        $stats = $ddhService->getMatchStats($matchId, $superligaId);
        $map = substr($matchId, -1);
        
        return array(
            'stats' => $stats,
            'map' => $map
        );
    }
    
    /**
     * @Route("/overlay-selec-picks/{champion}/{side}/{rol}", name="lol_overlay_selec_picks")
     * @Template("AppBundle:Stream:LeagueOfLegends/picks_bans/picks.html.twig")
     */
    public function renderPicks($champion = 113, $side = 100, $rol = 1)
    {
        switch($rol)
        {
            case 1: 
                $top = 198;
                $role = "top";
                break;
            case 2:
                $top = 324;
                $role = "jungler";
                break;
            case 3:
                $top = 450;
                $role = "mid";
                break;
            case 4:
                $top = 605;
                $role = "adc";
                break;
            case 5:
                $top = 730;
                $role = "support";
                break;
        }
        
        $ddhService = $this->get('api.ddh');
        
        $winratio = $ddhService->getWinratioByChampOnCompetition($champion, "T2018V-L");
        
        $wins = $ddhService->getGamesPlayedByChampOncompetitionWin($champion, "T2018V-L");
        
        $gamesPlayed = $ddhService->getGamesPlayedByChampOncompetition($champion, "T2018V-L");
        
        return array(
            'side' => $side,
            'role' => $role,
            'top' => $top,
            'winr' =>  $winratio,
            'wins' => $wins,
            'games' => $gamesPlayed
        );
    }
    
    /**
     * @Route("/overlay-selec-bans/{champion}/{side}/{position}", name="lol_overlay_selec_bans")
     * @Template("AppBundle:Stream:LeagueOfLegends/picks_bans/bans.html.twig")
     */
    public function renderBans($champion = 3, $side = 100, $position = 1)
    {
        switch($position)
        {
            case 1: 
                $left = 56;
                break;
            case 2:
                $left = 184;
                break;
            case 3:
                $left = 310;
                break;
            case 4:
                $left = 484;
                break;
            case 5:
                $left = 611;
                break;
            case 6:
                $left = 1168;
                break;
            case 7:
                $left = 1294;
                break;
            case 8:
                $left = 1468;
                break;
            case 9:
                $left = 1595;
                break;
            case 10:
                $left = 1722;
                break;
            
        }
        
        $ddhService = $this->get('api.ddh');
        
        $banr = $ddhService->getBanratioByChampionOnCompetition($champion, "T2018V-L");
        
        return array(
            'left' => $left,
            'side' => $side,
            'banr' => $banr
        );
    }
    
    /**
     * @Route("/overlay-stats-ingame", name="lol_overlay_stats_ingame")
     * @Template("AppBundle:Stream:LeagueOfLegends/stats_ingame.html.twig")
     */
    public function renderStatsIngame(Request $request)
    {
        $champion = $request->query->get('champion', 40);
        $side = $request->query->get('side', 100);
        $rol = $request->query->get('rol', 1);
        $playerId = $request->query->get('player', 36);
        $season = $request->query->get('season', 'T2018V-L');
        $stat = $request->query->get('stat', 'kda');
        $text = $request->query->get('text', 'Placeholder');
        
        switch($rol)
        {
            case 1:
                $top = 151;
                $role = "top";
                break;
            case 2:
                $top = 254;
                $role = "jungler";
                break;
            case 3:
                $top = 358;
                $role = "mid";
                break;
            case 4:
                $top = 461;
                $role = "adc";
                break;
            case 5:
                $top = 564;
                $role = "support";
                break;
        }
        
        $ddhService = $this->get('api.ddh');
        
        $stats = $ddhService->getStatsByPlayerAndChampion($playerId, $season, $champion);
        
        return array(
            'side' => $side,
            'role' => $role,
            'top' => $top,
            'stat' => $stat,
            'stats' => $stats,
            'text' => $text
        );
    }
    
    /**
     * @Route("/overlay-pantallas", name="lol_overlay_murcia")
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_pantallas.html.twig")
     */
    public function renderOverlayPantallas()
    {
        $ddhService = $this->get('api.ddh');
    
        $players = array(
            '100_1' => 'Werlyb',
            '100_2' => 'Selfmade',
            '100_3' => 'Nemesis',
            '100_4' => 'Crownshot',
            '100_5' => 'Falco',
            '200_1' => 'Xyraz',
            '200_2' => 'Cinkrof',
            '200_3' => 'Hatrixx',
            '200_4' => 'Jeskla',
            '200_5' => 'Treatz',
            '200_6' => 'Flaxxish',
            '200_7' => 'Bluerzor',
            '200_8' => 'Magifelix',
            '200_9' => 'Sanchez',
            '200_10' => 'Klaj',
        );
        $champions = $ddhService->getChampions();
        
        return array(
            'champions' => $champions,
            'players' => $players
        );
    }
    
    /**
     * @Route("/overlay-pantallas-gamergy", name="lol_overlay_gamergy")
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_pantallas_gamergy.html.twig")
     */
    public function renderOverlayPantallasGamergy()
    {
        $ddhService = $this->get('api.ddh');
        $champions = $ddhService->getChampions();
        
        $teams = [
            'kiyf' => 'KIYF',
            'vgia' => 'Vodafone Giants',
            'mad' => 'Mad Lions',
            'army' => 'ASUS'
        ];
        
        return array(
            'champions' => $champions,
            'teams' => $teams
        );
    }
    
    /**
     * @Route("/overlay-comparativa-stats", name="lol_overlay_comparativa_stats")
     * @Template("AppBundle:Stream:LeagueOfLegends/comparativa_stats.html.twig")
     */
    public function renderComparativaIngame(Request $request)
    {
        $ddhService = $this->get('api.ddh');
        
        $localPlayer = $request->query->get('local', 36);
        $visitorPlayer = $request->query->get('visitor', 31);
        $matchId = $request->query->get('match', "T2018P-L");
        
        $statsLocal = $ddhService->getStatsByPlayer($localPlayer, $matchId);
        $statsVisitor = $ddhService->getStatsByPlayer($visitorPlayer, $matchId);
        
        return array(
            'local' => $statsLocal,
            'visitor' => $statsVisitor,
            'localPlayer' => $localPlayer,
            'visitorPlayer' => $visitorPlayer
        );
    }
    
    /**
     * @Route("/overlay-mvp-lol", name="lol_overlay_mvp")
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_mvp.html.twig")
     */
    public function renderMVP(Request $request)
    {
        $ddhService = $this->get('api.ddh');
        
        $match = $request->query->get('match', 'T2018P-S-J3-P1-');
        $player = $request->query->get('player', 175);
        
        return [
            'matches' => [
                $ddhService->getPlainStatsByPlayer($player, sprintf('%s%s', $match, 1)),
                $ddhService->getPlainStatsByPlayer($player, sprintf('%s%s', $match, 2))
            ],
            'player' => $player
        ];
    }
    
    /**
     * @Route("/overlay-ingame-picks", name="lol_overlay_ingame_picks")
     * @Template("AppBundle:Stream:LeagueOfLegends/picks_bans/ingame_picks.html.twig")
     */
    public function renderIngamePicks(Request $request)
    {
        $champion = $request->query->get('champion', 113);
        $side = $request->query->get('side', 100);
        $rol = $request->query->get('rol', 1);
        
        switch($rol)
        {
            case 1:
                $top = 150;
                $role = "top";
                break;
            case 2:
                $top = 253;
                $role = "jungler";
                break;
            case 3:
                $top = 356;
                $role = "mid";
                break;
            case 4:
                $top = 459;
                $role = "adc";
                break;
            case 5:
                $top = 562;
                $role = "support";
                break;
        }
        
        $ddhService = $this->get('api.ddh');
        
        $winratio = $ddhService->getWinratioByChampOnCompetition($champion, "T2018V-L");
        
        $wins = $ddhService->getGamesPlayedByChampOncompetitionWin($champion, "T2018V-L");
        
        $gamesPlayed = $ddhService->getGamesPlayedByChampOncompetition($champion, "T2018V-L");
        
        return array(
            'side' => $side,
            'role' => $role,
            'top' => $top,
            'winr' =>  $winratio,
            'wins' => $wins,
            'games' => $gamesPlayed
        );
    }
    
    /**
     * @Route("/msi-tweet/{id}/{hashtag}", name="overlay_tweet_msi")
     * @Template("AppBundle:Stream:LeagueOfLegends/twitter.html.twig")
     */
    public function renderTweet($id, $hashtag)
    {
        /** @var TwitterService $twitter */
        $twitter = $this->get('api.twitter_connection');
        
        $tweet = $twitter->getTweetById($id);

        return array(
            'tweet' => $tweet,
            'hashtag' => $hashtag
        );
    }
    
    /**
     * CASPAR ACTIONS
     */
    
    /**
     * @Route("/caspar-play-lol/{id}/{switch}/{drake}", name="caspar_play_lol", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_ingame.html.twig")
     */
    public function sendToCaspar($id, $switch=0, $drake=0)
    {        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lol/'.$id.'/'.$switch.'/'.$drake, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-switch/{id}/{switch}/{drake}", name="caspar_play_lol_switch", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_ingame.html.twig")
     */
    public function sendToCasparSwitch($id, $switch=1, $drake=0)
    {                
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lol/'.$id.'/'.$switch.'/'.$drake, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-online/{id}/{switch}/{drake}", name="caspar_play_lol_online", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_ingame_online.html.twig")
     */
    public function sendToCasparOnline($id, $switch=0, $drake=0)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lol-online/'.$id.'/'.$switch.'/'.$drake, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-switch-online/{id}/{switch}/{drake}", name="caspar_play_lol_switch_online", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_ingame_online.html.twig")
     */
    public function sendToCasparSwitchOnline($id, $switch=1, $drake=0)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lol-online/'.$id.'/'.$switch.'/'.$drake, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-picks-bans/{name}/{blueTeam}/{purpleTeam}", name="caspar_play_lol_picks_bans", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/picks.html.twig")
     */
    public function sendToCasparPicksBans($name, $blueTeam, $purpleTeam)
    {
         $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lol-picks/'.$name.'/'.$blueTeam.'/'.$purpleTeam, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-ticker/{superligaId}/{matchId}", name="caspar_play_lol_ticker", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay-lol-ticker.html.twig")
     */
    public function sendToCasparTicker($superligaId, $matchId)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        $sloId = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($matchId);
         
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lol-ticker/'.$superligaId.'/'.$sloId->getSuperligaId(), $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-postmatch/{superligaId}/{matchId}", name="caspar_play_lol_postmatch", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/overlay_postmatch.html.twig")
     */
    public function sendToCasparPostmatch($superligaId, $matchId)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        $sloId = $this->getDoctrine()->getRepository('CompetitionBundle:Match')->find($matchId);
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/overlay-lol-postmatch/'.$superligaId.'/'.$sloId->getSuperligaId(), $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-picks/{champion}/{side}/{rol}/{layer}", name="caspar_play_lol_picks", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/picks_bans/picks.html.twig")
     */
    public function sendToCasparPicks($champion, $side, $rol, $layer)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));

        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml($layer, '/overlay-selec-picks/'.$champion.'/'.$side.'/'.$rol, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-bans/{champion}/{side}/{pos}/{layer}", name="caspar_play_lol_bans", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/picks_bans/picks.html.twig")
     */
    public function sendToCasparBans($champion, $side, $pos, $layer)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));

        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml($layer, '/overlay-selec-bans/'.$champion.'/'.$side.'/'.$pos, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-stats-ingame", name="caspar_play_lol_stats_ingame", options={"expose"=true})
     * @Template("AppBundle:Stream:LeagueOfLegends/picks_bans/picks.html.twig")
     */
    public function sendToCasparStatsIngame(Request $request)
    {
        $champion = $request->query->get('champion');
        $side = $request->query->get('side');
        $rol = $request->query->get('rol');
        $player = $request->query->get('player');
        $season = $request->query->get('season');
        $stat = $request->query->get('stat');
        $text = $request->query->get('text');
        $layer = $request->query->get('layer');
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf("/overlay-stats-ingame?champion=%s&side=%s&rol=%s&player=%s&season=%s&stat=%s&text=%s", $champion, $side, $rol, $player, $season, $stat, $text);
        
        $casparConnector->sendHtml($layer, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-mvp", name="caspar_play_lol_mvp", options={"expose"=true})
     */
    public function sendToCasparMVP(Request $request) 
    {
        $match = $request->query->get('match');
        $match = substr($match, 0, -1);
        
        $player = $request->query->get('player');
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf('/overlay-mvp-lol?match=%s&player=%s', $match, $player);
        
        $casparConnector->sendHtml(21, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-lol-comparativa", name="caspar_play_lol_comparativa", options={"expose"=true})
     */
    public function sendToCasparComparativa(Request $request)
    {
        $match = $request->query->get('match');
        
        $local = $request->query->get('local');
        $visitor = $request->query->get('visitor');
        
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'lol_ingame_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $route = sprintf('/overlay-comparativa-stats?match=%s&local=%s&visitor=%s', $match, $local, $visitor);
        
        $casparConnector->sendHtml(25, $route, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-play-tweet-msi/{id}/{hashtag}", name="caspar_play_tweet_msi", options={"expose"=true})
     */
    public function sendToCasparTweet($id, $hashtag)
    {
        $transition = $this->getDoctrine()->getRepository('CasparBundle:CasparParameters')->findOneBy(array('key' => 'twitter_transition'));
        
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->sendHtml(20, '/msi-tweet/'.$id.'/'.$hashtag, $transition->getValue());
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-stop", name="caspar_stop_lol", options={"expose"=true})
     */
    public function stop()
    {
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->fadeToBlack(20);
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-stop-21", name="caspar_stop_21_lol", options={"expose"=true})
     */
    public function stop21()
    {
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->fadeToBlack(21);
        
        return new JsonResponse();
    }
    
    /**
     * @Route("/caspar-stop-22", name="caspar_stop_22_lol", options={"expose"=true})
     */
    public function stop22()
    {
        /** @var MessageSenderService $casparConnector */
        $casparConnector = $this->get('caspar.message_sender');
        
        $casparConnector->fadeToBlack(22);
        
        return new JsonResponse();
    }
}
