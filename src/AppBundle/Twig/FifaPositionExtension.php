<?php

namespace AppBundle\Twig;

/**
 * Class FifaPositionExtension.
 */
class FifaPositionExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('toAcronym', array($this, 'toAcronym')),
            new \Twig_SimpleFilter('toStat', array($this, 'toStat')),
        );
    }
    
    /**
     * @param integer $number
     *
     * @return string
     */
    public function toAcronym($position)
    {
        if($position == 'Left Wing') {
            $p = 'EI';
        }
        else if($position == 'Right Wing') {
            $p = 'ED';
        }
        else if($position == 'Centre Back') {
            $p = 'DFC';
        }
        else if($position == 'Centre Midfielder') {
            $p = 'MC';
        }
        else if($position == 'Striker') {
            $p = 'DC';
        }
        else if($position == 'Centre Forward') {
            $p = 'SD';
        }
        else if($position == 'Left Back') {
            $p = 'LI';
        }
        else if($position == 'Centre Attacking Midfielder') {
            $p = 'MCO';
        }
        else if($position == 'Left Midfielder') {
            $p = 'MI';
        }
        else if($position == 'Centre Defensive Midfielder') {
            $p = 'MCD';
        }
        else if($position == 'Right Back') {
            $p = 'LD';
        }
        else if($position == 'Right Midfielder') {
            $p = 'MD';
        }
        else if($position == 'Left Forward') {
            $p = 'SDI';
        }
        else if($position == 'Right Wing Back') {
            $p = 'CAD';
        }
        else if($position == 'Left Wing Back') {
            $p = 'CAI';
        }
        else if($position == 'Goalkeeper') {
            $p = 'POR';
        }
        
        return $p;
    }
    
    /**
     * @param integer $number
     *
     * @return string
     */
    public function toStat($stat)
    {
        switch($stat)
        {
            case 'fut.attribute.PAC':
                $s = 'RIT';
                break;
            case 'fut.attribute.SHO':
                $s = 'TIR';
                break;
            case 'fut.attribute.PAS':
                $s = 'PAS';
                break;
            case 'fut.attribute.DRI':
                $s = 'REG';
                break;
            case 'fut.attribute.DEF':
                $s = 'DEF';
                break;
            case 'fut.attribute.PHY':
                $s = 'FIS';
                break;
            case 'fut.attribute.DIV':
                $s = 'SAL';
                break;
            case 'fut.attribute.HAN':
                $s = 'EST';
                break;
            case 'fut.attribute.KIC':
                $s = 'SAQ';
                break;
            case 'fut.attribute.REF':
                $s = 'REF';
                break;
            case 'fut.attribute.SPD':
                $s = 'VEL';
                break;
            case 'fut.attribute.HAN':
                $s = 'POS';
                break;
            case 'fut.attribute.POS':
                $s = 'POS';
                break;
            default:
                $s = 'A';
        }
        
        return $s;
    }
}

