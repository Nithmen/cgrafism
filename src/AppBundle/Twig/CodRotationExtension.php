<?php

namespace AppBundle\Twig;

/**
 * Class CodRotationExtension.
 */
class CodRotationExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('toText', array($this, 'toText')),
            new \Twig_SimpleFilter('toImage', array($this, 'toImage')),
            new \Twig_SimpleFilter('toImageTlb', array($this, 'toImageTlb'))
        );
    }
    
    /**
     * @param integer $number
     *
     * @return string
     */
    public function toText($rotation)
    {
        switch($rotation)
        {
            case 1: 
                $text = "PRIMERA";
                break;
            case 2: 
                $text = "SEGUNDA";
                break;
            case 3: 
                $text = "TERCERA";
                break;
        }
        
        return $text;
    }
    
    /**
     * ByD: Returns image for data value.
     * 
     * @param integer $imageValue
     * 
     * @return string
     */
    public function toImage($imageValue)
    {
        switch($imageValue)
        {
            case 0:
                $img = "";
                break;
                
            case 1:
                $img = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/Call+of+Duty/byd/death.png";
                break;
                
            case 2:
                $img = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/Call+of+Duty/byd/explosion.png";
                break;
                
            case 3:
                $img = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/Call+of+Duty/byd/defuse.png";
                break;
            case 4:
                $img = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/Call+of+Duty/byd/timer.png";
                break;
                
            default:
                $img = "";
        }
        
        return $img;
    }
    
    /**
     * Tlb: Returns image for data value.
     *
     * @param integer $imageValue
     *
     * @return string
     */
    public function toImageTlb($imageValue)
    {
        switch($imageValue)
        {
            case 0:
                $img = "";
                break;
                
            case 1:
                $img = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/Call+of+Duty/ctf/bandera-1.png";
                break;
                
            case 2:
                $img = "https://s3-eu-west-1.amazonaws.com/superliga-static/Stream/Call+of+Duty/ctf/bandera-2.png";
                break;

            default:
                $img = "";
        }
        
        return $img;
    }
}