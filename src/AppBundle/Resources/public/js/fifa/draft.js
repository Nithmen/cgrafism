var state = {
		mode:			'dev',
		id: 			null,
		loadedTeams:	[],
		turn:			null
};

$(function(){
	
	var $rootElement = $('#match-draft');
	
	state.id = $rootElement.attr('data-id');
	setMode($rootElement.attr('data-mode'));
	
	// Load drafts from the teams
	$('.js-load-draft').each(function(){
		var teamId =  $(this).attr('data-team-id');
		var draftId = $(this).attr('data-draft-id');
		
		var request = $.ajax({
			url: '/api/fifa/draft/' + draftId
		});
		
		request.done(function(response){
			var team = response['data'];
			addTeam(team['competition_profile']);
			
			addDraft(team['competition_profile'], team['fifa_players']);
		});
	});
	
	printState('initialState');
	
	$rootElement.on('click', '.card', function(){
		
		var $modal = $('#card-modal-list');
		var cardNumber = $(this).attr('card-number');
		var teamId = $(this).parents('.draft').attr('data-team-id');
		
		var request = $.ajax({
			url: '/admin/fut/cards'
		});
		
		request.done(function(response){
			$modal.find('.modal-body').append(response);
			
			$modal.modal('show');
			$modal.data('card-number', cardNumber);
			$modal.data('team-id', teamId);
			
		});
		
	});
	
	$('#card-modal-list').on('click', '.js-card-choice', function() {
		var $this = $(this);
		
		var $modal = $('#card-modal-list');
		
		var cardId = $this.attr('card-id');
		var cardNumber = $modal.data('card-number');
		var teamId = $modal.data('team-id');
		
		addPlayerToDraft(teamId, cardId, cardNumber);
		
		var card = $(this).parents('.card');
		
		setTimeout(function(){
			printCard(teamId, card, cardNumber);
			$modal.modal('hide');
			printState('playerToDraft');
		},200);
	});
	
	$('#card-modal-list').on('click','#search', function(){
		var $modal = $('#card-modal-list');
		var toSearch = $('#card-modal-search').val();
		
		var request = $.ajax({
			url: '/api/fifa/search/cards?name='+toSearch,
		});
		
		request.done(function(response){
			if(response['data'].length !== 0) {
				var request = $.ajax({
					url: '/admin/fut/cards/search',
					data: { data: response },
					type: 'json',
					method: 'POST'
				});
				
				request.done(function(response2){
					$modal.find('.modal-body').html(response2);
				});
			}
			else {
				$modal.find('.modal-body').html("No se han encontrado jugadores.");
			}
		});
	});
	
	// Flush state
	$rootElement.on('click','#save', function(){
		const { loadedTeams } = state;
		
		var match = $('#match-draft').attr('data-id');
		
		let data = loadedTeams.map((draft) => {
			return {
				id: draft.id,
				order: draft.order,
				draft: draft.draft.map((player) => {
					return {
						cardNumber: player.cardNumber,
						order: player.order,
						id: player.player.id
					};
				})
			}
		});

		var request = $.ajax({
			url: '/admin/fut/save',
			data: { data : data, match: match },
			method: 'POST',
			type: 'json'
		});
		
		request.done(function(){});
		
		request.fail(function(){});
	});
});


//State functions.

/**
 * Print current state.
 * 
 * @returns void
 */
function printState(action) 
{
	if (state.mode == 'dev') {
		console.log(action, state);	
	}
}

/**
 * Sets the mode in state.
 * 
 * @param mode - The current application mode.
 * 
 * @returns void
 */
function setMode(mode) 
{
	state.mode = mode;
}

/**
 * Change the current team turn on the draft.
 * 
 * @param teamId - Competition profile id.
 *  
 * @returns void
 */
function changeTurn(teamId)
{
	state.turn = teamId;
}

/**
 * Add a team to the state.
 * 
 * @param teamId - Competition profile id.
 * 
 * @returns void
 */
function addTeam(teamId)
{
	const { loadedTeams } = state;
	
	let found = loadedTeams.findIndex(team => team.id == teamId);
	
	// If found
	if(typeof loadedTeams[found] !== 'undefined') {
		return;
	}
	
	let teams = loadedTeams;
	
	teams.push({
		id : teamId,
		draft : [],
		order : 1
	});
	
	state.loadedTeams = teams;
}

/**
 * Retrieve drafts to the state.
 * 
 * @param teamId - Competition profile Id.
 * @param players
 * 
 * @returns void
 */
function addDraft(teamId, players)
{
	const { loadedTeams } = state;
	
	let found = loadedTeams.findIndex(team => team.id == teamId);
	
	// If not found
	if(typeof loadedTeams[found] === 'undefined') {
		return;
	}
	
	var draft = loadedTeams[found]['draft'];
	
	for(var x in players)
	{
		draft.push({
			player : players[x]['player'],
			order : players[x]['order'],
			cardNumber : players[x]['card_number']
		});
	}
	
	loadedTeams[found]['draft'] = draft;
	
	state.loadedTeams = loadedTeams;
}

/**
 * Render a card on the field.
 * 
 * @param teamId		- Competition profile Id.
 * @param cardId		- Card Id.
 * @param cardNumber	- Card position.
 *  
 * @returns void
 */
function printCard(teamId, $card, cardNumber)
{
	const { loadedTeams } = state;
	
	let found = loadedTeams.findIndex(team => team.id == teamId);
	
	// If not found
	if(typeof loadedTeams[found] === 'undefined') {
		return;
	}
	
	var draft = loadedTeams[found]['draft'];
	
	let position = draft.findIndex(card => card.cardNumber == cardNumber);
	
	// If not found
	if(typeof draft[position] === 'undefined') {
		return;
	}
	
	$('.draft[data-team-id="' + teamId + '"]').find('.card[card-number="' + cardNumber + '"]').parent().html($card);
}

/**
 * Add a player to a team's draft.
 * 
 * @param teamId - Competition profile Id.
 * @param cardId - Database card Id.
 * @param cardNumber - Card position on the draft.
 * 
 * @returns void
 */
function addPlayerToDraft(teamId, cardId, cardNumber)
{
	const { loadedTeams } = state;
	
	let found = loadedTeams.findIndex(team => team.id == teamId);
	
	// If not found
	if(typeof loadedTeams[found] === 'undefined') {
		return;
	}
	
	var draft = loadedTeams[found]['draft'];
	
	let cardFound = draft.findIndex(player => player.cardNumber == cardNumber);
	
	// If found
	if(typeof draft[cardFound] !== 'undefined') {
		draft.splice(cardFound, 1);
	}
	
	var request = $.ajax({
		url: '/api/fifa/card/' + cardId
	});
	
	request.done(function(response){
		var player = {
				player : response['data'],
				order : Object.keys(draft).length + 1,
				cardNumber : parseInt(cardNumber)
		};
		
		draft.push(player);
		
		loadedTeams[found]['draft'] = draft;
		state.loadedTeams = loadedTeams;
	});
}