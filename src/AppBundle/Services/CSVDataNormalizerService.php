<?php

namespace AppBundle\Services;

/**
 * Class CSVDataNormalizerService.
 */
class CSVDataNormalizerService
{
    /**
     * Extract the data from a CSV.
     * 
     * @return array
     */
    public function extractData($file)
    {
        $data = [];
        
        $i = 1;
        if (($gestor = fopen($file, "r")) !== FALSE) {
            while (($row = fgetcsv($gestor, 1000, ",")) !== FALSE) {
                if($i < 3) {
                    $i++;
                    continue;
                }
                
                
                $key = sprintf('rotation_%s', $i - 2);
                
                foreach($row as $index => $column) 
                {
                    if($column == "") {
                        unset($row[$index]);
                    }
                }
                
                $data[$key] = $row;
                $i++;
            }
            fclose($gestor);
        }

        return $data;
    }
}

