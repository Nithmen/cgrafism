<?php

namespace GameBundle\Entity;

Use Doctrine\ORM\Mapping as ORM;

/**
 * Game Entity
 * 
 * @ORM\Entity
 * @ORM\Table(name="game__game")
 *
 * @author John
 */
class Game {
   
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=50)
     * 
     * var string
     */
    protected $name;
    
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function __toString() {
        return sprintf("%s", $this->getName());
    }
}
