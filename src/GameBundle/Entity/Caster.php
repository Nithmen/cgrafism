<?php

namespace GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity
 * @ORM\Table(name="game__caster")
 *
 * @author John
 */
class Caster {
   
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=25)
     * 
     * @var string
     */
    protected $alias;
    
    /**
     * @ORM\Column(type="string", length=25)
     * 
     * @var string
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", length=25)
     * 
     * @var string
     */
    protected $lastname;
    
    /**
     * @ORM\Column(type="string", length=30)
     * 
     * @var string
     */
    protected $twitter = '@';
    
    /**
     * @ORM\ManyToOne(targetEntity="Game", fetch="EAGER")
     * @JoinColumn(name="game", referencedColumnName="id", nullable=true)
     * 
     * @var Game
     */
    protected $game;
    
    public function getId() {
        return $this->id;
    }

    public function getAlias() {
        return $this->alias;
    }

    public function getName() {
        return $this->name;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function getTwitter() {
        return $this->twitter;
    }

    public function getGame() {    
        return $this->game;
    }

    public function setAlias($alias) {
        $this->alias = $alias;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setLastname($lastname) {
        $this->lastname = $lastname;
        return $this;
    }

    public function setTwitter($twitter) {
        $this->twitter = $twitter;
        return $this;
    }

    public function setGame(Game $game) {
        $this->game = $game;
        return $this;
    }
    
    public function __toString() 
    {
        return sprintf("%s", $this->getAlias());
    }
}
