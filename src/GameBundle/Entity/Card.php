<?php

namespace GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Application\Sonata\MediaBundle\Entity\Media;

/**
 * @ORM\Entity
 * @ORM\Table(name="game__card")
 *
 * @author John
 */
class Card {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *  
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=30)
     * 
     * @var string
     */
    protected $name;
    
    /**
     * @ORM\Column(type="text")
     */
    protected $description;
    
    /**
     * @ORM\Column(type="string", nullable=false)
     * 
     */
    protected $elixir;
    
    /**
     * @ORM\Column(type="string", length=50)
     * 
     */
    protected $rarity;  
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     * 
     * @var Media
     */
    protected $image;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="big_media_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $bigImage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="pip_media_id", referencedColumnName="id", nullable=true)
     *
     * @var Media
     */
    protected $pipImage;
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getElixir() {
        return $this->elixir;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setElixir($elixir) {
        $this->elixir = $elixir;
        return $this;
    }
    
    public function __toString()
    {
        return sprintf($this->getName());
    }
    
    public function getRarity() {
        return $this->rarity;
    }

    public function setRarity($rarity) {
        $this->rarity = $rarity;
        return $this;
    }
    /**
     * @return the $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }
    /**
     * @return the $bigImage
     */
    public function getBigImage()
    {
        return $this->bigImage;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $bigImage
     */
    public function setBigImage($bigImage)
    {
        $this->bigImage = $bigImage;
        return $this;
    }    
    
    /**
     * @return the $pipImage
     */
    public function getPipImage()
    {
        return $this->pipImage;
    }
    
    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $pipImage
     */
    public function setPipImage($pipImage)
    {
        $this->pipImage = $pipImage;
        return $this;
    } 
}
