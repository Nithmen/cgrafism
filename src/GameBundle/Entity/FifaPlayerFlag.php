<?php

namespace GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\MediaBundle\Entity\Media;

/**
 * @ORM\Entity
 * @ORM\Table(name="game__fifa_player_flag")
 *
 * @author John
 */
class FifaPlayerFlag 
{
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *  
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=50)
     * 
     * @var string
     */
    protected $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     * 
     * @var Media
     */
    protected $image;
    
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }


    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    public function __toString()
    {
        return sprintf($this->getName());
    }
 
    /**
     * @return $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }
}
