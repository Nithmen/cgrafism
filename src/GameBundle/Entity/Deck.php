<?php

namespace GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PreUpdate;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="game__deck")
 * @ORM\HasLifecycleCallbacks()
 * 
 * @author John
 */
class Deck {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     * 
     */
    protected $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="CompetitionBundle\Entity\Match", inversedBy="decks")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id", nullable=true)
     * 
     * @var \CompetitionBundle\Entity\Match
     */
    protected $match;
    
    /**
     * @ORM\Column(name ="`set`", type="integer", nullable=true)
     * 
     */
    protected $set;
    
    /**
     * @ORM\ManyToOne(targetEntity="CompetitionBundle\Entity\Player", fetch="EAGER")
     * @JoinColumn(name="player", referencedColumnName="id", nullable=true)
     * 
     */
    protected $player;
    
    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Card")
     * @ORM\JoinTable(name="game__deck_cards",
     * joinColumns={@JoinColumn(name="deck_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="card_id", referencedColumnName="id", unique=false)})
     * 
     */
    protected $cards;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     * 
     */
    protected $elixir;
    
    /**
     * @ORM\ManyToOne(targetEntity="Card", fetch="EAGER")
     * @JoinColumn(name="winCondition", referencedColumnName="id", nullable=true)
     * 
     * @var Game
     */
    protected $winCondition;
    
    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getPlayer() {
        return $this->player;
    }

    public function getCards() {
        return $this->cards;
    }

    public function getElixir() {
        return $this->elixir;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setPlayer($player) {
        $this->player = $player;
        return $this;
    }

    public function setCards($cards) {
        $this->cards = $cards;
        return $this;
    }

    public function setElixir($elixir) {
        $this->elixir = $elixir;
        return $this;
    }
    
    public function getMatch()
    {
        return $this->match;
    }

    public function getSet()
    {
        return $this->set;
    }

    public function setMatch($match)
    {
        $this->match = $match;
        return $this;
    }

    public function setSet($set)
    {
        $this->set = $set;
        return $this;
    }
        
    public function __toString() 
    {
        return sprintf("%s - %s", $this->getName(), $this->getPlayer());
    }
        
    /**
     * @return the $winCondition
     */
    public function getWinCondition()
    {
        return $this->winCondition;
    }

    /**
     * @param \GameBundle\Entity\Game $winCondition
     */
    public function setWinCondition($winCondition)
    {
        $this->winCondition = $winCondition;
        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate() 
    {
        $elixir = 0;
        
        $cards = (float)($this->cards->count());
                
        foreach($this->cards->toArray() as $card)
        {
            $elixir += $card->getElixir();
        }
        
        $this->elixir = $elixir/(float)$cards;
    }
}
