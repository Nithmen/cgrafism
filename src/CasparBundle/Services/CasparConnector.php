<?php

namespace CasparBundle\Services;

class CasparConnector
{    
    private $address;
    
    private $port;
    
    private $socket;
    
    /**
     * 
     * @param unknown $address
     * @param unknown $port
     */
    public function __construct($address, $port)
    {
        $this->address = $address;
        $this->port = $port;
    }
    
    /**
     * 
     * @return boolean
     */
    public function connect() 
    {
        if($this->socket)
        {
            return false;
        }
        
        try 
        {
            $errno = false;
            $errstr = null;
            
            $this->socket = fsockopen($this->address, $this->port, $errno, $errstr, 10);
            
            return true;
        }
        catch(\Exception $ex)
        {
            return false;
        }
    }
    
    /**
     * 
     * @param string $msg
     * 
     * @return boolean
     */
    public function makeRequest($msg) 
    {
        if(!$this->socket)
        {
            return false;
        }

        try 
        {
            fwrite($this->socket, $msg . "\r\n");
            
            $line = fgets($this->socket);
            $line = explode(" ", $line);
            
            $status = intval($line[0], 10);
            
            $hasResponse = true;
            $response = null;
            
            switch ($status)
            {
                case 200: // several lines followed by empty line
                    $endSequence = "\r\n\r\n";
                    break;
                    
                case 201: // one line of data returned
                    $endSequence = "\r\n";
                    break;
                
                default:
                    $hasResponse = false;
            }
            
            if ($hasResponse) 
            {
                $response = stream_get_line($this->socket, 1000000, $endSequence);
            }
            
            return array('status' => $status, 'response' => $response);
        }
        catch(\Exception $ex)
        {
            return false;
        }
    }
    
    /**
     * 
     * @param string $string
     * 
     * @return mixed
     */
    public static function escapeString($string) 
    {
        return str_replace('"', '\"', str_replace("\n", '\n', str_replace('\\', '\\\\', $string)));
    }
    
    /**
     * 
     * @return boolean
     */
    public function disconnect() 
    {
        if(!$this->socket)
        {
            return false;    
        }
        
        try 
        {
            fclose($this->socket);
            
            return true;
        }
        catch(\Exception $ex)
        {
            return false;
        }
    }
}

