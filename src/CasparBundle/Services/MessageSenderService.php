<?php

namespace CasparBundle\Services;

use CasparBundle\Services\CasparConnector;
use CasparBundle\Actions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Kernel;

class MessageSenderService
{
    /**
     * 
     * @var CasparConnector
     */
    private $connector;
    
    /**
     * 
     * @var Kernel
     */
    private $env;
    
    /**
     * 
     * @param CasparConnector $connector
     */
    public function __construct(CasparConnector $connector, $env)
    {
        $this->connector = $connector;
        $this->env = $env;
    }
     
     public function sendHtml($layer, $html, $transition)
     {
         $this->connector->connect();

         if($this->env == 'dev') {
             $this->connector->makeRequest(sprintf('%s 1-%s [HTML] "http://localhost:8000%s" %s', Actions::PLAY, $layer, $html, $transition));
         }
         else {
             $this->connector->makeRequest(sprintf('%s 1-%s [HTML] "http://cgrafism.com%s" %s', Actions::PLAY, $layer, $html, $transition));
         }
         
         $this->connector->disconnect();
     }
     
     public function stop($layer)
     {
         $this->connector->connect();
         
         $this->connector->makeRequest(sprintf('%s 1-%s ', Actions::STOP, $layer));
         
         $this->connector->disconnect();
     }
     
     public function fadeToBlack($layer)
     {
         $this->connector->connect();
         
         $this->connector->makeRequest(sprintf('%s 1-%s "#00000000" MIX 12 Linear RIGHT ', Actions::PLAY, $layer));
         
         $this->connector->disconnect();
     }
     
     private function renderView($name)
     {
         return $this->renderView($name);
     }
}


